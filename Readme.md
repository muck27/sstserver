The Django and React app have been dockerized for portability reasons.

1. Install Docker (https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)
2. Install Docker-Compose (https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-16-04)
3. uvlearn_open.conf is the nginx conf file. Substitute your machine ip address in there
4. Build cvat engine and in the future build training/testing engines
5. sudo docker exec -it cvat_8019 bash -ic 'python3 ~/manage.py 
5. run $docker build . -t django_app
6. run $docker run -it -p 3000:3000 -p 8000:8000 -v /var/run/docker.sock:/var/run/docker.sock django_app bash
7. Inside /automatic_annotator_tool/react_app:
        run $ npm install 
        run $ npm install pm2 -g 
	run $ pm2 start npm -- start
     
8. run $service nginx start
9. Inside django_app/
 	run $ python s3_continuous.py (syncs all new objects in s3 to your container. The new objects can be viewed in /data/downloads/)
        run backend $uwsgi --socket socket_101.sock --module uvlearn.wsgi -p 2 --master --chmod-socket=777

##########pruned model of git
