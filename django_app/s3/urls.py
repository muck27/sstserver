from django.conf.urls import url
from django.urls import path
from . import views

from . import views
urlpatterns = [
    path('', views.s3_projects.as_view() ),   
    path('<int:p_id>/', views.s3_sites.as_view() ),   
    path('<int:p_id>/<int:s_id>/', views.s3_dates.as_view() ),   
    path('<int:p_id>/<int:s_id>/<int:d_id>', views.s3_cam_id.as_view() ),   
    path('<int:p_id>/<int:s_id>/<int:d_id>/<int:c_id>/', views.s3_images.as_view() ),   
    path('<int:p_id>/<int:s_id>/<int:d_id>/<int:c_id>/<int:i_id>/', views.s3_get_image.as_view() ),   
]

