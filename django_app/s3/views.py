import sys
user = "clyde"
import os.path
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
import subprocess
import numpy as np
from django.shortcuts import render
from django.http import HttpResponse
import django
django.setup()
sys.path.append('/home/'+user+'/Downloads/automatic_annotator_tool/django_app/')
#from .models import projects, sites, dates, images, cam_id
from django.views.generic import ListView, CreateView, UpdateView 
from django.forms import modelformset_factory
import os
import boto3
import uuid
from django.views.decorators.csrf import csrf_exempt
import json
import psycopg2
import sys
import shutil
import psycopg2
import json
import os
import os.path
from os import path
import zipfile
import glob
import botocore
from PIL import Image, ImageDraw
import datetime
########API view

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse

#####
projects_list='s3/lists/projects.list'
sites_list='s3/lists/sites.list'
dates_list='s3/lists/dates.list'
cam_id_list='s3/lists/cam_id.list'
images_list='s3/lists/images.list'
bucket= 'uploads.live.uncannysurveillance.com'
########

class s3_projects(APIView):
    def get(self,request):
        if path.exists(projects_list):
            os.remove(projects_list)
        command = 'aws s3 ls s3://{} > p_input.txt'.format(bucket)
        os.system(command)
        infile = "p_input.txt"
        outfile = "p_output.txt"
        delete_list = ["PRE","/"]
        fin = open(infile)
        fout = open(outfile, "w+")
        for line in fin:
            for word in delete_list:
                line = line.replace(word, "")
            fout.write(line)
        fin.close()
        fout.close()

        with open('p_output.txt') as f:
            content = f.readlines()
        f.close()
        data= []
        counter = 1
        content = [x.strip() for x in content]
        g = open(projects_list, "w")
        for x in content:
            tmp={}
            tmp['id']=counter
            tmp['project']=x
            data.append(tmp)
            counter = counter + 1

        json.dump(data,g)
        g.close()
        os.remove("p_input.txt")
        os.remove("p_output.txt")
        return Response(data)


class s3_sites(APIView):
    def get(self,request,p_id):
        f = open(projects_list)
        data = json.load(f)
        for x in data:
            if x['id'] == p_id:
                project = x['project'].replace(' ','\ ')
                command = 'aws s3 ls s3://{}/{}/ > s_input.txt'.format(bucket,project)
                os.system(command)
                infile = "s_input.txt"
                outfile = "s_output.txt"
                delete_list = ["PRE","/"]
                fin = open(infile)
                fout = open(outfile, "w+")
                for line in fin:
                    for word in delete_list:
                        line = line.replace(word, "")
                    fout.write(line)
                fin.close()
                fout.close()

                with open('s_output.txt') as f:
                    content = f.readlines()
                f.close()
                data= []
                counter = 1
                content = [x.strip() for x in content]
                g = open(sites_list, "w")
                for x in content:
                    tmp={}
                    tmp['id']=counter
                    tmp['site']=x
                    data.append(tmp)
                    counter = counter + 1

                json.dump(data,g)
                g.close()
                os.remove("s_input.txt")
                os.remove("s_output.txt")
 
       
        return Response(data)


class s3_dates(APIView):
    def get(self,request,p_id,s_id):
        f = open(projects_list)
        p_data = json.load(f)
        for p in p_data:
            if p['id'] == p_id:
                project = p['project']
                project.replace(' ','\ ')
                g = open(sites_list)
                s_data = json.load(g)
                g.close()
                for s in s_data:
                    if s['id'] == s_id:
                          
                        site = s['site'].replace(' ','\ ')
                        command = 'aws s3 ls s3://{}/{}/{}/ > d_input.txt'.format(bucket,project,site)
                        os.system(command)
                        infile = "d_input.txt"
                        outfile = "d_output.txt"
                        delete_list = ["PRE","/"]
                        fin2 = open(infile)
                        fout2 = open(outfile, "w+")
                        for line in fin2:
                            for word in delete_list:
                                line = line.replace(word, "")
                            fout2.write(line)
                        fin2.close()
                        fout2.close()

                        with open('d_output.txt') as h:
                            content = h.readlines()
                        h.close()
                        dates_data= []
                        counter = 1
                        content = [x.strip() for x in content]
                        i = open(dates_list, "w")
                        for x in content:
                            tmp={}
                            tmp['id']=counter
                            tmp['dates']=x
                            dates_data.append(tmp)
                            counter = counter + 1
                        json.dump(dates_data,i)
                        i.close()
                g.close()
 
        f.close()
        os.remove("d_input.txt")
        os.remove("d_output.txt")
        return Response(dates_data)



class s3_cam_id(APIView):
    def get(self,request,p_id,s_id,d_id):
        f = open(projects_list)
        p_data = json.load(f)
        f.close()
        for p in p_data:
            if p['id'] == p_id:
                project = p['project'].replace(' ','\ ')
                g = open(sites_list)
                s_data = json.load(g)
                g.close()
                for s in s_data:
                    if s['id']==s_id:
                        site = s['site'].replace(' ','\ ')
                        h= open(dates_list)
                        d_data = json.load(h)
                        h.close()
                        for d in d_data:
                            if d['id'] == d_id:
                                date = d['dates'].replace(' ', '\ ')
                                command = 'aws s3 ls s3://{}/{}/{}/{}/> c_input.txt'.format(bucket,project,site,date)
                                os.system(command)
                                infile = "c_input.txt"
                                outfile = "c_output.txt"
                                delete_list = ["PRE","/"]
                                fin2 = open(infile)
                                fout2 = open(outfile, "w+")
                                for line in fin2:
                                    for word in delete_list:
                                        line = line.replace(word, "")
                                    fout2.write(line)
                                fin2.close()
                                fout2.close()
                                with open('c_output.txt') as h:
                                    content = h.readlines()
                                h.close()
                                cam_id_data= []
                                counter = 1
                                content = [x.strip() for x in content]
                                i = open(cam_id_list, "w")
                                for x in content:
                                    tmp={}
                                    tmp['id']=counter
                                    tmp['cam_id']=x
                                    cam_id_data.append(tmp)
                                    counter = counter + 1
                                json.dump(cam_id_data,i)
                                i.close()
        os.remove("c_input.txt")
        os.remove("c_output.txt")
        return Response(cam_id_data)


class s3_images(APIView):
    def get(self,request,p_id,s_id,d_id,c_id):
        f = open(projects_list)
        p_data = json.load(f)
        f.close()
        for p in p_data:
            if p['id'] == p_id:
                project = p['project'].replace(' ','\ ')
                g = open(sites_list)
                s_data = json.load(g)
                g.close()
                for s in s_data:
                    if s['id']==s_id:
                        site = s['site'].replace(' ','\ ')
                        h= open(dates_list)
                        d_data = json.load(h)
                        h.close()
                        for d in d_data:
                            if d['id'] == d_id:
                                date = d['dates'].replace(' ', '\ ')
                                f = open(cam_id_list)
                                i_data = json.load(f)
                                f.close()
                                print('1')
                                for i in i_data:
                                    if i['id'] == c_id:
                                        images=i['cam_id'].replace(' ','\ ')
                                        grep = "| awk '{print $4}'"
                                        command = 'aws s3 ls s3://{}/{}/{}/{}/{}/ {}> i_input.txt'.format(bucket,project,site,date,images,grep)
                                        os.system(command)
                                        infile = "i_input.txt"
                                        outfile = "i_output.txt"
                                        delete_list = ["PRE","/"]
                                        fin2 = open(infile)
                                        fout2 = open(outfile, "w+")
                                        for line in fin2:
                                            for word in delete_list:
                                                line = line.replace(word, "")
                                            fout2.write(line)
                                        fin2.close()
                                        fout2.close()
                                        with open('i_output.txt') as h:
                                            content = h.readlines()
                                        h.close()
                                        image_data= []
                                        counter = 1
                                        content = [x.strip() for x in content]
                                        i = open(images_list, "w")
                                        for x in content:
                                            tmp={}
                                            tmp['id']=counter
                                            tmp['images']=x
                                            image_data.append(tmp)
                                            counter = counter + 1
                                        json.dump(image_data,i)
                                        i.close()
        os.remove("i_input.txt")
        os.remove("i_output.txt")
        return Response(image_data)



class s3_get_image(APIView):
    def get(self,request,p_id,s_id,d_id,c_id,i_id):
        f = open(projects_list)
        p_data = json.load(f)
        f.close()
        for p in p_data:
            if p['id'] == p_id:
                project = p['project'].replace(' ','\ ')
                g = open(sites_list)
                s_data = json.load(g)
                g.close()
                for s in s_data:
                    if s['id']==s_id:
                        site = s['site'].replace(' ','\ ')
                        h= open(dates_list)
                        d_data = json.load(h)
                        h.close()
                        for d in d_data:
                            if d['id'] == d_id:
                                date = d['dates'].replace(' ', '\ ')
                                f = open(cam_id_list)
                                i_data = json.load(f)
                                f.close()
                                for i in i_data:
                                    if i['id'] == c_id:
                                        image_id=i['cam_id'].replace(' ','\ ')
                                        f = open(images_list)
                                        images = json.load(f)
                                        f.close()
                                        image_data=[]
                                        for image in images:
                                            if image['id'] == i_id:
                                                image_data.append(image)
        return Response(image_data)


