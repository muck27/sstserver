from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [

    path('projects/',views.get_projects.as_view()),
    path('sites/<int:p_id>',views.get_sites.as_view()),
    path('dates/<int:s_id>',views.get_dates.as_view()),
    path('cam_ids/<int:d_id>',views.get_camids.as_view()),
    path('get_instances/',views.get_instances.as_view()),
    path('create_dataset/',views.create_dataset.as_view()),
    path('remote_processes/',views.get_remote_processes.as_view()),
#    path('get_datasets/',views.get_datasets.as_view()),
    path('dates/',views.train_dates.as_view()),
    path('anchor_box/',views.get_anchor_boxes.as_view()),
#    path('train_module/',views.train_module.as_view()),
#    path('post_train_data/',views.post_train_data.as_view()),
    path('map_module/',views.map_module.as_view()),
#    path('get_results/',views.get_results.as_view()),
    path('upload_detector/',views.upload_detector.as_view()),
#    path('upload_instance/',views.upload_instance.as_view()),


    path('classifier_sites/<int:p_id>',views.get_classifier_sites.as_view()),
    path('classifier_dates/<int:s_id>',views.get_classifier_dates.as_view()),
    path('classifier_frameworks/',views.classifier_frameworks.as_view()),
    path('create_dataset_classifier/',views.create_dataset_classifier.as_view()),
    path('upload_classifier/',views.upload_classifier.as_view()),



]


