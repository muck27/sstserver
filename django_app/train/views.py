from django.shortcuts import render
import math
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
import psycopg2
from uvdata.models import deeplearning_processes,remote_processes,infer_models,datasetvsmodel,classifier,remote_instances,projects, sites, images, box, classes,cam_id,bbox,dates,darknet_models, train_progress, train_detector_containers
import os
import numpy as np
import zmq
import json
import subprocess
import docker
import os
import sys
import time
import json
import docker
import random
import shutil
from django.db.models import Max
from django.db.models import Min
import boto3
import botocore
import cv2


client = boto3.client('s3',aws_access_key_id='AKIAQHGYCFENHDO6QSMH',aws_secret_access_key= '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')
paginator = client.get_paginator('list_objects')



train_ports = [55650,55640,55630,55620,55610]
other_ports = [
              {"train_port":55650,"anchor_port":54650,"map_port":53650},
              {"train_port":55640,"anchor_port":54640,"map_port":53640},
              {"train_port":55630,"anchor_port":54630,"map_port":53630},
              {"train_port":55620,"anchor_port":54620,"map_port":53620},
              {"train_port":55610,"anchor_port":54610,"map_port":53610}
	      ]

def Diff(li1, li2):
    return (list(set(li1) - set(li2)))


def get_classes_db(s_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select classes_id from uvdata_sites_site_classes where sites_id = {}".format(s_id)
        cur.execute(query)
        model_records = cur.fetchall()
        c_list=[]
        for x in model_records:
            c_list.append(x[0])
        return(c_list)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()



class get_projects(APIView):

    def get(self,request):
        response=[]
        p_list= projects.objects.all()
        for p in p_list:
            tmp={}
            tmp["value"] = p.id
            tmp["text"] = p.project_name
            response.append(tmp)
        return Response(response)



class get_sites(APIView):

    def get(self,request,p_id):
        response=[]
        s_list= sites.objects.filter(project_id=p_id)
        for s in s_list:
            tmp={}
            tmp["value"] = s.id
            tmp["text"] = s.site_name
            tmp["class_info"]=[]
            class_values=get_classes_db(s.id)
            for c in class_values:
                img_count = images.objects.filter(site_id=s.id).filter(classes__contains=[c]).filter(human_or_not=True)
                tmp2={}
                tmp2["class_name"] = classes.objects.get(id=c).class_name
                tmp2["total_images"] = img_count.count()
                tmp["class_info"].append(tmp2)
            response.append(tmp)
        return Response(response)



class get_classifier_sites(APIView):
    def get(self,request,p_id):
        response=[]
        s_list= sites.objects.filter(project_id=p_id)
        for s in s_list:
            tmp={}
            tmp["value"] = s.id
            tmp["text"] = s.site_name
            tmp["class_info"]=[]
            classifier_info = classifier.objects.filter(site_id = s.id).distinct('classifier')
            final_info = {}
            for x in classifier_info:
                final_info[x.classifier]= [] 
                attr_info = classifier.objects.filter(site_id= s.id).filter(classifier = x.classifier)
                for y in attr_info:
                    final_info[x.classifier].append(y.attribute)
            print(final_info)
            for c in final_info.keys():
                for attr in final_info[c]:
                    box_count = bbox.objects.filter(site_id=s.id).filter(classifier__contains = {c:attr}).filter(box_type='human').distinct('image_id')
                    tmp2={}
                    tmp2["class_name"] = "{} {}".format(c,attr)
                    tmp2["total_images"] = box_count.count()
                    tmp["class_info"].append(tmp2)

            response.append(tmp)
        return Response(response)



class get_camids(APIView):

    def get(self,request,d_id):
        response=[]
        c_list= cam_id.objects.filter(date_id=d_id)
        for c in c_list:
            img_count = images.objects.filter(cam_id=c.id)
            tmp={}
            tmp["id"] = c.id
            tmp["cam_id"] = c.cam_id
            tmp["total_images"] = img_count.count()
            tmp["annotated_images"] = img_count.filter(human_or_not=True).count()
            response.append(tmp)
        return Response(response)


class get_dates(APIView):

    def get(self,request,s_id):
        response=[]
        class_values=get_classes_db(s_id)  
        d_list= dates.objects.filter(site_id=s_id)
        for d in d_list:
            tmp={}
            tmp["value"] = d.id
            tmp["text"] = d.date
            tmp["class_info"]=[]
            for c in class_values:
               # img_count = images.objects.filter(date_id=d.id).filter(classes__contains=[c]).filter(human_or_not=True)
                class_name = classes.objects.get(id=c).class_name
                img_count = bbox.objects.filter(date_id=d.id).filter(box_class=class_name).filter(box_type='human').distinct('image_id')
                tmp2={}
                tmp2["class_name"] = classes.objects.get(id=c).class_name
                tmp2["total_images"] = img_count.count()
                tmp["class_info"].append(tmp2)
            response.append(tmp)
        return Response(response)





class get_classifier_dates(APIView):
    def get(self,request,s_id):
        response=[]
        classifier_info = classifier.objects.filter(site_id = s_id).distinct('classifier')
        final_info = {}
        for x in classifier_info:
            final_info[x.classifier]= []
            attr_info = classifier.objects.filter(site_id= s_id).filter(classifier = x.classifier)
            for y in attr_info:
                final_info[x.classifier].append(y.attribute)
        print(final_info)

        d_list= dates.objects.filter(site_id=s_id)
        c_list = []
        for d in d_list:
            tmp={}
            tmp["value"] = d.id
            tmp["text"] = d.date
            tmp["class_info"]=[]
            for c in final_info.keys():
                if c not in c_list:
                    c_list.append(c)
                for attr in final_info[c]:
                    box_count = bbox.objects.filter(date_id=d.id).filter(classifier__contains = {c:attr}).filter(box_type='human').distinct('image_id')
                    tmp2={}
                    tmp2["class_name"] = "{} {}".format(c,attr)
                    tmp2["total_images"] = box_count.count()
                    tmp["class_info"].append(tmp2)
            response.append(tmp)

        final_response = {}
        final_response['response'] = response
        final_c_list = []
        for c in c_list: 
            final_c_list.append({"text":c,"value":c})
        final_response['classifiers'] = final_c_list
        return Response(final_response)


class get_datasets(APIView):

    def get(self,request):
        response=[]
        for x in os.listdir('/datadrive/train_data/'):
            response.append(x)
        return Response(response)


def find_port():
    with open('train_ports.txt') as ports:
        port=json.load(ports)
    avail_ports = Diff(train_ports,port["closed_ports"])
    train_port = random.choice(avail_ports)
    port["closed_ports"].append(train_port)
    tmp ={}
    tmp["closed_ports"] = port["closed_ports"]
    with open('train_ports.txt','w') as ports:
        json.dump(tmp,ports)

    return(train_port)


def get_anno_class(site_name):
    
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select * from uvdata_darknet_classes where project=%s"
        cur.execute(query,(site_name,))
        class_info = cur.fetchall()
        response = []
        for x in class_info:
            tmp={}
            tmp["class"] = x[2]
            tmp["id"]    = x[3]
            response.append(tmp)
        return (response) 
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_train_progress table",error)
    finally:
        if (con):
            cur.close()
            con.close()


def split_data(image_list,train_folder,split,p_name,s_name):
    '''
    create random probs ,data dir, train and test dir  and copy files in them
    '''
    random_probs = []
    train_probs = int(math.floor(split*10))    
    test_probs = 10-int(math.ceil(split*10))    
    for x in range(train_probs):
        random_probs.append(1)
    for x in range(test_probs):
        random_probs.append(0)
    
    site_name = s_name
    destination_dir = "/datadrive/train_data/{}@port{}".format(site_name.replace(' ','_'),train_folder)
    destination = "mkdir {}".format(destination_dir)
    os.system(destination)
    train_list = "{}/train.txt".format(destination_dir)
    test_list = "{}/test.txt".format(destination_dir)
    for x in image_list:
        os.system("wget --no-check-certificate --no-proxy {} -P {}".format(x,destination_dir))
 
    os.system("mkdir {}/train".format(destination_dir))
    os.system("mkdir {}/test".format(destination_dir))
    os.system("mkdir {}/annos".format(destination_dir))
    i_list = []
    for root, dirs, files in os.walk("{}".format(destination_dir)):
        for filename in files:
            i_list.append(filename)

    for x in i_list:
        a = random.choice(random_probs)
        if a ==1:
            source = "{}/{}".format(destination_dir,x)
            dest   = "{}/train".format(destination_dir)
            shutil.move(source,dest)
        if a == 0:
            source = "{}/{}".format(destination_dir,x)
            dest   = "{}/test".format(destination_dir)
            shutil.move(source,dest)

    '''
    create train and test list
    '''
    list_1 = open(train_list,'w+')
    list_2 = open(test_list,'w+')
    train_dir = "{}/train".format(destination_dir)
    test_dir  = "{}/test".format(destination_dir)

    for root, dirs, files in os.walk("{}".format(train_dir)):
        for filename in files:
            list_1.write('/images/train/'+ filename)
            list_1.write('\n')
    list_1.close()

    for root, dirs, files in os.walk("{}".format(test_dir)):
        for filename in files:
            list_2.write('/images/test/'+ filename)
            list_2.write('\n')
    list_2.close()

    '''
    download annotations for all filesssssssssssssss
    '''
    new_train = open(train_list,'r')
    new_test = open(test_list,'r')
    training_list = new_train.read().splitlines()
    testing_list = new_test.read().splitlines()
    new_train.close()
    new_test.close()
    classes = get_anno_class(site_name)
    for img in image_list:
        anno_file = '{}/annos/'.format(destination_dir) +img.split('/')[-1].replace('.jpg','.txt') 
        f = open(anno_file,'+w')
        image_id = images.objects.get(filename=img.split('/')[-1]).id
        anno_psql= bbox.objects.filter(image_id=image_id).filter(box_type='human')
        for a in anno_psql:
            for c in classes:
                if a.box_class == c['class']:
                    class_id = str(c['id'])
                    x_1 = str((a.x + a.w/2)/a.width)
                    y_1 = str((a.y + a.h/2)/a.height)
                    w_1 = str((a.w/a.width))
                    h_1 = str((a.h/a.height))     
                    f.write('{} {} {} {} {}\n'.format(class_id,x_1,y_1,w_1,h_1)) 
        f.close()

 
    '''
    place the annotations in train and test folder
    '''
    for x in training_list:
        src = '{}/annos/'.format(destination_dir) + x.split('/')[-1].replace('.jpg','.txt') 
        dest = '{}/train/'.format(destination_dir)
        shutil.move(src,dest)
        
    for x in testing_list:
        src = '{}/annos/'.format(destination_dir) + x.split('/')[-1].replace('.jpg','.txt')
        dest = '{}/test/'.format(destination_dir)
        shutil.move(src,dest)
        
#    for img in image_list:
#        img_check = img.split('/')[7]
#        for x in training_list:
#            train_check = x.split('/')[3]
#            if train_check == img_check:
#                ann_file = train_check.split('.')[0]+".txt"
#                tmp_file = "{}/train/{}".format(destination_dir,ann_file)
#                print(img)
#                split_img = img.split('/')
#                s_name   = split_img[4]
#                s_id     = sites.objects.get(site_name = s_name).id
#                d_name   = split_img[5]
#                d_id     = dates.objects.filter(date= d_name).get(site_id = s_id).id
#                c_name   = split_img[6]
#                c_id     = cam_id.objects.filter(date_id= d_id).get(cam_id=c_name).id
#                image_id = images.objects.filter(site_id=s_id).filter(date_id=d_id).filter(cam_id=c_id).get(filename=train_check).id
#                f = open(tmp_file,'w+')
#                print(s_id,d_id,c_id,image_id,classes)
#                anno_psql= bbox.objects.filter(site_id=s_id).filter(date_id=d_id).filter(cam_id=c_id).filter(image_id=image_id).filter(box_type='human')
#                for a in anno_psql:
#                    for c in classes:
#                        if a.box_class == c["class"]:
#                            print(c,a.x,a.w,a.y,a.h)
#                            f.write(str(c["id"]))
#                            f.write(" ")
#                            f.write(str((a.x+a.w)/(2*a.width)))
#                            f.write(" ")
#                            f.write(str((a.y+a.h)/(2*a.height)))
#                            f.write(" ")
#                            f.write(str(a.w/a.width))
#                            f.write(" ")
#                            f.write(str(a.h/a.height))
#                            f.write('\n')
#                f.close()
#
#        for x in testing_list:
#            test_check = x.split('/')[3]       
#            if test_check==img_check:
#                ann_file = test_check.split('.')[0]+".txt"
#                tmp_file = "{}/test/{}".format(destination_dir,ann_file)
#                split_img = img.split('/')
#                s_name   = split_img[4]
#                s_id     = sites.objects.get(site_name = s_name).id
#                d_name   = split_img[5]
#                d_id     = dates.objects.filter(date= d_name).get(site_id = s_id).id
#                c_name   = split_img[6]
#                c_id     = cam_id.objects.filter(date_id= d_id).get(cam_id=c_name).id
#                image_id = images.objects.filter(site_id=s_id).filter(date_id=d_id).filter(cam_id=c_id).get(filename=test_check).id
#                f = open(tmp_file,'w+')
#                anno_psql= bbox.objects.filter(site_id=s_id).filter(date_id=d_id).filter(cam_id=c_id).filter(image_id=image_id).filter(box_type='human')
#                for a in anno_psql:
#                    for c in classes:
#                        if a.box_class == c["class"]:
#                            f.write(str(c["id"]))
#                            f.write(" ")
#                            f.write(str((a.x+a.w)/(2*a.width)))
#                            f.write(" ")
#                            f.write(str((a.y+a.h)/(2*a.height)))
#                            f.write(" ")
#                            f.write(str(a.w/a.width))
#                            f.write(" ")
#                            f.write(str(a.h/a.height))
#                            f.write('\n')
#                f.close()
#    
#    
    print('split done')
    return(site_name)



def train_response(iter_val,train_process):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "insert into uvdata_train_progress(iter,loss,avg_loss,lr,process_name) values(%s,%s,%s,%s,%s)"
        cur.execute(query,(iter_val["iter"],iter_val["loss"],iter_val["avg_loss"],iter_val["lr"],train_process))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_train_progress table",error)
    finally:
        if (con):
            cur.close()
            con.close()



def update_container_info(port,job,total_images,container_name,s_name):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "insert into uvdata_train_detector_containers (port,job,total_images,container_name,model) values (%s,%s,%s,%s,%s)"
        cur.execute(query,(port,job,total_images,container_name,s_name))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not insert in train detector containers table",error)
    finally:
        if (con):
            cur.close()
            con.close()


def create_data(request,port):
    keys = request.keys()
    '''
    query db based on keys from UI
    '''
    if ('c_id' in keys):
        server_images = []
        d_id   = cam_id.objects.get(id=request['c_id']).date_id
        d_query= dates.objects.get(id=d_id)
        d_name = d_query.date
        s_id   = d_query.site_id
        p_id   = d_query.project_id
        s_name = sites.objects.get(id = s_id).site_name
        p_name = projects.objects.get(id=p_id).project_name
        c_name = cam_id.objects.get(id=request['c_id']).cam_id
        image_list = images.objects.filter(cam_id=request.data['c_id']).filter(human_or_not=True)
        for x in image_list:
            f_name = x.filename
            file_format = "/data/downloads/{}/{}/{}/{}/{}".format(p_name,s_name,d_name,c_name,f_name)
            server_images.append(file_format)
        job= "/{}/{}/{}/{}".format(p_name,s_name,d_name,c_name)


    if  ('d_id' in keys):
        server_images = []
        for d in request['d_id']:
            d_query= dates.objects.get(id=d)
            d_name = d_query.date
            s_id   = d_query.site_id
            p_id   = d_query.project_id
            s_name = sites.objects.get(id=s_id).site_name
            p_name = projects.objects.get(id=p_id).project_name 
            image_list = images.objects.filter(date_id=d).filter(human_or_not=True)
          #  image_list = images.objects.filter(date_id=d).filter(human_or_not=False)
            for x in image_list:
                c_id = x.cam_id
                c_name = cam_id.objects.get(id=c_id).cam_id
                f_name = x.filename
               # file_format = "/data/downloads/{}/{}/{}/{}/{}".format(p_name,s_name,d_name,c_name,f_name)
                file_format = x.image_url
                server_images.append(file_format)
        job= "/{}/{}/{}".format(p_name,s_name,str(d_name))


    if  ('s_id' in keys) :
        server_images = []
        s_query = sites.objects.get(id=request['s_id'])
        p_id    = s_query.project_id
        p_name  = projects.objects.get(id=p_id).project_name
        s_name  = sites.objects.get(id=request['s_id']).site_name
        image_list = images.objects.filter(site_id=request['s_id']).filter(human_or_not=True)
        for x in image_list:
            d_id =  x.date_id
            c_id =  x.cam_id
            d_name = dates.objects.get(id=d_id).date
            c_name = cam_id.objects.get(id=c_id).cam_id
            f_name = x.filename
            file_format = "/data/downloads/{}/{}/{}/{}/{}".format(p_name,s_name,d_name,c_name,f_name)
            server_images.append(file_format)
        job= "/{}/{}".format(p_name,s_name)
   
    '''
    update train_detector_containers for future reference
    '''
    container_name = "train_detector_{}_{}".format(s_name,port)
    check_container_entry = train_detector_containers.objects.filter(port=port).count()
    if check_container_entry > 0 :
        print('multiple containers present....delete from db') 
        return('bad_db')
    
    else:
        update_container_info(port,job,len(server_images),container_name,s_name)
    
    '''
    send for data for split and annotations
    '''
    site_name = split_data(server_images,port,request['split'],p_name,s_name)
    return(site_name)



def get_model_info(site_name):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query_2 ="select * from uvdata_darknet_models where project = %s and model=darknet"
        cur.execute(query_2,(site_name,))
        container_info = cur.fetchone()
        return(container_info)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()


 
class create_dataset(APIView):


    def post(self,request):
        #train_port = find_port()
        #for x in other_ports:
        #    if str(x["train_port"]) == str(train_port):
        #        map_port = x["map_port"]
        #        anchor_port = x["anchor_port"]


        '''
        make entry in db
        '''


        d_ids = eval(request.data['d_id'])
        all_dates = []
        if type(d_ids) is tuple:
            for x in d_ids:
                all_dates.append(int(x))
        if type(d_ids) is int:
            all_dates.append(int(d_ids))


        target_instance = request.data['instance']
        instance_id = remote_instances.objects.get(public_ip = target_instance).id
        split  = request.data['split']
        model_type = request.data['model']
        s_id = dates.objects.get(id = all_dates[0]).site_id
        s_name = sites.objects.get(id=s_id).site_name.replace(' ','_').replace(',','_')
        p_name = projects.objects.get(id=sites.objects.get(id=s_id).project_id).project_name.replace(' ','_').replace(',','_')
        d_list = ''
        for _id in all_dates:
            d_list += str(_id)

        title = '{}@{}@{}'.format(p_name,s_name,d_list)
        uid = random.randint(1000,10000)

        uploaded_file = request.FILES['class_list']
        class_ids = []
        for line in uploaded_file:
           class_id = classes.objects.get(class_name = line.decode().replace('\r\n','')).id
           class_ids.append(class_id)  

       # entry in datasetvsmodel
        check_entry = datasetvsmodel.objects.filter(title=title)
        if len(check_entry) == 0:
            datasetvsmodel.objects.create(title=title,date_id=all_dates,uid=uid,model_type=model_type,split=split,attributes=class_ids)


       # entry in remote_processess
        current_process_id = deeplearning_processes.objects.filter(process = model_type).get(process_name='createDataset').id
        dataset_id = datasetvsmodel.objects.get(title=title).id
        check_entry = remote_processes.objects.filter(instance_id = instance_id)
        if len(check_entry) == 0:
            if model_type == 'detector':
                process = [1]

            params = {}
            params['model'] = []
            params['dataset'] = [dataset_id]

            completed = {}
            completed['model'] = []
            completed['dataset'] = []


            remote_processes.objects.create(instance_id = instance_id, process = process,params=params,completed=completed)


        else:
            current_processes = check_entry[0].process
            current_params  = check_entry[0].params

            update_processes = []
            if current_process_id not in current_processes:
                current_processes.append(int(current_process_id))
                
               # for x in current_processes:
               #     update_processes.append(int(x))
 
      #      update_processes.append(current_process_id)


            dataset_params = []
            if dataset_id not in current_params['dataset']:
                for x in current_params['dataset']:
                    dataset_params.append(x) 
            dataset_params.append(dataset_id)

            current_params['dataset'] = dataset_params

            current_completed = check_entry[0].completed

            print(current_completed,current_params,update_processes)
            remote_processes.objects.filter(instance_id = instance_id).update(process = current_processes,params=current_params,completed=current_completed)
        return Response("dataset and training docker have been created and are running on port ")




class create_dataset_classifier(APIView):
    def post(self,request):

        '''
        make entry in db
        '''
        classifier_type = request.data['classifier_type']
        d_ids = request.data['d_id']
        all_dates = []
        for x in d_ids:
            all_dates.append(int(x))
        target_instance = request.data['instance']
        instance_id = remote_instances.objects.get(public_ip = target_instance).id
        split  = request.data['split']
        s_id = dates.objects.get(id = d_ids[0]).site_id
        s_name = sites.objects.get(id=s_id).site_name.replace(' ','_')
        p_name = projects.objects.get(id=sites.objects.get(id=s_id).project_id).project_name.replace(' ','_')
        d_list = ''
        for _id in d_ids:
            d_list += str(_id)
        title = '{}@{}@{}'.format(p_name,s_name,d_list)
        uid = random.randint(1000,10000)
        class_ids  = []
        for x in classifier.objects.filter(site_id=s_id).filter(classifier=classifier_type):
            if x.attribute in request.data['attributes']:
                class_ids.append(x.id)

        '''
        entry in datasetvsmodel
        '''
        check_entry = datasetvsmodel.objects.filter(title=title)
        if len(check_entry) == 0:
            datasetvsmodel.objects.create(title=title,date_id=all_dates,uid=uid,model_type='classifier',split=split,attributes=class_ids)


        '''
        entry in remote_processess
        '''
        current_process_id = deeplearning_processes.objects.filter(process = 'classifier').get(process_name='createDataset').id
        dataset_id = datasetvsmodel.objects.filter(model_type='classifier').get(title=title).id
        check_entry = remote_processes.objects.filter(instance_id = instance_id)
        if len(check_entry) == 0:
            process = [current_process_id]
            params = {}
            params['classifier_model'] = []
            params['classifier_dataset'] = [dataset_id]
            params['model'] = []
            params['dataset'] = []

            completed = {}
            completed['classifier_model'] = []
            completed['classifier_dataset'] = []
            completed['model'] = []
            completed['dataset'] = []

            remote_processes.objects.create(instance_id = instance_id, process = process,params=params,completed=completed)

        else:
            current_processes = check_entry[0].process
            current_params  = check_entry[0].params
            update_processes = []
            if current_process_id not in current_processes:
                for x in current_processes:
                    update_processes.append(int(x))
            update_processes.append(current_process_id)
            dataset_params = []
            if dataset_id not in current_params['classifier_dataset']:
                for x in current_params['classifier_dataset']:
                    dataset_params.append(x)
            dataset_params.append(dataset_id)
            current_params['classifier_dataset'] = dataset_params
            current_completed = check_entry[0].completed
            print(current_completed['dataset'])
            current_completed['classifier_dataset'].append(int(dataset_id))
            print(current_completed['classifier_dataset'])
            remote_processes.objects.filter(instance_id = instance_id).update(process = update_processes,params=current_params,completed=current_completed)


        ''' 
        s_id = dates.objects.get(id = d_ids[0]).site_id
     #   classifiers = attributes.objects.get(site_id = s_id).attribute[classifier_type]
        classifier_info = classifier.objects.filter(site_id = s_id).distinct('classifier')
        final_info = {}
        attrs  = []
        for x in classifier_info:
            final_info[x.classifier]= []
            attr_info = classifier.objects.filter(site_id= s_id).filter(classifier = x.classifier)
            for y in attr_info:
                final_info[x.classifier].append(y.attribute)
                attrs.append(y.id)
        classifiers = final_info[classifier_type]
        print(classifiers)
        train_port = find_port()
        dir_name = '/datadrive/train_data/{}@port{}'.format(classifier_type,train_port)
        os.mkdir('{}'.format(dir_name),mode = 0o777)
        dirs = ['train','test']
        for dir_ in dirs:
            os.mkdir('{}/{}'.format(dir_name,dir_),mode = 0o777)
            for c in classifiers:
                dir_path = '{}/{}/{}'.format(dir_name,dir_,c) 
                os.mkdir('{}'.format(dir_path),mode = 0o777)




        check_db = datasetvsmodel.objects.filter(title = '{}@port{}'.format(classifier_type,train_port))
        if len(check_db) == 0:
            datasetvsmodel.objects.create(title = '{}@port{}'.format(classifier_type,train_port),date_id = d_ids,uid = train_port,model_type = 'classifier',attributes=attrs) 
            print('done') 

        random_probs = []
        train_probs = int(math.floor(split*10))
        test_probs = 10-int(math.ceil(split*10))
        for x in range(train_probs):
            random_probs.append(1)
        for x in range(test_probs):
            random_probs.append(0)




        final_list = []
        for d in d_ids:
            image_list = images.objects.filter(date_id = d).filter(human_or_not=True)
            for i in image_list:
                choice = random.choice(random_probs)
                boxes = bbox.objects.filter(image_id = i.id).filter(box_type='human').filter(classifier__has_key=classifier_type)
                
                wget_cmd = 'wget -P {} {}'.format(dir_name,i.image_url)
                os.system(wget_cmd)


                img_read =  cv2.imread('{}/{}'.format(dir_name,i.filename))
                counter = 0;
                for box in boxes:
                    print(choice, box)
                    crop_img = img_read[int(box.y):int(box.y)+int(box.h), int(box.x):int(box.x)+int(box.w)]
                    new_crop_name = i.filename.split('.jpg')[0] + '_{}.jpg'.format(box.id)
                    if choice == 1:
                        cv2.imwrite('{}/train/{}/{}'.format(dir_name,box.classifier[classifier_type],new_crop_name),crop_img)

                    else:
                        cv2.imwrite('{}/test/{}/{}'.format(dir_name,box.classifier[classifier_type],new_crop_name),crop_img)
                    
                os.remove('{}/{}'.format(dir_name,i.filename))
        '''
        return Response('dataset has been registered')



class get_anchor_boxes(APIView):

    def get(self,request):
        response_data = []

        query_processes = remote_processes.objects.all()
        for instance in query_processes:
            tmp = {} 
            tmp['instance'] = remote_instances.objects.get(id = instance.instance_id).public_ip
            tmp['models'] = []
            tmp['datasets'] = []
            for x in instance.completed['model']:
                tmp2= {}
                tmp2['text'] = x
                tmp2['value'] = x
                tmp['models'].append(tmp2)

            for x in instance.completed['dataset']:
                tmp2= {}
                tmp2['text'] = datasetvsmodel.objects.get(id=x).title
                tmp2['value'] = datasetvsmodel.objects.get(id=x).title
                tmp['datasets'].append(tmp2)

            response_data.append(tmp)
         
        
        return Response(response_data)

    def post(self,request):
        instance = request.data['instance']
        dataset  = request.data['folder']
        height  = request.data['height']
        width   = request.data['width']
        clusters= request.data['clusters']
        model_folder = request.data['model'] 
 
        '''
        get instance  process 
        '''
        task_id = deeplearning_processes.objects.get(process='detector',process_name='calculateAnchorBox').id

        instance_id = remote_instances.objects.get(public_ip = instance).id
        check_process = remote_processes.objects.filter(instance_id = instance_id)
        if len(check_process) != 0:

           '''
           update process column
           '''
           
           current_processes= check_process[0].process
           if task_id not in current_processes:
               current_processes.append(task_id)

           '''
           update params column
           '''

           current_params = check_process[0].params
           param_keys = current_params.keys()
           if 'detectorAnchors' not in param_keys:
               anchor_map = []
               tmp = {}
               tmp['model'] = infer_models.objects.get(model=model_folder).id
               tmp['dataset'] = datasetvsmodel.objects.get(title=dataset).id
               tmp['height'] = height
               tmp['width'] = width
               tmp['clusters'] = clusters
               anchor_map.append(tmp)
               current_params['detectorAnchors'] = anchor_map

           else: 
               anchor_map = current_params['detectorAnchors']
               tmp = {}
               tmp['model'] = infer_models.objects.get(model=model_folder).id
               tmp['dataset'] = datasetvsmodel.objects.get(title=dataset).id
               tmp['height'] = height
               tmp['width'] = width
               tmp['clusters'] = clusters
               anchor_map.append(tmp)


           current_params['detectorAnchors'] = anchor_map


           '''
           update completed params
           '''
           current_completed = check_process[0].completed
           param_keys = current_completed.keys()
           if 'detectorAnchors' not in param_keys:
               anchor_map = []
               tmp = {}
               tmp['model'] = infer_models.objects.get(model=model_folder).id
               tmp['dataset'] = datasetvsmodel.objects.get(title=dataset).id
               tmp['height'] = height
               tmp['width'] = width
               tmp['clusters'] = clusters
               anchor_map.append(tmp)
           else:  
               anchor_map = current_params['detectorAnchors']
               tmp = {}
               tmp['model'] = infer_models.objects.get(model=model_folder).id
               tmp['dataset'] = datasetvsmodel.objects.get(title=dataset).id
               tmp['height'] = height
               tmp['width'] = width
               tmp['clusters'] = clusters
               anchor_map.append(tmp)


           current_completed['detectorAnchors'] = anchor_map


           '''
           update table finally
           '''
           print(current_params,current_completed)
           remote_processes.objects.filter(instance_id = instance_id).update(process = current_processes,params=current_params,completed=current_completed)
           
           return Response('anchor box process registered')


        else:
            return Response('create dataset and model on target instancce')



class train_module(APIView):

    def get(self,request):
        train_ports = [5561,5562,5563,5564,5565]
        config_dir   = '/datadrive/detectors/darknet/config/'
        model_dir   = '/datadrive/detectors/darknet/downloaded_models/'
        train_dir = '/datadrive/detectors/darknet/'
        data_dir    = '/datadrive/train_data/'
        project_list=['Cattleya','syn-com']
        response = {}
        response['w_n_c'] = {}
        response['data']  = []
        response['models']  = []
        response['w_n_c']['config'] = []
        response['w_n_c']['weights'] = []
        ''' 
        for x in os.listdir(config_dir):
            tmp={}
            tmp['text'] = x
            tmp['value'] = x
            response['w_n_c']['config'].append(tmp)

        for x in train_ports:
            tmp={}
            tmp['text'] = 'backup_{}'.format(x)
            tmp['value'] = 'backup_{}'.format(x)
            tmp['weights'] =[]
            for y in os.listdir(train_dir+tmp['text']):
                tmp2={}
                tmp2['text'] = y
                tmp2['value'] = y
                tmp['weights'].append(tmp2)
            response['w_n_c']['weights'].append(tmp)
        '''
        for x in os.listdir(data_dir):
            tmp = {}
            tmp['text'] = x
            tmp['value'] = x
            response['data'].append(tmp)   

        for x in os.listdir(model_dir):
            tmp = {}
            tmp['text'] = x
            tmp['value'] = x
            response['models'].append(tmp)   

        return Response(response)
   

    def post(self,request):
        '''
        get data, weight and config 
        '''
        data_dir = "/datadrive/train_data/"
        weight = '/app/downloaded_models/{}/model.weights'.format(request.data['model_folder'])
        config = '/app/downloaded_models/{}/model.cfg'.format(request.data['model_folder'])
        data   = request.data["data_folder"]
        port   = request.data["data_folder"].split('@port')[1]
        site_name   = request.data["data_folder"].split('@port')[0]

        
        ''' start docker container '''
        model_query = darknet_models.objects.filter(project=site_name).get(model="darknet")
        image_id = model_query.image_id
        directory = model_query.directory
        img_folder  = "/datadrive/train_data/{}".format(data)
        project     = site_name
        container_attr= "train"
        backup_dir  = "backup_{}".format(port)
        '''
        start docker 
        '''
        container_command = "echo 'admin@123' | sudo -S python3 scripts/start_darknet.py {} {} {} {} {} {}".format(img_folder,str(image_id),directory,container_attr,str(port),site_name)
        print(container_command)
        os.system(container_command)

        container_name = "darknet_train_{}_{}".format(site_name,port)
        curl_command = 'apt-get install -y libcurl4-openssl-dev'
        os.system('docker exec {} {}'.format(container_name,curl_command))
        os.system('docker exec {} bash -c cd /app'.format(container_name))

        client = docker.from_env()
        command="./darknet detector train /app/downloaded_models/{}/model.data {} {} -port {}".format(request.data['model_folder'],config,weight,str(port))
        print(command)
        backup_dir = '/app/backup_{}/'.format(str(port))
        print(backup_dir)
        container_name = "darknet_train_{}_{}".format(site_name,port)
        target_container = client.containers.get(container_id = container_name)
        print(target_container)
        log_cont = target_container.exec_run(cmd=command,detach=True)
       
        '''
        setup zmq socket
        '''
        context = zmq.Context()
        socket = context.socket(zmq.PAIR)
        host_socket = "tcp://localhost:{}".format(port)
        socket.connect(host_socket)
        socket.send_string(backup_dir)
        print('training docker started')


#        while True:
#            try:
#                message =socket.recv()
#                iter_val = json.loads(message.decode('utf-8'))
#                print(iter_val)
##               train_response(iter_val,train_process)
#            except zmq.error.Again as e:
#                print('port stopped responding')
#                '''
#                close docker and start new one
#                '''
#                os.system("echo admin@123 |sudo -S service docker restart")
#                target_container.stop()
#                target_container.remove(force=True)
#                
#                container_command = "echo 'admin@123' | sudo -S python scripts/start_darknet.py {} {} {} {} {} {}".format(img_folder,str(image_id),directory,container_attr,str(port),site_name)
#                os.system(container_command)
#                command="./darknet detector train /app/cfg/model.data /app/{} /app/backup_{}/train_last.weights -port {}".format(config,str(port),str(port))
#                print(command)
#                container_name = "darknet_train_{}_{}".format(site_name,port)
#                target_container = client.containers.get(container_id = container_name)
#                log_cont = target_container.exec_run(cmd=command,stream=True, privileged=True)
#                socket.send_string(site_name)
#
 
        return Response("training on")        




class map_module(APIView):

 
    def post(self,request):

        dataset_query = datasetvsmodel.objects.get(title=request.data['dataset'])
        model_query   = infer_models.objects.get(model=request.data['model'])
        instance_query = remote_instances.objects.get(public_ip=request.data['instance'])
        process_id = deeplearning_processes.objects.get(process_name = 'calculateMAP',process=model_query.model_type).id


        check_query = remote_processes.objects.filter(instance_id=instance_query.id)
        if len(check_query) != 0:
            '''
            update process
            '''
            current_process = check_query[0].process
            if process_id not in current_process:
                current_process.append(process_id)  
           

            '''
            update params
            '''
            params = check_query[0].params
            param_keys = params.keys()
            if 'detectorMAP' not in param_keys:
                params['detectorMAP'] = []
                tmp ={}
                tmp['model'] = model_query.id
                tmp['dataset'] = dataset_query.id
                params['detectorMAP'].append(tmp) 


            else:
                map_list = params['detectorMAP']
                tmp ={}
                tmp['model'] = model_query.id
                tmp['dataset'] = dataset_query.id
                if tmp not in map_list:
                   map_list.append(tmp)
                params['detectorMAP'] = map_list


            '''
            update completed params
            '''
            completed_params = check_query[0].completed
            param_keys = completed_params.keys()
            if 'detectorMAP' not in param_keys:
                completed_params['detectorMAP'] = []
            else:
                map_list = completed_params['detectorMAP']
                tmp ={}
                tmp['model'] = model_query.id
                tmp['dataset'] = dataset_query.id
                if tmp not in map_list:
                   map_list.append(tmp)    
                completed_params['detectorMAP'] = map_list 

            '''
            update db
            '''
            remote_processes.objects.filter(instance_id = instance_query.id).update(process = current_process,params=params,completed=completed_params)

            return Response('process registered')
        else:
            return Response('create dataset and models first')



        '''
        get data, weight and config 
        model_dir = '/app/downloaded_models/{}'.format(request.data['model'])
        data_dir = "/datadrive/train_data/"
        weight = '{}/model.weights'.format(model_dir)
        config = '{}/model.cfg'.format(model_dir)
        data   = request.data["data_folder"]
        port   = request.data["data_folder"].split('@port')[1]
        site_name   = request.data["data_folder"].split('@port')[0]
        for x in other_ports:
            if str(x["train_port"]) == str(port):
                map_port = x["map_port"]
       
        img_folder  = "/datadrive/train_data/{}".format(data)
        container_attr = "map"
        project     = site_name
        print(site_name)
        model_query = darknet_models.objects.filter(model="darknet")[0]
        image_id = model_query.image_id
        directory = model_query.directory
        container_attr="map"
        backup_dir = "backup_{}".format(port)

        container_command = "echo 'admin@123' | sudo -S python3 scripts/start_darknet.py {} {} {} {} {} {}".format(img_folder,str(image_id),directory,container_attr,str(map_port),site_name)
        print(container_command)
        os.system(container_command)

        container_name = "darknet_{}_{}_{}".format(container_attr,site_name,map_port)
        curl_command = 'apt-get install -y libcurl4-openssl-dev'
        os.system('docker exec {} {}'.format(container_name,curl_command))
        os.system('docker exec {} bash -c cd /app'.format(container_name))

        command="./darknet detector map {}/model.data {} {} -port {}".format(model_dir,config,weight,str(map_port))
        print(command)

        client = docker.from_env()
        container_name = "darknet_{}_{}_{}".format(container_attr,site_name,map_port)
        target_container = client.containers.get(container_id = container_name)
        log_cont = target_container.exec_run(cmd=command,stream=True, privileged=True,detach=True)
        

        context = zmq.Context()
        socket = context.socket(zmq.PAIR)
        host_socket = "tcp://localhost:{}".format(str(map_port))
        socket.connect(host_socket) 
        socket.send_string(site_name)
        print('mAP docker started')
        try:
            message =socket.recv()
            print(message)
            iter_val = json.loads(message.decode('utf-8'))
            print(iter_val)
        except zmq.error.Again as e:
            print('port stopped responding')
         
           
            target_container.stop()
            target_container.remove(force=True)
   
       

        target_container.stop()
        target_container.remove()
        socket.close()
        '''



class post_train_data(APIView):
    def post(self,request):
        for x in request.data:
            d = json.loads(x)
            print(d)
            update = train_progress.objects.create(iter =int(d['iter']) ,loss=float(d['loss']), avg_loss=float(d['avg_loss']),lr=float(d['lr']),port=d['port'])
            update.save()
            print(x)
        return Response('ok working')


class get_results(APIView):
    def get(self,request):
        queryset = train_progress.objects.values('port').distinct()
        ports = []
        response_data = []
        for port in queryset:
            ports.append(port)

        for p in ports:
            all_objects = train_progress.objects.filter(port=p['port'])

            '''
            max iterations
            '''
            itermax = all_objects.aggregate(Max('iter'))['iter__max']
            itermin = all_objects.aggregate(Min('iter'))['iter__min']
            model_iterations = itermax - itermin
            '''
            avg loss and current lr
            '''            
            avg_loss = train_progress.objects.get(iter=itermax).avg_loss
            lr = train_progress.objects.get(iter=itermax).lr
            '''
            Training folder
            '''
            for x in os.listdir('/data/train_data/'):
                if int(x.split('@port')[1]) == int(p['port']):
                    training_folder = x
                    break

            tmp = {}
            tmp['folder'] = training_folder
            tmp['cfg_iter'] = itermax
            tmp['iters'] = model_iterations
            tmp['current_lr'] = lr
            tmp['port'] = p['port']
            tmp['iters'] = model_iterations
            tmp['avg_loss'] = avg_loss
            '''
            graph values
            '''
            tmp['graph_values'] = []
            graph_iters = []
            for i in range(5):
                graph_iters.append(itermin+i)
        
            for i in range(5):
                graph_iters.append(int(itermin + int(itermax-itermin)/2 + i))

            for i in range(5):
                graph_iters.append(itermax-i)
    
            for x in graph_iters:
                tmp2 = {}
                tmp2['iter'] = x 
                try:
                    tmp2['loss'] = all_objects.get(iter=x).loss
                    print(tmp2['loss']) 
                    tmp['graph_values'].append(tmp2)
                except:
                    continue
            response_data.append(tmp)            

        return Response(response_data)




class upload_detector(APIView):
    def get(self,request):

        '''
        weights
        '''
        response = []
        a = paginator.paginate(Bucket='uvdata-models',Delimiter='/',Prefix = 'detector/weights/')
        for result in a:
            for x in result['Contents']:
                if x['Key'].split('/')[-1] is not '':
                    tmp ={}
                    tmp['text'] = x['Key'].split('/')[-1]
                    tmp['value'] = x['Key'].split('/')[-1]
                    response.append(tmp)


        '''
        configs
        '''
        response_1 = []
        a = paginator.paginate(Bucket='uvdata-models',Delimiter='/',Prefix = 'detector/config/')
        for result in a:
            for x in result['Contents']:
                if x['Key'].split('/')[-1] is not '':
                    tmp ={}
                    tmp['text'] = x['Key'].split('/')[-1]
                    tmp['value'] = x['Key'].split('/')[-1]
                    response_1.append(tmp)
      
       
        '''
        remote instances
        ''' 
        response_2 = []
        query = remote_instances.objects.all()
        for x in query:
            tmp = {}
            tmp['text'] = x.public_ip
            tmp['value'] = x.public_ip
            response_2.append(tmp)  

        final_response = {
           'weights': response,'configs':response_1, 'instances':response_2
        }



        return Response(final_response)



    def post(self,request):


        if request.data['detector_type'] == 'detector':
            target_instance = request.data['instance_id']
            target_weight = request.data['weight']
            target_cfg    = request.data['cfg']
            current_process_id = deeplearning_processes.objects.get(process='detector',process_name='downloadModel').id
            '''
#            update infer_models table
            '''
            uploaded_file = request.FILES['names_file']
            class_ids = []
            for line in uploaded_file:
               class_id = classes.objects.get(class_name = line.decode().replace('\r\n','')).id
               class_ids.append(class_id)
            check_model = infer_models.objects.filter(model_type='detector',model =request.data['model_name'])
            if len(check_model)==0:
                tmp = {}
                tmp['weight'] = target_weight
                tmp['config'] = target_cfg


                infer_models.objects.create(model_type='detector',model =request.data['model_name'],detector=class_ids,config = tmp)
            instance_id = remote_instances.objects.get(public_ip = target_instance).id
            '''
#            update remote_process table
            '''
            query = remote_processes.objects.filter(instance_id=instance_id)
            if len(query) == 0:
                params = {}
                params['dataset'] = []
                params['model'] = [request.data['model_name']]
                params['classifier_model'] = []

                completed = {}
                completed['dataset'] = []
                completed['model'] = []
                completed['classifier_model'] = []


                remote_processes.objects.create(instance_id = instance_id,process=[current_process_id],params=params,completed=completed)
            else:
                query_instance = remote_processes.objects.get(instance_id = instance_id)
                current_processes = query_instance.process
                update_processes = []
                current_params = query_instance.params
                update_params = []
                if current_process_id not in current_processes:
                    for x in current_processes:
                        update_processes.append(int(x))
                if request.data['model_name'] not in current_params['model']:
                    for x in current_params['model']:
                        update_params.append(x)
                update_params.append(request.data['model_name'])
                current_params['model'] = update_params
                update_processes.append(int(current_process_id))

                current_completed = query_instance.completed
                #new_model = current_completed['model'].append(request.data['model_name'])
                #current_completed['model']  = new_model
                remote_processes.objects.filter(instance_id = instance_id).update(process = update_processes,params=current_params,completed=current_completed)

         
        if request.data['detector_type'] == 'sst-detector':
            target_instance = request.data['instance_id']
            target_weight = request.data['weight']
            target_cfg    = request.data['cfg']
            target_weight_roi = request.data['roi_weight']
            target_proposal_cfg = request.data['proposal_cfg']
            current_process_id = deeplearning_processes.objects.get(process='sst-detector',process_name='downloadModel').id

            '''
#            update infer_models table
            '''
            uploaded_file = request.FILES['names_file']
            class_ids = []
            for line in uploaded_file:
               class_id = classes.objects.get(class_name = line.decode().replace('\r\n','')).id
               class_ids.append(class_id)
            check_model = infer_models.objects.filter(model_type='sst-detector',model =request.data['model_name'])
            if len(check_model)==0:
                tmp = {}
                tmp['weight'] = target_weight
                tmp['config'] = target_cfg
                tmp['roi-weight'] = target_weight_roi
                tmp['proposal_config'] = target_proposal_cfg

                infer_models.objects.create(model_type='sst-detector',model =request.data['model_name'],detector=class_ids,config = tmp)
            instance_id = remote_instances.objects.get(public_ip = target_instance).id
            '''
#            update remote_process table
            '''
            query = remote_processes.objects.filter(instance_id=instance_id)
            if len(query) == 0:
                params = {}
                params['dataset'] = []
                params['model'] = [request.data['model_name']]
                params['classifier_model'] = []

                completed = {}
                completed['dataset'] = []
                completed['model'] = []
                completed['classifier_model'] = []

                remote_processes.objects.create(instance_id = instance_id,process=[current_process_id],params=params,completed=completed)
            else:
                query_instance = remote_processes.objects.get(instance_id = instance_id)
                current_processes = query_instance.process
                update_processes = []
                current_params = query_instance.params
                update_params = []
                if current_process_id not in current_processes:
                    for x in current_processes:
                        update_processes.append(int(x))
                if request.data['model_name'] not in current_params['model']:
                    for x in current_params['model']:
                        update_params.append(x)
                update_params.append(request.data['model_name'])
                current_params['model'] = update_params
                update_processes.append(int(current_process_id))
                current_completed = query_instance.completed
                #new_model = current_completed['model'].append(request.data['model_name'])
                #current_completed['model']  = new_model
                remote_processes.objects.filter(instance_id = instance_id).update(process = update_processes,params=current_params,completed=current_completed)


        return Response('model specifications have been updated to db')



class upload_classifier(APIView):
    def get(self,request):

        final_response = {}
        response = []
        a = paginator.paginate(Bucket='uvdata-models',Delimiter='/',Prefix = 'classifier/')
        for result in a:
            for x in result['CommonPrefixes']:
                a = x['Prefix'].split('classifier/')[1].replace('/','') 
                tmp ={}
                tmp['text'] = a
                tmp['value'] = a 
                response.append(tmp)


        final_response['models'] = response
        site_response = []
        site_query = classifier.objects.all().distinct('site_id')
        for x in site_query:
            tmp = {}
            tmp['text'] = sites.objects.get(id = x.site_id).site_name
            tmp['value'] = sites.objects.get(id = x.site_id).site_name
            tmp['id'] = x.site_id
            tmp['classifier_info'] = []
            for y in classifier.objects.filter(site_id = x.site_id).distinct('classifier'):
                tmp2 = {}
                tmp2['text'] = y.classifier
                tmp2['value'] = y.classifier
                tmp2['attributes'] = []
                for z in classifier.objects.filter(site_id = x.site_id).filter(classifier=y.classifier):
                    tmp3 = {}
                    tmp3['text'] = z.attribute
                    tmp3['value'] = z.attribute
                    tmp2['attributes'].append(tmp3)

                tmp['classifier_info'].append(tmp2)
            site_response.append(tmp)
        final_response['classifier'] = site_response

        '''
        remote instances
        '''
        response_2 = []
        query = remote_instances.objects.all()
        for x in query:
            tmp = {}
            tmp['text'] = x.public_ip
            tmp['value'] = x.public_ip
            response_2.append(tmp)
        final_response['instances'] = response_2

        return Response(final_response)

    def post(self,request):


        target_instance = request.data['instance']
        target_model = request.data['model']
        target_classifier = request.data['classifier']
        target_attributes = request.data['attributes']
        target_site = request.data['site']
        current_process_id = deeplearning_processes.objects.get(process='classifier',process_name='downloadModel').id
        '''
        update infer_models table
        '''
        
        attrs = classifier.objects.filter(site_id = int(target_site))
        attr_ids = []
        for x in attrs:
            if x.attribute in target_attributes:
                attr_ids.append(x.id)

        check_model = infer_models.objects.filter(model_type='classifier',model=target_model)
        if len(check_model)==0:
            tmp = {}
            infer_models.objects.create(model_type='detector',model =target_model,classifier=attr_ids)

        instance_id = remote_instances.objects.get(public_ip = target_instance).id
        '''
        update remote_process table
        '''
        query = remote_processes.objects.filter(instance_id=instance_id)
        if len(query) == 0:
            params = {}
            params['dataset'] = []
            params['model'] = []
            params['classifier_model'] = [request.data['model']]
            params['classifier_dataset'] = []

            completed = {}
            completed['dataset'] = []
            completed['model'] = []
            completed['classifier_model'] = []
            completed['classifier_dataset'] = []
            remote_processes.objects.create(instance_id = instance_id,process=[current_process_id],params=params,completed=completed)

        else:
            query_instance = remote_processes.objects.get(instance_id = instance_id)
            current_processes = query_instance.process
            update_processes = []
            current_params = query_instance.params
            update_params = []
            if current_process_id not in current_processes:
                for x in current_processes:
                    update_processes.append(int(x))
            if request.data['model'] not in current_params['classifier_model']:
                for x in current_params['classifier_model']:
                    update_params.append(x)
            update_params.append(request.data['model'])
            current_params['classifier_model'] = update_params
            update_processes.append(int(current_process_id))
            current_completed = query_instance.completed       
            new_model = current_completed['classifier_model'].append(request.data['model'])
            current_completed['classifier_model']  = new_model
            remote_processes.objects.filter(instance_id = instance_id).update(process = update_processes,params=current_params,completed=current_completed) 
        '''
        target_model = request.data['model']
        target_classifier = request.data['classifier']
        target_attributes = request.data['attributes']
     
        dir_name = '/datadrive/classifiers/{}'.format(target_model)
        os.mkdir('{}'.format(dir_name),mode = 0o777)


        classifier_query = classifier.objects.filter(site_id=int(request.data['site'])).filter(classifier= target_classifier)
        check_db = infer_models.objects.filter(model = target_model).filter(model_type='classifier')
        if len(check_db) == 0:
            attrs = []
            for c in classifier_query:
                if c.attribute  in target_attributes:
                    attrs.append(c.id)
            infer_models.objects.create(model = target_model,model_type = 'classifier',classifier = attrs)
        

        dl_model = 'aws s3 cp s3://uvdata-models/classifier/{}/ {} --recursive'.format(target_model,dir_name)
        os.system(dl_model)
        '''
        return Response('hello')






class upload_instance(APIView):
    def post(self,request):
        req_1 = request.data
        storage = req_1['storage']
        user    = req_1['user']
        gpu_availability = req_1['gpu_available'] 
        pub_ip = req_1['public_ip']
        parent_dir = req_1['parent_dir']

        a = remote_instances.objects.create(public_ip = str(pub_ip),user=str(user), gpu_availability = gpu_availability, free_space=storage, )
        a.save() 
 
        return Response('got the vm')



'''
classifier apis
'''


class classifier_frameworks(APIView):
    def get(self,request):
        response = [{ 'key': 'd', 'text': 'torch', 'value': 'torch' }]
        return Response(response)





'''
get active instances
'''

class get_instances(APIView):
    def get(self,request):
        response = []
        query = remote_instances.objects.all()
        for _instance in query:
            t = time.localtime()
            current_time = str(time.strftime("%H:%M:%S", t)).split(':')
            current_hr = int(current_time[0])
            current_min = int(current_time[1])
            registered_time = str(_instance.ping).split(':')
            registered_hr = int(registered_time[0])
            registered_min = int(registered_time[1])
            if current_hr == registered_hr:
                delta = current_min -registered_min
                if delta < 1:
                    tmp ={}
                    tmp['text'] = _instance.public_ip
                    tmp['value'] = _instance.public_ip
                    response.append(tmp)

 
        return Response(response)




class get_remote_processes(APIView):
    def get(self,request):
        response = []
        query = remote_processes.objects.all()
        for x in query:
            tmp ={}
            tmp['instance'] = remote_instances.objects.get(id=x.instance_id).public_ip
            tmp['models']  = x.completed['model']
            tmp['datasets'] = []
            for y in x.completed['dataset']:
                tmp['datasets'].append(datasetvsmodel.objects.get(id=y).title)

            '''
            get anchor box values
            '''

            
            tmp['anchorBox'] = []
            if 'detectorAnchors' in x.completed.keys():
                for y in x.completed['detectorAnchors']:
                    tmp2 = {}
                    tmp2['dataset'] = datasetvsmodel.objects.get(id=y['dataset']).title
                    tmp2['model'] = infer_models.objects.get(id=y['model']).model
                    if 'value' not in y.keys():
                        continue
                    tmp2['value'] = y['value']
                    tmp['anchorBox'].append(tmp2)



            '''
            get MAP box values
            '''
            tmp['detectorMAP'] = []
            if 'detectorMAP' in x.completed.keys():
                for y in x.completed['detectorMAP']:
                    tmp2 = {}
                    tmp2['dataset'] = datasetvsmodel.objects.get(id=y['dataset']).title
                    tmp2['model'] = infer_models.objects.get(id=y['model']).model
                    if 'value' not in y.keys():
                         continue
                    tmp2['value'] = y['value']
                    tmp['detectorMAP'].append(tmp2)


            tmp['Infer'] = []
            if 'Infer&Save' in x.completed.keys():
                for y in x.completed['Infer&Save']:
                    tmp2 = {}
                    tmp2['dataset'] = datasetvsmodel.objects.get(id=y['dataset']).title
                    tmp2['model'] = infer_models.objects.get(id=y['model']).model
                    if 'value' not in y.keys():
                         continue
                    tmp2['value'] = y['value']
                    tmp['Infer'].append(tmp2)
            response.append(tmp)
        return Response(response)



class train_dates(APIView):
  
    def post(self,request):
        from datetime import date
        dataset_query = datasetvsmodel.objects.get(title=request.data['data_folder'])
        model_query   = infer_models.objects.get(model=request.data['model_folder'])
        instance_query = remote_instances.objects.get(public_ip=request.data['instance'])
        '''
        get instance  process 
        '''
        task_id = deeplearning_processes.objects.get(process=model_query.model_type,process_name='Train&Save').id
        check_process = remote_processes.objects.filter(instance_id = instance_query.id)
        if len(check_process) != 0:
           '''
           update process column
           '''
           current_processes= check_process[0].process
           if task_id not in current_processes:
               current_processes.append(task_id)
           '''
           update params
           '''
           params = check_process[0].params
           param_keys = params.keys()
           if 'Train&Save' not in param_keys:
               params['Train&Save'] = []
               tmp ={}
               tmp['model'] = model_query.id
               tmp['dataset'] = dataset_query.id
               params['Train&Save'].append(tmp)
           else:
               train_list = params['Train&Save']
               tmp ={}
               tmp['model'] = model_query.id
               tmp['dataset'] = dataset_query.id
               if tmp not in train_list:
                  train_list.append(tmp)
               params['Train&Save'] = train_list

           '''
           update completed params
           '''
           completed_params = check_process[0].completed
           param_keys = completed_params.keys()
           if 'Train&Save' not in param_keys:
               completed_params['Train&Save'] = []
               #tmp ={}
               #tmp['model'] = model_query.id
               #tmp['dataset'] = dataset_query.id
               #completed_params['Infer&Save'].append(tmp)
           else:
               tmp ={}
               tmp['model'] = model_query.id
               tmp['dataset'] = dataset_query.id
               if tmp not in completed_params['Train&Save']:
                   completed_params['Train&Save'].append(tmp)
           '''
           update db
           '''
           remote_processes.objects.filter(instance_id = instance_query.id).update(process = current_processes,params=params,completed=completed_params)
           return Response('process registered')
        else:
           return Response('create dataset and models first')

