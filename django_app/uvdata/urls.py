from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^projects/(?P<project_id>[0-9]+)/$', views.site_list, name='sites'),
    url(r'^projects/(?P<project_id>[0-9]+)/sites/(?P<site_id>[0-9]+)/$', views.site_details, name='siteview'),
    url(r'^projects/(?P<project_id>[0-9]+)/sites/(?P<site_id>[0-9]+)/download/$', views.downloads, name='downloads'),
    
]

