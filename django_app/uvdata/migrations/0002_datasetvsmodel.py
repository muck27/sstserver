# Generated by Django 2.2.3 on 2020-08-26 10:33

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uvdata', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='datasetvsmodel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=4096)),
                ('date_id', django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), size=None)),
                ('uid', models.IntegerField()),
                ('model_type', models.CharField(max_length=4096)),
                ('attributes', django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), null=True, size=None)),
            ],
        ),
    ]
