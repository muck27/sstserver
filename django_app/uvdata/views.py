# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.template import loader
from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from uvdata.models import projects, sites, images, box
from django.db.models import Count, Sum
from django.forms import modelformset_factory
from django import forms
from uvdata.forms import SearchForm

def index(request):
    projects_list = projects.objects.annotate(num_sites=Count('sites')).order_by('project_date').all()
    template = loader.get_template('uvdata/projects.html')
    context = {
	'project_list': projects_list,
    }
    return HttpResponse(template.render(context, request))
    #return HttpResponse("Hello, world. You're at the Starting page of the application.")



def site_list(request, project_id):

    form = SearchForm(project_id=project_id)
    #form = SearchForm()
    site_list = sites.objects.filter(project__pk=project_id).annotate(num_images=Count('images'))
    template = loader.get_template('uvdata/sites.html')
    
    if request.method == "POST":
        image_list=[]
        form=SearchForm(request.POST,project_id=project_id)
        site_name=request.POST.get('site')
        if form.is_valid():
            site_name=form.cleaned_data.get('site')
            project_name=projects.objects.get(id=project_id)
            print(project_name, site_name)
            with open("download.txt", "w") as f:
                for count,site in enumerate(site_name):
                    project_names=projects.objects.get(project_name=project_name)
                    a=project_names.sites_set.get(site_name=site)
                    b=list(a.images_set.all())
                    print(b)
                    for x,y  in enumerate(b):   
                        f.writelines(str(y))

    
    context = {
	    'site_list':site_list,
        'form': form,
    }
    return HttpResponse(template.render(context, request))

def downloads(request, project_id,site_id):
    images_list = images.objects.filter(site__pk=site_id).annotate(num_boxes = Count('box'))
    site_name=sites.objects.get(id=site_id)
    project_name=projects.objects.get(id=project_id)
    total_images=sites.objects.get(id=site_id).site_images
    image_filename="image_download.txt"
    json_filename="anno_download.txt"
    #print(filename.format(project_name,site_name))
    f= open("/home/clyde/Downloads/new/automatic_annotator_tool/queried_data/" + image_filename,"w+")
    g= open("/home/clyde/Downloads/new/automatic_annotator_tool/queried_data/"+json_filename,"w+")
    for x in images_list:
        f.write(x.image_url )
        g.write(x.json_url )
        f.write("\n")
        g.write("\n")
    f.close()    
    g.close()    

    template = loader.get_template('uvdata/downloads.html')
    context = {
	'image_list':images_list
    }
    return HttpResponse(template.render(context, request))


def site_details(request, project_id, site_id):
    images_list = images.objects.filter(site__pk=site_id).annotate(num_boxes = Count('box'))
    template = loader.get_template('uvdata/site_details.html')
    context = {
	'image_list':images_list
    }
    return HttpResponse(template.render(context, request))

