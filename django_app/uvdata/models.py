# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.postgres.fields import ArrayField, JSONField
from django.db import models

# Create your models here.


class classes(models.Model):
        class_name = models.CharField(max_length=1024)
        def __str__(self):
                return self.class_name

class projects(models.Model):
        project_name = models.CharField(max_length=1024)
        bucket = models.CharField(max_length=1024,blank=True)
        project_description = models.CharField(max_length=4096)
        def __str__(self):
                return self.project_name

class sites(models.Model):
        project = models.ForeignKey(projects, on_delete=models.CASCADE)
        site_name = models.CharField(max_length=2024)
        site_images = models.IntegerField()
        annotated_images = models.IntegerField()
        bucket = models.CharField(max_length=1024,blank=True)
        site_classes = models.ManyToManyField(classes) # have to think about how to add statistics
        alerts  = ArrayField(models.CharField(max_length = 1024, blank=True),null=True)
        def __str__(self):
                return self.site_name


class classifier(models.Model):
        attribute = models.CharField(max_length=2024)
        classifier = models.CharField(max_length=2024)
        site = models.ForeignKey(sites, on_delete=models.CASCADE)
#        def __str__(self):
#                return self.site



class dates(models.Model):
        project = models.ForeignKey(projects, on_delete=models.CASCADE)
        site = models.ForeignKey(sites, on_delete=models.CASCADE)
        date = models.DateField(auto_now_add=True)
        models = ArrayField(models.IntegerField(),null=True)
        def __str__(self):
                return self.date


class cam_id(models.Model):

       date = models.ForeignKey(dates, on_delete=models.CASCADE)
       cam_id = models.CharField(max_length=2024)
       angle = models.FloatField(blank=True)
       comments = ArrayField(ArrayField(models.CharField(max_length=4096)),null=True)       
       roi  = ArrayField(ArrayField(models.FloatField()),null=True)
       configured = models.BooleanField(default=False)
       configured_angle = models.FloatField(blank=True,default=0)
       def __str__(self):
                return self.cam_id

class images(models.Model):
        attributes = ArrayField(models.IntegerField(null=True))
        classes = ArrayField(models.IntegerField(null=True))
        infer_annotations  = models.BooleanField(default=True)
        site = models.ForeignKey(sites, on_delete=models.CASCADE)
        cam = models.ForeignKey(cam_id, on_delete=models.CASCADE)
        date = models.ForeignKey(dates, on_delete=models.CASCADE)
        filename=models.CharField(max_length=1024)
        human_or_not = models.BooleanField(default=True)
        image_url = models.CharField(max_length=4096)
        json_url = models.CharField(max_length=4096)
        time = models.CharField(max_length=1024)
        model_detections = models.IntegerField(null=True)
        human_detections = models.IntegerField(null=True)
        infer_detections = models.IntegerField(null=True)
        alerts =JSONField(blank=True, null=True)
        def __str__(self):
                return self.image_url


class bbox(models.Model):
        image = models.ForeignKey(images, on_delete=models.CASCADE)
        date = models.ForeignKey(dates, on_delete=models.CASCADE)
        cam = models.ForeignKey(cam_id, on_delete=models.CASCADE)
        x = models.FloatField()
        y = models.FloatField()
        w = models.FloatField()
        h = models.FloatField()
        box_type = models.CharField(max_length = 4096)        
        box_class = models.CharField(max_length=4096)
        height = models.FloatField()
        width = models.FloatField()
        date_created = models.DateField(auto_now_add=True)
        conf  = models.FloatField()
        site = models.ForeignKey(sites, on_delete = models.CASCADE)
        session = models.BooleanField(default=False) 
        port = models.IntegerField(null=True)
        completed = models.BooleanField(default=False)
        classifier =JSONField()
        filename = models.CharField(max_length=1024)
        task_id = models.IntegerField(null=True)
        frame_id = models.IntegerField(null=True)
        annotator = models.CharField(max_length=4096,null=True)
        alerts =JSONField(blank=True, null=True)
        pose = ArrayField(ArrayField(models.IntegerField()),null=True)
        classifier_conf  = models.FloatField(null=True)

class infer_bbox(models.Model):
        image = models.ForeignKey(images, on_delete=models.CASCADE)
        date = models.ForeignKey(dates, on_delete=models.CASCADE)
        cam = models.ForeignKey(cam_id, on_delete=models.CASCADE)
        x = models.FloatField()
        y = models.FloatField()
        w = models.FloatField()
        h = models.FloatField()
        box_type = models.CharField(max_length = 4096)
        box_class = models.CharField(max_length=4096)
        height = models.FloatField()
        width = models.FloatField()
        date_created = models.DateField(auto_now_add=True)
        conf  = models.FloatField()
        site = models.ForeignKey(sites, on_delete = models.CASCADE)
        session = models.BooleanField(default=False)
        port = models.IntegerField(null=True)
        completed = models.BooleanField(default=False)
        classifier =JSONField()
        filename = models.CharField(max_length=1024)
        task_id = models.IntegerField(null=True)
        frame_id = models.IntegerField(null=True)
        annotator = models.CharField(max_length=4096,null=True)
        alerts =JSONField(blank=True, null=True)
        pose = ArrayField(ArrayField(models.IntegerField()),null=True)
        classifier_conf  = models.FloatField(null=True)


class infer_models(models.Model):
        model = models.CharField(max_length=4096)
        detector = ArrayField(models.IntegerField(),null=True)
        classifier = ArrayField(models.IntegerField(),null=True)
        model_type = models.CharField(max_length=4096)
        config=JSONField(blank=True, null=True)       


class box(models.Model):
        classes = models.ForeignKey(classes, on_delete=models.CASCADE)
        image = models.ForeignKey(images, on_delete=models.CASCADE)
        bbox  = models.ForeignKey(bbox, on_delete=models.CASCADE)
        bbox_type = models.CharField(max_length=4096)        

class darknet_models(models.Model):
        project = models.CharField(max_length=4096)
        model = models.CharField(max_length=4096)
        directory = models.CharField(max_length=4096)
        image_id = models.CharField(max_length=4096)


class train_detector_containers(models.Model):
        port           = models.IntegerField()
        job            = models.CharField(max_length=4096)
        total_images   = models.IntegerField()
        container_name = models.CharField(max_length=4096)
        model          = models.CharField(max_length=4096,null=True)

class darknet_classes(models.Model):
        project = models.CharField(max_length=4096)
        class_name = models.CharField(max_length=4096)
        class_id = models.IntegerField()
        total_classes = models.IntegerField()

class infer_train(models.Model):
        infer_name=models.CharField(max_length=4096)
        train_name=models.CharField(max_length=4096)

class train_progress(models.Model):
        iter = models.IntegerField()
        loss = models.FloatField()
        avg_loss = models.FloatField()
        lr = models.FloatField()
        port   = models.IntegerField()

class ann_tasks(models.Model):
        port = models.IntegerField()
        filename = models.CharField(max_length=4096)
        cam_id = models.CharField(max_length=4096)
        ann_progress = models.BooleanField(default=False) 

class cvat(models.Model):
        port = models.IntegerField()
        job = models.CharField(max_length=4096)
        task = models.CharField(max_length=4096,null=True)
        predictions = models.CharField(max_length=4096)
        filters =JSONField(blank=True, null=True)
        annotators = ArrayField(models.CharField(max_length=4096,default=None),null=True,blank=True)

class login(models.Model):
        user = models.CharField(max_length=4096)
        password = models.CharField(max_length=4096)
        user_type = models.CharField(max_length=4096)




class remote_instances(models.Model):
        public_ip  = models.CharField(max_length=4096)
        free_space = models.IntegerField()
        gpu_availability = models.BooleanField(default=False)
        gpu_memory = models.IntegerField(null=True)
        user  = models.CharField(max_length=4096,null=True)
        ping  = models.TimeField(auto_now=False, auto_now_add=False,null=True)  
        parent_dir = models.CharField(max_length=8000,null=True)

class dataset_model(models.Model):
       title = models.CharField(max_length=4096)
       date_id = ArrayField(models.IntegerField())
       uid  = models.IntegerField()
       model_type =  models.CharField(max_length=4096)
       attributes = ArrayField(models.IntegerField(),null=True)

class datasetvsmodel(models.Model):
       title = models.CharField(max_length=4096)
       date_id = ArrayField(models.IntegerField())
       uid  = models.IntegerField()
       model_type =  models.CharField(max_length=4096, null=True)
       attributes = ArrayField(models.IntegerField(),null=True)
       split  = models.FloatField(blank=True,null=True)


class deeplearning_processes(models.Model):
       process =  models.CharField(max_length=4096)
       process_name =  models.CharField(max_length=4096)  

class remote_processes(models.Model):
       instance_id = models.IntegerField()
       process  =  ArrayField(models.IntegerField())
       params=JSONField(blank=True, null=True)       
       completed=JSONField(blank=True, null=True)       

      



