from django import forms
from uvdata.models import classes,projects,sites,images,box
from django.forms import ModelForm


class SearchForm(forms.Form):

    site = forms.ModelMultipleChoiceField(queryset=None)
 

    def __init__(self,*args,**kwargs):
        project_id = kwargs.pop("project_id")     
        super(SearchForm, self).__init__(*args,**kwargs)
        self.fields['site'].queryset = sites.objects.filter(project_id=project_id)

