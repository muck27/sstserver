# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin


from .models import classes, projects, sites, images, box,cam_id,bbox,dates
# Register your models here.


admin.site.register(classes)
admin.site.register(projects)
admin.site.register(sites)
admin.site.register(images)
admin.site.register(box)
admin.site.register(bbox)
admin.site.register(cam_id)
admin.site.register(dates)
