from __future__ import absolute_import, division, print_function
from datetime import datetime
import scipy
import imageio
from copy import deepcopy
import json
import glob
import os
import time
import itertools
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt 
import sys
user = "clyde"
import os.path
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
import subprocess
import numpy as np
from django.shortcuts import render
from django.http import HttpResponse
import django
django.setup()
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "uvlearn.settings")
sys.path.append('/home/'+user+'/Downloads/automatic_annotator_tool/django_app/')
from uvdata.models import projects, sites, images, box, classes,cam_id,bbox,dates,login,cvat
from django.views.generic import ListView, CreateView, UpdateView 
from django.forms import modelformset_factory
import os
import boto3
import uuid
from django.views.decorators.csrf import csrf_exempt
import json
import psycopg2
import sys
import shutil
import psycopg2
import json
import os
import os.path
from os import path
import zipfile
import glob
import botocore
from PIL import Image, ImageDraw
import datetime
import random
from shapely.geometry import Polygon
import time
from uvlearn import settings
from PIL import Image
from scripts.ssim.ssim import SSIM
from download_s3.models import projects_s3,sites_s3,dates_s3,images_s3,cams_s3,images_s3
from os import path
########API view
from rest_framework.pagination import PageNumberPagination
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
from django.core.exceptions import ObjectDoesNotExist
import glob
import cv2

'''
connect to s3 bucket 
'''
#client = boto3.client('s3',aws_access_key_id='AKIAQHGYCFENHDO6QSMH',aws_secret_access_key='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')
#paginator = client.get_paginator('list_objects')

#session = boto3.Session(
#    aws_access_key_id='AKIAQHGYCFENHDO6QSMH',
#    aws_secret_access_key='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',
#)
#s3 = session.resource('s3')



def dates_insert(date,p_id,s_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute("insert into uvdata_dates (date,project_id,site_id) values (%s,%s,%s)",(date,p_id,s_id))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()





def add_site_classes(site_id,class_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute("insert into uvdata_sites_site_classes (sites_id,classes_id) values (%s,%s)",(site_id,class_id))
        con.commit()
        print('site-classes complete')

    except (Exception, psycopg2.Error) as error:
        print("Key value exists in db ",error)
    finally:
        if (con):
            cur.close()
            con.close()


def check_site_class(site_id,class_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute("select * from uvdata_sites_site_classes where sites_id = %s and classes_id =%s",(site_id,class_id))
        get_val = cur.fetchall()
        return (get_val)
        print('site-classes complete')

    except (Exception, psycopg2.Error) as error:
        print("Key value exists in db ",error)
    finally:
        if (con):
            cur.close()
            con.close()


def Diff(li1, li2):
    return (list(set(li1) - set(li2)))


class localUpload(APIView):
    def get(self,request):
        base_dir = '/data/downloads/localData/'
        dirs = os.listdir(base_dir)
        response = []
        all_classes = classes.objects.all()
        class_list = []
        for x in all_classes:
            tmp = {}
            tmp['text'] = x.class_name
            tmp['value'] = x.class_name
            class_list.append(tmp)
        count = 1
        for x in dirs:
            if x != 'videoanalytics':
                tmp ={}
                tmp['text'] = x
                tmp['value'] = x
                tmp['sites'] =  []
                tmp['classes'] =  class_list
                for y in os.listdir(base_dir+'/'+x):
                    tmp2 = {}
                    tmp2['text'] = y
                    tmp2['value'] = y
                    tmp['sites'].append(tmp2)

                response.append(tmp)
                count = count + 1

        return Response(response)


class postLocalUpload(APIView):
    def post(self,request):
        
        p_name = request.data['project']
        s_name = request.data['site']
        file_type = request.data['type']
       
        if file_type == 'images+jsons':
            base_dir = '/data/downloads/localData/{}/{}'.format(p_name,s_name)
            dates_list = os.listdir(base_dir)

            '''
            update postgres db 
            '''

            '''
            update project db
            '''
            check_db_project = projects.objects.filter(bucket='localdatauv').filter(project_name = p_name).count()
            if check_db_project == 0 :
                update_projects = projects.objects.create(project_name = p_name, bucket= 'localdatauv',project_description = 'images+jsons')
                update_projects.save()

            '''
            update site db
            '''
            p_id = projects.objects.filter(bucket='localdatauv').get(project_name=p_name).id
            check_db_site = sites.objects.filter(bucket='localdatauv').filter(site_name = s_name).count()
            if check_db_site == 0:
                 update_sites = sites.objects.create(site_name= s_name, bucket='localdatauv',project_id = p_id,site_images = 1,annotated_images =0)
                 update_sites.save()

            s_id = sites.objects.filter(bucket='localdatauv').get(site_name=s_name).id

            '''
            update date db
            '''

            for d in dates_list:
                check_db_date = dates.objects.filter(project_id=p_id).filter(site_id =s_id).filter(date=d).count()
                if check_db_date == 0:
                    dates_insert(d,p_id,s_id)
   

            for d in dates_list:

                date_dir = base_dir + '/{}'.format(d)
                cams_list = os.listdir(date_dir)
                for c in cams_list:

                    '''
                    update camera db
                    '''
                    d_id = dates.objects.filter(site_id=s_id).get(date=d).id
                    check_db_camera  = cam_id.objects.filter(date_id=d_id).filter(cam_id = c).count()
                    if check_db_camera == 0:
                        update_db_camera = cam_id.objects.create(cam_id= c ,date_id = d_id,angle=0)
                        update_db_camera.save()


                    c_id = cam_id.objects.filter(cam_id = c).get(date_id = d_id).id

                    json_dir = date_dir+'/{}'.format(c)+'/*.json'
                    json_files = glob.glob(json_dir)
                    for f in json_files:
                        img_name = f.split('/')[8].replace('.json','.jpg')
                        print(f + ' is being uploaded')
                        with open(f) as g:
                            data = json.load(g)
                        height = data['height']
                        width  = data['width']
                        boxes  = data['bbox']
                        img_attrs = []
                        img_classes = []
          
                        for box in boxes:
                            '''
                            classes with attributes 
                            '''
                            if box['name'] == 'face':
                                class_name = 'face'
                                attr = {}   
                                for k,v in  box['attributes'].items():  
                                    attr[k] = list(v)[0]
                                check_attrs = attributes.objects.filter(site_id = s_id).filter(attribute=attr).count()
                                if check_attrs == 0:
                                    attr_upload  = attributes.objects.create(attribute = attr, site_id = s_id)
                                    attr_upload.save()
                                attr_id = attributes.objects.filter(site_id = s_id).get(attribute=attr).id
                                if attr_id  not in img_attrs :
                                    img_attrs.append(attr_id)

                            if box['name'] == 'Acc':
                                class_name = 'accessories'
                                attr = {}   
                                for k,v in  box['attributes'].items():  
                                    attr[k] = list(v)[0]
                                check_attrs = attributes.objects.filter(site_id = s_id).filter(attribute=attr).count()
                                if check_attrs == 0:
                                    attr_upload  = attributes.objects.create(attribute = attr, site_id = s_id)
                                    attr_upload.save()
                                attr_id = attributes.objects.filter(site_id = s_id).get(attribute=attr).id
                                if attr_id  not in img_attrs :
                                    img_attrs.append(attr_id)
 

                       
                            '''
                            classes without attributes
                            '''
                            if box['name'] == 'Person':
                                class_name = 'person'
 
                            if box['name'] == 'Head':
                                class_name = 'person' 


                            
                            '''
                            upload classes 
                            '''
                            check_class = classes.objects.filter(class_name = class_name).count()
                            if check_class == 0:
                               class_upload = classes.objects.create(class_name = class_name)
                               class_upload.save()


                            class_id = classes.objects.get(class_name = class_name).id
                            if class_id not in img_classes:
                                img_classes.append(class_id)


                            '''
                            sites and classes table
                            '''
                            site_class_count  =  check_site_class(s_id,class_id)
                            if len(site_class_count) ==  0: 
                                add_site_classes(s_id,class_id)



                        
                        '''
                        push image db
                        '''          
                        
                        check_image_db = images.objects.filter(cam_id=c_id).filter(filename=img_name).count()
                        if check_image_db == 0:
                            update_image_db = images.objects.create(
                                attributes = img_attrs,
                                classes = img_classes,
                                filename = img_name,
                                infer_annotations = False,
                                human_or_not = False,
                                image_url = 'https://localdatauv.s3.ap-south-1.amazonaws.com/{}/{}/{}/{}/{}'.format(p_name,s_name,d,c,img_name),
                                json_url = 'https://localdatauv.s3.ap-south-1.amazonaws.com/',
                                time = int(time.time()),
                                model_detections = len(boxes),
                                human_detections = 0,
                                infer_detections = 0,
                                alerts = {},
                                cam_id = c_id,
                                site_id = s_id,
                                date_id = d_id,
                            )

                            update_image_db.save()

                        img_id = images.objects.filter(cam_id=c_id).get(filename=img_name).id

                        for box in boxes:               
                            '''
                            upload bbox               
                            '''   
                            x = (box['coordinates']['xmin'])                              
                            y = (box['coordinates']['ymin'])                              
                            w = (box['coordinates']['xmax'] - box['coordinates']['xmin'])                              
                            h = (box['coordinates']['ymax'] - box['coordinates']['ymin'])               

                            if box['name'] == 'Person':
                                class_name = 'person'
                                attr = {}

                            if box['name'] == 'Head':
                                class_name = 'person'
                                attr = {}

                            if box['name'] == 'face':
                                class_name = 'face'
                                attr = {}
                                for k,v in  box['attributes'].items():
                                    attr[k] = list(v)[0]
                                check_attrs = attributes.objects.filter(site_id = s_id).filter(attribute=attr).count()
                                if check_attrs == 0:
                                    attr_upload  = attributes.objects.create(attribute = attr, site_id = s_id)
                                    attr_upload.save()
                                attr_id = attributes.objects.filter(site_id = s_id).get(attribute=attr).id



                            update_bbox_db = bbox.objects.create(
                                x=x , y=y, w=w, h=h,
                                box_type     = 'model',
                                box_class    = class_name,
                                height       = height,
                                width        = width,
                                date_created = d ,
                                conf         = 0.6,
                                session      = False,
                                completed    = False,
                                classifier   = attr,
                                site_id      = s_id,
                                date_id      = d_id,
                                cam_id       = c_id,
                                image_id     = img_id,
                                filename     = img_name

                            )

                            update_bbox_db.save()
  
                        print( f + 'has been uploaded')
                        '''
                        remove json 
                        '''                       
                        os.remove(f)          


        if file_type == 'video':
            '''
            take in default classes
            '''
           
            if p_name == 'ActionData':
                classes_list = ['person']
                classifier_list = [{"classifier":"action","attributes":["sitting","standing","bending","fallen_down","hand_waving","not_visible"]}]

            if p_name == 'IngramMicro':
                classes_list = ['person','cycle']
                classifier_list = []

            base_dir = '/data/downloads/localData/{}/{}'.format(p_name,s_name)

            p_query= projects.objects.filter(project_name=p_name).filter(bucket='localdatauv')
            s_query = sites.objects.filter(site_name = s_name).filter(bucket='localdatauv')
            if p_query:
                p_id = p_query[0].id
            if s_query:
                s_id = s_query[0].id 
            

                upload_dates  = []
                for d in dates.objects.filter(site_id = s_id):
                    upload_dates.append(str(d.date))
                 
                local_date_list = os.listdir(base_dir)
                dates_list = Diff(local_date_list,upload_dates)
           
            else:
                dates_list =  os.listdir(base_dir)


            print(dates_list)
            '''
            slice videos 
            '''
 
            for d in dates_list:
                date_dir = base_dir + '/{}'.format(d)
                cams_list = os.listdir(date_dir)
                for c in cams_list:
                    mp4_dir = date_dir+'/{}/'.format(c) + '*.mp4'
                    avi_dir = date_dir+'/{}/'.format(c) + '*.avi'
                    file_dir = date_dir + '/{}/'.format(c)
                    mp4_files = glob.glob(mp4_dir) 
                    avi_files = glob.glob(avi_dir) 
                    for f in mp4_files:
                        input_video = f
                        output_frame = f.split('.mp4')[0].split('/')[8]
                        splice_command = " ffmpeg -i {} -r 0.5 {}/{}_%06d.jpg".format(input_video,file_dir,output_frame)
                        os.system(splice_command)
                        os.remove(input_video)

                    for f in avi_files:
                        input_video = f
                        output_frame = f.split('.mp4')[0].split('/')[8]
                        splice_command = " ffmpeg -i {} -r 0.5 {}/{}_%06d.jpg".format(input_video,file_dir,output_frame)
                        os.system(splice_command)
                        os.remove(input_video)

                   
            '''
            update postgres db 
            '''
         
            '''
            update project db
            '''
            check_db_project = projects.objects.filter(bucket='localdatauv').filter(project_name = p_name).count()
            if check_db_project == 0 :
                update_projects = projects.objects.create(project_name = p_name, bucket= 'localdatauv',project_description = 'videos')
                update_projects.save()

            '''
            update site db
            ''' 
            p_id = projects.objects.filter(bucket='localdatauv').get(project_name=p_name).id
            check_db_site = sites.objects.filter(bucket='localdatauv').filter(site_name = s_name).count()
            if check_db_site == 0:
                 update_sites = sites.objects.create(site_name= s_name, bucket='localdatauv',project_id = p_id,site_images = 1,annotated_images =0,slerts=[])
                 update_sites.save()

            s_id = sites.objects.filter(bucket='localdatauv').get(site_name=s_name).id

            '''
            update classes 
            '''
            for class_name in classes_list:
                check_classes_db = classes.objects.filter(class_name = class_name).count()
                if check_classes_db == 0:
                    update_class_db = classes.objects.create(class_name = class_name)
                    update_class_db.save()
            
            for class_name in classes_list:                
                class_id = classes.objects.get(class_name = class_name).id
                check_site_classes = check_site_class(s_id,class_id)
                if len(check_site_classes) == 0:
                    add_site_classes(s_id,class_id)


            '''
            update classifier
            '''
          #  for x in classifier_list:
          #      classifier_ = x['classifier']
          #      attributes_ = x['attributes']
          #      for y in attributes_:
          #         check_attr = attributes.objects.filter(attribute= {classifier_:y})
          #         if check_attr:
          #             continue
          #         else:
          #             update_attr = attributes.objects.create(attribute={classifier_:y},site_id  = s_id)
          #             update_attr.save()

                       

            '''
            update date db
            '''
                        
            unsynced_dates = []
            for d in dates_list:
                check_db_date = dates.objects.filter(project_id=p_id).filter(site_id =s_id).filter(date=d).count()
                if check_db_date == 0:
                    unsynced_dates.append(d)
                    dates_insert(d,p_id,s_id)

            
            for d in unsynced_dates:
 

                    date_dir = base_dir + '/{}'.format(d)
                    cams_list = os.listdir(date_dir)
                    for c in cams_list:

                        '''
                        update camera db
                       # '''
                        d_id = dates.objects.filter(site_id =s_id).get(date=d).id
                        check_db_camera  = cam_id.objects.filter(date_id=d_id).filter(cam_id = c).count()
                        if check_db_camera == 0:
                            update_db_camera = cam_id.objects.create(cam_id= c ,date_id = d_id,angle=0)
                            update_db_camera.save()


                        c_id = cam_id.objects.filter(cam_id = c).get(date_id = d_id).id
                      
                        file_dir = date_dir+'/{}'.format(c)
                        files = os.listdir(file_dir)
                        
                        '''
                        get sample height and width from images
                        '''
                        sample_img = cv2.imread(file_dir+'/'+files[0])
                        height, width, channels = sample_img.shape
                        for f in files:

                            '''
                            update images in db
                            '''
                            check_image_db = images.objects.filter(cam_id=c_id).filter(filename=f).count()
                            if check_image_db == 0:
                                update_image_db = images.objects.create(
                                    attributes = [],
                                    classes = [],
                                    filename = f,
                                    infer_annotations = False,
                                    human_or_not = False,
                                    image_url = 'https://localdatauv.s3.ap-south-1.amazonaws.com/{}/{}/{}/{}/{}'.format(p_name,s_name,d,c,f),
                                    json_url = 'https://localdatauv.s3.ap-south-1.amazonaws.com/',
                                    time = int(time.time()),
                                    model_detections = 0,                                
                                    human_detections = 0,                                
                                    infer_detections = 0,  
                                    alerts = {},
                                    cam_id = c_id,
                                    site_id = s_id,
                                    date_id = d_id,
		                ) 
  
                                update_image_db.save()
           

                            i_id = images.objects.filter(cam_id = c_id).get(filename = f).id

                            '''
                            update_bbox 
                            '''
                            check_bbox_db = bbox.objects.filter(cam_id =c_id).filter(image_id = i_id).count()
                            if check_bbox_db == 0:
                                update_bbox_db = bbox.objects.create(
                                    x=0 , y=0, w=0, h=0,
                                    box_type     = 'model',
                                    box_class    = 'person', 
                                    height       =  height,
                                    width        =  width,
                                    date_created = d ,  
                                    conf         = 0.6,
                                    session      = False, 
                                    completed    = False, 
                                    classifier   = {}, 
                                    site_id      = s_id,
                                    date_id      = d_id,
                                    cam_id       = c_id,
                                    image_id     = i_id,
                                    filename     = f
 
                                ) 
                           
                                update_bbox_db.save()

                            print('{}/{} is pushed to db'.format(file_dir,f))
                    
 
                    local_path = '/data/downloads/localData/{}/{}/{}'.format(p_name,s_name,d)
                    s3_path = 's3://localdatauv/{}/{}/{}'.format(p_name,s_name,d)
                    upload_cmd = 'aws s3 cp --recursive {} {} --profile localdata'.format(local_path,s3_path)
                    print(upload_cmd)
                    os.system(upload_cmd)


              

        if file_type == 'images':
            '''
            take in default classes
            '''
            if p_name == 'ActionData':
                classes_list = ['person']
                classifier_list = [{"classifier":"action","attributes":["sitting","standing","bending","fallen_down","hand_waving","not_visible"]}]


            if p_name == 'IngramMicro':
                classes_list = ['person','cycle']
                classifier_list = []


            if p_name == 'localCattleya':
                classes_list = ['person','head']
                classifier_list = []


            base_dir = '/data/downloads/localData/{}/{}'.format(p_name,s_name)
            p_query= projects.objects.filter(project_name=p_name).filter(bucket='localdatauv')
            s_query = sites.objects.filter(site_name = s_name).filter(bucket='localdatauv')
            if p_query:
                p_id = p_query[0].id
            if s_query:
                s_id = s_query[0].id


                upload_dates  = []
                for d in dates.objects.filter(site_id = s_id):
                    upload_dates.append(str(d.date))

                local_date_list = os.listdir(base_dir)
                dates_list = Diff(local_date_list,upload_dates)

            else:
                dates_list =  os.listdir(base_dir)


                   
            '''
            update postgres db 
            '''
         
            '''
            update project db
            '''
            check_db_project = projects.objects.filter(bucket='localdatauv').filter(project_name = p_name).count()
            if check_db_project == 0 :
                update_projects = projects.objects.create(project_name = p_name, bucket= 'localdatauv',project_description = 'videos')
                update_projects.save()

            '''
            update site db
            ''' 
            p_id = projects.objects.filter(bucket='localdatauv').get(project_name=p_name).id
            check_db_site = sites.objects.filter(bucket='localdatauv').filter(site_name = s_name).count()
            if check_db_site == 0:
                 update_sites = sites.objects.create(site_name= s_name, bucket='localdatauv',project_id = p_id,site_images = 1,annotated_images =0)
                 update_sites.save()

            s_id = sites.objects.filter(bucket='localdatauv').get(site_name=s_name).id

            '''
            update classes 
            '''
            for class_name in classes_list:
                check_classes_db = classes.objects.filter(class_name = class_name).count()
                if check_classes_db == 0:
                    update_class_db = classes.objects.create(class_name = class_name)
                    update_class_db.save()

            for class_name in classes_list:
                class_id = classes.objects.get(class_name = class_name).id
                check_site_classes = check_site_class(s_id,class_id)
                if len(check_site_classes) == 0:
                    add_site_classes(s_id,class_id)




            '''
            update classifier
            '''
            for x in classifier_list:
                classifier_ = x['classifier']
                attributes_ = x['attributes']
                for y in attributes_:
                   check_attr = attributes.objects.filter(attribute= {classifier_:y})
                   if check_attr:
                       continue
                   else:
                       update_attr = attributes.objects.create(attribute={classifier_:y},site_id  = s_id)
                       update_attr.save()


            '''
            update date db
            '''
            unsynced_dates = []
            for d in dates_list:
                check_db_date = dates.objects.filter(project_id=p_id).filter(site_id =s_id).filter(date=d).count()
                if check_db_date == 0:
                    unsynced_dates.append(d)
                    dates_insert(d,p_id,s_id)

            
            for d in unsynced_dates:
 

                    date_dir = base_dir + '/{}'.format(d)
                    cams_list = os.listdir(date_dir)
                    for c in cams_list:

                        '''
                        update camera db
                        '''
                        d_id = dates.objects.filter(site_id=s_id).get(date=d).id
                        check_db_camera  = cam_id.objects.filter(date_id=d_id).filter(cam_id = c).count()
                        if check_db_camera == 0:
                            update_db_camera = cam_id.objects.create(cam_id= c ,date_id = d_id,angle=0)
                            update_db_camera.save()


                        c_id = cam_id.objects.filter(cam_id = c).get(date_id = d_id).id
                      
                        file_dir = date_dir+'/{}'.format(c)
                        files = os.listdir(file_dir)
                        
                        for f in files:

                            '''
                            get height and width from images
                            '''

                            sample_img = cv2.imread(file_dir+'/'+f)
                            height, width, channels = sample_img.shape

                            '''
                            update images in db
                            '''
                            check_image_db = images.objects.filter(cam_id=c_id).filter(filename=f).count()
                            if check_image_db == 0:
                                update_image_db = images.objects.create(
                                    attributes = [],
                                    classes = [],
                                    filename = f,
                                    infer_annotations = False,
                                    human_or_not = False,
                                    image_url = 'https://localdatauv.s3.ap-south-1.amazonaws.com/{}/{}/{}/{}/{}'.format(p_name,s_name,d,c,f),
                                    json_url = 'https://localdatauv.s3.ap-south-1.amazonaws.com/',
                                    time = int(time.time()),
                                    model_detections = 0,                                
                                    human_detections = 0,                                
                                    infer_detections = 0,  
                                    alerts = {},
                                    cam_id = c_id,
                                    site_id = s_id,
                                    date_id = d_id,
		                ) 
  
                                update_image_db.save()
           

                            i_id = images.objects.filter(cam_id = c_id).get(filename = f).id

                            '''
                            update_bbox 
                            '''
                            check_bbox_db = bbox.objects.filter(cam_id =c_id).filter(image_id = i_id).count()
                            if check_bbox_db == 0:
                                update_bbox_db = bbox.objects.create(
                                    x=0 , y=0, w=0, h=0,
                                    box_type     = 'model',
                                    box_class    = 'person', 
                                    height       =  height,
                                    width        =  width,
                                    date_created = d ,  
                                    conf         = 0,
                                    session      = False, 
                                    completed    = False, 
                                    classifier   = {}, 
                                    site_id      = s_id,
                                    date_id      = d_id,
                                    cam_id       = c_id,
                                    image_id     = i_id,
                                    filename     = f
 
                                ) 
                           
                                update_bbox_db.save()

                            print('{}/{} is pushed to db'.format(file_dir,f))

                    local_path = '/data/downloads/localData/{}/{}/{}'.format(p_name,s_name,d)
                    s3_path = 's3://localdatauv/{}/{}/{}'.format(p_name,s_name,d)
                    upload_cmd = 'aws s3 cp --recursive {} {} --profile localdata --acl public-read'.format(local_path,s3_path)
                    print(upload_cmd)
                    os.system(upload_cmd)
                                

        return Response('posted successfully')

