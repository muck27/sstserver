from django.apps import AppConfig


class LocaldataConfig(AppConfig):
    name = 'localData'
