import sys
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "uvlearn.settings")
os.system("export DJANGO_SETTINGS_MODULE=uvlearn.settings")
user="clyde"
sys.path.append('/home/'+user+'/Downloads/automatic_annotator_tool/django_app/')
import django
django.setup()
import psycopg2
import json
import glob
import shutil
from download_s3.models import projects_s3,sites_s3,dates_s3,images_s3,cams_s3,images_s3,sync_projects,sync_sites
import boto3
import botocore
client = boto3.client('s3')
paginator = client.get_paginator('list_objects')
from os import walk
from scripts.send_psql import upload_anpr_psql,upload_atm_psql

################
bucket_name = "uploads.live.uncannysurveillance.com"
base_folder = "/data/downloads/"
###############

sync_projects = sync_projects.objects.all()
for p in sync_projects:
    s_list = sites_s3.objects.filter(project_id=p.project_id)
    for s in s_list:
        d_list = dates_s3.objects.filter(site_id = s.id)
        for d in d_list:
            c_list = cams_s3.objects.filter(date_id=d.id)
            for c in c_list:
                p_name = str(p.project)
                if " " in p_name:
                    p_name = p_name.replace(" ","\ ")
                if "(" in p_name:
                    p_name = p_name.replace("(","\(")
                if ")" in p_name:
                    p_name = p_name.replace(")","\)")
         
                s_name = str(s.site)
                if " " in s_name:
                    s_name = s_name.replace(" ","\ ")
                if "(" in s_name:
                    s_name = s_name.replace("(","\(")
                if ")" in p_name:
                    s_name = s_name.replace(")","\)")

                d_name = d.date
                c_name = c.cam_id
                ann_dir   ="mkdir {}{}/{}/{}/{}/annotations".format(base_folder,p_name,s_name,d_name,c_name)
                os.system(ann_dir)

                aws_dir = "uploads.live.uncannysurveillance.com/{}/{}/{}/{}".format(p_name,s_name,d_name,c_name)
                download_dir = "{}{}/{}/{}/{}".format(base_folder,p_name,s_name,d_name,c_name)
                command = "aws s3 sync s3://{} {} --exclude '*.json' ".format(aws_dir, download_dir)
                os.system(command)

                ann_aws_dir = "uploads.live.uncannysurveillance.com/{}/{}/{}/{}".format(p_name,s_name,d_name,c_name)
                ann_download_dir = "{}{}/{}/{}/{}/annotations".format(base_folder,p_name,s_name,d_name,c_name)
                ann_command = "aws s3 sync s3://{} {} --exclude '*.jpg'".format(ann_aws_dir, ann_download_dir)
                print(ann_command)
                os.system(ann_command)

                for (dirpath, dirnames, filenames) in walk(ann_download_dir):
                    for x in filenames:
                        upload_atm_psql(ann_download_dir+'/'+x)

                shutil.rmtree(ann_download_dir)
print("projects have been synced on server")




sync_sites = sync_sites.objects.all()
for s in sync_sites:
    p_id = sites_s3.objects.get(id=s.site_id).project_id
    d_list = dates_s3.objects.filter(site_id=s.site_id)
    for d in d_list:
        c_list = cams_s3.objects.filter(date_id=d.id)
        for c in c_list:
            p_name = str(projects_s3.objects.get(id=p_id).project)
            if " " in p_name:
                p_name = p_name.replace(" ","\ ")
            if "(" in p_name:
                p_name = p_name.replace("(","\(")
            if ")" in p_name:
                p_name = p_name.replace(")","\)")
     
            s_name = str(s.site)
            if " " in s_name:
                s_name = s_name.replace(" ","\ ")
            if "(" in s_name:
                s_name = s_name.replace("(","\(")
            if ")" in p_name:
                s_name = s_name.replace(")","\)")

            d_name = d.date
            c_name = c.cam_id
            ann_dir   ="mkdir {}{}/{}/{}/{}/annotations".format(base_folder,p_name,s_name,d_name,c_name)
            os.system(ann_dir)

            jpg_aws_dir = "uploads.live.uncannysurveillance.com/{}/{}/{}/{}".format(p_name,s_name,d_name,c_name)
            jpg_download_dir = "{}{}/{}/{}/{}".format(base_folder,p_name,s_name,d_name,c_name)
            jpg_command = "aws s3 sync s3://{} {} --exclude '*.json'".format(jpg_aws_dir, jpg_download_dir)
            print(jpg_command)
            os.system(jpg_command)
            
            ann_aws_dir = "uploads.live.uncannysurveillance.com/{}/{}/{}/{}".format(p_name,s_name,d_name,c_name)
            ann_download_dir = "{}{}/{}/{}/{}/annotations".format(base_folder,p_name,s_name,d_name,c_name)
            ann_command = "aws s3 sync s3://{} {} --exclude '*.jpg'".format(ann_aws_dir, ann_download_dir)
            print(ann_command)
            os.system(ann_command)
           
            for (dirpath, dirnames, filenames) in walk(ann_download_dir):
                for x in filenames:
                    upload_atm_psql(ann_download_dir+'/'+x)
 
           shutil.rmtree(ann_download_dir)
                
print("sites have been synced")
