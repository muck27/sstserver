from django.shortcuts import render
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
import psycopg2
import os
import numpy as np
import zmq
import json
import subprocess
import docker
import os
import sys
import time
import json
import docker
import random
import shutil
from PIL import Image 
import cv2
import math
import random
import shutil
import docker
import zmq

train_ports = [4565,4564,4563,4562,4561]
other_ports = [
              {"train_port":4565,"anchor_port":4465,"map_port":4665},
              {"train_port":4564,"anchor_port":4464,"map_port":4664},
              {"train_port":4563,"anchor_port":4463,"map_port":4663},
              {"train_port":4562,"anchor_port":4462,"map_port":4662},
              {"train_port":4561,"anchor_port":4461,"map_port":4661}
	      ]


def update_container_info(port,job,total_images,container_name):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "insert into uvdata_train_detector_containers (port,job,total_images,container_name) values (%s,%s,%s,%s)"
        cur.execute(query,(port,job,total_images,container_name,))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not insert in train detector containers table",error)
    finally:
        if (con):
            cur.close()
            con.close()



def find_port():
    with open('classifier_ports.txt') as ports:
        port=json.load(ports)
    avail_ports = Diff(train_ports,port["closed_ports"])
    train_port = random.choice(avail_ports)
    port["closed_ports"].append(train_port)
    tmp ={}
    tmp["closed_ports"] = port["closed_ports"]
    with open('classifier_ports.txt','w') as ports:
        json.dump(tmp,ports)

    return(train_port)


def get_classifier(s_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select * from uvdata_attributes where site_id=%s"
        cur.execute(query,(str(s_id)))
        results = cur.fetchall()
        keys = [] 
        for x in results: 
            for k,v in x[1].items():
                if k not in keys:
                    keys.append(k)        
        return (keys)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_train_progress table",error)
    finally:
        if (con):
            cur.close()
            con.close()




def Diff(li1, li2):
    return (list(set(li1) - set(li2)))


class get_projects(APIView):

    def get(self,request):
        response=[]
        p_list= projects.objects.all()
        for p in p_list:
            tmp={}
            tmp["id"] = p.id
            tmp["project"] = p.project_name
            response.append(tmp)
        return Response(response)



class get_sites(APIView):

    def get(self,request,p_id):
        response=[]
        s_list= sites.objects.filter(project_id=p_id)
        for s in s_list:
            img_count = images.objects.filter(site_id=s.id)
            tmp={}
            tmp["id"] = s.id
            tmp["site"] = s.site_name
            tmp["total_images"] = img_count.count()
            tmp["annotated_images"] = img_count.filter(human_or_not=True).count()
            response.append(tmp)
        return Response(response)


class get_camids(APIView):

    def get(self,request,d_id):
        response=[]
        c_list= cam_id.objects.filter(date_id=d_id)
        for c in c_list:
            img_count = images.objects.filter(cam_id=c.id)
            tmp={}
            tmp["id"] = c.id
            tmp["cam_id"] = c.cam_id
            tmp["total_images"] = img_count.count()
            tmp["annotated_images"] = img_count.filter(human_or_not=True).count()
            response.append(tmp)
        return Response(response)


class get_dates(APIView):

    def get(self,request,s_id):
        response=[]
        d_list= dates.objects.filter(site_id=s_id)
        for d in d_list:
            img_count = images.objects.filter(date_id=d.id)
            tmp={}
            tmp["id"] = d.id
            tmp["date"] = d.date
            tmp["total_images"] = img_count.count()
            tmp["annotated_images"] = img_count.filter(human_or_not=True).count()
            response.append(tmp)
        return Response(response)


def get_classifiers(s_id,attr):
    from uvdata.models import attributes
    results = attributes.objects.filter(site_id=s_id).filter(attribute__has_key=attr)
    response = []
    for r in results:
        response.append(r.attribute)
    return (response)


def crop_n_save(path,img_dir,box,count):
    img = cv2.imread(path)
    crop_img = img[int(box[1]):int(box[1])+int(box[3]), int(box[0]):int(box[0])+int(box[2])]
    save_file = "{}/{}_{}.jpg".format(img_dir,path.split('/')[7].split('.')[0],count)
    cv2.imwrite(save_file,crop_img)




def train_test_split(source,train_dest,test_dest,val_dest,split,class_images):
 
    '''
    delete extra images
    '''
    count = 0
    for root, dirs, files in os.walk("{}".format(source)):
        for filename in files:
            count = count + 1
            if count > class_images:
                os.remove("{}{}".format(root,filename))


    '''
    create probability function based on split
    '''
    random_probs = []
    train_probs = int(math.floor(split*10))
    test_probs = int((10-int(math.ceil(split*10)))/2)
    val_probs = int((10-int(math.ceil(split*10)))/2)
    for x in range(train_probs):
        random_probs.append(1)
    for x in range(test_probs):
        random_probs.append(0)    
    for x in range(val_probs):
        random_probs.append(2)

    '''
    segregate images to train/test based on random choice 
    '''
    i_list = []
    for root, dirs, files in os.walk("{}".format(source)):
        for filename in files:
            i_list.append("{}{}".format(root,filename))
    
    for x in i_list:
        a = random.choice(random_probs)
        if a == 1:
            source = x
            dest   = train_dest
            shutil.move(source,dest)
        if a == 0:
            source = x
            dest   = test_dest
            shutil.move(source,dest)
        if a == 2:
            source = x
            dest   = val_dest
            shutil.move(source,dest)



def split_data(s_name,s_id,server_images,port,split,attr,total_images,class_images):
    base_dir = '/data/train_data/'
    classifiers = get_classifiers(s_id,attr)
    dirs = []
    count = 0
    tmp_attrs = []
    check_classifiers = []
    for x in classifiers:
        for k,v in x.items():
            if k == attr:
                if v not in tmp_attrs:
                    tmp_attrs.append(v) 
                    tmp = {}
                    tmp2 = {}
                    tmp["id"] = count 
                    tmp["classifier"] = {}
                    tmp["classifier"][k]=v
                    tmp2[k]=v
                    dirs.append(tmp)
                    check_classifiers.append(tmp2)
                    count = count + 1
                break
      
    '''
    create parent directory
    '''   
    root_dir = "{}{}@port{}".format(base_dir,s_name,port)
    os.system("mkdir {}".format(root_dir))    
    train_dir = root_dir + '/train/'
    test_dir  = root_dir + '/test/'
    val_dir  = root_dir + '/val/'
    os.system('mkdir {}'.format(train_dir))
    os.system('mkdir {}'.format(test_dir))
    os.system('mkdir {}'.format(val_dir))
   
    '''
    create classifier directories
    '''
    for x in dirs:
        dir_name = "{}/{}".format(root_dir,x["id"])
        os.system("mkdir {}".format(dir_name))


    '''
    crop and segregate data 
    '''
    
    for img in server_images:
        det_count = 0
        for box in img['bbox_n_attribute']:
            if box['attribute'] in check_classifiers:
                b = json.dumps(box['attribute'],sort_keys=True)
                for x in dirs:
                    a = json.dumps(x['classifier'], sort_keys=True)
                    if a == b:
                        img_dir = "{}/{}".format(root_dir,x["id"]) 
                        crop_n_save(img['path'],img_dir,box["bbox"],det_count)
                        det_count = det_count + 1

    '''
    delete extra images
    '''
 
    for dir_ in dirs:
        source = "{}/{}/".format(root_dir, dir_['id'])
        train_dest   = "{}{}/".format(train_dir,dir_['id'])
        test_dest    = "{}{}/".format(test_dir,dir_['id'])
        val_dest    = "{}{}/".format(val_dir,dir_['id'])
        os.system('mkdir {}'.format(train_dest))
        os.system('mkdir {}'.format(test_dest))
        os.system('mkdir {}'.format(val_dest))
        train_test_split(source,train_dest,test_dest,val_dest,split,class_images)
#
#    '''
#    augmentations for train images
#    '''
#    for dir_ in dirs:
#        source = "{}/{}/".format(train_dir, dir_['id'])
#        train_imgs= os.listdir(source)
#        print(len(train_imgs))
#
    for dir_ in dirs:
        source = "{}/{}/".format(root_dir, dir_['id'])
        shutil.rmtree(source)

    return (root_dir)


def create_data(request,port):
    from uvdata.models import bbox
    keys = request.keys()
    '''
    query db based on keys from UI
    '''
    if ('c_id' in keys):
        server_images = []
        d_id   = cam_id.objects.get(id=request['c_id']).date_id
        d_query= dates.objects.get(id=d_id)
        d_name = d_query.date
        s_id   = d_query.site_id
        p_id   = d_query.project_id
        s_name = sites.objects.get(id = s_id).site_name
        p_name = projects.objects.get(id=p_id).project_name
        c_name = cam_id.objects.get(id=request['c_id']).cam_id
        image_list = images.objects.filter(cam_id=request.data['c_id']).filter(human_or_not=True)
        for x in image_list:
            f_name = x.filename
            file_format = "/data/downloads/{}/{}/{}/{}/{}".format(p_name,s_name,d_name,c_name,f_name)
            tmp ={}
            tmp['path'] = file_format
            tmp['bbox_n_attribute'] = []
            bboxes = bbox.objects.filter(image_id=str(x.id)).filter(box_type='human').filter(classifier__has_key=request['attr'])
            if bboxes:
                for box in bboxes:
                    tmp2= {}
                    tmp2['bbox'] = [box.x,box.y,box.w,box.h]
                    check = box.classifier
                    for k,v in check.items():
                        if k == request['attr']:
                            tmp2['attribute'] = {}
                            tmp2['attribute'][request['attr']]= v
                    tmp['bbox_n_attribute'].append(tmp2)
            else:
                break

            server_images.append(tmp)

        job= "/{}/{}/{}/{}".format(p_name,s_name,d_name,c_name)

    if  ('d_id' in keys):
        server_images = []
        for d_id in request['d_id']:
            d_query= dates.objects.get(id=d_id)
            d_name = d_query.date
            s_id   = d_query.site_id
            p_id   = d_query.project_id
            s_name = sites.objects.get(id=s_id).site_name
            p_name = projects.objects.get(id=p_id).project_name
            image_list = images.objects.filter(date_id=d_id).filter(human_or_not=True)
            for x in image_list:
                c_id = x.cam_id
                c_name = cam_id.objects.get(id=c_id).cam_id
                f_name = x.filename
                file_format = "/data/downloads/{}/{}/{}/{}/{}".format(p_name,s_name,d_name,c_name,f_name)
                tmp ={}
                tmp['path'] = file_format
                tmp['bbox_n_attribute'] = []
                bboxes = bbox.objects.filter(image_id=str(x.id)).filter(box_type='human').filter(classifier__has_key=request['attr'])
                if bboxes:
                    for box in bboxes:
                        tmp2= {}
                        tmp2['bbox'] = [box.x,box.y,box.w,box.h]
                        check = box.classifier
                        for k,v in check.items():
                            if k == request['attr']:
                                tmp2['attribute'] = {}
                                tmp2['attribute'][request['attr']]= v
                        tmp['bbox_n_attribute'].append(tmp2)
                else:
                    break

                server_images.append(tmp)

        job= "/{}/{}/{}".format(p_name,s_name,str(d_name))

    if  ('s_id' in keys) :
        server_images = []
        s_query = sites.objects.get(id=request['s_id'])
        p_id    = s_query.project_id
        p_name  = projects.objects.get(id=p_id).project_name
        s_name  = sites.objects.get(id=request['s_id']).site_name
        s_id    = request['s_id']
        image_list = images.objects.filter(site_id=request['s_id']).filter(human_or_not=True)
        for x in image_list:
            d_id =  x.date_id
            c_id =  x.cam_id
            d_name = dates.objects.get(id=d_id).date
            c_name = cam_id.objects.get(id=c_id).cam_id
            f_name = x.filename
            file_format = "/data/downloads/{}/{}/{}/{}/{}".format(p_name,s_name,d_name,c_name,f_name)
            tmp ={}
            tmp['path'] = file_format
            tmp['bbox_n_attribute'] = []
            bboxes = bbox.objects.filter(image_id=str(x.id)).filter(box_type='human').filter(classifier__has_key=request['attr'])
            if bboxes:
                tmp['height'] = bboxes[0].height
                tmp['width'] = bboxes[0].width
                for box in bboxes:
                    tmp2= {}
                    tmp2['bbox'] = [box.x,box.y,box.w,box.h]
                    check = box.classifier
                    for k,v in check.items():
                        if k == request['attr']:
                            tmp2['attribute'] = {}
                            tmp2['attribute'][request['attr']]= v
                    tmp['bbox_n_attribute'].append(tmp2)
            else:
                break
 
            server_images.append(tmp)
        job= "/{}/{}".format(p_name,s_name)

    '''
    update containers for future reference
    '''
    container_name = "train_classifier_{}_{}".format(s_name,port)
   # update_container_info(port,job,len(server_images),container_name)
    '''
    send for data for split and annotations
    '''
    os.mkdir('/data/train_data/{}@port{}'.format(s_name,port))
    for x in server_images:
        img_path = x['path']
        attributes = x['bbox_n_attribute']
        '''
        copy img
        '''
        shutil.copyfile(img_path,'/data/train_data/{}@port{}/{}'.format(s_name,port,img_path.split('/')[7]))
        
        '''
        create json
        '''

        annotations = {}
        annotations['person'] = []
        annotations['height'] = x['height']
        annotations['width'] = x['width']
        for y in attributes:
            tmp = {}
            tmp['attribute'] = y['attribute']
            tmp['coordinates'] = y['bbox']
            annotations['person'].append(tmp)
        
        with open('/data/train_data/{}@port{}/{}.json'.format(s_name,port,img_path.split('/')[7].split('.jpg')[0]), 'w') as outfile:
            json.dump(annotations, outfile)
        print(annotations)
   # root_dir = split_data(s_name,s_id,server_images,port,request['split'],request['attr'],request['total_images'],request['class_images'])
   # return(root_dir)





class classifier_attributes(APIView):
    def get(self,request,s_id):
        classifiers = get_classifier(s_id)
        return Response(classifiers)

 

class augmentation(APIView):

    def post(self,request):
        '''
        1. Show Augmentation Filters
        2. Show Total bboxes
        '''
        tmp = {}
        tmp["filters"] = ['crop','resize','noise']
        keys = request.data.keys()
        if 's_id' in keys:
            tmp["bbox_count"] = bbox.objects.filter(site_id = request.data['s_id']).filter(box_type='human').count()
        if 'd_id' in keys:
            tmp["bbox_count"] = bbox.objects.filter(date_id = request.data['d_id']).filter(box_type='human').count()
        if 'c_id' in keys:
            tmp["bbox_count"] = bbox.objects.filter(cam_id = request.data['c_id']).filter(box_type='human').count()
        return Response(tmp)



class get_date_split(APIView):
    def get(self,request,s_id,attr):
        date_structure = dates.objects.filter(site_id=int(s_id))
        response = []
        for d in date_structure:
            tmp = {}
            tmp["d_id"] = d.id
            tmp["total_boxes"] = bbox.objects.filter(date_id=d.id).filter(box_type='human').filter(classifier__has_key=attr).count()
            attr_keys = attributes.objects.filter(attribute__has_key=attr).filter(site_id=s_id)
            class_attrs = []
            for x in attr_keys:
                for k,v in x.attribute.items():
                    if k == attr:
                        if v in class_attrs:
                            break
                        else:
                            class_attrs.append(v)

            tmp["class_split"]=[]
            for x in class_attrs:
                tmp2 = {}
                a = bbox.objects.filter(date_id=d.id).filter(box_type='human')
                count = 0  
                for y in a:       
                    for k,v in y.classifier.items():
                        if v == x:
                            count = count + 1
                tmp2[x] = count
                tmp["class_split"].append(tmp2)  
            response.append(tmp)
                 
        return Response(response)



class create_dataset(APIView):
        
    def post(self,request):
        '''
        post takes data either s_id, d_id or c_id, split, attr , total_images, class_images 
        find training port
        '''
#        train_port = find_port()
        train_port = 4463
        '''
        creating dataset based on input parameters p_id,s_id,d_id and c_id 
        '''
        dataset = create_data(request.data,train_port)
       
        return Response("port is {} and data folder is {}".format(train_port,dataset))



class get_w_n_c(APIView):

   def get(self,request,frame,project):
       models = darknet_models.objects.filter(model=frame).get(project=project)
       response = {}
       w  = []
       c  = []
       dir_ = models.directory
       weight_dir = "{}/weights".format(dir_)
       config_dir = "{}/configs".format(dir_)
       for x in os.listdir(weight_dir):
           w.append("{}/{}".format(weight_dir,x))
       for x in os.listdir(config_dir):
           c.append("{}/{}".format(config_dir,x))
       response['w'] = w
       response['c'] = c
       return Response(response)


class train_module(APIView):
  
    def get(self,request):
        base_dir = "/data/train_data/"
        response_data = []
        for x in os.listdir(base_dir):
            response_data.append(x)
        return Response(response_data)


    def post(self,request): 
        cfg =  request.data['cfg']
        weight = request.data['weight']
        data_folder = request.data['folder']
        framework  = request.data['framework']
        port = data_folder.split('@port')[1]
        site = data_folder.split('@port')[0]
        
        image_dir   = "/data/train_data/{}".format(data_folder)
        docker_id = darknet_models.objects.filter(model=framework).get(project=site).image_id
        if cfg.split('/')[3] != weight.split('/')[3]:
            return Response('choose the correct weights and cfg files')

        '''
        setup zmq socket
        '''
        context = zmq.Context()
        socket = context.socket(zmq.PAIR)
        host_socket = "tcp://localhost:{}".format(str(port))
        socket.connect(host_socket)
                
        '''
        start docker 
        '''
        mount_model = "/data/train/{}".format(cfg.split('/')[3])
        os.system("echo 'admin@123' | sudo -S python scripts/start_classifier.py {} {} {} {} {} {}".format(image_dir,docker_id,mount_model,site,port,'train'))
        client = docker.from_env()
        container_name = "classifier_{}_train_{}".format(site,port)
        target_container = client.containers.get(container_id = container_name)
        install_zmq = "pip install pyzmq"
        log_cont = target_container.exec_run(cmd=install_zmq,stream=True, privileged=True)
        start_train = "python train_googlenet.py {}".format(port)
        log_cont = target_container.exec_run(cmd=start_train,stream=True, privileged=True)
        
        print('starting training docker')                    
        socket.send_string("start process and send reply")
        while True:
            try:
                message =socket.recv_json()
                print(message)
        
            except zmq.error.Again as e:
                print('test failed, answer timed out')
                socket.send_string("restart")
        
 
        return Response('training started')


