from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [

    path('projects/',views.get_projects.as_view()),
    path('sites/<int:p_id>',views.get_sites.as_view()),
    path('dates/<int:s_id>',views.get_dates.as_view()),
    path('classifier/<int:s_id>',views.classifier_attributes.as_view()),
    path('cam_ids/<int:d_id>',views.get_camids.as_view()),
    path('class_split/<int:s_id>/<str:attr>',views.get_date_split.as_view()),
    path('create_dataset',views.create_dataset.as_view()),
    path('w_n_c/<str:frame>/<str:project>',views.get_w_n_c.as_view()),
    path('augmentation',views.augmentation.as_view()),
    path('train_module',views.train_module.as_view()),
#    path('map_module',views.map_module.as_view()),
]


