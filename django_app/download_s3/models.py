from __future__ import unicode_literals

from django.db import models

# Create your models here.




class projects_s3(models.Model):
        project = models.CharField(max_length=1024)
        bucket  = models.CharField(max_length=1024, null=True)
        def __str__(self):
                return self.project


class sites_s3(models.Model):
        project = models.ForeignKey(projects_s3, on_delete=models.CASCADE)
        site = models.CharField(max_length=1024)
        bucket  = models.CharField(max_length=1024, null=True)
        def __str__(self):
                return self.site

class dates_s3(models.Model):
        project = models.ForeignKey(projects_s3, on_delete=models.CASCADE)
        site = models.ForeignKey(sites_s3, on_delete=models.CASCADE)
        date = models.CharField(max_length=2024)
        sync= models.BooleanField(default=False)
        bucket  = models.CharField(max_length=1024, null=True)
        contents = models.IntegerField(null=True)
        def __str__(self):
                return self.date

class cams_s3(models.Model):
        project = models.ForeignKey(projects_s3, on_delete=models.CASCADE)
        site = models.ForeignKey(sites_s3, on_delete=models.CASCADE)
        date = models.ForeignKey(dates_s3,on_delete=models.CASCADE)
        cam_id = models.CharField(max_length=2024)
        bucket  = models.CharField(max_length=1024, null=True)
        def __str__(self):
                return self.cam_id

class images_s3(models.Model):
        project = models.ForeignKey(projects_s3, on_delete=models.CASCADE)
        site = models.ForeignKey(sites_s3, on_delete=models.CASCADE)
        date = models.ForeignKey(dates_s3,on_delete=models.CASCADE)
        cam_id = models.ForeignKey(cams_s3,on_delete=models.CASCADE)
        image = models.CharField(max_length=2024)
        def __str__(self):
                return self.image


class sync_projects(models.Model):
        project = models.ForeignKey(projects_s3, on_delete=models.CASCADE)
 

class sync_sites(models.Model):
        site = models.ForeignKey(sites_s3, on_delete=models.CASCADE)


# Create your models here.
