import sys
import time
user = "clyde"
import os.path
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
import subprocess
import numpy as np
import os 
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "uvlearn.settings")
from django.shortcuts import render
from django.http import HttpResponse
import django
django.setup()
sys.path.append('/home/'+user+'/Downloads/automatic_annotator_tool/django_app/')
from django.views.generic import ListView, CreateView, UpdateView 
from .models import projects_s3, sites_s3,dates_s3,cams_s3 
from django.forms import modelformset_factory
import os
import boto3
import uuid
from django.views.decorators.csrf import csrf_exempt
import json
import psycopg2
import sys
import shutil
import psycopg2
import json
import os
import os.path
from os import path
import zipfile
import glob
import botocore
from PIL import Image, ImageDraw
import datetime
from uvdata.models import projects,sites,classes,images,cam_id,dates
from os import walk
from scripts.send_psql import upload_coko_psql ,upload_cattleya_psql,upload_syncom_psql,upload_athena_psql,upload_tcc_psql,upload_byte_psql
from datetime import datetime
#from scripts.send_psql_dev import upload_syscom_psql 
########API view

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse

########
#client = boto3.client('s3', aws_access_key_id='AKIAQHGYCFENLPX52B5L', aws_secret_access_key='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',)
#s3 = boto3.resource('s3')
import cv2
import imutils


######
bucket= 'uploads.live.uncannysurveillance.com'




#######


def get_all_projects():
    a=projects_s3.objects.all()
    project_data=[]
    for x in a:
        tmp={}
        tmp['id'] = x.id
        tmp['project'] = x.project
        project_data.append(tmp)
    return (project_data)

def get_sites(p_id):
    s_list = sites_s3.objects.filter(project_id=p_id)
    sites_data=[]
    for site in s_list:
        tmp={}
        tmp['id']=site.id
        tmp['site']=site.site
        sites_data.append(tmp)
    print(sites_data)
    return(sites_data)


def get_dates(p_id,s_id):
    d_list = dates_s3.objects.filter(project_id=p_id).filter(site_id=s_id)
    dates_data=[]
    for dates in d_list:
        tmp={}
        tmp['id']=dates.id
        tmp['dates']=dates.date
        dates_data.append(tmp)
    print(dates_data)
    return(dates_data)

def get_date(p_id,s_id,d_id):
    d_list =  dates_s3.objects.filter(project_id=p_id).filter(site_id=s_id).get(id= d_id)
    return(d_list.date)
    
   
class list_s3_projects(APIView):
    def get(self,request):
        response_data = {}
        functionality=[]
        tmp={}
        tmp['id']='copy'
        tmp['functionality']='copy'
        functionality.append(tmp)
        tmp={}
        tmp['id']='sync'
        tmp['functionality']='sync'
        functionality.append(tmp)
        project_data=get_all_projects()       
        response_data["project_data"] = project_data
        response_data["functionality"] = functionality
        return Response(response_data)



class download_s3_project(APIView):
    def get(self,request,p_id,control,folder):
        p_data = get_all_projects()
           
        if control=='copy':
            functionality ='copy'
        if control=='sync':
            functionality ='sync'
        create_dir = 'mkdir {}{}{}{}'.format(download_projects,functionality,'/',folder)
        os.system(create_dir)
        for x in p_data:
            if x['id'] == p_id:
                project = x['project'].replace(' ','\ ')
                if control=='copy':
                    command = 'aws s3 sync s3://{}/{}/ {}copy/{}/ --exclude "*.jpg"'.format(bucket,project,download_projects,folder)
                if control=='sync':
                    command = 'aws s3 sync s3://{}/{}/ {}sync/{}/ --exclude "*.jpg"  '.format(bucket,project,download_projects,folder)
                print(command)
                os.system(command)
        
        s_tree = os.listdir(projects_dir+functionality+'/'+folder)
        for s in s_tree:
            d_tree= os.listdir(projects_dir+functionality+'/'+folder+'/'+s)
            for d in d_tree:
                cam_id = os.listdir(projects_dir+functionality+'/'+folder+'/'+s+'/'+d)
                for id_ in cam_id:
                    files = os.listdir(projects_dir+functionality+'/'+folder+'/'+s+'/'+d+'/'+id_)
                    for image in files:
                        
                        upload_atm_psql(projects_dir+functionality+'/'+folder+'/'+s+'/'+d+'/'+id_+'/'+image)####upload data to psql
                    fileList = glob.glob(projects_dir+functionality+'/'+folder+'/'+s+'/'+d+'/'+id_ +'/*.json', recursive=True)
                    for x in fileList:
                        os.remove(x)
                    print("files have been uploaded to psql") 
        
        return Response(response_data)


############


class list_s3_dates(APIView):

    def get(self,request,p_id,s_id):
        response_data={}
        dates_data = get_dates(p_id,s_id)
        functionality = []
        tmp={}
        tmp['id']='copy'
        tmp['functionality']='copy'
        functionality.append(tmp)
        tmp={}
        tmp['id']='sync'
        tmp['functionality']='sync'
        response_data['functionality'] = functionality
        response_data['dates']= dates_data
        print(response_data)
        return Response(response_data)

class download_s3_date(APIView):

    def get(self,request,p_id,s_id,d_id,control,folder):
        p_name = projects_s3.objects.get(id=p_id).project
        s_name = sites_s3.objects.filter(project_id=p_id).get(id=s_id).site
        d_name = dates_s3.objects.get(id=d_id)
        functionality ='copy'
        create_dir = 'mkdir {}{}{}{}'.format(download_dates,functionality,'/',folder)
        os.system(create_dir)
        command = 'aws s3 sync s3://{}/{}/{}/{} {}copy/{}/ --exclude "*.jpg"'.format(bucket,p_name,s_name,d_name,download_dates,folder)
        print(command)
        os.system(command)
        cam_ids = os.listdir(dates_dir+functionality+'/'+folder)
        for id_ in cam_ids:
            if id_ != 'file_description.list':
                files = os.listdir(dates_dir +functionality+'/'+ folder + '/' + id_)
                for image in files:
                    upload_atm_psql(dates_dir+functionality+'/'+folder+'/'+id_ +'/'+image)####upload data to psql
                fileList = glob.glob(dates_dir+functionality+'/'+folder+'/'+id_+'/*.json', recursive=True)
                for x in fileList:
                     os.remove(x)
                print("files have been uploaded to psql")

        return Response('files have been uploaded to psql')

################
class list_s3_sites(APIView):
    def get(self,request,p_id):
        response_data={}
        sites_data = get_sites(p_id)
        functionality = []
        tmp={}
        tmp['id']='copy'
        tmp['functionality']='copy'
        functionality.append(tmp)
        tmp={}
        tmp['id']='sync'
        tmp['functionality']='sync'
        response_data['functionality'] = functionality
        response_data['sites_data'] = sites_data
        print(response_data)
        return Response(response_data)
                                   

class download_s3_site(APIView):

    def get(self,request,p_id,s_id,control,folder):
        p_data = get_all_projects()
        for x in p_data:
            if x['id'] == p_id:
                project = x['project'].replace(' ','\ ')
                s_data= get_sites(p_id)
                if control=='copy':
                    functionality ='copy'
                if control=='sync':
                    functionality ='sync'

                create_dir = 'mkdir {}{}{}{}'.format(download_sites,functionality,'/',folder)
                os.system(create_dir)
                for s in s_data:
                    if s['id'] == s_id:
                        site= s['site'].replace(' ','\ ')
                        if control=='copy':
                            command = 'aws s3 sync s3://{}/{}/{}/ {}copy/{}/ --exclude "*.jpg"'.format(bucket,project,site,download_sites,folder)
                        if control=='sync':
                            command = 'aws s3 sync s3://{}/{}/{}/ {}sync/{}/ --exclude "*.jpg"'.format(bucket,project,site,download_sites,folder)
                        print(command)
                        os.system(command)
                         
                        

                s_tree = os.listdir(sites_dir+functionality+'/'+folder+'/')
                for s in s_tree:
                    if s != 'file_description.list' and s!='prev_files.txt':
                        cam_ids= os.listdir(sites_dir+functionality+'/'+folder+'/'+s)
                        for id_ in cam_ids:
                            files = os.listdir(sites_dir + functionality+'/'+folder + '/' + s+'/'+id_)
                            for image in files:
                                upload_atm_psql(sites_dir+functionality+'/'+folder+'/'+ s +'/'+ id_ +'/'+image)####upload data to psql
                                       
                            fileList = glob.glob(sites_dir+functionality+'/'+folder+'/'+ s + '/'+id_+'/*.json', recursive=True)
                            for x in fileList:
                                os.remove(x)
                            print("files have been uploaded to psql")
               
        return Response(response_data)

#######################

class list_s3_cams(APIView):
    def get(self,request,p_id,s_id,d_id):
        response_data=[]
        p_name = projects_s3.objects.get(id=p_id).project
        np_id = projects.objects.get(project_name=p_name).id
        s_name = sites_s3.objects.filter(project_id = p_id).get(id=s_id).site
        ns_id = sites.objects.filter(project_id = np_id).get(site_name=s_name).id
        cam_ids = cam_id.objects.filter(site_id=ns_id)
        print(cam_ids)
        for x in cam_ids:
            tmp={}
            tmp["id"] = x.id
            tmp["cam"] = x.cam_id
            response_data.append(tmp)      
        return Response(response_data)


class download_s3_cams(APIView):

    def get(self,request,p_id,s_id,d_id,c_id,control,folder):
        p_name = projects_s3.objects.get(id=p_id).project
        s_name = sites_s3.objects.filter(project_id=p_id).get(id=s_id).site
        d_name = dates_s3.objects.get(id=d_id)
        c_name = cam_id.objects.get(id=c_id).cam_id
        functionality ='copy'
        create_dir = 'mkdir {}{}{}{}'.format(download_cams,functionality,'/',folder)
        os.system(create_dir)
        command = 'aws s3 sync s3://{}/{}/{}/{}/{} {}copy/{}/ --exclude "*.jpg"'.format(bucket,p_name,s_name,d_name,c_name,download_cams,folder)
        print(command)
        os.system(command)
        img_list = os.listdir(download_cams+functionality+'/'+folder)
        for id_ in img_list:
            upload_atm_psql(download_cams+functionality+'/'+folder+'/'+id_)####upload data to psql
        fileList = glob.glob(download_cams+functionality+'/'+folder+'/'+id_+'/*.json', recursive=True)
        for x in fileList:
            os.remove(x)
        print("files have been uploaded to psql")

        return Response(dates)


    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select site_id from download_s3_sync_sites"
        cur.execute(query)
        model_records = cur.fetchall()
        s_ids = []
        for x in model_records:
            s_ids.append(x[0])
        #return(s_ids)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in projects_sync",error)
    finally:
        if (con):
            cur.close()
            con.close()

def add_p_2_db(id_):
   try:
       con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
       cur = con.cursor()
       query = "select project_id from download_s3_sync_projects where project_id = {}".format(id_)
       cur.execute(query)
       model_records = cur.fetchall()
       if len(model_records) == 0:
           cur.execute("insert into download_s3_sync_projects (project_id) values (%s)",(id_,))
           con.commit()
   except (Exception, psycopg2.Error) as error:
       print("could not insert in projects_sync",error)
   finally:
       if (con):
           cur.close()
           con.close()


def delete_p_2_db(id_):
   try:
       con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
       cur = con.cursor()
       query = "delete from download_s3_sync_projects where project_id = {}".format(id_)
       cur.execute(query)
       con.commit()
   except (Exception, psycopg2.Error) as error:
       print("could not insert in projects_sync",error)
   finally:
       if (con):
           cur.close()
           con.close()

def add_s_2_db(id_):
   try:
       con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
       cur = con.cursor()
       query = "select site_id from download_s3_sync_sites where site_id = {}".format(id_)
       cur.execute(query)
       model_records = cur.fetchall()
       if len(model_records) == 0:
           cur.execute("insert into download_s3_sync_sites (site_id) values (%s)",(id_,))
           con.commit()
   except (Exception, psycopg2.Error) as error:
       print("could not insert in projects_sync",error)
   finally:
       if (con):
           cur.close()
           con.close()


def delete_s_2_db(id_):
   try:
       con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
       cur = con.cursor()
       query = "delete from download_s3_sync_sites where site_id = {}".format(id_)
       cur.execute(query)
       con.commit()
   except (Exception, psycopg2.Error) as error:
       print("could not insert in projects_sync",error)
   finally:
       if (con):
           cur.close()
           con.close()


class sync_projects(APIView):

    def get(self,request):
        p_list = projects_s3.objects.all()
        response_data = []
        watchlist_projects = get_synced_projects()
        print(watchlist_projects)
        for p in p_list:
            tmp={}
            tmp["p_id"] = p.id
            if p.id in watchlist_projects:
                tmp["synced"] = True
            else:
                tmp["synced"] = False
            tmp["projects"] = p.project
            s_list = sites_s3.objects.filter(project_id = p.id)
            tmp["sites"] = []
            for s in s_list:
                tmp1={}
                tmp1["site"] = s.site
                tmp1["s_id"] = s.id
                tmp["sites"].append(tmp1)
            response_data.append(tmp)

        return Response(response_data)

    def post(self,request):
        id_ =request.data['p_id']
        function = request.data['function'] 
        if function ==1:
            add_p_2_db(id_)
            response = "project added" 
        if function == 0:
            delete_p_2_db(id_)
            response = "project deleted" 

        return Response(response)
        


class sync_sites(APIView):

    def get(self,request,p_id):
        s_list = sites_s3.objects.filter(project_id= p_id)
        response_data = []
        watchlist_sites = get_synced_sites()
        for s in s_list:
            tmp={}
            tmp["s_id"] = s.id
            if s.id in watchlist_sites:
                tmp["synced"] = True
            else:
                tmp["synced"] = False

            tmp["s_name"] = s.site
            response_data.append(tmp)
        return Response(response_data)

    def post(self,request,p_id):
        id_ =request.data['s_id']
        function = request.data['function']
        print('hello')
        if function ==1:
            add_s_2_db(id_)
            response = "site added"
        if function == 0:
            delete_s_2_db(id_)
            response = "site deleted"

        return Response(response)






########################
def get_synced_projects():
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select project_id from download_s3_sync_projects"
        cur.execute(query)
        model_records = cur.fetchall()
        p_ids = []
        for x in model_records:
            p_ids.append(x[0])
        return(p_ids)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in projects_sync",error)
    finally:
        if (con):
            cur.close()
            con.close()


def get_synced_sites():
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select site_id from download_s3_sync_sites"
        cur.execute(query)
        model_records = cur.fetchall()
        s_ids = []
        for x in model_records:
            s_ids.append(x[0])
        return(s_ids)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in projects_sync",error)
    finally:
        if (con):
            cur.close()
            con.close()

def update_projects_s3(p_id):
   try:
       con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
       cur = con.cursor()
       cur.execute("insert into download_s3_sync_projects (project_id) values (%s)",(p_id,))
       con.commit()
   except (Exception, psycopg2.Error) as error:
       print("could not insert into download_s3_sync_projects",error)
   finally:
       if (con):
           cur.close()
           con.close()


def update_sites_s3(s_id):
   try:
       con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
       cur = con.cursor()
       cur.execute("insert into download_s3_sync_sites (site_id) values (%s)",(s_id,))
       con.commit()
   except (Exception, psycopg2.Error) as error:
       print("could not insert in sites_sync",error)
   finally:
       if (con):
           cur.close()
           con.close()



def update_dates_s3(d_id):
   try:
       con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
       cur = con.cursor()
       cur.execute("update download_s3_dates_s3 set sync = %s where id=%s",(True,d_id))
       con.commit()
   except (Exception, psycopg2.Error) as error:
       print("could not insert in sites_sync",error)
   finally:
       if (con):
           cur.close()
           con.close()



def get_unique_date(p_id,s_id,d_name):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select * from uvdata_dates where date = %s and project_id=%s and site_id=%s"
        cur.execute(query,(d_name,p_id,s_id))
        model_records = cur.fetchone()
        return(model_records)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in projects_sync",error)
    finally:
        if (con):
            cur.close()
            con.close()


def sync_dates(d,base_dir,bucket_name):
    from PIL import Image
    base_folder = base_dir
    c_list = cams_s3.objects.filter(date_id=d.id).filter(bucket=bucket_name)
    p_id = d.project_id
    s_id = d.site_id
    p_name = str(projects_s3.objects.filter(bucket=bucket_name).get(id=p_id).project)
    s_name = str(sites_s3.objects.filter(bucket=bucket_name).get(id=s_id).site)
    d_name = d.date
    for c in c_list:
        
        if "," in p_name:
            p_name_aws = p_name.replace(',','\,')
            p_name_dir = p_name
        else:
            p_name_aws = p_name
            p_name_dir = p_name
      
        if " " in p_name_aws:
            p_name_aws = p_name_aws.replace(" ","\ ")
            p_name_dir = p_name_dir.replace(" ","_")
        
        if "(" in p_name_aws:
            p_name_aws = p_name_aws.replace("(","\(")
            p_name_dir = p_name_dir.replace("(","")

        if ")" in p_name_aws:
            p_name_aws = p_name_aws.replace(")","\)")
            p_name_dir = p_name_dir.replace(")","")



        if "," in s_name:
            s_name_aws = s_name.replace(',','\,')
            s_name_dir = s_name
        else:
            s_name_aws = s_name
            s_name_dir = s_name

        if " " in s_name_aws:
            s_name_aws = s_name_aws.replace(" ","\ ")
            s_name_dir = s_name_dir.replace(" ","_")

        if "(" in s_name_aws:
            s_name_aws = s_name_aws.replace("(","\(")
            s_name_dir = s_name_dir.replace("(","")

        if ")" in s_name_aws:
            s_name_aws = s_name_aws.replace(")","\)")
            s_name_dir = s_name_dir.replace(")","")



        c_name = c.cam_id
        if "," in c_name:
            c_name_aws = c_name.replace(',','\,')
            c_name_dir = c_name
        else:
            c_name_aws = c_name
            c_name_dir = c_name
     

        if "&" in c_name:
            c_name_aws = c_name_aws.replace("&","\&")
      #      c_name_dir = c_name_dir.replace("&","\&")

 
        if " " in c_name:
            c_name_aws = c_name_aws.replace(" ","\ ")
            c_name_dir = c_name_dir.replace(" ","_")

        if "(" in c_name:
            c_name_aws = c_name_aws.replace("(","\(")
            c_name_dir = c_name_dir.replace("(","")

        if ")" in c_name:
            c_name_aws = c_name_aws.replace(")","\)")
            c_name_aws = c_name_dir.replace(")","")


        ann_dir   ="{}{}/{}/{}/{}/annotations".format(base_folder,p_name_dir,s_name_dir,d_name,c_name_dir)
        if os.path.exists(ann_dir) == False:
            os.mkdir(ann_dir)

        ann_aws_dir = "{}/{}/{}/{}/{}".format(bucket_name,p_name_aws,s_name_aws,d_name,c_name_aws)
        ann_download_dir = "{}{}/{}/{}/{}/annotations".format(base_folder,p_name_dir,s_name_dir,d_name,c_name_dir)
        ann_command = "aws s3 sync s3://{} '{}' --exclude '*.jpg' ".format(ann_aws_dir, ann_download_dir)
        print(ann_command)
        os.system(ann_command)
        jpg_aws_dir = "{}/{}/{}/{}/{}".format(bucket_name,p_name_aws,s_name_aws,d_name,c_name_aws)
        jpg_download_dir = "{}{}/{}/{}/{}".format(base_folder,p_name_dir,s_name_dir,d_name,c_name_dir)
        jpg_command = "aws s3 sync s3://{} {} --exclude '*.json'".format(jpg_aws_dir, jpg_download_dir)
        print(jpg_command)
        print('images have been downloaded')
 
        for (dirpath, dirnames, filenames) in walk(ann_download_dir.replace('\\','')):
            for x in filenames:
                if p_name=="Cattleya":
                    upload_cattleya_psql(ann_download_dir.replace('\\','')+'/'+x,d_name,bucket_name)
                if p_name == 'SynCom':
                    upload_syncom_psql(ann_download_dir.replace('\\','')+'/'+x,d_name,bucket_name)
                if p_name == 'City of Kitchener, Ontario':
                    print('hello')
                    upload_coko_psql(ann_download_dir.replace('\\','')+'/'+x,d_name,bucket_name)
                if p_name == 'Athena Security':
                    upload_athena_psql(ann_download_dir.replace('\\','')+'/'+x,d_name,bucket_name) 
                
                if p_name == 'ACG PAM Pharma Technologies':
                    upload_athena_psql(ann_download_dir.replace('\\','')+'/'+x,d_name,bucket_name)

                if p_name =='TCC Call Center':
                    upload_tcc_psql(ann_download_dir.replace('\\','')+'/'+x,d_name,bucket_name)

                if p_name == 'Zicom':
                    upload_athena_psql(ann_download_dir.replace('\\','')+'/'+x,d_name,bucket_name)

                if p_name == 'Bytewise solutions':
                    upload_coko_psql(ann_download_dir.replace('\\','')+'/'+x,d_name,bucket_name)

                if p_name =='City of Guelph':
                    upload_coko_psql(ann_download_dir.replace('\\','')+'/'+x,d_name,bucket_name)

        shutil.rmtree(ann_download_dir.replace('\\',''))
        if p_name == 'Cattleya':
            p_id = projects.objects.get(project_name=p_name).id
            s_id = sites.objects.get(site_name=s_name).id
            d_id = get_unique_date(p_id,s_id,d_name)
            angle = int(cam_id.objects.filter(date_id=d_id).get(cam_id=c_name).angle)*(-1)
            for (dirpath, dirnames, filenames) in walk(jpg_download_dir.replace('\\','')):
                for x in filenames:
                    image = cv2.imread(jpg_download_dir.replace('\\','')+'/'+x)
                    rotated = imutils.rotate_bound(image, angle)
                    os.remove(jpg_download_dir.replace('\\','')+'/'+x)
                    cv2.imwrite(jpg_download_dir.replace('\\','')+'/'+x, rotated) 
 
    return(1)


class view_projects(APIView):

    def get(self,request):
        response_data=[]
        buckets = projects_s3.objects.values('bucket').distinct()
        for b in buckets:
            p_list = projects_s3.objects.filter(bucket=b['bucket'])
            tmp={}
            tmp['bucket'] = b['bucket'] 
            tmp['info'] =[]
            watchlist_projects = get_synced_projects()
            for p in p_list:
                tmp2={}
                tmp2["p_id"] = p.id
                if p.id in watchlist_projects:
                    tmp2["synced"] = True
                else:
                    tmp2["synced"] = False

                tmp2["projects"] = p.project
                tmp['info'].append(tmp2)
            response_data.append(tmp)

        return Response(response_data)


class view_sites(APIView):

    def get(self,request,p_id):
        
        response_data=[]
        
        s_list = sites_s3.objects.filter(project_id=p_id)
        watchlist_sites = get_synced_sites()

        for s in s_list:
            tmp={}
            if s.id in watchlist_sites:
                tmp["synced"] = True
            else:
                tmp["synced"] = False
            tmp["s_id"] = s.id
            tmp["sites"] = s.site
            response_data.append(tmp)

        return Response(response_data)

class view_dates(APIView):

    def get(self,request,p_id,s_id):
        response_data=[]
        d_list = dates_s3.objects.filter(project_id=p_id).filter(site_id = s_id).filter(sync=False)
        print(p_id,s_id)
        for d in d_list:
            tmp={}
            tmp["d_id"] = d.id
            tmp["dates"] = d.date
            tmp['objects'] = d.contents
            response_data.append(tmp)

        print(response_data)
       # response_data.sort(key=lambda x:datetime.strptime(str(x['dates']),'%Y-%m-%d'))
        return Response(response_data)

    def post(self,request,p_id,s_id):
        d_id = request.data["d_id"]
        bucket_name = dates_s3.objects.get(id=d_id).bucket
        if bucket_name == 'uploads.live.videoanalytics':
            base_dir = '/data/downloads/videoanalytics/'
        if bucket_name == 'uploads.live.uncannysurveillance.com':
            base_dir = '/data/downloads/'

        d_query = dates_s3.objects.get(id=d_id)
        success = sync_dates(d_query,base_dir,bucket_name)
        if success ==1:
            update_projects_s3(p_id)
            update_sites_s3(s_id)
            update_dates_s3(d_id)
            return Response("date has been successfully synced")
           
        if success ==0:
            return Response("date does not have json files")

