from django.conf.urls import url
from django.urls import path
from . import views

from . import views
urlpatterns = [
    path('', views.list_s3_projects.as_view() ),
    path('<int:p_id>', views.list_s3_sites.as_view() ),
    path('<int:p_id>/<int:s_id>/', views.list_s3_dates.as_view() ),
    path('<int:p_id>/<int:s_id>/<int:d_id>', views.list_s3_cams.as_view() ),
    path('<int:p_id>/<str:control>/<str:folder>', views.download_s3_project.as_view() ),
    path('<int:p_id>/<int:s_id>/<str:control>/<str:folder>', views.download_s3_site.as_view() ),
    path('<int:p_id>/<int:s_id>/<int:d_id>/<str:control>/<str:folder>', views.download_s3_date.as_view() ),
    path('<int:p_id>/<int:s_id>/<int:d_id>/<int:c_id>/<str:control>/<str:folder>', views.download_s3_cams.as_view() ),

    path('sync/', views.view_projects.as_view() ),
    path('sync/<int:p_id>/', views.view_sites.as_view() ),
    path('sync/<int:p_id>/<int:s_id>', views.view_dates.as_view() ),

]
