from django.apps import AppConfig


class DownloadS3Config(AppConfig):
    name = 'download_s3'
