"""uvlearn URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
#from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from uvlearn import views
from django.urls import path
from rest_framework_swagger.views import get_swagger_view
from django.urls import path

#schema_view = get_swagger_view(title='Pastebin API')

urlpatterns = [
    path('',views.SwaggerSchemaView.as_view()),
    url(r'^admin/', admin.site.urls,name='admin'),
    url(r'^search/',include('search.urls'),name='search'),
    url(r'^s3/',include('s3.urls'),name='s3'),
    url(r'^infer/',include('infer.urls'),name='infer'),
    url(r'^train/',include('train.urls'),name='train'),
    url(r'^download_s3/',include('download_s3.urls'),name='s3'),
    url(r'^annotator/',include('annotator.urls'),name='annotator'),
    url(r'^classifier/',include('classifier.urls'),name='classifier'),
    url(r'^gendata/',include('gendata.urls'),name='gendata'),
    url(r'^localData/',include('localData.urls'),name='localtransfer'),
]
# + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

