from django.conf.urls import url
from django.urls import path
from . import views
urlpatterns = [

    path('<int:d_id>/',views.list_objects.as_view()),
    path('post_info/',views.post_info.as_view()),
    path('active/',views.active_annotator_tasks.as_view()),

]

