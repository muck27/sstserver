import sys
import random
user = "clyde"
import os.path
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
import subprocess
import numpy as np
from django.shortcuts import render
from django.http import HttpResponse
import django
django.setup()
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "uvlearn.settings")
sys.path.append('/home/'+user+'/Downloads/automatic_annotator_tool/django_app/')
from uvdata.models import projects, sites, images, box, classes ,cam_id,bbox,dates,cvat
from django.views.generic import ListView, CreateView, UpdateView 
from django.forms import modelformset_factory
import os
import boto3
import uuid
from django.views.decorators.csrf import csrf_exempt
import json
import psycopg2
import sys
import shutil
import psycopg2
import json
import os
import os.path
from os import path
import zipfile
import glob
import botocore
from PIL import Image, ImageDraw
import datetime
import random
from shapely.geometry import Polygon
import time
from uvlearn import settings
import docker
from uvlearn.settings import BASE_DIR
########API view
from rest_framework.pagination import PageNumberPagination
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
import ast

cvat_ports = [8010,8011,8012,8013,8014,8015,8016,8017,8018,8019,8020,8021,8022,8023,8024,8025,8026,8027,8028,8029,8030]
all_ports=[
		{
		  "cvat":8010,
		  "server":5001,
		  "viewer":5601,
                  "ui"  :7080
		},
                {
                  "cvat":8011,
                  "server":5002,
                  "viewer":5602,

                  "ui"  :7081
                },
                {
                  "cvat":8012,
                  "server":5003,
                  "viewer":5603,

                  "ui"  :7082
                },
                {
                  "cvat":8013,
                  "server":5004,
                  "viewer":5604,

                  "ui"  :7083
                },
                {
                  "cvat":8014,
                  "server":5005,
                  "viewer":5605,

                  "ui"  :7084
                },
                {
                  "cvat":8015,
                  "server":5006,
                  "viewer":5606,

                  "ui"  :7085
                },
                {
                  "cvat":8016,
                  "server":5007,
                  "viewer":5607,

                  "ui"  :7086
                },
                {
                  "cvat":8017,
                  "server":5008,
                  "viewer":5608,

                  "ui"  :7087
                },
                {
                  "cvat":8018,
                  "server":5009,
                  "viewer":5609,

                  "ui"  :7088
                },
                {
                  "cvat":8019,
                  "server":5010,
                  "viewer":5610,

                  "ui"  :7089
                },




                {
                  "cvat":8020,
                  "server":5011,
                  "viewer":5611,
                  "ui"  :7090
                },
                {
                  "cvat":8021,
                  "server":5012,
                  "viewer":5612,
                  "ui"  :7091
                },
                {
                  "cvat":8022,
                  "server":5013,
                  "viewer":5613,
                  "ui"  :7092
                },
                {
                  "cvat":8023,
                  "server":5014,
                  "viewer":5614,
                  "ui"  :7093
                },
                {
                  "cvat":8024,
                  "server":5015,
                  "viewer":5615,
                  "ui"  :7094
                },
                {
                  "cvat":8025,
                  "server":5016,
                  "viewer":5616,
                  "ui"  :7095
                },
                {
                  "cvat":8026,
                  "server":5017,
                  "viewer":5617,
                  "ui"  :7096
                },
                {
                  "cvat":8027,
                  "server":5018,
                  "viewer":5618,
                  "ui"  :7097
                },


                {
                  "cvat":8028,
                  "server":5019,
                  "viewer":5619,
                  "ui"  :7098
                },
                {
                  "cvat":8029,
                  "server":5020,
                  "viewer":5620,
                  "ui"  :7099
                },
                {
                  "cvat":8030,
                  "server":5021,
                  "viewer":5621,
                  "ui"  :7100
                }

               ]

		

def docker_env(ANNOTATE_PATH,CVAT_PORT,UI_PORT,SPLIT,SEEN,WORKERS,PREDS,FILTERS,CAMS,TYPE,BATCH_NUMBER,CVAT_TYPE,p_name,s_name,d_name,bucket):
  os.remove("./cvat/.env")
  f= open("./cvat/.env","w+")
  print(CVAT_PORT)
  f.write('ANNOTATE_PATH={}\n'.format(ANNOTATE_PATH))
  f.write('CVAT_PORT={}\n'.format(CVAT_PORT))
  f.write('UI_PORT={}\n'.format(UI_PORT))
  f.write('SPLIT={}\n'.format(SPLIT))
  f.write('SEEN={}\n'.format(SEEN))
  f.write('WORKERS={}\n'.format(WORKERS))
  f.write('PREDICTIONS={}\n'.format(PREDS))
  f.write('FILTERS={}\n'.format(FILTERS))
  f.write('CAMS={}\n'.format(CAMS))
  f.write('BATCH_TYPE={}\n'.format(TYPE))
  f.write('BATCH_NUMBER={}\n'.format(BATCH_NUMBER))
  f.write('CVAT_TYPE={}\n'.format(CVAT_TYPE))
  f.write('p_name={}\n'.format(p_name))
  f.write('s_name={}\n'.format(s_name))
  f.write('d_name={}\n'.format(d_name))
  f.write('bucket={}\n'.format(bucket))
  f.close()


def analytics_env(CVAT_PORT,SERVER_PORT,VIEWER_PORT):
#  os.remove("./cvat/components/analytics/.env")
  f= open("./cvat/components/analytics/.env","w+")
  f.write('CVAT_PORT={}\n'.format(CVAT_PORT))
  f.write('SERVER_PORT={}\n'.format(SERVER_PORT))
  f.write('VIEWER_PORT={}\n'.format(VIEWER_PORT))
  f.close()

def get_sites_classes(s_list):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        c_list = []
        for x in s_list:
            query = "select classes_id from uvdata_sites_site_classes where sites_id = {}".format(x.id)
            cur.execute(query)
            model_records = cur.fetchall()
            for x in model_records:
                c_list.append(classes.objects.get(id=x[0]).class_name)
        return(c_list)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()

#####################
def get_classes(s_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        c_list = []
        query = "select classes_id from uvdata_sites_site_classes where sites_id = {}".format(s_id)
        cur.execute(query)
        model_records = cur.fetchall()
        for x in model_records:
            c_list.append(classes.objects.get(id=x[0]).class_name)
        return(c_list)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()

####################
def get_images(p_id,s_id,d_id):
    bbox_imgs = bbox.objects.filter(date_id = d_id).filter(site_id=s_id).filter(date_id=d_id).filter(box_type="human")
    return(bbox_imgs)

#####################
def get_annotations(p_id,s_id,d_id):
    class_ids = get_classes(s_id)
    image_ids  = get_images(p_id,s_id,d_id)
    if not image_ids:
        data = {}
        return data

    data = {}

    data["info"] = {}
    data["info"]["description"] = "/data/downloads/{}/{}/{}/".format(p_id,s_id,d_id)
    data['categories']=[]
    for c in class_ids:
        tmp={}
        tmp["id"]=classes.objects.get(class_name=c).id
        tmp["name"]=c
        tmp["supercategory"]=""
        data["categories"].append(tmp)

    data["images"] = []
    for i in image_ids.distinct('image_id'):
        tmp = {}
        tmp["id"]= i.image_id
        tmp["file_name"] = images.objects.get(id=i.image_id).filename
        tmp["height"]   = int(i.height)
        tmp["width"]    = int(i.width)
        data["images"].append(tmp) 
       
    data["annotations"]=[]
    for a in image_ids:
        tmp={}
        tmp["category_id"]= classes.objects.get(class_name=a.box_class).id  
        tmp["id"] = a.id
        tmp["image_id"]= a.image_id
        tmp["bbox"]=[a.x,a.y,a.w,a.h]
        data["annotations"].append(tmp)


    return(data)

def Diff(li1, li2): 
    return (list(set(li1) - set(li2))) 



def update_image_table(i_id,dets):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "update uvdata_images set human_or_not=%s , human_detections = %s where id=%s"
        cur.execute(query,(True,dets,i_id,))
        query2 = "update uvdata_bbox set completed  = %s where image_id=%s"
        cur.execute(query2,(True,i_id,))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not update bbox qq12",error)
    finally:
        if (con):
            cur.close()
            con.close()

def remove_tag(i_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "update uvdata_bbox set session=False,port=NULL,task_id=NULL,frame_id=NULL where image_id=%s"
        cur.execute(query,(i_id,))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not update bbox qq12",error)
    finally:
        if (con):
            cur.close()
            con.close() 


def remove_no_anns(i_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "delete from uvdata_bbox where id = %s"
        cur.execute(query,(i_id,))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not update bbox qq12",error)
    finally:
        if (con):
            cur.close()
            con.close() 

######################

def get_filters(s_id):
    response = sites.objects.get(id = s_id).alerts
    print(response)
    return(response)



class list_objects(APIView):

    def get(self,request,d_id):
        print(d_id)
        base_folder = "/data/downloads"
        s_id = dates.objects.get(id=d_id).site_id

        i_list = images.objects.filter(date_id=d_id)
        c_list = cam_id.objects.filter(date_id=d_id).count()
        tmp={}
        tmp["total_images"] = i_list.count()
        tmp["human_annotated"]= i_list.filter(human_or_not=True).count()
        tmp["total_cams"]     = c_list
        tmp['filters'] = get_filters(s_id)
        return Response(tmp)


class post_info(APIView):

    def post(self,request):
        from uvdata.models import cvat
        os.chdir(BASE_DIR)
        print(request.data)
        p_id = int(request.data['p_id'])
        s_id = int(request.data['s_id'])
        d_id = int(request.data['d_id'])
        bucket = projects.objects.get(id=p_id).bucket
        if bucket == 'uploads.live.videoanalytics':
            base_dir = '/data/downloads/videoanalytics/'
        else:
            base_dir = '/data/downloads/'


        if request.data['split']:
            split_num = float(request.data['split'])
        else:
            split_num = 0
        cvat_type = str(request.data['type'])
 
        seen = str(request.data['func'])
        predictions = str(request.data['preds'])  
        filters = request.data['filters']  
        cam_list = request.data['cams']
        batching_type= request.data['batching']
        batching_number = request.data['batchnumber']

        total_filters = get_filters(s_id) 
        filter_json = {}
        null_filter=0
        for x in total_filters:
            if x in filters:
                filter_json[x] = True
                null_filter = null_filter + 1
            else:
                filter_json[x] = False
            
        if null_filter == 0:
            filter_json=[]

        workers=4 
        print(p_id,s_id,d_id,split_num,seen,predictions,filter_json,cam_list,batching_type,batching_number)
        p_name   = projects.objects.get(id=p_id).project_name
        s_name   = sites.objects.get(id=s_id).site_name
        d_name   = dates.objects.get(id=d_id).date
 
        if ' ' in p_name:
            p_name = p_name.replace(' ','_')
        if ' ' in s_name:
            s_name = s_name.replace(' ','_')
   
        '''
        update table about the new cvat process
        '''        
        job_name= "/{}/{}/{}/".format(p_name,s_name,d_name)

        check = cvat.objects.filter(job=job_name)

        if check:
            for x in check:
                port = x.port
            return Response("No Duplicate of cvat job allowed. Current job running on port {}".format(port))   
        else:

            path = "{}{}/{}/{}/".format(base_dir,p_name,s_name,d_name)
            new_path = path.replace('_','\ ').replace(',','\,')
            with open('cvat_ports.txt') as ports:
                port=json.load(ports)
            avail_ports = Diff(cvat_ports,port["closed_ports"])
            cvat_port = random.choice(avail_ports)
            port["closed_ports"].append(cvat_port)
            tmp ={}
            tmp["closed_ports"] = port["closed_ports"]
            with open('cvat_ports.txt','w') as ports:
                json.dump(tmp,ports)
          

            for x in all_ports:
                if x["cvat"] == cvat_port:
                    ui_port=x["ui"]

 
            docker_env(new_path,cvat_port,ui_port,split_num,seen,workers,predictions,filter_json,cam_list,batching_type,batching_number,cvat_type, projects.objects.get(id=p_id).project_name, sites.objects.get(id=s_id).site_name, dates.objects.get(id=d_id).date,bucket)
            os.chdir("./cvat/")
            cvat_upload  = cvat.objects.create(port=cvat_port,job=job_name,predictions=predictions,filters=filter_json,task=cvat_type)
            cvat_upload.save()
            compose_command = "docker-compose --project-name {}-{}-{} up --no-recreate -d ".format(p_name,s_name,d_name)
  #          compose_command = "docker-compose -f docker-compose.yml -f components/analytics/docker-compose.analytics.yml  --project-name {}-{}-{} up --no-recreate -d ".format(p_name,s_name,d_name)
            os.system(compose_command)
     

            return Response('go to http://34.93.111.56/:{}'.format(cvat_port))


class active_annotator_tasks(APIView):

    def get(self,request):
        ports = bbox.objects.exclude(port=None).distinct('port')
        response=[]
        for p in ports:
            tmp={}
            tmp["port"] = p.port
            tmp["date"] = dates.objects.get(id=p.date_id).date
            tmp["site"] = sites.objects.get(id=p.site_id).site_name
            p_id  = sites.objects.get(id=p.site_id).project_id
            tmp["project"] = projects.objects.get(id=p_id).project_name
            response.append(tmp)
        return Response(response)


    def post(self,request):
        port = request.data['port']

        '''
        kill dockers running on port

        '''
        client = docker.from_env()
        docker_names = ['cvat_{}'.format(port),'cvat_ui_{}'.format(port),'cvat_db_{}'.format(port),'cvat_redis_{}'.format(port)]
        container_ids= []
        a=client.containers.list(all=True)
        for x in a:
            if x.name in docker_names: 
                container_ids.append(x.id)
        for x in container_ids:
            container= client.containers.get(x)
            container.stop()
            container.wait()
            container.remove()

        '''
        update uvdata_images table
        ''' 
        task_name = cvat.objects.get(port=port).task
        if task_name == 'date-annotate': 
            completed_ids = bbox.objects.filter(port=port).filter(completed=True)
            port_ids      = bbox.objects.filter(port=port)


            for i in completed_ids:
                detection_count  = bbox.objects.filter(image_id=i.image_id).filter(box_type='human').count() 
                update_image_table(i.image_id,detection_count)
                print(i.id) 
            for i in port_ids:
                remove_tag(i.image_id)   
                print(i.id) 
            task = cvat.objects.get(port=port).delete()

        if task_name == 'date-roi':
            task = cvat.objects.get(port=port).delete()
            port_ids      = bbox.objects.filter(port=port)
            for i in port_ids:
                remove_tag(i.image_id)
                print(i.id)          
        

        '''
        remove port from cvat_ports
        ''' 
        with open('cvat_ports.txt') as ports:
            port_list=json.load(ports)
        
        port_list['closed_ports'].remove(int(port))
        with open('cvat_ports.txt','w') as ports:
            json.dump(port_list,ports)

        
        return Response('docker containers for port {} have been stopped'.format(port)) 
