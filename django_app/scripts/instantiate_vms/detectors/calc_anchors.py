import shutil
import requests
import configparser
import urllib.request
import psycopg2
from os import path
import os
import json
from psycopg2.extras import Json
import random
import docker
import zmq
import json
'''
set user name and directory for install
'''
config = configparser.ConfigParser()
config.read('../sst.env')

user = config.get('System','user')
GPU  = config.get('System','GPU')
dl_dir = config.get('System','dl_dir')
client_loc = config.get('System','client_loc')
public_ip = urllib.request.urlopen('https://ident.me').read().decode('utf8')


detector_image = config.get('Detector','detector_docker')
detector_type = config.get('Detector','detector_type')
start_port = config.get('Detector','start_port')
end_port = config.get('Detector','end_port')
mount_code = config.get('Detector','code_dir')


'''
#write code for gpu devices

'''

def lst2pgarr(alist):
    return '{' + ','.join(alist) + '}'

def get_instance_id():
    try:
        select_target_query="select * from uvdata_remote_instances where public_ip= %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(str(public_ip),))
        return(cur.fetchone()[0])
    except (Exception, psycopg2.Error) as error:
        print("could not get instance id",error)
    finally:
        if (con):
            cur.close()
            con.close()


def check_instance(instance_id):
    try:
        select_target_query="select * from uvdata_remote_processes where instance_id = %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(str(instance_id),))
        return(cur.fetchone())
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()



def get_dataset(dataset_id):
    try:
        select_target_query="select * from uvdata_datasetvsmodel where id = %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(str(dataset_id),))
        return(cur.fetchone())
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()


def get_model(model_id):
    try:
        select_target_query="select * from uvdata_infer_models where id = %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(str(model_id),))
        return(cur.fetchone())
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()




def get_model_params(model_name):
    try:
        select_target_query="select * from uvdata_infer_models where model = %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(str(model_name),))
        return(cur.fetchone())
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()


def convert_class_ids(class_ids):

    class_names = []
    for x in class_ids:
        try:
            select_target_query="select * from uvdata_classes where id = %s"
            con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
            cur = con.cursor()
            cur.execute((select_target_query),(str(x),))
            class_names.append(cur.fetchone()[1])
        except (Exception, psycopg2.Error) as error:
            print("could not insert in uvdata_dates",error)
        finally:
            if (con):
                cur.close()
                con.close()

    return(class_names)



def remove_entry(process,params,completed):

    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute("update uvdata_remote_processes set process = (%s) , params = %s , completed = %s where instance_id= (%s)",[process,params,completed,str(instance_id)])
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not remove entry from db",error)
    finally:
        if (con):
            cur.close()
            con.close()





'''
get instance id
'''
instance_id = get_instance_id()

print("Your instance id is :",instance_id)

'''
check if process is running
'''

isRunning = check_instance(instance_id)
if 3 in isRunning[2]:


    '''
    iterate over all anchor-box processes
    '''
    count = 0

    results = []
    for _calc in isRunning[3]['detectorAnchors']:

        '''
        get values
        '''
        dataset_query = get_dataset(_calc['dataset'])
        dataset_name = dataset_query[1]
        model_query = get_model(_calc['model'])
        model_name = model_query[1]
        height = str(_calc['height'])
        width = str(_calc['width'])
        clusters = str(_calc['clusters'])


        '''
        find free port
        '''

        anchor_port = random.randint(int(start_port),int(end_port))

        '''
        mount and start container
        '''

        img_dir  = '{}uvsst/data/{}'.format(dl_dir,dataset_name)
        model_dir  = '{}uvsst/models/{}'.format(dl_dir,model_name)


        if detector_type == 'darknet':

            detector_image = detector_image
            client = docker.from_env()
            volumes = {mount_code:{'bind':'/app','mode':'rw'},model_dir:{'bind': '/model', 'mode': 'rw'}, img_dir:{'bind': '/images', 'mode': 'rw'}}
            ports = {}
            open_port   = "{}/tcp".format(str(anchor_port))
            ports[open_port]  =int(anchor_port)
            stdin_open=True
            tty = True
            container_name="anchorBox_{}".format(str(count))
            detach=True
            container = client.containers.run(image=detector_image, volumes=volumes,ports=ports, stdin_open=stdin_open, tty=tty,name=container_name,detach=True,working_dir='/app')

            '''
            install extra libraries
            '''
            curl_command = 'apt-get install -y libcurl4-openssl-dev'
            os.system('docker exec {} {}'.format(container_name,curl_command))
            os.system('docker exec {} bash -c cd /app'.format(container_name))


            '''
            run command
            '''
            command = "./darknet detector calc_anchors /images/test/ -num_of_clusters {} -width {} -height {} -port {}".format(clusters,width,height,str(anchor_port))
            print(command)
            client = docker.from_env()
            target_container = client.containers.get(container_id = container_name)
            log_cont = target_container.exec_run(cmd=command,detach=True)


            '''
            start socket for communication
            '''

            context = zmq.Context()
            socket = context.socket(zmq.PAIR)
            host_socket = "tcp://localhost:{}".format(str(anchor_port))
            socket.connect(host_socket)
            socket.send_string('start container')
            print('anchor-box docker started')

            count = count + 1
            try:
                message =socket.recv()
                iter_val = json.loads(message.decode('utf-8'))
                print(iter_val)
            except:
                print('unable to start anchor ')



            '''
            save results
            '''
            _calc['value'] = iter_val['anchor_box']
            results.append(_calc)

            '''
            stop container
            '''
            target_container.stop()
            target_container.remove()
            socket.close()



    print(results)
    '''
    task is complete so remove entry from  db
    '''
    current_instance = check_instance(instance_id)
    current_process = current_instance[2]

    updated_process = []
    for y in current_process:
        if y != 3:
            updated_process.append(y)

    updated_params = current_instance[3]
    updated_params['detectorAnchors'] = []

    completed_params = current_instance[4]
    completed_params['detectorAnchors'] = results

    remove_entry(updated_process,json.dumps(updated_params),json.dumps(completed_params))


