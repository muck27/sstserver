import shutil
import requests
import configparser
import urllib.request
import psycopg2
from os import path
import os
import json
from psycopg2.extras import Json


'''
set user name and directory for install
'''
config = configparser.ConfigParser()
config.read('../sst.env')

user = config.get('System','user')
GPU  = config.get('System','GPU')
dl_dir = config.get('System','dl_dir')
public_ip = urllib.request.urlopen('https://ident.me').read().decode('utf8')


'''
#write code for gpu devices

'''

def lst2pgarr(alist):
    return '{' + ','.join(alist) + '}'

def get_instance_id():
    try:
        select_target_query="select * from uvdata_remote_instances where public_ip= %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(str(public_ip),))
        return(cur.fetchone()[0])
    except (Exception, psycopg2.Error) as error:
        print("could not get instance id",error)
    finally:
        if (con):
            cur.close()
            con.close()


def check_instance(instance_id):
    try:
        select_target_query="select * from uvdata_remote_processes where instance_id = %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(str(instance_id),))
        return(cur.fetchone())
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()


def get_model_params(model_name):
    try:
        select_target_query="select * from uvdata_infer_models where model = %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(str(model_name),))
        return(cur.fetchone())
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()


def convert_class_ids(class_ids):

    class_names = []
    for x in class_ids:
        try:
            select_target_query="select * from uvdata_classes where id = %s"
            con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
            cur = con.cursor()
            cur.execute((select_target_query),(str(x),))
            class_names.append(cur.fetchone()[1])
        except (Exception, psycopg2.Error) as error:
            print("could not insert in uvdata_dates",error)
        finally:
            if (con):
                cur.close()
                con.close()

    return(class_names)



def remove_entry(process,params):

    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute("update uvdata_remote_processes set process = (%s) , params = %s where instance_id= (%s)",[process,params,str(instance_id)])
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not remove entry from db",error)
    finally:
        if (con):
            cur.close()
            con.close()





'''
get instance id
'''
instance_id = get_instance_id()

print("Your instance id is :",instance_id)

'''
check if process is running
'''

isRunning = check_instance(instance_id)

if 6 in isRunning[2]:

    '''
    download model instruction has been given
    '''
    model_names = isRunning[3]['model']
    for x in model_names:
        model_params = get_model_params(x)
        target_config = model_params[5]['config']
        target_weight = model_params[5]['weight']


        '''
         create directories and download
        '''

        parent_dir = '{}/uvsst/'.format(dl_dir)
        if not path.exists(parent_dir) :
            os.mkdir(parent_dir,0o666 )


        detector_dir = '{}/models/'.format(parent_dir)
        if not path.exists(detector_dir) :
            os.mkdir(detector_dir,0o666 )


        model_dir  = '{}{}'.format(detector_dir,x)
        if not path.exists(model_dir) :
            os.mkdir(model_dir,0o666 )


        '''
        download weights and cfg to folder
        '''
        print('download model')
        dl_weight = 'aws s3 cp s3://uvdata-models/detector/weights/{} {}/model.weights'.format(target_weight,model_dir)
        os.system(dl_weight)

        print('download model')

        dl_cfg = 'aws s3 cp s3://uvdata-models/detector/config/{} {}/model.cfg'.format(target_config,model_dir)
        os.system(dl_cfg)

        '''
        set labels.txt
        '''

        class_names = convert_class_ids(model_params[4])
        f = open('{}/det.names'.format(model_dir),'+w')
        class_count = 0
        for name in class_names:
            f.write(name)
            f.write('\n')
            class_count = class_count + 1

        f.close()


        '''
        create model.data file for darknet
        '''

        f = open('{}/model.data'.format(model_dir),'+w')
        f.write('classes = {} \n'.format(class_count))
        f.write('names = /model/model.data \n')
        f.close()



        '''
        task is complete so remove entry from  db
        '''
        current_instance = check_instance(instance_id)
        current_process = current_instance[2]
        current_model_params  = current_instance[3]['model']

        updated_process = []
        for y in current_process:
            if y != 6:
                updated_process.append(y)

        updated_model_params = []
        for y in current_model_params:
            if y!= x:
                updated_model_params.append(y)


        updated_params = current_instance[3]
        updated_params['model'] = updated_model_params
        print(json.dumps(updated_params))
        remove_entry(updated_process,json.dumps(updated_params))

