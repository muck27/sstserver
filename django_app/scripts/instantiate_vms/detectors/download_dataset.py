import shutil
import requests
import configparser
import urllib.request
import psycopg2
from os import path
import os
import json
from psycopg2.extras import Json
import math
import random


'''
set user name and directory for install
'''
config = configparser.ConfigParser()
config.read('../sst.env')

user = config.get('System','user')
GPU  = config.get('System','GPU')
dl_dir = config.get('System','dl_dir')
public_ip = urllib.request.urlopen('https://ident.me').read().decode('utf8')


'''
#write code for gpu devices

'''

def lst2pgarr(alist):
    return '{' + ','.join(alist) + '}'

def get_instance_id():
    try:
        select_target_query="select * from uvdata_remote_instances where public_ip= %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(str(public_ip),))
        return(cur.fetchone()[0])
    except (Exception, psycopg2.Error) as error:
        print("could not get instance id",error)
    finally:
        if (con):
            cur.close()
            con.close()


def check_instance(instance_id):
    try:
        select_target_query="select * from uvdata_remote_processes where instance_id = %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(str(instance_id),))
        return(cur.fetchone())
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()


def get_dataset_params(_id):
    try:
        select_target_query="select * from uvdata_datasetvsmodel where id = %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(_id,))
        return(cur.fetchone())
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()


def convert_class_ids(class_ids):

    class_names = []
    for x in class_ids:
        try:
            select_target_query="select * from uvdata_classes where id = %s"
            con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
            cur = con.cursor()
            cur.execute((select_target_query),(str(x),))
            class_names.append(cur.fetchone()[1])
        except (Exception, psycopg2.Error) as error:
            print("could not insert in uvdata_dates",error)
        finally:
            if (con):
                cur.close()
                con.close()

    return(class_names)



def remove_entry(process,params):

    try:
        select_target_query="update uvdata_remote_processes set params = %s , process = %s where instance_id= %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute("update uvdata_remote_processes set process = (%s) , params = %s where instance_id= (%s)",[process,params,str(instance_id)])
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not remove entry from db",error)
    finally:
        if (con):
            cur.close()
            con.close()



def datewise_annotated_images(d_id):
    try:
        select_target_query="select * from uvdata_images where date_id = %s and human_or_not = %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(d_id,True))
        return(cur.fetchall())
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()





def imagewise_annotations(i_id):
    try:
        select_target_query="select x,y,w,h,box_class,height,width from uvdata_bbox where image_id = %s and box_type = %s"
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute((select_target_query),(i_id,'human'))
        return(cur.fetchall())
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()






'''
get instance id
'''
instance_id = get_instance_id()

print("Your instance id is :",instance_id)

'''
check if process is running
'''

isRunning = check_instance(instance_id)

if 1 in isRunning[2]:

    '''
    check relevant directories
    '''
    parent_dir = '{}/uvsst/'.format(dl_dir)
    if not path.exists(parent_dir) :
        os.mkdir(parent_dir,0o666 )


    data_dir = '{}data/'.format(parent_dir)
    if not path.exists(data_dir) :
        os.mkdir(data_dir,0o666 )




    '''
    get dataset hierarchy
    '''
    dataset_ids = isRunning[3]['dataset']
    for _dataset in dataset_ids:
        dataset_params = get_dataset_params(_dataset)
        title = dataset_params[1]
        d_ids = dataset_params[2]
        model_type = dataset_params[4]
        split = float(dataset_params[6])
        classes = dataset_params[5]

        class_list = convert_class_ids(classes)
        '''
        create random probabilities
        '''
        random_probs = []
        train_probs = int(math.floor(split*10))
        test_probs = 10-int(math.ceil(split*10))
        for x in range(train_probs):
            random_probs.append(1)
        for x in range(test_probs):
            random_probs.append(0)


        '''
        create dl directory
        '''

        dataset_folder = '{}{}/'.format(data_dir,title)
        if not path.exists(dataset_folder):
            os.mkdir(dataset_folder,0o666)

        train_folder = '{}train/'.format(dataset_folder)
        if not path.exists(train_folder):
            os.mkdir(train_folder,0o666)

        test_folder = '{}test/'.format(dataset_folder)
        if not path.exists(test_folder):
            os.mkdir(test_folder,0o666)

        for date in d_ids:

            '''
            get image list from db(Need to update with a filter of classes!!!!!!!!!!!!! change UI)
            '''

            image_list = datewise_annotated_images(date)

            train_file = '{}/train.txt'.format(dataset_folder)
            g = open(train_file,'+w')

            test_file = '{}/test.txt'.format(dataset_folder)
            h = open(test_file,'+w')



            for i in image_list:

                '''
                check train/test and download image and annotations
                '''
                prob = random.choice(random_probs)



                if prob == 0:
                    img_loc = i[6]
                    os.system("wget --no-check-certificate --no-proxy {} -P {}".format(img_loc,test_folder))
                    annotations = imagewise_annotations(i[0])

                    '''
                    open annotations file and write annotations
                    '''
                    img_name = i[4]
                    anno_file = '{}/{}'.format(test_folder,img_name.replace('.jpg','.txt'))
                    f = open(anno_file,'+w')

                    for ann in annotations:
                        class_name = ann[4]
                        if class_name in class_list:
                            c_id = class_list.index(class_name)
                            x_1 = str((ann[0] + ann[2]/2)/ann[5])
                            y_1 = str((ann[1] + ann[3]/2)/ann[6])
                            w_1 = str((ann[2]/ann[5]))
                            h_1 = str((ann[3]/ann[6]))
                            f.write('{} {} {} {} {}\n'.format(c_id,x_1,y_1,w_1,h_1))


                    f.close()
                    h.write('/images/test/{}\n'.format(img_name))




                if prob == 1:
                    img_loc = i[6]
                    os.system("wget --no-check-certificate --no-proxy {} -P {}".format(img_loc,train_folder))
                    annotations = imagewise_annotations(i[0])

                    '''
                    open annotations file and write annotations
                    '''
                    img_name = i[4]
                    anno_file = '{}/{}'.format(train_folder,img_name.replace('.jpg','.txt'))
                    f = open(anno_file,'+w')

                    for ann in annotations:
                        class_name = ann[4]
                        if class_name in class_list:
                            c_id = class_list.index(class_name)
                            x_1 = str((ann[0] + ann[2]/2)/ann[5])
                            y_1 = str((ann[1] + ann[3]/2)/ann[6])
                            w_1 = str((ann[2]/ann[5]))
                            h_1 = str((ann[3]/ann[6]))
                            f.write('{} {} {} {} {}\n'.format(c_id,x_1,y_1,w_1,h_1))


                    f.close()
                    g.write('/images/train/{}\n'.format(img_name))



        '''
        task is complete so remove entry from  db
        '''
        current_instance = check_instance(instance_id)
        current_process = current_instance[2]
        current_dataset_params  = current_instance[3]['dataset']

        updated_process = []
        for y in current_process:
            if y != 1:
                updated_process.append(y)

        updated_dataset_params = []
        for y in current_dataset_params:
            print(y,_dataset)
            if y!= _dataset:
                updated_dataset_params.append(y)


        updated_params = current_instance[3]
        updated_params['dataset'] = updated_dataset_params
        print(json.dumps(updated_params))
        remove_entry(updated_process,json.dumps(updated_params))

