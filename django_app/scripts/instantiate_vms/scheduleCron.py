from crontab import CronTab
'''
user inputs
get python path by 'which python'
'''
user = 'root'
python_path = '/home/clyde/Downloads/automatic_annotator_tool/django_app/env/bin/python'
instantiate_file_path = '/home/clyde/Downloads/automatic_annotator_tool/django_app/scripts/instantiate_vms/'


my_cron = CronTab(user=user)
job = my_cron.new(command='cd {} && {} {}instantiate_instance.py'.format(instantiate_file_path,python_path,instantiate_file_path))
job.minute.every(1)
my_cron.write()

