import os
from uvdata.models import sites,dates,cam_id


final_dates = []
d_list = dates.objects.filter(site_id=1)
for x in d_list:
    final_dates.append(x.id)

final_c_list = []
for x in final_dates:
    c_list = cam_id.objects.filter(date = x).exclude(configured = False)
    for y in c_list:
        tmp = {} 
        tmp['cam_id'] = y.cam_id
        tmp['json_angle'] = y.angle
        tmp['comments'] = y.comments
        tmp['roi']    = y.roi
        tmp['configured_angle'] = y.configured_angle
        final_c_list.append(tmp)


f = open('configured_cams.txt','+w')
f.write(str(final_c_list))
f.close()
