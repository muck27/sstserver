import os
import sys
import numpy as np
import zmq
import json
import docker
# Have to add code for docker ->


image_dir = str(sys.argv[1])
docker_id = str(sys.argv[2])
mount_model = str(sys.argv[3])
container_name = str(sys.argv[4])
open_port = "{}/tcp".format(str(sys.argv[5]))



client = docker.from_env()
image_id = docker_id
volumes = {mount_model:{'bind': '/app', 'mode': 'ro'}, image_dir:{'bind': '/images', 'mode': 'ro'}} #proj/site/date
ports = {}
ports[open_port]=int(sys.argv[5])

stdin_open=True
tty = True
name="{}_{}".format(str(sys.argv[4]),str(sys.argv[5]))
detach=True
command="./darknet detector test cfg/model.data model.cfg model.weights {}".format(str(sys.argv[5]))

container = client.containers.run(image=image_id, volumes=volumes,ports=ports, stdin_open=stdin_open, tty=tty,name=name,detach=True)
workdir="/app"
print(container.id)
 
log_cont = container.exec_run(cmd=command, workdir='/app',stream=True)
print('[assedo')
