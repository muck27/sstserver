import psycopg2

s_id = 1
d_id=2
c_id =2
try:
    con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
    cur = con.cursor()
    c_list = []
    query1 = "select * from uvdata_bbox where cam_id =%s and date_id=%s and box_type=%s"
    cur.execute(query1,(c_id,d_id,'human'))
    model_records = cur.fetchall()
    new_records = []
    for x in model_records:
        query2="select filename from uvdata_images where id=%s"
        cur.execute(query2,(x[13],))
        i_name = cur.fetchone()
        query3="select cam_id from uvdata_cam_id where id =%s"
        cur.execute(query3,(x[11],))
        c_name = cur.fetchone()

        query4 = "select * from uvdata_ann_tasks where filename=%s and cam_id=%s"
        cur.execute(query4,(i_name[0],c_name[0]))
        ann_task = cur.fetchone()        
        if not ann_task:
            print('image ann in progress')
            print(x[13])
            new_records.append(x[13])
    print(new_records) 
             
except (Exception, psycopg2.Error) as error:
    print("could not insert in uvdata_bbox",error)
finally:
    if (con):
        cur.close()
        con.close()

