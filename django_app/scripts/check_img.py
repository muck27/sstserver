from uvdata.models import images,bbox


d_list = [269,270,271]
i_final = []
for d in d_list:
    i_list = images.objects.filter(date_id = d)
    for i in i_list:
        command = 'wget {}'.format(i.image_url)
        os.system(command)
        
        with open(i.filename, 'rb') as f:
            check_chars = f.read()[-2:]    
  
        if check_chars != b'\xff\xd9':
            i_final.append(i.id)
            remove_boxes = bbox.objects.filter(filename= i.filename).delete()
            remove_images = i.delete()
            os.remove(i.filename)
        else:
            os.remove(i.filename)

print(i_final)
