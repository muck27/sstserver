import os
import sys
import numpy as np
import zmq
import json
import docker
# Have to add code for docker ->
#sudo python scripts/train_script.py /data/train_data/TestPlace2/train b756d7ae8269 /data/train//data/train/testplace2/ TestPlace2 5564

image_dir = str(sys.argv[1])
docker_id = str(sys.argv[2])
mount_model = str(sys.argv[3])
container_name = str(sys.argv[4])
open_port = "{}/tcp".format(str(sys.argv[5]))
weight    = str(sys.argv[6])
config    = str(sys.argv[7])


client = docker.from_env()
image_id = docker_id
volumes = {mount_model:{'bind': '/app', 'mode': 'rw'}, image_dir:{'bind': '/images', 'mode': 'rw'}} #proj/site/date
ports = {}
ports[open_port]=int(sys.argv[5])
stdin_open=True
tty = True
name="train_detector_{}_{}".format(str(sys.argv[4]),str(sys.argv[5]))
detach=True
command="./darknet detector train /app/cfg/model.data /app/{} /app/{} -port {}".format(config,weight,str(sys.argv[5]))
print(command)
container = client.containers.run(runtime="nvidia",image=image_id, volumes=volumes,ports=ports, stdin_open=stdin_open, tty=tty,name=name,detach=True)
workdir="/app"
 
log_cont = container.exec_run(cmd=command, workdir='/app',stream=True)
