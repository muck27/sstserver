#!/usr/bin/env python3
import os
import sys
sys.path.append('/home/clyde/Downloads/automatic_annotator_tool/django_app/env/lib/python3.5/site-packages')
import docker

image_dir   = str(sys.argv[1])
docker_id   = str(sys.argv[2])
mount_model = str(sys.argv[3])
container_name = str(sys.argv[4])
open_port   = "{}/tcp".format(str(sys.argv[5]))


client = docker.from_env()
image_id = docker_id
volumes = {mount_model:{'bind': '/app', 'mode': 'rw'}, image_dir:{'bind': '/images', 'mode': 'rw'}} #proj/site/date
ports = {}
ports[open_port]  =int(sys.argv[5])
stdin_open=True
tty = True
name="darknet_{}_{}_{}".format(str(sys.argv[4]),str(sys.argv[6]),str(sys.argv[5]))
detach=True
container = client.containers.run(image=image_id, volumes=volumes,ports=ports, stdin_open=stdin_open, tty=tty,name=name,detach=True,working_dir='/app')
 
