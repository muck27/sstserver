# Copyright (C) 2018 Intel Corporation
#
# SPDX-License-Identifier: MIT

import os
import re
import traceback
from ast import literal_eval
import shutil
from datetime import datetime
from tempfile import mkstemp
import psycopg2
from django.http import HttpResponseBadRequest
from django.shortcuts import redirect, render
from django.conf import settings
from sendfile import sendfile
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework import status
from rest_framework import viewsets
from rest_framework import serializers
from rest_framework.decorators import action
from rest_framework import mixins
from django_filters import rest_framework as filters
import django_rq
from django.db import IntegrityError


from . import annotation, task, models
from cvat.settings.base import JS_3RDPARTY, CSS_3RDPARTY
from cvat.apps.authentication.decorators import login_required
import logging
from .log import slogger, clogger
from cvat.apps.engine.models import StatusChoice, Task, Job, Plugin
from cvat.apps.engine.serializers import (TaskSerializer, UserSerializer,
   ExceptionSerializer, AboutSerializer, JobSerializer, ImageMetaSerializer,
   RqStatusSerializer, TaskDataSerializer, LabeledDataSerializer,
   PluginSerializer, FileInfoSerializer, LogEventSerializer,
   ProjectSerializer, BasicUserSerializer)
from cvat.apps.annotation.serializers import AnnotationFileSerializer
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from cvat.apps.authentication import auth
from rest_framework.permissions import SAFE_METHODS
from cvat.apps.annotation.models import AnnotationDumper, AnnotationLoader
from cvat.apps.annotation.format import get_annotation_formats



def Diff(li1, li2):
    return (list(set(li1) - set(li2)))


# Server REST API
@login_required
def dispatch_request(request):
    """An entry point to dispatch legacy requests"""
    if request.method == 'GET' and 'id' in request.GET:
        return render(request, 'engine/annotation.html', {
            'css_3rdparty': CSS_3RDPARTY.get('engine', []),
            'js_3rdparty': JS_3RDPARTY.get('engine', []),
            'status_list': [str(i) for i in StatusChoice]
        })
    else:
        return redirect('/dashboard/')

class ServerViewSet(viewsets.ViewSet):
    serializer_class = None

    # To get nice documentation about ServerViewSet actions it is necessary
    # to implement the method. By default, ViewSet doesn't provide it.
    def get_serializer(self, *args, **kwargs):
        pass

    @staticmethod
    @action(detail=False, methods=['GET'], serializer_class=AboutSerializer)
    def about(request):
        from cvat import __version__ as cvat_version
        about = {
            "name": "Computer Vision Annotation Tool",
            "version": cvat_version,
            "description": "CVAT is completely re-designed and re-implemented " +
                "version of Video Annotation Tool from Irvine, California " +
                "tool. It is free, online, interactive video and image annotation " +
                "tool for computer vision. It is being used by our team to " +
                "annotate million of objects with different properties. Many UI " +
                "and UX decisions are based on feedbacks from professional data " +
                "annotation team."
        }
        serializer = AboutSerializer(data=about)
        if serializer.is_valid(raise_exception=True):
            return Response(data=serializer.data)

    @staticmethod
    @action(detail=False, methods=['POST'], serializer_class=ExceptionSerializer)
    def exception(request):
        serializer = ExceptionSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            additional_info = {
                "username": request.user.username,
                "name": "Send exception",
            }
            message = JSONRenderer().render({**serializer.data, **additional_info}).decode('UTF-8')
            jid = serializer.data.get("job_id")
            tid = serializer.data.get("task_id")
            if jid:
                clogger.job[jid].error(message)
            elif tid:
                clogger.task[tid].error(message)
            else:
                clogger.glob.error(message)

            return Response(serializer.data, status=status.HTTP_201_CREATED)

    @staticmethod
    @action(detail=False, methods=['POST'], serializer_class=LogEventSerializer)
    def logs(request):
        serializer = LogEventSerializer(many=True, data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = { "username": request.user.username }
            for event in serializer.data:
                message = JSONRenderer().render({**event, **user}).decode('UTF-8')
                jid = event.get("job_id")
                tid = event.get("task_id")
                if jid:
                    clogger.job[jid].info(message)
                elif tid:
                    clogger.task[tid].info(message)
                else:
                    clogger.glob.info(message)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

    @staticmethod
    @action(detail=False, methods=['GET'], serializer_class=FileInfoSerializer)
    def share(request):
        param = request.query_params.get('directory', '/')
        if param.startswith("/"):
            param = param[1:]
        directory = os.path.abspath(os.path.join(settings.SHARE_ROOT, param))

        if directory.startswith(settings.SHARE_ROOT) and os.path.isdir(directory):
            data = []
            content = os.scandir(directory)
            for entry in content:
                entry_type = None
                if entry.is_file():
                    entry_type = "REG"
                elif entry.is_dir():
                    entry_type = "DIR"

                if entry_type:
                    data.append({"name": entry.name, "type": entry_type})

            serializer = FileInfoSerializer(many=True, data=data)
            if serializer.is_valid(raise_exception=True):
                return Response(serializer.data)
        else:
            return Response("{} is an invalid directory".format(param),
                status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    @action(detail=False, methods=['GET'], url_path='annotation/formats')
    def formats(request):
        data = get_annotation_formats()
        return Response(data)

class ProjectFilter(filters.FilterSet):
    name = filters.CharFilter(field_name="name", lookup_expr="icontains")
    owner = filters.CharFilter(field_name="owner__username", lookup_expr="icontains")
    status = filters.CharFilter(field_name="status", lookup_expr="icontains")
    assignee = filters.CharFilter(field_name="assignee__username", lookup_expr="icontains")

    class Meta:
        model = models.Project
        fields = ("id", "name", "owner", "status", "assignee")

class ProjectViewSet(auth.ProjectGetQuerySetMixin, viewsets.ModelViewSet):
    queryset = models.Project.objects.all().order_by('-id')
    serializer_class = ProjectSerializer
    search_fields = ("name", "owner__username", "assignee__username", "status")
    filterset_class = ProjectFilter
    ordering_fields = ("id", "name", "owner", "status", "assignee")
    http_method_names = ['get', 'post', 'head', 'patch', 'delete']

    def get_permissions(self):
        http_method = self.request.method
        permissions = [IsAuthenticated]

        if http_method in SAFE_METHODS:
            permissions.append(auth.ProjectAccessPermission)
        elif http_method in ["POST"]:
            permissions.append(auth.ProjectCreatePermission)
        elif http_method in ["PATCH"]:
            permissions.append(auth.ProjectChangePermission)
        elif http_method in ["DELETE"]:
            permissions.append(auth.ProjectDeletePermission)
        else:
            permissions.append(auth.AdminRolePermission)

        return [perm() for perm in permissions]

    def perform_create(self, serializer):
        if self.request.data.get('owner', None):
            serializer.save()
        else:
            serializer.save(owner=self.request.user)

    @action(detail=True, methods=['GET'], serializer_class=TaskSerializer)
    def tasks(self, request, pk):
        self.get_object() # force to call check_object_permissions
        queryset = Task.objects.filter(project_id=pk).order_by('-id')
        queryset = auth.filter_task_queryset(queryset, request.user)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True,
                context={"request": request})
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True,
            context={"request": request})
        return Response(serializer.data)

############

def get_site_id(site_name,bucket):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select id from uvdata_sites where site_name=%s and bucket = %s"
        cur.execute(query,(site_name,bucket))
        s_id = cur.fetchone()
        return(s_id)
    except (Exception, psycopg2.Error) as error:
        print("could not get cam ids qq3",error)
    finally:
        if (con):
            cur.close()
            con.close()

def get_date_id(s_id,date_name):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select id from uvdata_dates where date=%s and site_id=%s"
        cur.execute(query,(date_name,s_id,))
        d_id = cur.fetchone()
        print(d_id,flush=True)
        return(d_id)
    except (Exception, psycopg2.Error) as error:
        print("could not get cam ids qq3",error)
    finally:
        if (con):
            cur.close()
            con.close()

def get_classes(site_name):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select * from uvdata_sites where site_name=%s "
        cur.execute(query,(site_name,))
        s_id = cur.fetchone()
        check_list=[]
        query_classes = "select * from uvdata_sites_site_classes where sites_id=%s"
        cur.execute(query_classes,(s_id[0],))
        class_ids = cur.fetchall()
        for id_ in class_ids:
            query_class_names = "select * from uvdata_classes where id=%s"
            cur.execute(query_class_names,(id_[2],))
            c_name = cur.fetchone()
            check_list.append(c_name[1])
        return(check_list)
    except (Exception, psycopg2.Error) as error:
        print("could not get classes qq6",error)
    finally:
        if (con):
            cur.close()
            con.close()


def get_cam_id(c_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select cam_id from uvdata_cam_id where id=%s"
        cur.execute(query,(c_id,))
        c_name = cur.fetchone()
        return(c_name)
    except (Exception, psycopg2.Error) as error:
        print("could not get cam ids qq3",error)
    finally:
        if (con):
            cur.close()
            con.close()   



def unconfigured_cams(d_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select * from uvdata_cam_id where date_id=%s and configured=%s"
        cur.execute(query,(d_id,False,))
        c_list = []
        c_ids = cur.fetchall()
        for c in c_ids:
            c_list.append(c[0])
        return(c_list)
    except (Exception, psycopg2.Error) as error:
        print("could not get cam ids qq3",error)
    finally:
        if (con):
            cur.close()
            con.close()


def get_cam_ids(d_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select * from uvdata_cam_id where date_id=%s"
        cur.execute(query,(d_id,))
        c_list = []
        c_ids = cur.fetchall()
        for c in c_ids:
            c_list.append(c[0])
        return(c_list)
    except (Exception, psycopg2.Error) as error:
        print("could not get cam ids qq3",error)
    finally:
        if (con):
            cur.close()
            con.close()

def get_i_count(s_id,d_id,c_id,port):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select distinct (image_id) from uvdata_bbox where site_id=%s and date_id=%s and cam_id=%s and port is null"
        cur.execute(query,(s_id,d_id,c_id,))
        i_ids = cur.fetchall()
        list_1= []
        for i in i_ids:
            list_1.append(i)
        return(len(list_1))
    except (Exception, psycopg2.Error) as error:
        print("could not get f id qq10",error)
    finally:
        if (con):
            cur.close()
            con.close()


def get_seen_imgs(s_id,d_id,limit,port):
    human_annotated='human'
    count =0 
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query ="select distinct (filename) from uvdata_bbox where and box_type=%s and site_id=%s and date_id=%s and session=%s limit %s"
        cur.execute(query,(human_annotated,s_id,d_id,False,limit,))
        human_list = cur.fetchall()
        image_list = []
        for x in human_list:
            image_list.append(x[0])
     
        for y in image_list:
            query = "update uvdata_bbox set session=%s, port= %s where filename=%s and date_id = %s and box_type=%s"
            cur.execute(query,(True,port,y,d_id,human_annotated,)) 
            count = count +1 
            con.commit()
        return(image_list)
    except (Exception, psycopg2.Error) as error:
        print("could not get human anno qq4",error)
    finally:
        if (con):
            cur.close()
            con.close()
 


def get_image_roi(c_id,alert,port):
    import json
    import psycopg2
    import ast
    if len(ast.literal_eval(alert)) != 0:
        json_alerts = ast.literal_eval(alert)
        try:
            con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
            cur = con.cursor()
            query ="select distinct filename,cam_id from uvdata_bbox where box_type=%s  and cam_id=%s and session=%s and alerts=%s limit 1"
            cur.execute(query,('model',c_id,False, json.dumps(json_alerts),))
            human_list = cur.fetchall()
            image_list = []
            new_list = []
            for x in human_list:
                tmp ={}
                tmp['img'] = x[0]
                tmp['cam_id'] = x[1]
                query_url = "select  image_url from uvdata_images where cam_id=%s and filename=%s"
                cur.execute(query_url,(x[1],str(x[0])))
                tmp['url']=cur.fetchone()[0]
                image_list.append(tmp)

          
            print('printinhg image list',flush=True) 
            print(image_list)           
            for y in image_list:
                query = "update uvdata_bbox set session=%s, port= %s where filename=%s and  cam_id=%s  and box_type=%s"
                cur.execute(query,(True,port,y['img'],y['cam_id'],'model',))
                con.commit()
            return(image_list)
        except (Exception, psycopg2.Error) as error:
            print("get_image_roi function is throwing error",error)
        finally:
            if (con):
                cur.close()
                con.close()
  









def get_human_anno_date(d_id,seen,port,batch_number,alerts):
    import json
    import psycopg2
    import ast
    print(d_id,batch_number,port,alerts,flush=True)
    if len(ast.literal_eval(alerts)) == 0:
        if seen=="seen":
            human_annotated="human"
            count =0
            print(alerts,flush=True)
            try:
                con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
                cur = con.cursor()
                query ="select distinct filename,cam_id from uvdata_bbox where  box_type=%s and date_id=%s and session=%s  order by filename limit %s "
                cur.execute(query,(human_annotated,d_id,False,batch_number,))
                human_list = cur.fetchall()
                print(human_list,flush=True)
                image_list = []
                for x in human_list:
                    tmp ={}
                    tmp['img'] = x[0]
                    tmp['cam_id'] = x[1]

                    '''
                    get image url
                    '''
                    query = "select image_url from uvdata_images where filename=%s and cam_id=%s " 
                    cur.execute(query,(tmp['img'],tmp['cam_id']))
                    tmp['img_url']= cur.fetchall()[0][0]
                    print(tmp,flush=True)

                    image_list.append(tmp)

                for y in image_list:
                    print(y,flush=True)
                    query = "update uvdata_bbox set session=%s, port= %s where filename=%s and date_id = %s and cam_id=%s and box_type=%s"
                    cur.execute(query,(True,port,str(y['img']),d_id,y['cam_id'],human_annotated,))
                    count = count +1
                    con.commit()
                return(image_list)
            except (Exception, psycopg2.Error) as error:
                print("could not get human anno qq4",error)
            finally:
                if (con):
                    cur.close()
                    con.close()


        else:
            human_annotated="model"
            try:
                con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
                cur = con.cursor()
                query = "select distinct filename,cam_id from uvdata_bbox where  box_type=%s and date_id=%s and session=%s and completed=%s order by filename limit %s "
                cur.execute(query,(human_annotated,d_id,False,False,batch_number,))
                human_list = cur.fetchall()
                image_list = []
                new_list = []
                print(human_list,flush=True)
                for x in human_list:
                    tmp ={}
                    tmp['img'] = x[0]
                    tmp['cam_id'] = x[1]
                    '''
                    get image url
                    '''
                    query = "select image_url from uvdata_images where filename=%s and cam_id=%s "                           
                    cur.execute(query,(tmp['img'],tmp['cam_id']))
                    tmp['img_url'] = cur.fetchall()[0][0]
                    print(tmp,flush=True)

                    image_list.append(tmp)

                for y in image_list:
                    query = "update uvdata_bbox set session=%s, port = %s where filename=%s and date_id = %s and cam_id=%s  and box_type=%s"
                    cur.execute(query,(True,port,str(y['img']),d_id,y['cam_id'],human_annotated,))
                    con.commit()

                return(image_list)
            except (Exception, psycopg2.Error) as error:
                print("could not get human anno qq4",error)
            finally:
                if (con):
                    cur.close()
                    con.close()


    else:
        json_alerts = ast.literal_eval(alerts)
        if seen=="seen":
            human_annotated="human"
            count =0
            print(alerts,flush=True)
            try:
                con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
                cur = con.cursor()
                query ="select distinct filename,cam_id from uvdata_bbox where box_type=%s  and date_id=%s and session=%s and alerts=%s order by filename limit %s"
                cur.execute(query,(human_annotated,d_id,False, json.dumps(json_alerts),batch_number,))
                human_list = cur.fetchall()
                image_list = []
                new_list = []
                print(human_list,flush=True)
                for x in human_list:
                    tmp ={}
                    tmp['img'] = x[0]
                    tmp['cam_id'] = x[1]
                    tmp ={}
                    tmp['img'] = x[0]
                    tmp['cam_id'] = x[1]
                    '''
                    get image url
                    '''
                    query = "select image_url from uvdata_images where filename=%s and cam_id=%s "
                    cur.execute(query,(tmp['img'],tmp['cam_id']))
                    tmp['img_url'] = cur.fetchall()[0][0]
                    print(tmp,flush=True)

                    image_list.append(tmp)


                for y in image_list:
                    query = "update uvdata_bbox set session=%s, port= %s where filename=%s and date_id = %s and cam_id=%s  and box_type=%s"
                    cur.execute(query,(True,port,y['img'],d_id,y['cam_id'],human_annotated,))
                    count = count +1
                    con.commit()
                return(image_list)
            except (Exception, psycopg2.Error) as error:
                print("could not get human anno qq4",error)
            finally:
                if (con):
                    cur.close()
                    con.close()


        else:
            human_annotated="model"
            try:
                con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
                cur = con.cursor()
                query = "select distinct filenamei,cam_id from uvdata_bbox where box_type=%s  and date_id=%s and session=%s and completed=%s and alerts=%s order by filename limit %s "
                cur.execute(query,(human_annotated,d_id,False,False,json.dumps(json_alerts),limit,))
                human_list = cur.fetchall()
                print(human_list,flush=True)
                image_list = []
                new_list = []
                for x in human_list:
                    tmp ={}
                    tmp['img'] = x[0]
                    tmp['cam_id'] = x[1]
                    image_list.append(tmp)


                for x in image_list:
                    query = "update uvdata_bbox set session=%s, port = %s where filename=%s and date_id = %s and cam_id=%s  and box_type=%s"
                    cur.execute(query,(True,port,x['img'],d_id,x['cam_id'],human_annotated,))
                    con.commit()

                return(image_list)
            except (Exception, psycopg2.Error) as error:
                print("could not get human anno qq4",error)
            finally:
                if (con):
                    cur.close()
                    con.close()




def get_human_anno(s_id,d_id,c_id,seen,port,limit,alerts):
    import json
    import psycopg2
    import ast
    if len(ast.literal_eval(alerts)) == 0:
        if seen=="seen":
            human_annotated="human"
            count =0
            print(alerts,flush=True)
            try:
                con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
                cur = con.cursor()
                query ="select distinct (filename) from uvdata_bbox where cam_id=%s and box_type=%s and site_id=%s and date_id=%s and session=%s  limit %s"
                cur.execute(query,(c_id,human_annotated,s_id,d_id,False,limit,))
                human_list = cur.fetchall()
                image_list = []
                for x in human_list:
                    image_list.append(x[0])
         #           print(x[0],flush=True)


                for y in image_list:
                    query = "update uvdata_bbox set session=%s, port= %s where filename=%s and date_id = %s and cam_id = %s and box_type=%s"
                    cur.execute(query,(True,port,y,d_id,c_id,human_annotated,))
                    count = count +1
                    con.commit()
                return(image_list)
            except (Exception, psycopg2.Error) as error:
                print("could not get human anno qq4",error)
            finally:
                if (con):
                    cur.close()
                    con.close()

        else:
            human_annotated="model"
            try:
                con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
                cur = con.cursor()
                query = "select distinct (filename) from uvdata_bbox where cam_id=%s and box_type=%s and site_id=%s and date_id=%s and session=%s and completed=%s  limit %s "
                cur.execute(query,(c_id,human_annotated,s_id,d_id,False,False,limit,))
                human_list = cur.fetchall()
                image_list = []
                new_list = []
                for x in human_list:
                    image_list.append(x[0])
                for x in image_list:
                    query = "update uvdata_bbox set session=%s, port = %s where filename=%s and date_id = %s and cam_id = %s and box_type=%s"
                    cur.execute(query,(True,port,x,d_id,c_id,human_annotated,))
                    con.commit()

                return(image_list)
            except (Exception, psycopg2.Error) as error:
                print("could not get human anno qq4",error)
            finally:
                if (con):
                    cur.close()
                    con.close()

    else:
        json_alerts = ast.literal_eval(alerts) 
        if seen=="seen":
            human_annotated="human"
            count =0
            print(alerts,flush=True) 
            try:
                con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
                cur = con.cursor()
                query ="select distinct (filename) from uvdata_bbox where cam_id=%s and box_type=%s and site_id=%s and date_id=%s and session=%s and alerts=%s limit %s"
                cur.execute(query,(c_id,human_annotated,s_id,d_id,False, json.dumps(json_alerts),limit,))
                human_list = cur.fetchall()
                image_list = []
                new_list = []
                for x in human_list:
                    image_list.append(x[0])
 
                for y in image_list:
                    query = "update uvdata_bbox set session=%s, port= %s where filename=%s and date_id = %s and cam_id = %s and box_type=%s"
                    cur.execute(query,(True,port,y,d_id,c_id,human_annotated,)) 
                    count = count +1 
                    con.commit()
                return(image_list)
            except (Exception, psycopg2.Error) as error:
                print("could not get human anno qq4",error)
            finally:
                if (con):
                    cur.close()
                    con.close()


        else:
            human_annotated="model"
            try:
                con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
                cur = con.cursor()
                query = "select distinct (filename) from uvdata_bbox where cam_id=%s and box_type=%s and site_id=%s and date_id=%s and session=%s and completed=%s and alerts=%s limit %s "
                cur.execute(query,(c_id,human_annotated,s_id,d_id,False,False,json.dumps(json_alerts),limit,))
                human_list = cur.fetchall()
                image_list = []
                new_list = []
                for x in human_list:
                    image_list.append(x[0])

                for x in image_list:
                    query = "update uvdata_bbox set session=%s, port = %s where filename=%s and date_id = %s and cam_id = %s and box_type=%s"
                    cur.execute(query,(True,port,x,d_id,c_id,human_annotated,)) 
                    con.commit()
             
                return(image_list)
            except (Exception, psycopg2.Error) as error:
                print("could not get human anno qq4",error)
            finally:
                if (con):
                    cur.close()
                    con.close()


###########
class TaskFilter(filters.FilterSet):
    project = filters.CharFilter(field_name="project__name", lookup_expr="icontains")
    name = filters.CharFilter(field_name="name", lookup_expr="icontains")
    owner = filters.CharFilter(field_name="owner__username", lookup_expr="icontains")
    mode = filters.CharFilter(field_name="mode", lookup_expr="icontains")
    status = filters.CharFilter(field_name="status", lookup_expr="icontains")
    assignee = filters.CharFilter(field_name="assignee__username", lookup_expr="icontains")

    class Meta:
        model = Task
        fields = ("id", "project_id", "project", "name", "owner", "mode", "status",
            "assignee")

class TaskViewSet(auth.TaskGetQuerySetMixin, viewsets.ModelViewSet):
    queryset = Task.objects.all().prefetch_related(
            "label_set__attributespec_set",
            "segment_set__job_set",
        ).order_by('-id')
    serializer_class = TaskSerializer
    search_fields = ("name", "owner__username", "mode", "status")
    filterset_class = TaskFilter
    ordering_fields = ("id", "name", "owner", "status", "assignee")

    def get_permissions(self):
        http_method = self.request.method
        permissions = [IsAuthenticated]

        if http_method in SAFE_METHODS:
            permissions.append(auth.TaskAccessPermission)
        elif http_method in ["POST"]:
            permissions.append(auth.TaskCreatePermission)
        elif self.action == 'annotations' or http_method in ["PATCH", "PUT"]:
            permissions.append(auth.TaskChangePermission)
        elif http_method in ["DELETE"]:
            permissions.append(auth.TaskDeletePermission)
        else:
            permissions.append(auth.AdminRolePermission)

        return [perm() for perm in permissions]

    def perform_create(self, serializer):
        if self.request.data.get('owner', None):
            serializer.save()
        else:
            serializer.save(owner=self.request.user)

    def perform_destroy(self, instance):
        task_dirname = instance.get_task_dirname()
        super().perform_destroy(instance)
        shutil.rmtree(task_dirname, ignore_errors=True)

    @action(detail=True, methods=['GET'], serializer_class=JobSerializer)
    def jobs(self, request, pk):
        self.get_object() # force to call check_object_permissions
        queryset = Job.objects.filter(segment__task_id=pk)
        serializer = JobSerializer(queryset, many=True,
            context={"request": request})

        return Response(serializer.data)

    @action(detail=True, methods=['POST'], serializer_class=TaskDataSerializer)
    def data(self, request, pk):
        from django.http import QueryDict
        import psycopg2
        import math
        import time
        import random
        import os.path
        import json
        from os import path
        import ast
#############

        db_task= self.get_object()
        data_path = os.environ['ANNOTATE_PATH'] ###/data/downloads/TestOrg/
        n         = float(os.environ['SPLIT'])  ###0-100%
        port      = os.environ['CVAT_PORT']
        seen      = os.environ['SEEN']
        alerts    = os.environ['FILTERS']
        project_name = os.environ['p_name']
        site_name = os.environ['s_name']
        date_name = os.environ['d_name']
        bucket    = os.environ['bucket']
        s_id      = get_site_id(site_name,bucket)[0]
        print(s_id,date_name,flush=True)
        d_id      = get_date_id(s_id,date_name)[0]
        site_classes  = get_classes(site_name)
   
        if  os.environ['CVAT_TYPE'] == "date-roi":
            print('date roi is chosen')
            '''
            get cams wi	th rotation null from cam tables
            '''
            total_c_ids  = unconfigured_cams(d_id)


            '''
            limit them to 1000 cams 
            '''
            if path.exists('./cam_ids_{}.txt'.format(str(port))) == True:
                with open('cam_ids_{}.txt'.format(str(port))) as data:
                    cam_ids=json.load(data)
                c_ids = Diff(total_c_ids,cam_ids["c_ids"])[:int(os.environ['BATCH_NUMBER'])]
                for x in c_ids:
                    cam_ids["c_ids"].append(x)
                tmp ={}
                tmp["c_ids"] = cam_ids["c_ids"]
                print(tmp,flush=True)
                with open('cam_ids_{}.txt'.format(str(port)),'w') as f:
                    json.dump(tmp,f)
 
            else:
                c_ids = total_c_ids[:int(os.environ['BATCH_NUMBER'])]
                data = {}
                data["c_ids"]=c_ids
                print(data,flush=True)
                with open('./cam_ids_{}.txt'.format(str(port)), 'w') as f:
                    json.dump(data, f)
            

            '''
            extract one image per cam with specific alert for maximum benefit
            '''
                        
            
            anno_list = []
            cam_count = 0
            for c in c_ids:
                if project_name == 'Cattleya':
                    alert = "{'no_alert': False, 'ir': False, 'head': True, 'tamper': False, 'person': False}"

                get_image = get_image_roi(c,alert,port)
                if get_image:
                    anno_list.append(get_image)
                    cam_count= cam_count + 1
                else:
                    if project_name == 'Cattleya':
                        alert = "{'no_alert': False, 'ir': False, 'head': False, 'tamper': False, 'person': True}"
                    get_next_image = get_image_roi(c,alert,port)
                    if get_next_image:
                        anno_list.append(get_next_image)
                        cam_count = cam_count + 1
                    else:
                        continue


            print(cam_count, flush=True)
            print(anno_list,flush=True) 

            human_list=[] 
            non_duplicates = []
            all_files = []

         #   for x in anno_list:
         #       non_duplicates.append(x[])
 
            if project_name == 'Cattleya':
                for x in anno_list:
                    human_list.append("{}".format(x[0]['url']))
        

            if len(human_list) == 0:
                raise NotImplementedError("all images are asssigned to tasks")
            img_dict={}
            for idx,val in enumerate(human_list):
                remote_f = 'remote_files[{}]'.format(idx)
                img_dict[remote_f] = val
            data = QueryDict('', mutable=True)
            data.update(img_dict)
            serializer = TaskDataSerializer(db_task, data=data)

            if serializer.is_valid(raise_exception=True):
                serializer.save()
                task.create(db_task.id, serializer.data,seen,d_id,port)
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)



 
        if os.environ['CVAT_TYPE'] == "date-annotate":
            total_c_ids = get_cam_ids(d_id)
            '''
            transfer c_ids in batches
            '''
            if len(ast.literal_eval(os.environ['CAMS'])) == 0:
                c_ids = total_c_ids
                if str(os.environ['BATCH_TYPE']) == 'cam':
                    print('cam batching being carried out',flush=True)
                    if path.exists('./cam_ids_{}.txt'.format(str(port))) == True:
                        with open('cam_ids_{}.txt'.format(str(port))) as data:
                            cam_ids=json.load(data)
                        c_ids = Diff(total_c_ids,cam_ids["c_ids"])[:int(os.environ['BATCH_NUMBER'])]
                        for x in c_ids:
                            cam_ids["c_ids"].append(x)
                        tmp ={}
                        tmp["c_ids"] = cam_ids["c_ids"]
                        print(tmp,flush=True)
                        with open('cam_ids_{}.txt'.format(str(port)),'w') as f:
                            json.dump(tmp,f)

                    else:
                        c_ids = total_c_ids[:int(os.environ['BATCH_NUMBER'])]
                        data = {}
                        data["c_ids"]=c_ids
                        print(data,flush=True)
                        with open('./cam_ids_{}.txt'.format(str(port)), 'w') as f:
                            json.dump(data, f)
 
            if len(ast.literal_eval(os.environ['CAMS'])) != 0:
                c_ids = ast.literal_eval(os.environ['CAMS']) 

            list_1   = []
            human_list = []
            l_count = 0
            limit_check=[]
            total_start_time = time.time()
 

            if str(os.environ['BATCH_TYPE']) == 'cam':

                for c in c_ids:
                    tmp={}
                    cam_name  = get_cam_id(c)[0]
                    i_count   = get_i_count(s_id,d_id,c,port)
                    if i_count == 0:
                        continue

                    limit     = math.ceil((n*i_count)/100)
                    tmp2={}
                    tmp2['l'] = limit
                    tmp2['i'] = i_count
                    start_time = time.time()

                    anno_list = get_human_anno(s_id,d_id,c,seen,port,limit,alerts)
                    
                    end_time = time.time()
                    tmp2['len_anno_list']= len(anno_list)
                    tmp2['anno_list'] = anno_list
                    limit_check.append(tmp2)
                    
                    if len(anno_list) == 0:
                        continue
                    for x in range(limit):
                        if x < len(anno_list):
                            tmp={}
                            tmp["filename"] = anno_list[x]
                            tmp["c_name"]   = cam_name
                            list_1.append(tmp)

                total_end_time=time.time()
                non_duplicates = []
                all_files = []
                for x in list_1:
                    if x['filename'] not in all_files:
                        all_files.append(x['filename'])
                        tmp={}
                        tmp['filename'] = x['filename']
                        tmp['c_name'] = x['c_name']
                        non_duplicates.append(tmp)

                
                if project_name == 'Cattleya':
                    for x in non_duplicates:
                        img_path = "http://34.93.111.56:8000/{}/{}/{}".format(data_path,x["c_name"],x["filename"]).replace(' ','%20')
                        human_list.append(img_path)
   
                if project_name == 'SynCom':
                    for x in non_duplicates:
                        img_path = "http://34.93.111.56:8000/{}/{}/{}".format(data_path,x["c_name"],x["filename"]).replace(' ','%20')
                        human_list.append(img_path)

                if project_name == 'City of Kitchener, Ontario':
                    for x in non_duplicates:
                        img_path = 'http://34.93.111.56:8000/data/downloads/videoanalytics/{}/{}/{}/{}/{}'.format(project_name,site_name,date_name,x["c_name"],x["filename"]).replace(' ','%20')
                        human_list.append(img_path)


                if len(human_list) == 0:
                    raise NotImplementedError("all images are asssigned to tasks")
                img_dict={}
                for idx,val in enumerate(human_list):
                    remote_f = 'remote_files[{}]'.format(idx)
                    img_dict[remote_f] = val
                data = QueryDict('', mutable=True)
                data.update(img_dict)
                serializer = TaskDataSerializer(db_task, data=data)
   
                if serializer.is_valid(raise_exception=True):
                    serializer.save()
                    task.create(db_task.id, serializer.data,seen,d_id,port)
                    return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
   



            if str(os.environ['BATCH_TYPE']) == 'images':
                anno_list = get_human_anno_date(d_id,seen,port,int(os.environ['BATCH_NUMBER']),alerts)
                print('printing anno list',flush=True)
                print(anno_list)
                non_duplicates = []
                all_files = []
                for x in anno_list:
                    if x['img'] not in all_files:
                        all_files.append(x['img'])
                        tmp={}
                        tmp['filename'] = x['img']
                        cam_name  = get_cam_id(x['cam_id'])[0]                    
                        tmp['c_name'] = cam_name
                        print(cam_name,flush=True)
                        tmp['img_url'] = x['img_url']
                        non_duplicates.append(tmp)
               
  
                '''
                remove blankspaces from project site and date

                '''   
                
                if " " in project_name:
                    p_path = project_name.replace(' ','_')
                else:
                    p_path = project_name
                if "(" in project_name:
                    p_path = p_path.replace('(','')
                if ")" in project_name:
                    p_path = p_path.replace(')','')


                if " " in site_name:
                    s_path = site_name.replace(' ','_')
                else:
                    s_path = site_name
                if "(" in site_name:
                    s_path = s_path.replace('(','')
                if ")" in site_name:
                    s_path = s_path.replace(')','')


                 
                for x in non_duplicates:
                    human_list.append("{}".format(x['img_url'])) 



   #     # batching done here
                if str(os.environ['BATCH_TYPE']) == 'images':
                    print('image batching being carried out',flush=True)
                    if path.exists('./i_ids_{}.txt'.format(str(port))) == True:
                        with open('i_ids_{}.txt'.format(str(port))) as data:
                            image_ids=json.load(data)

                        i_ids = Diff(human_list,image_ids["i_ids"])[:int(os.environ['BATCH_NUMBER'])]
                        for x in i_ids:
                            image_ids["i_ids"].append(x)
                        tmp ={}
                        tmp["i_ids"] = image_ids["i_ids"]
                        with open('i_ids_{}.txt'.format(str(port)),'w') as f:
                            json.dump(tmp,f)
                        human_list = i_ids

                    else:
                        i_ids = human_list[:int(os.environ['BATCH_NUMBER'])]
                        data = {}
                        data["i_ids"]=i_ids
                        with open('./i_ids_{}.txt'.format(str(port)), 'w') as f:
                            json.dump(data, f)
                        human_list = i_ids


            

                if len(human_list) == 0:
                    raise NotImplementedError("all images are asssigned to tasks")
                img_dict={}
                for idx,val in enumerate(human_list):
                    remote_f = 'remote_files[{}]'.format(idx)
                    img_dict[remote_f] = val
                data = QueryDict('', mutable=True)
                data.update(img_dict)
                serializer = TaskDataSerializer(db_task, data=data)
   
                if serializer.is_valid(raise_exception=True):
                    serializer.save()
                    task.create(db_task.id, serializer.data,seen,d_id,port)
                    return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

###########
    #    if seen == "seen":
    #        total_c_ids = get_cam_ids(d_id)

    #        '''
    #        transfer c_ids in batches
    #        '''
    #        if len(ast.literal_eval(os.environ['CAMS'])) == 0:
    #            c_ids = total_c_ids
    #            if str(os.environ['BATCH_TYPE']) == 'cam':
    #                print('cam batching being carried out',flush=True)
    #                if path.exists('./cam_ids_{}.txt'.format(str(port))) == True:
    #                    with open('cam_ids_{}.txt'.format(str(port))) as data:
    #                        cam_ids=json.load(data)
    #                    c_ids = Diff(total_c_ids,cam_ids["c_ids"])[:int(os.environ['BATCH_NUMBER'])]
    #                    for x in c_ids:
    #                        cam_ids["c_ids"].append(x)
    #                    tmp ={}
    #                    tmp["c_ids"] = cam_ids["c_ids"]
    #                    print(tmp,flush=True)
    #                    with open('cam_ids_{}.txt'.format(str(port)),'w') as f:
    #                        json.dump(tmp,f)

    #                else:
    #                    c_ids = total_c_ids[:int(os.environ['BATCH_NUMBER'])]
    #                    data = {}
    #                    data["c_ids"]=c_ids
    #                    print(data,flush=True)
    #                    with open('./cam_ids_{}.txt'.format(str(port)), 'w') as f:
    #                        json.dump(data, f)

    #        if len(ast.literal_eval(os.environ['CAMS'])) != 0:
    #            c_ids = ast.literal_eval(os.environ['CAMS'])

#   #         if len(ast.literal_eval(os.environ['CAMS'])) == 0:
#   #             if path.exists('./cam_ids_{}.txt'.format(str(port))) == True:
#   #                 with open('cam_ids_{}.txt'.format(str(port))) as data:
#   #                     cam_ids=json.load(data)
#   #                 c_ids = Diff(total_c_ids,cam_ids["c_ids"])[:200]
#   #                 for x in c_ids:
#   #                     cam_ids["c_ids"].append(x)
#   #                 tmp ={}
#   #                 tmp["c_ids"] = cam_ids["c_ids"]
#   #                 print(tmp,flush=True)
#   #                 with open('cam_ids_{}.txt'.format(str(port)),'w') as f:
#   #                     json.dump(tmp,f)
#
#   #             else:
#   #                 c_ids = total_c_ids[:200]
#   #                 data = {}
#   #                 data["c_ids"]=c_ids
#   #                 print(data,flush=True)
#   #                 with open('./cam_ids_{}.txt'.format(str(port)), 'w') as f:
#   #                     json.dump(data, f)
#
#   #         if len(ast.literal_eval(os.environ['CAMS'])) != 0:
#   #             c_ids = ast.literal_eval(os.environ['CAMS'])

    #        list_1   = []
    #        human_list = []
    #        l_count = 0
    #        limit_check=[]
    #        total_start_time = time.time()
    #        for c in c_ids:
    #            tmp={}
    #            cam_name  = get_cam_id(c)[0]
    #            i_count   = get_i_count(s_id,d_id,c,port)
    #            if i_count == 0:
    #                continue
    #            limit     = math.ceil((n*i_count)/100)
    #            tmp2={}
    #            tmp2['l'] = limit
    #            tmp2['i'] = i_count
    #            start_time = time.time()
    #            anno_list = get_human_anno(s_id,d_id,c,seen,port,limit,alerts)
    #    #        if project_name=="Cattleya":
    #            end_time = time.time()
    #            tmp2['len_anno_list']= len(anno_list)
    #            tmp2['anno_list'] = anno_list
    #            limit_check.append(tmp2)
    #    
    #            if len(anno_list) == 0:
    #                continue
    #            for x in range(limit):
    #                if x < len(anno_list):
    #                    tmp={}
    #                    tmp["filename"] = anno_list[x]
    #                    tmp["c_name"]   = cam_name
    #                    list_1.append(tmp)
    #        total_end_time = time.time()
    #        print('total exec time is .........',flush=True)
    #        print(total_end_time-total_start_time,flush=True)
    #        print(list_1,flush=True)

    #        non_duplicates = []
    #        all_files = []
    #        for x in list_1:
    #            if x['filename'] not in all_files:
    #                all_files.append(x['filename'])
    #                tmp={}
    #                tmp['filename'] = x['filename']
    #                tmp['c_name'] = x['c_name']
    #                non_duplicates.append(tmp)

    #        if project_name == 'Cattleya':
    #            for x in non_duplicates:
    #                human_list.append("http://34.93.111.56:8000/data/downloads/{}/{}/{}/{}/{}".format(project_name,site_name,date_name,x["c_name"],x["filename"]))

    #        if project_name == 'SynCom':
    #            for x in non_duplicates:
    #                human_list.append("http://34.93.111.56:8000/data/downloads/videoanalytics/{}/{}/{}/{}/{}".format(project_name,site_name,date_name,x["c_name"],x["filename"]))

    ####### batching done here
    #        print(len(human_list),flush=True)
    #        if os.environ['BATCH_TYPE'] == 'images':
    #            print('image batching being carried out',flush=True)
    #            if path.exists('./i_ids_{}.txt'.format(str(port))) == True:
    #                with open('i_ids_{}.txt'.format(str(port))) as data:
    #                    image_ids=json.load(data)

    #                i_ids = Diff(human_list,image_ids["i_ids"])[:int(os.environ['BATCH_NUMBER'])]
    #                for x in i_ids:
    #                    image_ids["i_ids"].append(x)
    #                tmp ={}
    #                tmp["i_ids"] = image_ids["i_ids"]
    #                with open('i_ids_{}.txt'.format(str(port)),'w') as f:
    #                    json.dump(tmp,f)
    #                human_list = i_ids

    #            else:
    #                i_ids = human_list[:int(os.environ['BATCH_NUMBER'])]
    #                data = {}
    #                data["i_ids"]=i_ids
    #                with open('./i_ids_{}.txt'.format(str(port)), 'w') as f:
    #                    json.dump(data, f)
    #                human_list = i_ids




    #        if len(human_list) == 0:
    #            raise NotImplementedError("all images are asssigned to tasks")
    #        img_dict={}
    #        for idx,val in enumerate(human_list):
    #            remote_f = 'remote_files[{}]'.format(idx)
    #            img_dict[remote_f] = val
    #        data = QueryDict('', mutable=True)
    #        data.update(img_dict)
    #        serializer = TaskDataSerializer(db_task, data=data)
    #   
    #        if serializer.is_valid(raise_exception=True):
    #            serializer.save()
    #            task.create(db_task.id, serializer.data,seen,d_id,port)
    #            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)



##########

    @action(detail=True, methods=['GET', 'DELETE', 'PUT', 'PATCH'],
        serializer_class=LabeledDataSerializer)
    def annotations(self, request, pk):
        self.get_object() # force to call check_object_permissions
        if request.method == 'GET':
            data = annotation.get_task_data(pk, request.user)
            serializer = LabeledDataSerializer(data=data)
            if serializer.is_valid(raise_exception=True):
                return Response(serializer.data)
        elif request.method == 'PUT':
            if request.query_params.get("format", ""):
                return load_data_proxy(
                    request=request,
                    rq_id="{}@/api/v1/tasks/{}/annotations/upload".format(request.user, pk),
                    rq_func=annotation.load_task_data,
                    pk=pk,
                )
            else:
                serializer = LabeledDataSerializer(data=request.data)
                if serializer.is_valid(raise_exception=True):
                    data = annotation.put_task_data(pk, request.user, serializer.data)
                    return Response(data)
        elif request.method == 'DELETE':
            annotation.delete_task_data(pk, request.user)
            return Response(status=status.HTTP_204_NO_CONTENT)
        elif request.method == 'PATCH':
            action = self.request.query_params.get("action", None)
            if action not in annotation.PatchAction.values():
                raise serializers.ValidationError(
                    "Please specify a correct 'action' for the request")
            serializer = LabeledDataSerializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                try:
                    data = annotation.patch_task_data(pk, request.user, serializer.data, action)
                except (AttributeError, IntegrityError) as e:
                    return Response(data=str(e), status=status.HTTP_400_BAD_REQUEST)
                return Response(data)

    @action(detail=True, methods=['GET'], serializer_class=None,
        url_path='annotations/(?P<filename>[^/]+)')
    def dump(self, request, pk, filename):
        filename = re.sub(r'[\\/*?:"<>|]', '_', filename)
        username = request.user.username
        db_task = self.get_object() # call check_object_permissions as well
        timestamp = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
        action = request.query_params.get("action")
        if action not in [None, "download"]:
            raise serializers.ValidationError(
                "Please specify a correct 'action' for the request")

        dump_format = request.query_params.get("format", "")
        try:
            db_dumper = AnnotationDumper.objects.get(display_name=dump_format)
        except ObjectDoesNotExist:
            raise serializers.ValidationError(
                "Please specify a correct 'format' parameter for the request")

        file_path = os.path.join(db_task.get_task_dirname(),
            "{}.{}.{}.{}".format(filename, username, timestamp, db_dumper.format.lower()))

        queue = django_rq.get_queue("default")
        rq_id = "{}@/api/v1/tasks/{}/annotations/{}".format(username, pk, filename)
        rq_job = queue.fetch_job(rq_id)

        if rq_job:
            if rq_job.is_finished:
                if not rq_job.meta.get("download"):
                    if action == "download":
                        rq_job.meta[action] = True
                        rq_job.save_meta()
                        return sendfile(request, rq_job.meta["file_path"], attachment=True,
                            attachment_filename="{}.{}".format(filename, db_dumper.format.lower()))
                    else:
                        return Response(status=status.HTTP_201_CREATED)
                else: # Remove the old dump file
                    try:
                        os.remove(rq_job.meta["file_path"])
                    except OSError:
                        pass
                    finally:
                        rq_job.delete()
            elif rq_job.is_failed:
                exc_info = str(rq_job.exc_info)
                rq_job.delete()
                return Response(data=exc_info, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                return Response(status=status.HTTP_202_ACCEPTED)

        rq_job = queue.enqueue_call(
            func=annotation.dump_task_data,
            args=(pk, request.user, file_path, db_dumper,
                  request.scheme, request.get_host()),
            job_id=rq_id,
        )
        rq_job.meta["file_path"] = file_path
        rq_job.save_meta()

        return Response(status=status.HTTP_202_ACCEPTED)

    @action(detail=True, methods=['GET'], serializer_class=RqStatusSerializer)
    def status(self, request, pk):
        self.get_object() # force to call check_object_permissions
        response = self._get_rq_response(queue="default",
            job_id="/api/{}/tasks/{}".format(request.version, pk))
        serializer = RqStatusSerializer(data=response)

        if serializer.is_valid(raise_exception=True):
            return Response(serializer.data)

    @staticmethod
    def _get_rq_response(queue, job_id):
        queue = django_rq.get_queue(queue)
        job = queue.fetch_job(job_id)
        response = {}
        if job is None or job.is_finished:
            response = { "state": "Finished" }
        elif job.is_queued:
            response = { "state": "Queued" }
        elif job.is_failed:
            response = { "state": "Failed", "message": job.exc_info }
        else:
            response = { "state": "Started" }
            if 'status' in job.meta:
                response['message'] = job.meta['status']

        return response

    @action(detail=True, methods=['GET'], serializer_class=ImageMetaSerializer,
        url_path='frames/meta')
    def data_info(self, request, pk):
        try:
            db_task = self.get_object() # call check_object_permissions as well
            meta_cache_file = open(db_task.get_image_meta_cache_path())
        except OSError:
            task.make_image_meta_cache(db_task)
            meta_cache_file = open(db_task.get_image_meta_cache_path())

        data = literal_eval(meta_cache_file.read())
        serializer = ImageMetaSerializer(many=True, data=data['original_size'])
        if serializer.is_valid(raise_exception=True):
            return Response(serializer.data)

    @action(detail=True, methods=['GET'], serializer_class=None,
        url_path='frames/(?P<frame>\d+)')
    def frame(self, request, pk, frame):
        """Get a frame for the task"""

        try:
            # Follow symbol links if the frame is a link on a real image otherwise
            # mimetype detection inside sendfile will work incorrectly.
            db_task = self.get_object()
            path = os.path.realpath(db_task.get_frame_path(frame))
            return sendfile(request, path)
        except Exception as e:
            slogger.task[pk].error(
                "cannot get frame #{}".format(frame), exc_info=True)
            return HttpResponseBadRequest(str(e))

class JobViewSet(viewsets.GenericViewSet,
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin):
    queryset = Job.objects.all().order_by('id')
    serializer_class = JobSerializer

    def get_permissions(self):
        http_method = self.request.method
        permissions = [IsAuthenticated]

        if http_method in SAFE_METHODS:
            permissions.append(auth.JobAccessPermission)
        elif http_method in ["PATCH", "PUT", "DELETE"]:
            permissions.append(auth.JobChangePermission)
        else:
            permissions.append(auth.AdminRolePermission)

        return [perm() for perm in permissions]


    @action(detail=True, methods=['GET', 'DELETE', 'PUT', 'PATCH'],
        serializer_class=LabeledDataSerializer)
    def annotations(self, request, pk):
        self.get_object() # force to call check_object_permissions
        if request.method == 'GET':
            data = annotation.get_job_data(pk, request.user)
            return Response(data)
        elif request.method == 'PUT':
            if request.query_params.get("format", ""):
                return load_data_proxy(
                    request=request,
                    rq_id="{}@/api/v1/jobs/{}/annotations/upload".format(request.user, pk),
                    rq_func=annotation.load_job_data,
                    pk=pk,
                )
            else:
                serializer = LabeledDataSerializer(data=request.data)
                if serializer.is_valid(raise_exception=True):
                    try:
                        data = annotation.put_job_data(pk, request.user, serializer.data)
                    except (AttributeError, IntegrityError) as e:
                        return Response(data=str(e), status=status.HTTP_400_BAD_REQUEST)
                    return Response(data)
        elif request.method == 'DELETE':
            annotation.delete_job_data(pk, request.user)
            return Response(status=status.HTTP_204_NO_CONTENT)
        elif request.method == 'PATCH':
            action = self.request.query_params.get("action", None)
            if action not in annotation.PatchAction.values():
                raise serializers.ValidationError(
                    "Please specify a correct 'action' for the request")
            serializer = LabeledDataSerializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                try:
                    data = annotation.patch_job_data(pk, request.user,
                        serializer.data, action)
                except (AttributeError, IntegrityError) as e:
                    return Response(data=str(e), status=status.HTTP_400_BAD_REQUEST)
                return Response(data)

class UserViewSet(viewsets.GenericViewSet, mixins.ListModelMixin,
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin):
    queryset = User.objects.all().order_by('id')
    http_method_names = ['get', 'post', 'head', 'patch', 'delete']

    def get_serializer_class(self):
        user = self.request.user
        if user.is_staff:
            return UserSerializer
        else:
            is_self = int(self.kwargs.get("pk", 0)) == user.id or \
                self.action == "self"
            if is_self and self.request.method in SAFE_METHODS:
                return UserSerializer
            else:
                return BasicUserSerializer

    def get_permissions(self):
        permissions = [IsAuthenticated]
        user = self.request.user

        if not self.request.method in SAFE_METHODS:
            is_self = int(self.kwargs.get("pk", 0)) == user.id
            if not is_self:
                permissions.append(auth.AdminRolePermission)

        return [perm() for perm in permissions]

    @action(detail=False, methods=['GET'])
    def self(self, request):
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(request.user, context={ "request": request })
        return Response(serializer.data)

class PluginViewSet(viewsets.ModelViewSet):
    queryset = Plugin.objects.all()
    serializer_class = PluginSerializer

    # @action(detail=True, methods=['GET', 'PATCH', 'PUT'], serializer_class=None)
    # def config(self, request, name):
    #     pass

    # @action(detail=True, methods=['GET', 'POST'], serializer_class=None)
    # def data(self, request, name):
    #     pass

    # @action(detail=True, methods=['GET', 'DELETE', 'PATCH', 'PUT'],
    #     serializer_class=None, url_path='data/(?P<id>\d+)')
    # def data_detail(self, request, name, id):
    #     pass


    @action(detail=True, methods=['GET', 'POST'], serializer_class=RqStatusSerializer)
    def requests(self, request, name):
        pass

    @action(detail=True, methods=['GET', 'DELETE'],
        serializer_class=RqStatusSerializer, url_path='requests/(?P<id>\d+)')
    def request_detail(self, request, name, rq_id):
        pass

def rq_handler(job, exc_type, exc_value, tb):
    job.exc_info = "".join(
        traceback.format_exception_only(exc_type, exc_value))
    job.save()
    if "tasks" in job.id.split("/"):
        return task.rq_handler(job, exc_type, exc_value, tb)

    return True

def load_data_proxy(request, rq_id, rq_func, pk):
    queue = django_rq.get_queue("default")
    rq_job = queue.fetch_job(rq_id)
    upload_format = request.query_params.get("format", "")

    if not rq_job:
        serializer = AnnotationFileSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            try:
                db_parser = AnnotationLoader.objects.get(pk=upload_format)
            except ObjectDoesNotExist:
                raise serializers.ValidationError(
                    "Please specify a correct 'format' parameter for the upload request")

            anno_file = serializer.validated_data['annotation_file']
            fd, filename = mkstemp(prefix='cvat_{}'.format(pk))
            with open(filename, 'wb+') as f:
                for chunk in anno_file.chunks():
                    f.write(chunk)
            rq_job = queue.enqueue_call(
                func=rq_func,
                args=(pk, request.user, filename, db_parser),
                job_id=rq_id
            )
            rq_job.meta['tmp_file'] = filename
            rq_job.meta['tmp_file_descriptor'] = fd
            rq_job.save_meta()
    else:
        if rq_job.is_finished:
            os.close(rq_job.meta['tmp_file_descriptor'])
            os.remove(rq_job.meta['tmp_file'])
            rq_job.delete()
            return Response(status=status.HTTP_201_CREATED)
        elif rq_job.is_failed:
            os.close(rq_job.meta['tmp_file_descriptor'])
            os.remove(rq_job.meta['tmp_file'])
            exc_info = str(rq_job.exc_info)
            rq_job.delete()
            return Response(data=exc_info, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return Response(status=status.HTTP_202_ACCEPTED)
