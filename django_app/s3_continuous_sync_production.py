import sys
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "uvlearn.settings")
os.system("export DJANGO_SETTINGS_MODULE=uvlearn.settings")
from uvlearn.settings import BASE_DIR
sys.path.append(BASE_DIR)
import django
django.setup()
import psycopg2
import json
import glob
import shutil
from download_s3.models import projects_s3,sites_s3,dates_s3,images_s3,cams_s3,images_s3
import boto3
import botocore
client = boto3.client('s3',aws_access_key_id='AKIARX6S2JWR245EG3UD',aws_secret_access_key='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')
paginator = client.get_paginator('list_objects')
################
#bucket_name = "uploads.live.uncannysurveillance.com"


def get_projects_s3(bucket):
    '''
    searches s3 for all projects in bucket
    structure is a list of projects
    '''
    response_data = []
    a = paginator.paginate(Bucket=bucket,Delimiter='/')
    for result in a:
        for prefix in result.get('CommonPrefixes'):
            response_data.append(prefix.get('Prefix').replace('/',''))
    return response_data
    

 
def get_projects_psql(bucket):
    '''
    searches for all projects within postgres 
    structure is a list of project
    '''
    response_data = []
    query = projects_s3.objects.filter(bucket=bucket).values("project")
    for x in query:
        response_data.append(x["project"])
    return response_data


def diff(li1,li2):
    return (list(set(li1) - set(li2)))


def send_psql_projects(updated_projects,counter,bucket):
    '''
    updated_projects - project to be uploaded
    counter - last count in projects_s3 db
    '''
    next_count = counter + 1
    for x in updated_projects:
        try:
            con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",port=5432,user="postgres",password="uvdbuvdb")
            cur = con.cursor()
            cur.execute("insert into download_s3_projects_s3 (project,bucket) values (%s,%s)",(x,bucket))
            con.commit()
            next_count = next_count + 1
        except (Exception, psycopg2.Error) as error:
            print("could not insert in s3_projects",error)
        finally:
            if (con):
                cur.close()
                con.close()

def make_projects_dir(project_s3_list,base_dir):
    for p in project_s3_list:
        if p == "":
            continue
        if " " in p:
            p = p.replace(" ","_")

        
        dir_name = "{}{}".format(base_dir,p)
        if os.path.exists(dir_name) == False:
            os.mkdir(dir_name,mode=0o777)

#################

def get_sites_s3(bucket):
    '''
    1. fetch all projects in s3
    2. create prefix for all projects 
    3. Create a list of sites which contain key CommonPrefixes
    4. Create a dictionary with keys as project names and sites as values
    ''' 
    response_data = {}
    s3_list  = projects_s3.objects.filter(bucket=bucket).values("project")
    for project in s3_list:
        prefix = "{}/".format(project['project'])
        a = paginator.paginate(Bucket=bucket,Delimiter='/',Prefix=prefix)
        site_list =[]
        for result in a:
            if "CommonPrefixes" not in result:
                continue
            else:
                for site in result['CommonPrefixes']:
                    a = site["Prefix"].split('/',2)[1]
                    site_list.append(a)
        response_data[project['project']]= site_list

    return response_data

############sends a list of sites from psql with keys of the project ex [{"project_name_1":[site1,site2,]},{"project_name_2":[site3,site4]}]

def get_sites_psql(bucket):
    '''
    1. fetch all projects in postgres
    2. Create a dictionary with keys as project names and sites as values
    '''

    response_data = {}
    query = sites_s3.objects.filter(bucket=bucket).values("project_id").distinct()
    for x in query:
        p_id = x['project_id']
        p_name = projects_s3.objects.filter(id=p_id).get(bucket=bucket).project
        site_query = sites_s3.objects.filter(project_id=p_id).filter(bucket=bucket)
        s_list=[]
        for y in site_query:
            s_list.append(y.site)
        response_data[p_name] = s_list
     
    return response_data

#############updates psql table
def update_psql_sites(p_id,site,id_counter,bucket):
    ''' Updates all the the sites under a project y
        p_id - project id 
        site - site name 
        id_counter - the last id in the table
    '''
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",port=5432,user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute("insert into download_s3_sites_s3 (site,project_id,bucket) values (%s,%s,%s)",(site,p_id,bucket))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not insert in s3_projects",error)
    finally:
        if (con):
            cur.close()
            con.close()


def send_psql_sites(site_s3_list,site_psql_list,count,bucket):
    '''
    1. Iterates over site_s3_list 
    2. Iterates over site_psql_list
    3. Find all dates under psql and s3 list with common Project and create residual list 
    4. Upload the residual list
    '''
    id_counter = count + 1
    for k1,v1 in site_s3_list.items():
        for k2,v2 in site_psql_list.items():
            if k1 == k2:
                
                p_id = projects_s3.objects.filter(bucket=bucket).get(project=k1).id
                list_psql = v2
                list_s3   = v1
                residual_list = diff(list_s3,list_psql)
                for site in residual_list:
                    update_psql_sites(p_id,site,id_counter,bucket)
                    id_counter = id_counter + 1

    ''' 
    1. psql_list - projects for within postgres
    2. s3_list - projects from within s3
    3. remaining_projects- intersection of psql_list and s3_list
    4. Upload the remaining sites
    '''   
    psql_list = [] 
    new_project_query = sites_s3.objects.filter(bucket=bucket).values("project_id").distinct()
    for x in new_project_query:
        psql_list.append(projects_s3.objects.get(id =x["project_id"]).project)

    s3_list = [] 
    for k,v in site_s3_list.items():
        s3_list.append(k)
  
    remaining_projects = diff(s3_list,psql_list)
    for k,sites in site_s3_list.items():
        for p in remaining_projects:
            if k==p:           
                p_id = projects_s3.objects.filter(bucket=bucket).get(project=k).id 
                for site in sites:
                    update_psql_sites(p_id,site,id_counter,bucket)
                    id_counter=id_counter+1




def make_sites_dir(site_s3_list,base_dir):
    for k,vals in site_s3_list.items():
       if k == "":
           continue
       if " " in k:
           k = k.replace(" ","_")
       if "(" in k:
           k = k.replace("(","")
       if ")" in k:
           k = k.replace(")","")
       for v in vals:
           if " " in v:
               v = v.replace(" ","_")
           if "(" in v:
               v = v.replace("(","")
           if ")" in v:
               v = v.replace(")","")
           dir_name = "{}{}/{}".format(base_dir,k,v)
           print(dir_name)
           if os.path.exists(dir_name) == False:
               os.mkdir(dir_name,mode=0o777)
    
#################
def get_dates_s3(bucket):
    '''
    1. Get dates from S3
    2. returns a list of dictionaries with keys : project, site ,dates
    '''
    response_data = []
    s3_sites      = sites_s3.objects.filter(bucket=bucket)
    for site in s3_sites:
        if site.site == "LaCosta":
            continue
        else:
            p_name = projects_s3.objects.filter(bucket=bucket).get(id=site.project_id).project
            s_name = site.site
            d_list = []
            prefix = "{}/{}/".format(p_name,s_name)
            results = paginator.paginate(Bucket=bucket,Delimiter='/',Prefix=prefix)
            for result in results:
                if 'CommonPrefixes' in result.keys():
                    for prefix in result.get('CommonPrefixes'):
                        d_list.append(prefix.get('Prefix').split('/',3)[2])
                else:
                    continue
            tmp = {}
            tmp['project'] = p_name
            tmp['site'] = s_name
            tmp['dates'] = d_list
            response_data.append(tmp)
    return(response_data)
               
       


def get_dates_psql(bucket):#######sends a list of all dates present in s3 ex [{"project":'project1',"site":"site1","dates":['date1','date2','date3']},]
    '''
    1. Get dates from postgres
    2. returns a list of dictionaries with keys : project, site ,dates
    '''
    response_data = []
    site_list = dates_s3.objects.filter(bucket=bucket).values("site_id").distinct()
    for x in site_list:
        tmp = {}
        data= []
        date_list = dates_s3.objects.filter(bucket=bucket).filter(site_id=x["site_id"])
        for y in date_list:
            data.append(y.date)
        p_name = projects_s3.objects.filter(bucket=bucket).get(id = sites_s3.objects.filter(bucket=bucket).get(id = x["site_id"]).project_id).project
        s_name = sites_s3.objects.filter(bucket=bucket).get(id=x["site_id"]).site
     
        tmp["site"]    = s_name
        tmp["project"] = p_name
        tmp["dates"]   = data
        response_data.append(tmp)
    return response_data


def update_psql_dates(id_counter,date,p_id,s_id,bucket):
    '''
    1. id_counter- last entry in dates table
    2. date      - date name
    3. p_id      - project name
    4. s_id      - site name
    '''
    sync=False
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",port=5432,user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute("insert into download_s3_dates_s3 (date,project_id,site_id,sync,bucket) values (%s,%s,%s,%s,%s)",(date,p_id,s_id,sync,bucket))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not insert in s3_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()

        
def send_psql_dates(date_s3_list,date_psql_list,count,bucket):
    '''
    1. Iterate over s3 list and psql list and find common dictionaries that project and site
    2. Find residual list of dates for such dictionaries
    3. update the residual list of dates
    '''
    id_counter = count + 1
    for x in date_s3_list:
        for y in date_psql_list:
            if x["project"]==y["project"] and x["site"]==y["site"]:
                residual_list = diff(x["dates"],y["dates"])
                if len(residual_list) ==0:
                    break
                else:
                    p_id = projects_s3.objects.filter(bucket=bucket).get(project=x["project"]).id
                    s_id = sites_s3.objects.filter(project_id=p_id).filter(bucket=bucket).get(site=x["site"]).id
                    for date in residual_list:
                        update_psql_dates(id_counter,date,p_id,s_id,bucket)
                        id_counter = id_counter + 1

    '''
    1. done_project_list - projects present in postgres
    2. list comprehension to find dictionaries which do not have project keys from done_project_list
    '''
   # done_project_list = []
    done_site_list = []
    new_site_query = dates_s3.objects.filter(bucket=bucket).values("site_id").distinct()
    for x in new_site_query:
       # done_project_list.append(projects_s3.objects.get(id=sites_s3.objects.get(id = x["site_id"]).project_id).project)
        done_site_list.append(sites_s3.objects.get(id=x["site_id"]).site)

    final_list  = [x for x in date_s3_list if x["site"] not in done_site_list]
    for x in final_list:
        try:
            p_id = projects_s3.objects.filter(project=x['project']).get(bucket=bucket).id
        except projects_s3.DoesNotExist:
            continue
        print(p_id,x['site'],bucket)
        s_id = sites_s3.objects.filter(project_id=p_id).filter(site=x['site']).get(bucket=bucket).id
        date_list = x['dates']
        for y in date_list:
            update_psql_dates(id_counter,y,p_id,s_id,bucket)
            id_counter=id_counter+1
    


def make_dates_dir(date_s3_list,base_dir):
    for x in date_s3_list:
        p = x["project"]
        if p =="":
            continue
        if " " in p:
            p = p.replace(" ","_")
        if "(" in p:
            p = p.replace("(","")
        if ")" in p:
            p = p.replace(")","")
    
        s = x["site"]
        if " " in s:
            s = s.replace(" ","_")
        if "(" in s:
            s = s.replace("(","")
        if ")" in s:
            s = s.replace(")","")
    
        for d in x["dates"]:
            if " " in d:
                d = d.replace(" ","_")
            if "(" in d:
                d = d.replace("(","")
            if ")" in d:
                d = d.replace(")","")
    
            dir_name = "{}{}/{}/{}".format(base_dir,p,s,d)
            if os.path.exists(dir_name) == False:
                os.mkdir(dir_name,mode=0o777)
    
    
########################


def get_cams_s3(date_s3_list):
    response_data = []
    for result in date_s3_list:
        tmp={}
        tmp['project'] = result['project']
        tmp['site'] = result['site']
        for date in result['dates']:
            cam_list = []
            prefix         = "{}/{}/{}/".format(tmp['project'],tmp['site'],date)
            results = paginator.paginate(Bucket=bucket,Delimiter='/',Prefix=prefix)
            for result in results:
                if 'CommonPrefixes' in result:
                    for prefix in result['CommonPrefixes']:
                        cam_id = prefix.get('Prefix').split('/',4)[3]
                        cam_list.append(cam_id)
            tmp1={}
            tmp1["project"] = tmp["project"]
            tmp1["site"]    = tmp["site"]
            tmp1["date"]    = date
            tmp1["cam_id"]= cam_list
            response_data.append(tmp1)
    return (response_data) 

def get_cams_psql(bucket):
    response_data = []
    p_list = projects_s3.objects.filter(bucket=bucket)
    for p in p_list:
        tmp={}
        tmp["project"] = p.project
        s_list = sites_s3.objects.filter(project_id=p.id).filter(bucket=bucket)
        for s in s_list:
            tmp["site"]= s.site
            d_list = dates_s3.objects.filter(project_id=p.id).filter(site_id=s.id).filter(bucket=bucket)     
            for d in d_list:
                tmp1={}
                tmp['date'] = d.date
                c_list = cams_s3.objects.filter(project_id=p.id).filter(site_id=s.id).filter(date_id=d.id).filter(bucket=bucket)
                cam_list=[]
                for cams in c_list:
                    cam_list.append(cams.cam_id)
                tmp['cam_id'] = cam_list
                tmp1["project"]=tmp["project"]
                tmp1["site"]=tmp["site"]
                tmp1["date"]=d.date
                tmp1["cam_id"]=cam_list
                response_data.append(tmp1)
    return(response_data)



def upload_cam_s3(c,p_id,s_id,d_id,count,bucket):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",port=5432,user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute("insert into download_s3_cams_s3 (cam_id,date_id,project_id,site_id,bucket) values (%s,%s,%s,%s,%s)",(c,d_id,p_id,s_id,bucket))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not insert in s3_dates",error)
    finally:
        if (con):
            cur.close()
            con.close()



def send_psql_cams(cam_s3_list,cam_psql_list,counter,bucket):
    count = counter + 1
    for s3 in cam_s3_list:
        for psql in cam_psql_list:
            if s3["project"]==psql["project"] and s3["site"] == psql["site"] and s3["date"] ==psql["date"]:
                p_id = projects_s3.objects.filter(bucket=bucket).get(project=s3["project"]).id
                s_id = sites_s3.objects.filter(project_id=p_id).get(site=s3["site"]).id
                d_id = dates_s3.objects.filter(project_id=p_id).filter(site_id=s_id).get(date=s3['date']).id
                c_s3_list = s3["cam_id"] 
                c_psql_list = psql["cam_id"]
                residual_list = diff(c_s3_list,c_psql_list)
                for c in residual_list:
                    upload_cam_s3(c,p_id,s_id,d_id,count,bucket)
                    count= count+1
                 
    
    done_projects=[]
    t_p = cams_s3.objects.values("project_id").distinct()
    for x in t_p:
        p_name = projects_s3.objects.get(id=x["project_id"]).project
        done_projects.append(p_name)
    final_list  = [x for x in cam_s3_list if x["project"] not in done_projects]
    print(final_list)
    for x in final_list:
        p_id = projects_s3.objects.filter(bucket=bucket).get(project=x['project']).id
        s_id = sites_s3.objects.filter(project_id=p_id).get(site=x['site']).id
        d_id = dates_s3.objects.filter(project_id=p_id).filter(site_id=s_id).get(date=x['date']).id
        c_list = x['cam_id']
        for c in c_list:
            upload_cam_s3(c,p_id,s_id,d_id,count,bucket)
            count= count+1
      
    
   

def make_cams_dir(date_s3_list):
    for x in cam_s3_list:
        p = x["project"]
        if p =="":
            continue
        if " " in p:
            p = p.replace(" ","_")
        if "(" in p:
            p = p.replace("(","")
        if ")" in p:
            p = p.replace(")","")
    
        s = x["site"]
        if " " in s:
            s = s.replace(" ","_")
        if "(" in s:
            s = s.replace("(","")
        if ")" in s:
            s = s.replace(")","")
    
        d = x["date"]
        if " " in d:
            d = d.replace(" ","_")
        if "(" in s:
            d = d.replace("(","")
        if ")" in s:
            d = d.replace(")","")
    
        for c in x["cam_id"]:
            if " " in c:
                c = c.replace(" ","_")
            if "(" in d:
                c = c.replace("(","")
            if ")" in d:
                c = c.replace(")","")
   

            dir_name = "{}{}/{}/{}/{}".format(base_dir,p,s,d,c)
            if os.path.exists(dir_name) == False:
                os.mkdir(dir_name,mode=0o777)



     
#buckets = ["uploads.live.uncannysurveillance.com","uploads.live.videoanalytics"]
buckets = ["uploads.live.videoanalytics"]
#buckets = ["uploads.live.uncannysurveillance.com"]
######### 

for bucket in buckets: 

    if bucket == "uploads.live.uncannysurveillance.com":
        client = boto3.client('s3',aws_access_key_id='AKIARX6S2JWR245EG3UD',aws_secret_access_key='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')
        paginator = client.get_paginator('list_objects')
        base_dir = "/data/downloads/"  

    if bucket == "uploads.live.videoanalytics":
        client = boto3.client('s3',aws_access_key_id='AKIARX6S2JWR6AQ5F4PQ',aws_secret_access_key='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')
        paginator = client.get_paginator('list_objects')
        base_dir = "/data/downloads/videoanalytics/"  
 
    ############### projects upload
    project_psql_list = get_projects_psql(bucket)
    project_s3_list   = get_projects_s3(bucket)
    updated_projects  = diff(project_s3_list,project_psql_list)
    projects_send_psql= send_psql_projects(updated_projects,len(project_psql_list),bucket)
    make_projects_dir(project_s3_list,base_dir)
    print('projects are uploaded')
    ############### sites upload
    site_s3_list      = get_sites_s3(bucket)
    site_psql_list    = get_sites_psql(bucket)
    sites_send_psql   = send_psql_sites(site_s3_list,site_psql_list,sites_s3.objects.all().count(),bucket)
    make_sites_dir(site_s3_list,base_dir)
    print('sites are uploaded')
    ################### dates upload
    date_s3_list      = get_dates_s3(bucket)
    date_psql_list    = get_dates_psql(bucket)
    dates_send_psql   = send_psql_dates(date_s3_list,date_psql_list,dates_s3.objects.all().count(),bucket)
    make_dates_dir(date_s3_list,base_dir)
    print('dates are uploaded')
    ################## images upload
    cam_s3_list     = get_cams_s3(date_s3_list)
    cam_psql_list   = get_cams_psql(bucket)
    cam_send_psql   = send_psql_cams(cam_s3_list,cam_psql_list,cams_s3.objects.all().count(),bucket)
    make_cams_dir(date_s3_list)
    print('cams are uploaded')


 #   project_psql_list = get_projects_psql(bucket)
 #   imp_projects = ['Cattleya','SynCom'] 
  #   for project in project_psql_list:
  #       if project in imp_projects:
  #           p_id = projects_s3.objects.filter(bucket=bucket).get(project=project).id
  #           sites = sites_s3.objects.filter(project_id=p_id)
  #           for site in sites:
  #               s_id = site.id
  #               dates = dates_s3.objects.filter(site_id=s_id)
  #               for date in dates:
  #                   d_id = date.id
  #                   cams = cams_s3.objects.filter(date_id=d_id)
  #                   count = 0
  #                   for cam in cams:
  #                       cam_id = cam.id
  #                       path = '{}/{}/{}/{}'.format(project,site,date,cam)
  #                       new_path = path.replace(' ','\ ')
  #                       pages =  paginator.paginate(Bucket=bucket,Prefix=new_path)
  #                       for page in pages:
  #                           if 'Contents' in page.keys():
  #                               count = count + len(page['Contents'])
  #                   
  #                   date.contents = count
  #                   date.save()
  #                   print(d_id,count) 
