from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [

    path('get_modules/',views.get_modules.as_view()),
    path('dates/',views.infer_dates.as_view()),
    path('sites/',views.infer_sites.as_view()),
    path('active/',views.active_processes.as_view()),
    path('active/kill/',views.kill_infer_process.as_view()),

#    path('get_classifier_modules/',views.get_classifier_modules.as_view()),
    path('classifier_dates/',views.infer_classifier_dates.as_view()),
    path('delete_classifier_container/',views.delete_classifier_container.as_view()),


]


