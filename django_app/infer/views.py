from django.shortcuts import render
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
import psycopg2
from uvdata.models import  deeplearning_processes, remote_instances, remote_processes,datasetvsmodel,darknet_models,projects, sites, images, box, classes,cam_id,bbox,dates,train_detector_containers,infer_bbox,darknet_classes,infer_models
import os
import os
import numpy as np
import zmq
import json
import subprocess
import docker
import os
import sys
import numpy as np
import zmq
import json
import time
import os
import sys
import numpy as np
import zmq
import json
import docker
import random 
from datetime import date



avail_ports = [5556,5557,5558,5559,5560]
used_ports = []
open_port = random.choice(avail_ports)
used_ports.append(open_port)
avail_ports.remove(open_port)

#for x in avail_ports:
#    netstat_command = "netstat -tulp | grep {} | grep LISTEN".format(x)
#    a=os.system(netstat_command)  
#    if a!=0: 
#        open_port = x
###############


#### upload and download directives
base_folder="/data/downloads/"
infer_data = '/data/infer_data/'

queried_projects ='/data/queried_data/download_projects/'
queried_sites ='/data/queried_data/download_sites/'
queried_dates ='/data/queried_data/download_dates/'
queried_classes= '/data/queried_data/download_classes/'
################################
other_ports = [
              {"data_port":55650,"infer_port":53650},
              {"data_port":55640,"infer_port":53640},
              {"data_port":55630,"infer_port":53630},
              {"data_port":55620,"infer_port":53620},
              {"data_port":55610,"infer_port":53610}
              ]



def insert_infer_bbox(x,y,w,h,class_name,conf,c_id,d_id,i_id,s_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()

        cur.execute("insert into uvdata_bbox (x,y,w,h,box_type,box_class,conf,cam_id,date_id,image_id,site_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(x,y,w,h,'infer',class_name,conf,c_id,d_id,i_id,s_id))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()

def query_all_models():
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select * from uvdata_darknet_models"
        cur.execute(query)
        model_records = cur.fetchall()
        return(model_records)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()

def qery_model(model):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select * from uvdata_darknet_models where project= %s"
        cur.execute(query, (model,))
        model_records = cur.fetchall()
        return(model_records)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()



def infer_container_table(container_id,total_images,completed_images,image_id,filter_name,folder):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select * from uvdata_darknet_containers where docker_containers= %s"
        cur.execute(query, (container_id,))
        docker_records = cur.fetchone()
        if docker_records:
            cur.execute("update uvdata_darknet_containers set inferenced_images=(%s) where docker_containers = (%s)", (completed_images,container_id))
            con.commit()
        else:
            cur.execute("insert into uvdata_darknet_containers(docker_containers,total_images,inferenced_images,s3_filter,s3_folder,docker_image_id) values(%s,%s,%s,%s,%s,%s)",(container_id,total_images,completed_images,filter_name,folder,image_id))
            con.commit()

  
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_darknet_containers",error)
    finally:
        if (con):
            cur.close()
            con.close()


def remove_docker_entry(container_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        cur.execute("DELETE FROM uvdata_darknet_containers WHERE docker_containers = %s", (container_id,))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()


def list_containers():
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select * from uvdata_darknet_containers"
        cur.execute(query)
        model_records = cur.fetchall()
        return(model_records)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()

def get_image_name(image_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select image_id from uvdata_darknet_models where id = %s"
        cur.execute(query,(image_id,))
        model_records = cur.fetchall()
        return(model_records)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()

def psql_kill_req(process):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "delete from uvdata_darknet_containers where docker_containers = %s"
        cur.execute(query,(process,))
        con.commit()
    except (Exception, psycopg2.Error) as error:
        print("could not kill container",error)
    finally:
        if (con):
            cur.close()
            con.close()


def get_class_id(project,class_name):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select class_id from uvdata_darknet_classes where project = %s and class_name=%s"
        cur.execute(query,(project,class_name))
        model_records = cur.fetchone()
        return(model_records)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()



def get_site_classes(s_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select classes_id from uvdata_sites_site_classes where sites_id = %s"
        cur.execute(query,(str(s_id),))
        model_records = cur.fetchall()
        response = []
        for x in model_records:
            response.append(classes.objects.get(id=x[0]).class_name)
        return(response)

    except (Exception, psycopg2.Error) as error:
        print("could not fetch from site_classes",error)
    finally:
        if (con):
            cur.close()
            con.close()



#####################
def send_policy(total_images,completed_images,flag):
    return(int(completed_images/total_images*100))

def download_dates(date,site,project,model):
    if " " in project:
        project = project.replace(" ","\ ")
    if "(" in project:
        project = project.replace("(","\(")
    if ")" in project:
        project = project.replace(")","\)")
    if " " in site:
        site = site.replace(" ","\ ")
    if "(" in site:
        site = site.replace("(","\(")
    if ")" in site:
        site = site.replace(")","\)")

    p_id = projects.objects.get(project_name=project).id
    s_id = sites.objects.filter(project_id=p_id).get(site_name=site).id
    d_id = dates.objects.filter(project_id=p_id).filter(site_id=s_id).get(date=date).id
    c_ids = cam_id.objects.filter(date_id=d_id)
    image_ids = images.objects.filter(site_id=s_id).filter(date_id=d_id)


    '''
    training directory
    '''
    folder = project+'-'+site+'-'+date
    infer_folder = "mkdir {}{}".format(infer_data,folder)
    image_folder  = "mkdir {}{}/images".format(infer_data,folder)
    ann_folder   = "mkdir {}{}/annotations".format(infer_data,folder)
    os.system(infer_folder)
    os.system(image_folder)
    os.system(ann_folder)
    '''
    training list
    '''
    image_list = open(infer_data+folder+"/images/image_list.txt","w") 


    for c in c_ids:
        c_id = c.id
        c_i_ids = image_ids.filter(cam_id=c_id)
        for i_id in c_i_ids:
            dir_name = "{}{}/{}/{}/{}".format(base_folder,project,site,date,c.cam_id)
            json_file ="{}.json".format(i_id.filename.split(".jpg")[0])
            components = i_id.image_url.split('/',-1)
            s3_directory = "{}/{}/{}/{}/{}".format(components[0],components[1],components[2],components[3],components[4])
                    
            download_image="aws s3 sync s3://{} {}".format(s3_directory,dir_name)
            print(download_image)
#            os.system(download_image)
            copy_image = "cp {}/{} {}{}/images".format(dir_name,i_id.filename,infer_data,folder)
            copy_json =  "cp {}/{} {}{}/annotations".format(dir_name,json_file,infer_data,folder)
            os.system(copy_image)
            os.system(copy_json)
            json_path = "{}/{}".format(dir_name,json_file)
            if os.path.exists(json_path) == True:
                i_path = "/images/{}".format(i_id.filename)
                image_list.write(i_path)
                image_list.write("\n")

    image_list.close()
    return(folder)
 

def download_sites(site,project,model):
   
    if " " in project:
        project = project.replace(" ","\ ")
    if "(" in project:
        project = project.replace("(","\(")
    if ")" in project:
        project = project.replace(")","\)")
    if " " in site:
        site = site.replace(" ","\ ")
    if "(" in site:
        site = site.replace("(","\(")
    if ")" in site:
        site = site.replace(")","\)")

    p_id = projects.objects.get(project_name=project).id
    s_id = sites.objects.filter(project_id=p_id).get(site_name=site).id
    d_list = dates.objects.filter(project_id=p_id).filter(site_id=s_id)
    image_ids = images.objects.filter(site_id=s_id)

#####    
    '''
    training directory
    '''
    folder = project+'-'+site
    infer_folder = "mkdir {}{}".format(infer_data,folder)
    image_folder  = "mkdir {}{}/images".format(infer_data,folder)
    ann_folder   = "mkdir {}{}/annotations".format(infer_data,folder)
    os.system(infer_folder)
    os.system(image_folder)
    os.system(ann_folder)
    '''
    training list
    '''
    image_list = open(infer_data+folder+"/images/image_list.txt","w")
####

    for d in d_list:
        d_id = d.id
        cam_list = cam_id.objects.filter(date_id=d.id)
        for c in cam_list:
            c_id = c.id
            i_list = image_ids.filter(date_id=d.id).filter(cam_id=c.id)
            for i_id in i_list:
                dir_name = "{}{}/{}/{}/{}".format(base_folder,project,site,d.date,c.cam_id)
                json_file ="{}.json".format(i_id.filename.split(".jpg")[0])
                components = i_id.image_url.split('/',-1)
                s3_directory = "{}/{}/{}/{}/{}".format(components[0],components[1],components[2],components[3],components[4])
    
                download_image="aws s3 sync s3://{} {}".format(s3_directory,dir_name)
                os.system(download_image)
                copy_image = "cp {}/{} {}{}/images".format(dir_name,i_id.filename,infer_data,folder)
                copy_json =  "cp {}/{} {}{}/annotations".format(dir_name,json_file,infer_data,folder)
                os.system(copy_image)
                os.system(copy_json)

                json_path = "{}/{}".format(dir_name,json_file)
                if os.path.exists(json_path) == True:
                    i_path = "/images/{}".format(i_id.filename)
                    image_list.write(i_path)
                    image_list.write("\n")

    
    image_list.close()
    return(folder)




class infer_dates(APIView):

    def get(self,request):
        models = query_all_models()
        response_object = []
        for x in models:
            response_object.append(x[1])
        
        return Response(response_object)
  
    def post(self,request):
        from datetime import date


        dataset_query = datasetvsmodel.objects.get(title=request.data['data_folder'])
        model_query   = infer_models.objects.get(model=request.data['model_folder'])
        instance_query = remote_instances.objects.get(public_ip=request.data['instance'])


        '''
        get instance  process 
        '''
        task_id = deeplearning_processes.objects.get(process=model_query.model_type,process_name='Infer&Save').id
        check_process = remote_processes.objects.filter(instance_id = instance_query.id)
        if len(check_process) != 0:
           '''
           update process column
           '''
           current_processes= check_process[0].process
           if task_id not in current_processes:
               current_processes.append(task_id)

           '''
           update params
           '''
           params = check_process[0].params
           param_keys = params.keys()
           if 'Infer&Save' not in param_keys:
               params['Infer&Save'] = []
               tmp ={}
               tmp['model'] = model_query.id
               tmp['dataset'] = dataset_query.id
               params['Infer&Save'].append(tmp)
           else:
               infer_list = params['Infer&Save']
               tmp ={}
               tmp['model'] = model_query.id
               tmp['dataset'] = dataset_query.id
               if tmp not in infer_list:
                  infer_list.append(tmp)
               params['Infer&Save'] = infer_list
           '''
           update completed params
           '''
           completed_params = check_process[0].completed
           param_keys = completed_params.keys()
           if 'Infer&Save' not in param_keys:
               completed_params['Infer&Save'] = []
               #tmp ={}
               #tmp['model'] = model_query.id
               #tmp['dataset'] = dataset_query.id
               #completed_params['Infer&Save'].append(tmp)
           else:
               tmp ={}
               tmp['model'] = model_query.id
               tmp['dataset'] = dataset_query.id
               if tmp not in completed_params['Infer&Save']:
                   completed_params['Infer&Save'].append(tmp)
           '''
           update db
           '''
           remote_processes.objects.filter(instance_id = instance_query.id).update(process = current_processes,params=params,completed=completed_params)
           return Response('process registered')
        else:
           return Response('create dataset and models first')


        '''

        base_dir  = '/datadrive/detectors/darknet/'
        data_folder   = '/datadrive/train_data/' + request.data['data_folder']
        data_port     = request.data['data_folder'].split('@port')[-1]
        model_folder    = '/app/downloaded_models/{}'.format(request.data['model_folder'])
        today = date.today()
        date_created = today.strftime("%Y-%m-%d")
        get_model = train_detector_containers.objects.get(port=data_port).model
#        infer_classes = [] 
#        for x in  get_model:
#            c_name = x.class_name
#            c_id   = classes.objects.get(class_name = c_name).id
#            infer_classes.append(c_id)
#
       # print(get_model)
        job       = train_detector_containers.objects.get(port=data_port).job        
        date      = job.split('/')[-1]
        site      = job.split('/')[-2]
        project   = job.split('/')[-3]
        p_id      = projects.objects.get(project_name = project).id
        s_id      = sites.objects.filter(project_id = p_id).get(site_name = site).id
        d_id      = dates.objects.filter(site_id=s_id).get(date = date).id
        
        site_classes  = get_site_classes(s_id)
 
        final_classes = []
        get_classes = darknet_classes.objects.filter(project=get_model)
        infer_classes = []
        for x in  get_classes:
            c_name = x.class_name
            c_id   = classes.objects.get(class_name = c_name).id
            infer_classes.append(c_id)

        for c in get_classes:
             tmp = {}
             tmp['class'] = c.class_name
             tmp['id']    = c.class_id
             final_classes.append(tmp)
        

        model_query = darknet_models.objects.filter(model="darknet")[0]
        image_id = model_query.image_id
        directory = model_query.directory
        docker_image = 'darknet'
       # container_attr="map"
       # backup_dir = "backup_{}".format(port)
        for x in other_ports:
            if str(x['data_port']) == str(data_port):
                infer_port = x['infer_port']

        
        model_name = str(request.data['model_folder'])
        check_model  = infer_models.objects.filter(model = model_name)
        if not check_model:
            upload_model = infer_models.objects.create(model = model_name,model_type='detector',detector = infer_classes)
            upload_model.save()
         
        infer_model_id = int(infer_models.objects.get(model = model_name).id) 
        represent  = dates.objects.get(id = d_id)
        if represent.models:
            tmp = represent.models
            tmp.append(infer_model_id)
            represent.save()            
  
        else: 
            represent.models = [infer_model_id]
            represent.save()
        



        label_dict = {}
        count = 0
        f =  open('{}downloaded_models/{}/det.names'.format(base_dir,request.data['model_folder'])) 
        Lines = f.read().splitlines()

        for line in Lines:
            label_dict[count] = line
            count = count + 1

        print(label_dict)
 
        container_name = "darknet_infer_{}_{}".format(get_model.replace(' ','_'),infer_port)
        container_command   = "echo 'admin@123' | python3 scripts/start_darknet.py {} {} {} {} {} {}".format(data_folder,str(docker_image),directory,'infer',str(infer_port),get_model.replace(' ','_'))
        os.system(container_command)
        curl_command = 'apt-get install -y libcurl4-openssl-dev'
        os.system('docker exec {} {}'.format(container_name,curl_command))
        os.system('docker exec {} bash -c cd /app'.format(container_name))
       
        test_cmd = './darknet detector test {}/model.data {}/model.cfg {}/model.weights -port {} -save_labels -thresh 0.01 '.format(model_folder,model_folder,model_folder,infer_port)
        print(test_cmd)
        client = docker.from_env()
        target_container = client.containers.get(container_id = container_name)
        log_cont = target_container.exec_run(cmd=test_cmd,detach=True)

        context = zmq.Context()
        socket = context.socket(zmq.PAIR)
        host_socket = "tcp://localhost:{}".format(str(infer_port))
        socket.connect(host_socket)
        socket.send_string(get_model)



        completion=False
        while completion==False:
            message = socket.recv()
            if message.decode('utf-8')  == 'close_docker':
                completion=True

            else:
                str_message  = json.loads(message.decode('utf-8'))
                
                y = float(str_message['y'])
                x = float(str_message['x'])
                w = float(str_message['w'])
                h = float(str_message['h'])
                conf = float(str_message['prob'])
                class_id =  str_message['class_id']
                f_name = str_message['filename'].split('/')[-1]

                image_dets = images.objects.filter(date_id=d_id).get(filename=f_name)
                image_id   = image_dets.id
                cam_id     = image_dets.cam_id
                height     = int(bbox.objects.filter(image_id= image_id)[0].height)
                width      = int(bbox.objects.filter(image_id= image_id)[0].width)
                class_name = label_dict[int(class_id)]
                if class_name in site_classes:
                    bb_width = width*w
                    bb_height = height*h     
                    xmin = x*width - (bb_width/2)
                    ymin = y*height - (bb_height/2)
                    print(class_name,bb_width,bb_height,xmin,ymin) 
                    if bb_width > 0 and  bb_height > 0 and xmin > 0 and ymin > 0:        
                        infer_table = infer_bbox.objects.create(
                    		
                    		x = xmin , y = ymin , w = bb_width , h = bb_height,
                                    box_type = model_name, box_class = class_name , 
                                    height = height, width = width, date_created = date_created, conf = conf,
                                     classifier = '{}', filename = f_name, cam_id = cam_id , date_id = d_id , site_id = s_id, image_id = image_id 	  
                    	)  
 

                else:
                    continue                 

        target_container.stop()
        target_container.remove()
        socket.close()

        '''
        return Response('Request for inference accepted')





class infer_classifier_dates(APIView):
    def post(self,request):
        img_dir     = '/datadrive/train_data/{}/test'.format(request.data['data_folder'])
        mount_model = '/datadrive/classifiers/{}'.format(request.data['model_folder'])
        framework_image = request.data['framework_image']
        port = request.data['data_folder'].split('@port')[1]
        '''
        fetch image id 
        '''
        client = docker.from_env()
        containers = client.images.list()
        for x in containers:
            if x.tags == framework_image:
                docker_img_id = x.id

        '''
        run docker and mount image and model files
        '''

        client = docker.from_env()
        volumes = {mount_model:{'bind': '/app', 'mode': 'rw'}, img_dir:{'bind': '/images', 'mode': 'rw'}} #proj/site/date
        stdin_open=True
        tty = True
        name="classifier_{}_{}".format(str(port),str(request.data['model_folder']))
        detach=True
        container = client.containers.run(image=docker_img_id, volumes=volumes, stdin_open=stdin_open, tty=tty,name=name,detach=True,working_dir='/app')
       

        '''
        update infer_model table and dates
        '''
        check_db_model = infer_models.objects.filter()


        '''
        insert model to date
        '''
        dataset_dates = datasetvsmodel.objects.filter(model_type='classifier').get(uid=port).date_id
        model_id = infer_models.objects.filter(model_type='classifier').get(model=request.data['model_folder']).id
        print(dataset_dates,model_id) 
        for d in dataset_dates:
            new_list = [model_id]
            date_models = dates.objects.get(id = d)
            if (date_models.models):
                for x in date_models.models:
                   new_list.append(x)
                date_models.models = new_list
                date_models.save()

            else:
                date_models.models = new_list
                date_models.save()    


        '''
          
        '''
        test_cmd = 'python {}.py  {} {} {} '.format(str(request.data['model_folder']),request.data['data_folder'].split('@port')[0],str(request.data['model_folder']),name)
        print(test_cmd)
        client = docker.from_env()
        target_container = client.containers.get(container_id = name)
        log_cont = target_container.exec_run(cmd=test_cmd,detach=True)
             
        return Response('hellllaa')


class delete_classifier_container(APIView):
    def post(self,request):
        container = request.data['container_name']
        client = docker.from_env()
        target_container = client.containers.get(container)
        target_container.stop()
        target_container.remove()
            
        return Response('container deleted')
      

class infer_sites(APIView):

    def get(self,request):
        models = query_all_models()
        response_object = []
        for x in models:
            response_object.append(x[1])

        return Response(response_object)

    def post(self,request):
        site= request.data['site']
        project = request.data['project']
        model = request.data['model']

        model_params = query_model(model)
        for x in model_params:
            image_id = x[0]
            container_name = x[1]
            directory=x[3]
            docker_image = x[4]



        '''
        download and segregate images and annotations
        '''

        infer_folder = download_sites(site,project,model)

        '''
        establish connection with socket 
        '''

        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        host_socket = "tcp://localhost:{}".format(open_port)
        socket.connect(host_socket)
        socket.RCVTIMEO = 10000000 #we will only wait 2s for a reply


        container_id = "{}_{}".format(container_name,open_port)
        container_command = "sudo python scripts/infer_script.py {}{}/images {} {} {} {}".format(infer_data,infer_folder,docker_image,directory,container_name,open_port)
        os.system(container_command)
        print(container_command)
        file_path=infer_data+infer_folder+"/images/image_list.txt"
        infer_dir = "mkdir -m 777 {}{}/{}".format(infer_data,infer_folder,"infer_anno")
        os.system(infer_dir)
        total_images = 0
        completed_images = 0
        with open(file_path, 'r') as f:
            f = f.readlines()
            for line in f:
                total_images = total_images +1
        print(total_images)

        benchmark_images = [
        f[0].strip()[8:].strip('.jpg'),
        f[int(total_images*0.1)].strip()[8:].strip('.jpg'),
        f[int(total_images*0.1)].strip()[8:].strip('.jpg'),
        f[int(total_images*0.2)].strip()[8:].strip('.jpg'),
        f[int(total_images*0.3)].strip()[8:].strip('.jpg'),
        f[int(total_images*0.4)].strip()[8:].strip('.jpg'),
        f[int(total_images*0.5)].strip()[8:].strip('.jpg'),
        f[int(total_images*0.6)].strip()[8:].strip('.jpg'),
        f[int(total_images*0.7)].strip()[8:].strip('.jpg'),
        f[int(total_images*0.8)].strip()[8:].strip('.jpg'),
        f[int(total_images*0.9)].strip()[8:].strip('.jpg'),
        f[-1].strip()[8:].strip('.jpg')
        ]
        for path in f:
            path = path.strip()
            base_name = path[8:].strip('.jpg')
            filename = "{}{}/infer_anno/{}.txt".format(infer_data,infer_folder,base_name)
            g = open(filename,"w+")
            print(path)
            socket.send_string(path)
            message = socket.recv()
            completed_images = completed_images + 1
            filter_name="date"
           # for x in benchmark_images:
           #     if x == base_name:

            infer_container_table(container_id,total_images,completed_images,image_id,filter_name,infer_folder)
            json_message = json.loads(message.decode('utf-8'))
            image_name = path.split('/')[2]
            i_query =  images.objects.get(filename=image_name)
            i_id = i_query.id
            s_id = i_query.site_id
            c_id = i_query.cam_id
            d_id = i_query.date_id
            print(json_message)
            for box in json_message["bboxes"]:
                a = get_class_id(model,box['class'])
                g.write(str(a[0]))
                g.write(' ')
                g.write(str((box["x"]+box["w"])/(2*(json_message["width"]))))
                g.write(' ')
                g.write(str((box["x"]+box["w"])/(2*(json_message["height"]))))
                g.write(' ')
                g.write(str(box["w"]/json_message["width"]))
                g.write(' ')
                g.write(str(box["h"]/json_message["height"]))
                g.write("\n")
                insert_infer_bbox(box["x"],box["y"],box["w"],box["h"],box["class"],box['confidence'],c_id,d_id,i_id,s_id)
            g.close()
        os.system(c_1)
        os.system(c_2)

        return Response('Request for inference accepted')
    


class active_processes(APIView):
    def post(self,request):
        a = request.data['key']
        print(a)
        result = list_containers()
        containers = []
        for x in result:
            tmp = {}
            tmp["name"]=x[1]
            tmp["total_images"]=x[2]
            tmp["percentage"]=int(x[3]*100 /x[2]) 
            tmp["s3_filter"] = x[4]
            tmp["s3_folder"] = x[5]
            a = get_image_name(x[6])
            for y in a :
                tmp["image_name"] = y[0]
            containers.append(tmp)
        return Response(containers)

class kill_infer_process(APIView):

    def get(self,request):
        result = list_containers()
        containers = []
        for x in result:
            tmp = {}
            tmp["name"]=x[1]
            containers.append(tmp)
        return Response(containers)
 
    def post(self,request):
        process = request.data["process_id"]
        print(process)
        stop_req = "docker stop {}".format(process)
        os.system(stop_req)
        kill_req = "docker rm {}".format(process)
        os.system(kill_req)
        psql_kill_req(process)
        return Response("process has been killed")


class get_modules(APIView):

    def get(self,request):
        train_ports = [5561,5562,5563,5564,5565]
        config_dir   = '/datadrive/detectors/darknet/config/'
        train_dir = '/datadrive/detectors/darknet/'
        data_dir    = '/datadrive/train_data/'
        model_dir = '/datadrive/detectors/darknet/downloaded_models/'
        project_list=['Cattleya','syn-com']
        response = {}
        response['w_n_c'] = {}
        response['data']  = []
        response['w_n_c']['config'] = []
        response['models'] = []

        for x in os.listdir(model_dir):
            tmp = {}
            tmp['text'] = x
            tmp['value'] = x
            response['models'].append(tmp)
    
        for x in os.listdir(data_dir):
            tmp = {}
            tmp['text'] = x
            tmp['value'] = x
            response['data'].append(tmp)
        return Response(response)


class get_classifier_modules(APIView):
    def get(self,request):
        train_ports = [5561,5562,5563,5564,5565]
        data_dir    = '/datadrive/train_data/'
        model_dir   = '/datadrive/classifiers/'
        response = {}
        response['data']  = []
        response['models'] = []
        response['dockers'] = []
        for x in os.listdir(model_dir):
            tmp = {}
            tmp['text'] = x
            tmp['value'] = x
            response['models'].append(tmp)
    
        for x in os.listdir(data_dir):
            tmp = {}
            tmp['text'] = x
            tmp['value'] = x
            response['data'].append(tmp)

        client = docker.from_env()
        containers = client.images.list()
        for x in containers:
            tmp = {}
            tmp['text'] = x.tags
            tmp['value'] = x.tags
            response['dockers'].append(tmp)
 
        return Response(response)


