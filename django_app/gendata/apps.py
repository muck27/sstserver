from django.apps import AppConfig


class GendataConfig(AppConfig):
    name = 'gendata'
