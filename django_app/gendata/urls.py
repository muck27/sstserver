from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [

    path('projects/',views.get_projects.as_view()),
    path('sites/<int:p_id>',views.get_sites.as_view()),
    path('dates/<int:s_id>',views.get_dates.as_view()),
    path('cam_ids/<int:d_id>',views.get_camids.as_view()),
    path('classifiers/<int:s_id>',views.classifier_attributes.as_view()),
    path('classes/<int:s_id>',views.get_classes.as_view()),
    path('class_split/<int:s_id>/<str:attr>',views.get_date_split.as_view()),
    path('create_dataset',views.create_dataset.as_view()),
]

