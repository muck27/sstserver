from django import forms
from uvdata.models import classes,projects,sites,images,box
from django.forms import ModelForm


class project_search(forms.Form):

    project_name = forms.ModelChoiceField(widget=forms.CheckboxSelectMultiple, queryset=projects.objects.all())
 

class site_search(forms.Form):

    site_name= forms.ModelMultipleChoiceField(queryset=None,widget=forms.CheckboxSelectMultiple,)

    def __init__(self,*args,**kwargs):
        project_id = kwargs.pop("project_id")     
        super(site_search, self).__init__(*args,**kwargs)
        self.fields['site_name'].queryset = sites.objects.filter(project_id=project_id)
        self.fields['name']=forms.CharField(initial=None, max_length=1024)
class class_search(forms.Form):

    class_name=forms.ModelMultipleChoiceField(queryset=classes.objects.all(),widget=forms.CheckboxSelectMultiple,)
    name= forms.CharField(initial=None, max_length=1024)

class update_db(forms.Form):
    def __init__(self,*args,**kwargs):
        choices123 = kwargs.pop("choices123")     
        super(update_db, self).__init__(*args,**kwargs)

        self.fields['send'] = forms.MultipleChoiceField(choices=choices123,initial=None,widget=forms.CheckboxSelectMultiple,) 
       # self.fields['send'] = forms.FileField() 
#    send = forms.BooleanField(required=False)


class bounding_box(forms.Form):
    
    bounding_box_types=[("1","display image"),("2","bounding box")] 
    bounding_box=forms.ChoiceField(choices = bounding_box_types, label="", initial='', widget=forms.Select(), required=True)


