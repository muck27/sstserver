import sys
user = "clyde"
import os.path
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
import subprocess
import numpy as np
from django.shortcuts import render
from django.http import HttpResponse
import django
django.setup()
sys.path.append('/home/'+user+'/Downloads/automatic_annotator_tool/django_app/')
from .forms import project_search, site_search, class_search, update_db,bounding_box
from .serializers import projects_serializer,sites_serializer,images_serializer,classes_serializer
from uvdata.models import projects, sites, images, box, classes,cam_id
from django.views.generic import ListView, CreateView, UpdateView
from django.forms import modelformset_factory
import os
import boto3
import uuid
from django.views.decorators.csrf import csrf_exempt
import json
import psycopg2
import sys
import shutil
import psycopg2
import json
import os
import os.path
from os import path
import boto3
import zipfile
import glob
import botocore
from PIL import Image, ImageDraw
########API view
import datetime
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse

####credentials 

client = boto3.client('s3', aws_access_key_id='AKIARX6S2JWR245EG3UD', aws_secret_access_key='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',)
s3 = boto3.resource('s3')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "uvlearn.settings")


#######setting up cursor
try:
    con = psycopg2.connect(

            host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",
            database="uvdb4uv1",
            user="postgres",
            password="uvdbuvdb"

)
    con.autocommit = True
except:

    print ("Cannot connect to db")


cur = con.cursor()


### connection to s3
#client = boto3.client('s3', aws_access_key_id='AKIAQHGYCFENLPX52B5L', aws_secret_access_key='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',)
s3 = boto3.resource('s3')

#### upload and download directives

download_dir = "/home/clyde/Downloads/automatic_annotator_tool/data/queried_data/"
upload_dir = "/home/clyde/Downloads/automatic_annotator_tool/data/upload_data/"
tmp_dir="/home/clyde/Downloads/automatic_annotator_tool/data/"
display_dir="/home/clyde/Downloads/automatic_annotator_tool/data/display_images/"

######API VIews for Projects and Sites

class list_projects(APIView):
    
    def get(self,request):
        projects_data= projects.objects.all()
        project_serializer=projects_serializer(projects_data,many=True)
        return Response(project_serializer.data)	

class list_sites(APIView):
    
    def get(self,request,p_id):
        sites_data= sites.objects.filter(project_id=p_id)
        site_serializer= sites_serializer(sites_data,many=True)
        return Response(site_serializer.data)

class list_images(APIView):

    def get(self,request,p_id,s_id):
        images_data= images.objects.filter(site_id=s_id)
        image_serializer= images_serializer(images_data,many=True)
        return Response(image_serializer.data)


class draw_images(generics.ListCreateAPIView):

    def get(self,request,p_id,s_id,image_id):
        bb_form= bounding_box()
        p = projects.objects.get(id=p_id).project_name
        s = sites.objects.get(id=s_id).site_name.replace(' ','\\ ')
        c = cam_id.objects.get(site_id=s_id).cam_id
        d = str(images.objects.get(id=image_id).date).split(' ',2)[0]
        i= images.objects.get(id=image_id).filename
        download_url = p +'/'+s+'/'+d+'/'+c+'/'+i
        print(download_url)
        im = Image.open("/home/clyde/Downloads/automatic_annotator_tool/data/display_images/image.jpg")
        im.save('/home/clyde/Downloads/automatic_annotator_tool/django_app/static/tmp.jpg')

        return render(request,'search/display_image.html',{'bb_form':bb_form,})

    def post(self,request,p_id,s_id,image_id):
        bb_form=bounding_box(request.POST)
        if bb_form.is_valid():
            info=bb_form.cleaned_data.get('bounding_box')
            if info =='2':
                im = Image.open("/home/clyde/Downloads/automatic_annotator_tool/data/display_images/image.jpg")
                image = ImageDraw.Draw(im)

                with open(display_dir+"anpr.json", encoding='utf-8') as data_file:
                    data = json.loads(data_file.read())

                elements= 0
                counter = []
                for x in data:
                    if x == 'event_json': 
                        elements = elements + 1
                        counter.append(elements)

                for iterations in counter:
                    xmin=data['event_json']['event']['coordinates']['xmin']
                    ymin=data['event_json']['event']['coordinates']['ymin']
                    width=data['event_json']['event']['coordinates']['width']
                    height=data['event_json']['event']['coordinates']['height']
                    a=(xmin,ymin)
                    b=(xmin+width,ymin)
                    c=(xmin,ymin+height)
                    d=(xmin+width,ymin+height)
                    line_color = (0, 0, 255)
                    image.line([a,b], fill=line_color, width=2)
                    image.line([c,d], fill=line_color, width=2)
                    image.line([a,c], fill=line_color, width=2)
                    image.line([b,d], fill=line_color, width=2)
                    im.save('/home/clyde/Downloads/automatic_annotator_tool/django_app/static/tmp.jpg')

            if info == '1':
                print('inside 1 ')
                im = Image.open("/home/clyde/Downloads/automatic_annotator_tool/data/display_images/image.jpg")
                im.save('/home/clyde/Downloads/automatic_annotator_tool/django_app/static/tmp.jpg')


        return render(request,'search/display_image.html',{'bb_form':bb_form,})



class list_classes(APIView):

    def get(self,request):
        classes_data= classes.objects.all()
        class_serializer= classes_serializer(classes_data,many=True)
        return Response(class_serializer.data)


class list_class_sites(APIView):

    def get(self,request,class_id):
        select_target_query="select * from uvdata_sites_site_classes where classes_id = %s"
        try:
            cur.execute((select_target_query),(class_id,))
        except:
            print("could not fetch object class from db")
        site_class_records = cur.fetchall()
        site_ids=[]
        for x in site_class_records:
            site_ids.append(x[1])
        id_array=list(site_ids)
        sites_summary=[]
        for x in id_array:
            a=sites.objects.get(id=x)
            sites_summary.append({"id":a.id,"site_name":a.site_name})

        return Response(sites_summary)


######
def summary(request):
    
    projects_summary=projects.objects.all()
    sites_summary=sites.objects.all()
    classes_summary=classes.objects.all()

    return render(request,'search/search_summary.html',{'projects_summary':projects_summary,'sites_summary':sites_summary,'classes_summary':classes_summary,})
 

def projects_sites_tree(request):

    project_name = request.GET.get("name")
    print(project_name)
    project_id = projects.objects.get(project_name=project_name).id
    sites_summary= sites.objects.filter(project_id=project_id)
    return render(request,'search/sites_tree.html',{"project_name":project_name,"sites_summary":sites_summary,})


def projects_images_tree(request):

    site_name = request.GET.get("name")
    print(site_name)
    site_id = sites.objects.get(site_name=site_name).id
    images_summary= images.objects.filter(site_id=site_id)
    return render(request,'search/images_tree.html',{"site_name":site_name,"images_summary":images_summary,})

def projects_images_display(request):
    filename= request.GET.get("name")
    bb_form= bounding_box()
    if request.method == "POST":
        bb_form=bounding_box(request.POST)
        if bb_form.is_valid():
           info=bb_form.cleaned_data.get('bounding_box')
           if info =='2':
               im = Image.open("/home/clyde/Downloads/automatic_annotator_tool/data/display_images/image.jpg")
               image = ImageDraw.Draw(im)

               with open(display_dir+"anpr.json", encoding='utf-8') as data_file:
                   data = json.loads(data_file.read())

               elements= 0
               counter = []
               for x in data:
                   if x == 'event_json':
                       elements = elements + 1
                       counter.append(elements)

               for iterations in counter:
                   xmin=data['event_json']['event']['coordinates']['xmin']
                   ymin=data['event_json']['event']['coordinates']['ymin']
                   width=data['event_json']['event']['coordinates']['width']
                   height=data['event_json']['event']['coordinates']['height']
                   a=(xmin,ymin)
                   b=(xmin+width,ymin)
                   c=(xmin,ymin+height)
                   d=(xmin+width,ymin+height)
                   line_color = (0, 0, 255)
                   image.line([a,b], fill=line_color, width=2)
                   image.line([c,d], fill=line_color, width=2)
                   image.line([a,c], fill=line_color, width=2)
                   image.line([b,d], fill=line_color, width=2)
                   im.save('/home/clyde/Downloads/automatic_annotator_tool/django_app/static/tmp.jpg')
           
           if info == '1':
               im = Image.open("/home/clyde/Downloads/automatic_annotator_tool/data/display_images/image.jpg")
               im.save('/home/clyde/Downloads/automatic_annotator_tool/django_app/static/tmp.jpg')

    return render(request,'search/display_image.html',{'bb_form':bb_form,})




def classes_sites_tree(request):

    class_name = request.GET.get("name")
    print(class_name)
    class_id = classes.objects.get(class_name=class_name).id
    print(class_id)
    select_target_query="select * from uvdata_sites_site_classes where classes_id = %s"
    try:
        cur.execute((select_target_query),(class_id,))
    except:
        print("could not fetch object class from db")
    site_class_records = cur.fetchall()
    site_ids=[]
    for x in site_class_records:
        site_ids.append(x[1]) 
    id_array=list(site_ids)
    sites_summary=[]
    for x in id_array:
        temp=[]
        a=sites.objects.get(id=x)
        print(a)
        temp.append(a.site_name)
        temp.append(a.site_images)
        temp.append(a.annotated_images)
        sites_summary.append(temp)        

    return render(request,'search/classes_sites_tree.html',{"class_name":class_name,"sites_summary":sites_summary,})



def classes_images_tree(request):

    site_name = request.GET.get("name")
    site_id = sites.objects.get(site_name=site_name).id
    images_summary= images.objects.filter(site_id=site_id)

    return render(request,'search/classes_images_tree.html',{"site_name":site_name,"images_summary":images_summary,})





def search_sites(request):

    project_id_form = project_search()

    project_id=None
    if request.method == "POST":
        project_id_form=project_search(request.POST)
        if project_id_form.is_valid():
            project_name=project_id_form.cleaned_data.get('project_name')
            project_queryset= projects.objects.filter(project_name=project_name)
            for x in project_queryset:                
                project_id = x.id

    site_id_form = site_search(project_id=project_id) 
    if request.method == "POST":
        site_id_form = site_search(request.POST,project_id=project_id)
        site_ids=request.POST.getlist('site_name')
        name=request.POST.getlist('name')
        site_names=[]
        for x in site_ids:
            
            site_names.append(sites.objects.get(id=x).site_name)
        
        job_details = open("../job_details.txt","a")
  #      print(name)
        for job_name in name:
            job_details.write(job_name)
            job_details.write(";")
            mkdir="mkdir ../data/queried_data/" + job_name
            os.system(mkdir)	
            for site_id in  site_ids:
                image_list=images.objects.filter(site_id=site_id)
                for x in image_list:
                    download_destination_image=download_dir+job_name+"/" + x.filename
                    download_destination_json=download_dir+job_name+"/" + x.filename.strip(".jpg")+".json"
                    string= x.image_url.split('/',3)[3]
                    string=string.replace('%20','\\ ')
                    download_image="aws s3 cp s3://" + string + ' ' + download_destination_image
                    download_json="aws s3 cp s3://" + string +' '+ download_destination_json
                    os.system(download_image)
                    os.system(download_json)
            job_details.write(str(site_names))
            job_details.write(";")
            job= download_dir + job_name + '/'
            command = "du -h {}".format(job)
            job_space = subprocess.check_output(command,shell=True).decode('ascii').rstrip("\t"+download_dir+job_name+"\n")
            job_details.write(str(job_space))
            job_details.write("\n")
        job_details.close()

    return render(request,'search/search_sites.html',{'project_id_form':project_id_form,'site_id_form':site_id_form,})

def search_class(request):

    class_id_form = class_search() 
    if request.method == "POST":
        class_id_form = class_search(request.POST)
        class_ids=request.POST.getlist('class_name')
        name= request.POST.getlist('name')
        class_names=[]
        for x in class_ids:
            class_names.append(classes.objects.get(id=x))
        
        job_details = open("../job_details.txt","a")
        for job_name in name:
            mkdir="mkdir ../data/queried_data/" + job_name
            os.system(mkdir)
            job_details.write(job_name)
            job_details.write(";")
            for class_id in  class_ids:
                select_target_query="select * from uvdata_box  where box_class_id = %s"
                try:
                    cur.execute((select_target_query),(class_id,))
                except:
                    print("could not select from box table")
            
                records = cur.fetchall()
                class_image_list=[]
                for record in records:
                    if not record[2] in class_image_list:
                        class_image_list.append(record[2])

                for x in class_image_list:
                    x_image_url=images.objects.get(id=x).image_url
                    x_json_url=images.objects.get(id=x).json_url
                    x_image_name=images.objects.get(id=x).filename
                    print(x_image_url.split('/',3)[3].replace('%20','\\ '))
                    download_destination_image=download_dir+job_name+"/"+x_image_name
                    download_destination_json=download_dir+job_name+"/"+x_image_name.strip(".jpg")+".json"
                    download_image="aws s3 cp s3://" + x_image_url.split('/',3)[3].replace('%20','\\ ') +' '+ download_destination_image
                    download_json="aws s3 cp s3://" + x_json_url.split('/',3)[3].replace('%20','\\ ') +' '+ download_destination_json
                    os.system(download_image)
                    os.system(download_json)
 
 
            job_details.write(str(class_names))
            job_details.write(";")
            job= download_dir + job_name + '/'
            command = "du -h {}".format(job)
            job_space = subprocess.check_output(command,shell=True).decode('ascii').rstrip("\t"+download_dir+job_name+"\n")
            job_details.write(str(job_space))
            job_details.write("\n")
        job_details.close()

    return render(request,'search/search_class.html',{'class_id_form':class_id_form,})

def upload_db(request):

    choice_array=os.listdir(upload_dir)
    choices123= [(x,y) for x,y in enumerate(choice_array)]
    update_db_form = update_db(choices123=choices123)
    if request.method == "POST":
        update_db_form = update_db(request.POST,choices123=choices123)
        confirmation=request.POST.getlist('send')
        f= open("../data/zip.txt",'a')
        print(confirmation)
        for x in confirmation:
            f.write(choices123[int(x)][1].rstrip(".zip"))
            print(choices123[int(x)][1].rstrip(".zip"))
        f.close()
 
        if not confirmation:
            print("tick and submit to start uploading")
        
        if confirmation:   
            os.system('python3 scripts/send_s3_db.py')

        for x in confirmation:
            os.remove(upload_dir+choices123[int(x)][1])
    
    


    return render(request,'search/search_upload.html',{'update_db_form':update_db_form,})


@csrf_exempt
def upload_rtsp(request):



    def project_insert(project_table_project_name,project_table_project_description):
        try:
            cur.execute("insert into uvdata_projects (project_name,project_description) values (%s,%s)",(project_table_project_name,project_table_project_description))
        except:
            print ("could not insert in project table")

    def cam_insert(cam_name,site_id):
        try:
            cur.execute("insert into uvdata_cam_id (cam_id,site_id) values (%s,%s)",(cam_name,site_id))
        except:
            print ("could not insert in project table")

    def sites_update(site_table_site_name):
    
        site_images= images.objects.filter(site_id=sites.objects.get(site_name=site_table_site_name).id).count() + 1
        annotated_images= images.objects.filter(site_id=sites.objects.get(site_name=site_table_site_name).id).filter(human_or_not= True).count() + 1
    
        site_id= sites.objects.get(site_name=site_table_site_name).id
        try:
        
            cur.execute("UPDATE uvdata_sites SET site_images =%s WHERE id=%s", (site_images, site_id))
            cur.execute("UPDATE uvdata_sites SET annotated_images =%s WHERE id=%s", (annotated_images, site_id))
        except:
            print ("could not insert in sites table")
 

    def sites_insert(site_table_site_name):
    
        site_images=1
        annotated_images=1
        try:
            cur.execute("insert into uvdata_sites (site_name,site_images,annotated_images,project_id) values (%s,%s,%s,%s)",(site_table_site_name,site_images,annotated_images,projects.objects.get(project_name=project_table_project_name).id))   
        except:
            print ("could not insert in sites table")


    def images_insert(image_table_filename):

        try:
            cur.execute("insert into uvdata_images (filename,date,human_or_not,image_url,json_url,site_id) values (%s,%s,%s,%s,%s,%s)",(image_table_filename,image_table_date,image_table_human_or_not,image_table_image_url,image_table_json_url,sites.objects.get(site_name=site_table_site_name).id))
        except:
            print ("could not insert in images table")


    def class_insert(class_table_class_name):
    
        try:
            cur.execute("insert into uvdata_classes (class_name) values (%s)",(class_table_class_name,))
        except:
            print ("could not insert in classes table")



    def check_site_classes(site_id):
        select_target_query="select * from uvdata_sites_site_classes where sites_id = %s"
        try:
            cur.execute((select_target_query),(site_id,))
        except:
            print("could not fetch object class from db")


    def insert_site_classes(class_table_class_name):
        try:
            cur.execute("insert into uvdata_sites_site_classes (sites_id,classes_id) values (%s,%s)",(sites.objects.get(site_name=site_table_site_name).id,target_class_name[class_table_class_name]))
        except:
            print ("could not insert in classes_sites table")


    def check_box_records(class_id,image_id):
        select_target_query="select * from uvdata_box where box_class_id = %s and box_image_id= %s"
        try:
            cur.execute((select_target_query),(class_id,image_id,))
        except:
            print("cannot fetch from uvdata_box")


    def insert_box_records(class_id,image_id):
        try:
            cur.execute("insert into uvdata_box (box_class_id,box_image_id) values (%s,%s)",(class_id,image_id))
        except:
            print("could not insert in uvdata_box")


    target_class_name= {}
    available_classes=classes.objects.all().values()
    for x in available_classes:
        target_class_name.update({x['class_name']:x['id']})



    if request.method=="POST":
  
        json_recv= request.body.decode('utf-8')
       
    return render(request,'search/upload_rtsp.html',{})
