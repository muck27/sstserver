from rest_framework import serializers
from uvdata.models import projects,sites,images,classes


class projects_serializer(serializers.ModelSerializer):
   
    class Meta:
        model= projects
        fields='__all__'


class sites_serializer(serializers.ModelSerializer):

    class Meta:
        model= sites
        fields='__all__'

class images_serializer(serializers.ModelSerializer):

    class Meta:
        model= images
        fields=['id','filename']

class classes_serializer(serializers.ModelSerializer):

    class Meta:
        model= classes
        fields='__all__'

class dates_serializer(serializers.ModelSerializer):


    date = serializers.DateTimeField()
