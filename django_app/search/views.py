from __future__ import absolute_import, division, print_function
from datetime import datetime
import time
import imutils
import cv2 
import scipy
import imageio
from copy import deepcopy
import json
import glob
import os
import time
import itertools
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt 
import sys
user = "clyde"
import os.path
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
import subprocess
import numpy as np
from django.shortcuts import render
from django.http import HttpResponse
import django
django.setup()
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "uvlearn.settings")
sys.path.append('/home/'+user+'/Downloads/automatic_annotator_tool/django_app/')
from .forms import project_search, site_search, class_search, update_db,bounding_box
from .serializers import projects_serializer,sites_serializer,images_serializer,classes_serializer,dates_serializer
from uvdata.models import remote_instances, projects, sites, images, box, classes,cam_id,bbox,dates,login,cvat,infer_bbox,infer_models,classifier
from django.views.generic import ListView, CreateView, UpdateView 
from django.forms import modelformset_factory
import os
import boto3
import uuid
from django.views.decorators.csrf import csrf_exempt
import json
import psycopg2
import sys
import shutil
import psycopg2
import json
import os
import os.path
from os import path
import zipfile
import glob
import botocore
from PIL import Image, ImageDraw
import datetime
import random
from shapely.geometry import Polygon
import time
from uvlearn import settings
from PIL import Image
from scripts.ssim.ssim import SSIM
from download_s3.models import projects_s3,sites_s3,dates_s3,images_s3,cams_s3,images_s3

########API view
from rest_framework.pagination import PageNumberPagination
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
from django.core.exceptions import ObjectDoesNotExist
####credentials 
#client = boto3.client('s3', aws_access_key_id='AKIARX6S2JWR245EG3UD', aws_secret_access_key='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',)
s3 = boto3.resource('s3')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "uvlearn.settings")
bucket = s3.Bucket('uploads.live.uncannysurveillance.com')


download_dir = "/home/clyde/Downloads/automatic_annotator_tool/data/queried_data/"
upload_dir = "/home/clyde/Downloads/automatic_annotator_tool/data/upload_data/"
tmp_dir="/home/clyde/Downloads/automatic_annotator_tool/data/"
display_dir="/home/clyde/Downloads/automatic_annotator_tool/data/display_images/"


queried_projects ='/data/queried_data/download_projects/'
queried_sites ='/data/queried_data/download_sites/'
queried_dates ='/data/queried_data/download_dates/'
queried_classes= '/data/queried_data/download_classes/'

###################
def get_project_attributes(p_id):
    if p_id==1:
        return(
    {
     1:{"HeadGear": "CAP/TURBAN","Face": "NOFACE" }, 
     2:{"HeadGear": "CAP/TURBAN","Face": "FACE"},
     3:{"HeadGear": "FACEMASK/SCARF","Face": "NOFACE"},
     4:{"HeadGear": "FACEMASK/SCARF","Face": "FACE"},
     5:{"HeadGear": "NONE","Face": "NOFACE"},  
     6:{"HeadGear": "NONE","Face": "FACE"},
     7:{"HeadGear": "HELMET","Face": "NOFACE"},
     8:{"HeadGear": "HELMET","Face": "FACE"}
   })


def new_get_project_attributes(p_id):
    if p_id==1:
        return(
    {
     "HeadGear":["CAP/TURBAN","FACEMASK/SCARF","NONE","HELMET"],
     "Face":["NOFACE","FACE"]
    } 
 )

###################

COLORS = [
    '#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c',
    '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5',
    '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f',
    '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']


def calc_iou_individual(pred_box, gt_box):
    """Calculate IoU of single predicted and ground truth box
    Args:
        pred_box (list of floats): location of predicted object as
            [xmin, ymin, xmax, ymax]
        gt_box (list of floats): location of ground truth object as
            [xmin, ymin, xmax, ymax]
    Returns:
        float: value of the IoU for the two boxes.
    Raises:
        AssertionError: if the box is obviously malformed
    """
    x1_t, y1_t, x2_t, y2_t = gt_box
    x1_p, y1_p, x2_p, y2_p = pred_box

    if (x1_p > x2_p) or (y1_p > y2_p):
        raise AssertionError(
            "Prediction box is malformed? pred box: {}".format(pred_box))
    if (x1_t > x2_t) or (y1_t > y2_t):
        raise AssertionError(
            "Ground Truth box is malformed? true box: {}".format(gt_box))

    if (x2_t < x1_p or x2_p < x1_t or y2_t < y1_p or y2_p < y1_t):
        return 0.0

    far_x = np.min([x2_t, x2_p])
    near_x = np.max([x1_t, x1_p])
    far_y = np.min([y2_t, y2_p])
    near_y = np.max([y1_t, y1_p])

    inter_area = (far_x - near_x + 1) * (far_y - near_y + 1)
    true_box_area = (x2_t - x1_t + 1) * (y2_t - y1_t + 1)
    pred_box_area = (x2_p - x1_p + 1) * (y2_p - y1_p + 1)
    iou = inter_area / (true_box_area + pred_box_area - inter_area)
    return iou


def get_single_image_results(gt_boxes, pred_boxes, iou_thr):
    """Calculates number of true_pos, false_pos, false_neg from single batch of boxes.
    Args:
        gt_boxes (list of list of floats): list of locations of ground truth
            objects as [xmin, ymin, xmax, ymax]
        pred_boxes (dict): dict of dicts of 'boxes' (formatted like `gt_boxes`)
            and 'scores'
        iou_thr (float): value of IoU to consider as threshold for a
            true prediction.
    Returns:
        dict: true positives (int), false positives (int), false negatives (int)
    """

    all_pred_indices = range(len(pred_boxes))
    all_gt_indices = range(len(gt_boxes))
    if len(all_pred_indices) == 0:
        tp = 0
        fp = 0
        fn = len(gt_boxes)
        return {'true_pos': tp, 'false_pos': fp, 'false_neg': fn}
    if len(all_gt_indices) == 0:
        tp = 0
        fp = len(pred_boxes)
        fn = 0
        return {'true_pos': tp, 'false_pos': fp, 'false_neg': fn}

    gt_idx_thr = []
    pred_idx_thr = []
    ious = []
    for ipb, pred_box in enumerate(pred_boxes):
        for igb, gt_box in enumerate(gt_boxes):
            iou = calc_iou_individual(pred_box, gt_box)
            if iou > iou_thr:
                gt_idx_thr.append(igb)
                pred_idx_thr.append(ipb)
                ious.append(iou)

    args_desc = np.argsort(ious)[::-1]
    if len(args_desc) == 0:
        # No matches
        tp = 0
        fp = len(pred_boxes)
        fn = len(gt_boxes)
    else:
        gt_match_idx = []
        pred_match_idx = []
        for idx in args_desc:
            gt_idx = gt_idx_thr[idx]
            pr_idx = pred_idx_thr[idx]
            # If the boxes are unmatched, add them to matches
            if (gt_idx not in gt_match_idx) and (pr_idx not in pred_match_idx):
                gt_match_idx.append(gt_idx)
                pred_match_idx.append(pr_idx)
        tp = len(gt_match_idx)
        fp = len(pred_boxes) - len(pred_match_idx)
        fn = len(gt_boxes) - len(gt_match_idx)

    return {'true_pos': tp, 'false_pos': fp, 'false_neg': fn}


def calc_precision_recall(img_results):
    """Calculates precision and recall from the set of images
    Args:
        img_results (dict): dictionary formatted like:
            {
                'img_id1': {'true_pos': int, 'false_pos': int, 'false_neg': int},
                'img_id2': ...
                ...
            }
    Returns:
        tuple: of floats of (precision, recall)
    """
    true_pos = 0; false_pos = 0; false_neg = 0
    for _, res in img_results.items():
        true_pos += res['true_pos']
        false_pos += res['false_pos']
        false_neg += res['false_neg']

    try:
        precision = true_pos/(true_pos + false_pos)
    except ZeroDivisionError:
        precision = 0.0
    try:
        recall = true_pos/(true_pos + false_neg)
    except ZeroDivisionError:
        recall = 0.0

    return (precision, recall)

def get_model_scores_map(pred_boxes):
    """Creates a dictionary of from model_scores to image ids.
    Args:
        pred_boxes (dict): dict of dicts of 'boxes' and 'scores'
    Returns:
        dict: keys are model_scores and values are image ids (usually filenames)
    """
    model_scores_map = {}
    for img_id, val in pred_boxes.items():
        if len(val)>0:
            for score in val['scores']:
                if score not in model_scores_map.keys():
                    model_scores_map[score] = [img_id]
                else:
                    model_scores_map[score].append(img_id)
    return model_scores_map

def get_avg_precision_at_iou(gt_boxes, pred_boxes, iou_thr):
    """Calculates average precision at given IoU threshold.
    Args:
        gt_boxes (list of list of floats): list of locations of ground truth
            objects as [xmin, ymin, xmax, ymax]
        pred_boxes (list of list of floats): list of locations of predicted
            objects as [xmin, ymin, xmax, ymax]
        iou_thr (float): value of IoU to consider as threshold for a
            true prediction.
    Returns:
        dict: avg precision as well as summary info about the PR curve
        Keys:
            'avg_prec' (float): average precision for this IoU threshold
            'precisions' (list of floats): precision value for the given
                model_threshold
            'recall' (list of floats): recall value for given
                model_threshold
            'models_thrs' (list of floats): model threshold value that
                precision and recall were computed for.
    """
    model_scores_map = get_model_scores_map(pred_boxes)
    sorted_model_scores = sorted(model_scores_map.keys())

    # Sort the predicted boxes in descending order (lowest scoring boxes first):
    for img_id in pred_boxes.keys():
        if len(pred_boxes[img_id]) > 0:
            arg_sort = np.argsort(pred_boxes[img_id]['scores'])
            pred_boxes[img_id]['scores'] = np.array(pred_boxes[img_id]['scores'])[arg_sort].tolist()
            pred_boxes[img_id]['boxes'] = np.array(pred_boxes[img_id]['boxes'])[arg_sort].tolist()

    pred_boxes_pruned = deepcopy(pred_boxes)

    precisions = []
    recalls = []
    model_thrs = []
    img_results = {}
    total_tp=0
    total_fp=0
    total_fn=0
    # Loop over model score thresholds and calculate precision, recall
    for ithr, model_score_thr in enumerate(sorted_model_scores):
        # On first iteration, define img_results for the first time:
        img_ids = gt_boxes.keys() if ithr == 0 else model_scores_map[model_score_thr]
        for img_id in img_ids:
            gt_boxes_img = gt_boxes[img_id]
            if len(pred_boxes_pruned[img_id]) > 0:
                box_scores = pred_boxes_pruned[img_id]['scores']
                start_idx = 0
                for score in box_scores:
                    if score < model_score_thr:
#                         pred_boxes_pruned[img_id]
                        start_idx += 1
                    else:
                        break

                # Remove boxes, scores of lower than threshold scores:
                pred_boxes_pruned[img_id]['scores'] = pred_boxes_pruned[img_id]['scores'][start_idx:]
                pred_boxes_pruned[img_id]['boxes'] = pred_boxes_pruned[img_id]['boxes'][start_idx:]

                # Recalculate image results for this image
                img_results[img_id] = get_single_image_results(
                    gt_boxes_img, pred_boxes_pruned[img_id]['boxes'], iou_thr)
                total_tp = total_tp + img_results[img_id]['true_pos'] 
                total_fn = total_fn + img_results[img_id]['false_neg'] 
                total_fp = total_fp + img_results[img_id]['false_pos'] 
            else:
                img_results[img_id] = {'true_pos': 0, 'false_pos': 0, 'false_neg': len(gt_boxes_img)}
                total_tp = total_tp + img_results[img_id]['true_pos']
                total_fn = total_fn + img_results[img_id]['false_neg']
                total_fp = total_fp + img_results[img_id]['false_pos']
        prec, rec = calc_precision_recall(img_results)
        precisions.append(prec)
        recalls.append(rec)
        model_thrs.append(model_score_thr)

    precisions = np.array(precisions)
    recalls = np.array(recalls)
    prec_at_rec = []
    for recall_level in np.linspace(0.0, 1.0, 11):
        try:
            args = np.argwhere(recalls >= recall_level).flatten()
            prec = max(precisions[args])
        except ValueError:
            prec = 0.0
        prec_at_rec.append(prec)
    avg_prec = np.mean(prec_at_rec)

    return {
        'avg_prec': avg_prec,
        'precisions': precisions,
        'recalls': recalls,
        'model_thrs': model_thrs,
        'total_tp':total_tp,
        'total_fn':total_fn,
        'total_fp':total_fp
    }



def plot_pr_curve(
    precisions, recalls, category='Person', label=None, color=None, ax=None):
    """Simple plotting helper function"""

    if ax is None:
        plt.figure(figsize=(10,8))
        ax = plt.gca()

    if color is None:
        color = COLORS[0]
    ax.scatter(recalls, precisions, label=label, s=20, color=color)
    ax.set_xlabel('recall')
    ax.set_ylabel('precision')
    ax.set_title('Precision-Recall curve for {}'.format(category))
    ax.set_xlim([0.0,1.3])
    ax.set_ylim([0.0,1.2])
    return ax

######i
def get_sites_classes(s_list):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        c_list = []
        for x in s_list:
            query = "select classes_id from uvdata_sites_site_classes where sites_id = {}".format(x.id)
            cur.execute(query)
            model_records = cur.fetchall()
            print(model_records)
            for x in model_records:
                c_name = classes.objects.get(id=x[0]).class_name
                if c_name not in c_list:
                    c_list.append(c_name)


        return(c_list)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()



def get_site_classes(s_list):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        c_list = []
        for x in s_list:
            query = "select classes_id from uvdata_sites_site_classes where sites_id = {}".format(str(x))
            cur.execute(query)
            model_records = cur.fetchall()
            for rec in model_records:
                c_name = classes.objects.get(id=rec[0]).class_name
                if c_name not in c_list:
                    c_list.append(c_name)
        return(c_list)
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()


def get_site_classifiers(s_list):
     response = []
     response_2 = []
     for site in s_list:
         attrs = attributes.objects.filter(site_id=site)
         for attr in attrs:
             attr_keys = attr.attribute.keys()
             for k in attr_keys:
                 if k not in response:
                     response.append(k)
     print(response)
     for x in response:
         tmp =  {}
         tmp['classifier'] = x
         tmp['s_ids'] = []
         tmp['attributes'] = []
         for site in s_list:
             attrs = attributes.objects.filter(attribute__has_key=x).filter(site_id=site)
             if attrs:
                 if site not in tmp['s_ids']:
                     tmp['s_ids'].append(site)
                 for attr in attrs:
                     attr_value = attr.attribute[x]
                     if attr_value not in tmp['attributes']:
                         tmp['attributes'].append(attr_value)
         response_2.append(tmp)
     print(response_2)
     return(response_2)

def get_sites_classifiers(s_list):
     response = []
     response_2 = []
     query = classifier.objects.all().distinct('classifier')
     for x in query:
         tmp = {}
         tmp['s_ids'] = []
         tmp['classifier'] = x.classifier
         tmp['attributes'] = []
         for s in s_list:
             if classifier.objects.filter(classifier = x.classifier).filter(site_id = s.id):
                 tmp['s_ids'].append(s.id)
                 query_2 = classifier.objects.filter(site_id = s, classifier= x.classifier)
                 if query_2:
                     for attr in query_2:
                         if attr.attribute not in tmp['attributes']:
                             tmp['attributes'].append(attr.attribute)
         response.append(tmp)
         
     for r in response:
         if len(r['s_ids']) != 0:
             response_2.append(r)    
              
     return(response_2)



######################
class list_projects(APIView):
    
    def get(self,request):
        projects_data= projects.objects.all()
        response =[]
        for x in projects_data:
            tmp={}
            tmp["project_name"] = x.project_name
            tmp['id'] = x.id
            response.append(tmp)
        return Response(response)



class project_info(APIView):
    def get(self,request,p_id):
        projects_data= projects.objects.get(id=p_id)
        tmp = {}
        conf=0
        tmp["image_count"] =0
        for y in  sites.objects.filter(project_id=p_id):
            tmp["image_count"]=tmp["image_count"]+int(y.site_images)
       
        s_list  = sites.objects.filter(project_id=p_id)
        classes = get_sites_classes(s_list)
        classifiers = get_sites_classifiers(s_list)
        tmp["id"] = p_id
        tmp['class_info']=[]
        tmp['avg_conf'] = []
        tmp['classifier_info'] = []
        for x in classifiers:
            tmp3 =  {}
            tmp3['classifier'] = x['classifier'] 
            tmp3['attribute_count'] = []
            for y in x['attributes']:
                tmp2 = {}
                tmp2['attribute']  = y
                tmp2['h_count'] = 0
                tmp2['m_count'] = 0
                tmp2['i_count'] = 0
                for s_id in x['s_ids']:
                    tmp2['h_count']    = bbox.objects.filter(site_id=s_id).filter(classifier__contains={x['classifier']:y}).filter(box_type='human').count() + tmp2['h_count']     
                    tmp2['m_count']    = bbox.objects.filter(site_id=s_id).filter(classifier__contains={x['classifier']:y}).filter(box_type='model').count() + tmp2['m_count']     
                    tmp2['i_count']    = bbox.objects.filter(site_id=s_id).filter(classifier__contains={x['classifier']:y}).filter(box_type='infer').count() + tmp2['i_count']     
                tmp3['attribute_count'].append(tmp2)
            tmp['classifier_info'].append(tmp3)

             
        for c in classes:
            tmp1={}
            tmp1['class'] = c
            tmp1['h_count'] = 0
            tmp1['m_count'] = 0
            tmp1['i_count'] = 0
            tmp2={}
            tmp2['class'] = c
            h_conf = 0
            m_conf = 0
            i_conf = 0
            for s in s_list:
                h_query = bbox.objects.filter(box_class=c).filter(box_type="human").filter(site_id=s.id)
                m_query = bbox.objects.filter(box_class=c).filter(box_type="model").filter(site_id=s.id)
                i_query = bbox.objects.filter(box_class=c).filter(box_type="infer").filter(site_id=s.id)
                tmp1['h_count'] = int(h_query.count()) + tmp1['h_count']
                tmp1['m_count'] = int(m_query.count()) + tmp1['m_count']
                tmp1['i_count'] = int(i_query.count()) + tmp1['i_count']
       
                for h in h_query:
                    h_conf = h_conf + h.conf                 
                for m in m_query:
                    m_conf = m_conf + m.conf 
                for i in i_query:
                    i_conf = i_conf + i.conf
                
            tmp3={}
            if tmp1['h_count'] !=0:
                tmp3['h_conf'] =  float(h_conf/int(tmp1['h_count']))  
            else:
                tmp3['h_conf'] =  0        
            if tmp1['m_count'] !=0:
                tmp3['m_conf'] =  float(m_conf/int(tmp1['m_count']))
            else:
                tmp3['m_conf'] =  0            
            if tmp1['i_count'] !=0:
                tmp3['i_conf'] =  float(i_conf/int(tmp1['i_count']))
            else:
                tmp3['i_conf'] =  0
            tmp2['conf'] = tmp3
            tmp["avg_conf"].append(tmp2)
            tmp["class_info"].append(tmp1)

        return Response(tmp)	



class list_sites(APIView):
    
    def get(self,request,p_id):
        s_list= sites.objects.filter(project_id=p_id)
        response =[]
        for x in s_list:
            tmp={}
            tmp["id"]= x.id
            tmp["site_name"] = x.site_name
            response.append(tmp)
        return Response(response)



class site_info(APIView):
    def get(self,request,s_id):
        queryset =  images.objects.filter(site_id=s_id)
        p_id = sites.objects.get(id = s_id).project_id
        s_list = sites.objects.filter(project_id=p_id)
        tmp={}
        tmp["image_count"] = queryset.count()
        filters = get_filters(s_id)
        tmp['filters']=[]
        classes = get_sites_classes(s_list)
 
        if filters !=0:
            for f in filters:
                tmp2={}
                tmp2['count'] = queryset.filter(alerts__contains={f:True}).count()
                tmp2['filter']   = f
                tmp['filters'].append(tmp2)
        
         
        tmp["class_info"]=[]
        tmp['avg_conf'] = []
        for c in classes:
            h_query = bbox.objects.filter(box_class=c).filter(box_type="human").filter(site_id = s_id)   
            m_query = bbox.objects.filter(box_class=c).filter(box_type="model").filter(site_id = s_id)   
            i_query = bbox.objects.filter(box_class=c).filter(box_type="infer").filter(site_id = s_id)   
            tmp1={}
            tmp1['class'] = c
            tmp1['h_count'] = int(h_query.count())
            tmp1['m_count'] = int(m_query.count())
            tmp1['i_count'] = int(i_query.count())
            tmp['class_info'].append(tmp1)

            tmp2={}
            tmp2['class'] = c
            h_conf = 0
            m_conf = 0
            i_conf = 0
            for h in h_query:
                h_conf = h_conf + h.conf
            for m in m_query:
                m_conf = m_conf + m.conf
            for i in i_query:
                i_conf = i_conf + i.conf

            tmp3={}
            if tmp1['h_count'] !=0:
                tmp3['h_conf'] =  float(h_conf/int(tmp1['h_count']))
            if tmp1['m_count'] !=0:
                tmp3['m_conf'] =  float(m_conf/int(tmp1['m_count']))
            if tmp1['i_count'] !=0:
                tmp3['i_conf'] =  float(i_conf/int(tmp1['i_count']))
            tmp2['conf'] = tmp3
            tmp["avg_conf"].append(tmp2)
 
        return Response(tmp)



def get_filters(s_id):
    response = sites.objects.get(id = s_id).alerts
    print(response)
    return(response)

class list_dates(APIView):
    def get(self,request,p_id,s_id):
        from datetime import datetime

        d_list = dates.objects.filter(site_id=s_id)
        response=[]
        for d in d_list:
            tmp={}
            tmp["date"]=str(d.date)
            tmp["id"] = d.id
            response.append(tmp)
        response = sorted(response,key=lambda x: datetime.strptime(x['date'], '%Y-%m-%d'))
        return Response(response)




class date_meta(APIView):
    def get(self,request,d_id):
        s_id = dates.objects.get(id=d_id).site_id
        filters = get_filters(s_id)
        classes = get_sites_classes(sites.objects.filter(id= s_id))
        queryset = images.objects.filter(date_id = d_id)
        tmp={}
        tmp["image_count"]=queryset.count()
        tmp["cam_ids"] =[]
        queryset2 = cam_id.objects.filter(date_id=d_id)
        for query in queryset2:
            tmp3={}
            tmp3["id"]=query.id
            tmp3["cam"]=query.cam_id
            tmp["cam_ids"].append(tmp3)
        tmp['filters'] = []

        if filters !=0:
            for f in filters:
                tmp2={}
                tmp2['filter'] = f
                tmp2['count']  = queryset.filter(alerts__contains={f:True}).count()
                tmp['filters'].append(tmp2)

            
        tmp["class_info"]=[]
        tmp['avg_conf'] = []
        tmp["id"]= d_id
        for c in classes:
            h_query = bbox.objects.filter(box_class=c).filter(box_type="human").filter(date_id = d_id)
            m_query = bbox.objects.filter(box_class=c).filter(box_type="model").filter(date_id = d_id)
            i_query = bbox.objects.filter(box_class=c).filter(box_type="infer").filter(date_id = d_id)
            
            tmp1={}
            tmp1['class'] = c
            tmp1['h_count'] = int(h_query.count())
            tmp1['m_count'] = int(m_query.count())
            tmp1['i_count'] = int(i_query.count())
            tmp['class_info'].append(tmp1)

            tmp2={}
            tmp2['class'] = c
            h_conf = 0
            m_conf = 0
            i_conf = 0
            for h in h_query:
                h_conf = h_conf + h.conf
            for m in m_query:
                m_conf = m_conf + m.conf
            for i in i_query:
                i_conf = i_conf + i.conf

            h_count = tmp1['h_count']
            m_count = tmp1['m_count']
            i_count = tmp1['i_count']
            tmp3={}
            if h_count !=0:
                tmp3['h_conf'] =  float(h_conf/int(h_count))
            if m_count !=0:
                tmp3['m_conf'] =  float(m_conf/int(m_count))
            if i_count !=0:
                tmp3['i_conf'] =  float(i_conf/int(i_count))
            tmp2['conf'] = tmp3
            tmp["avg_conf"].append(tmp2)
 
        return Response(tmp)



class list_cams(APIView):
    def get(self,request,p_id,s_id,d_id):
        list_cams = cam_id.objects.filter(date_id = d_id)
        response=[]
        for c in list_cams:
            tmp={}
            tmp['id']= c.id
            tmp["cam"]=str(c.cam_id)
            response.append(tmp)
        return Response(response)


class cam_meta(APIView):
    def get(self,request,c_id):
        d_id = cam_id.objects.get(id=c_id).date_id
        s_id = dates.objects.get(id=d_id).site_id
        filters = get_filters(s_id)
        classes = get_sites_classes(sites.objects.filter(id= s_id))
        queryset=images.objects.filter(cam_id=c_id)
        tmp = {}
        tmp["image_count"]=queryset.count()
        tmp['filters'] = []
        
        if filters !=0:
            for f in filters:
                tmp2={}
                tmp2['filter'] = f
                tmp2['count']  =queryset.filter(alerts__contains={f:True}).count()
                tmp['filters'].append(tmp2)

        tmp["class_info"]=[]
        tmp['avg_conf'] = []
        for c in classes:
            h_bbox = bbox.objects.filter(box_class=c).filter(box_type="human").filter(cam_id=c_id)
            m_bbox = bbox.objects.filter(box_class=c).filter(box_type="model").filter(cam_id=c_id)
            i_bbox = bbox.objects.filter(box_class=c).filter(box_type="infer").filter(cam_id=c_id)
            tmp1={}
            tmp1['h_count'] = int(h_bbox.count())
            tmp1['m_count'] = int(m_bbox.count())
            tmp1['i_count'] = int(i_bbox.count())
            tmp1['class'] = c_id
            tmp['class_info'].append(tmp1)

            tmp2={}
            tmp2['class'] = c_id
            h_conf = 0
            m_conf = 0
            i_conf = 0
            for h in h_bbox:
                h_conf = h_conf + h.conf
            for m in m_bbox:
                m_conf = m_conf + m.conf
            for i in i_bbox:
                i_conf = i_conf + i.conf

            h_count = h_bbox.count()
            m_count = m_bbox.count()
            i_count = i_bbox.count()
            tmp3={}
            if h_count !=0:
                tmp3['h_conf'] =  float(h_conf/int(h_count))
            if m_count !=0:
                tmp3['m_conf'] =  float(m_conf/int(m_count))
            if i_count !=0:
                tmp3['i_conf'] =  float(i_conf/int(i_count))
            tmp2['conf'] = tmp3
            tmp["avg_conf"].append(tmp2)
        return Response(tmp)


class list_images(APIView):
    def get(self,request,p_id,s_id,d_id,c_id):

        prefix = "https://s3.ap-south-1.amazonaws.com/"
        i_list = images.objects.filter(cam_id=c_id).filter(date_id=d_id).filter(site_id=s_id)[:300]
        classes = get_sites_classes(sites.objects.filter(id= s_id))
        bucket = sites.objects.get(id = s_id).bucket
        response_data=[]
        count = 0

        for i in i_list:
            tmp={}
            tmp["id"] = i.id
            tmp["time"] = i.time
            tmp["filename"]=i.filename
            if bucket == 'localdatauv':
                tmp["image_url"]=i.image_url
            else:
                tmp["image_url"]=i.image_url
            tmp["site_name"]=sites.objects.get(id=s_id).site_name
            tmp["project_name"]=projects.objects.get(id=p_id).project_name
            tmp['coordinates']=[]
            m_conf=0
            m_count =1
            h_conf= 0
            h_count = 1
            i_conf = 0
            i_count =1

            for c in classes:

                human_coords = bbox.objects.filter(box_class=c).filter(image_id=i.id).filter(box_type="human")
                model_coords = bbox.objects.filter(box_class=c).filter(image_id=i.id).filter(box_type="model")
                infer_coords = infer_bbox.objects.filter(box_class=c).filter(image_id=i.id).filter(box_type="OPM_final") 
                tmp1={}
                tmp1["class"] = c
                tmp1["human_coords"]=[]
                for h in human_coords:
                    tmp2={}
                    tmp2["id"]=h.id
                    tmp2["coords"]= { "x":h.x, "y":h.y, "w":h.w, "h":h.h}
                    tmp2["conf"]=h.conf
                    tmp2["attributes"] = h.classifier
                    h_conf = h_conf + h.conf
                    h_count = h_count +1
                    tmp1["human_coords"].append(tmp2)

                tmp1["model_coords"]=[]
                for m in model_coords:
                    tmp2={}
                    tmp2["bbox_id"]=m.id
                    tmp2["coords"]= { "x":m.x, "y":m.y, "w":m.w, "h":m.h}
                    tmp2["conf"] = m.conf
                    tmp2["attributes"] = m.classifier
                    m_conf = m_conf + m.conf
                    m_count = m_count +1
                    tmp1["model_coords"].append(tmp2)

                tmp1["infer_coords"]=[]
                for ic in infer_coords:
                    tmp2={}
                    tmp2["bbox_id"]=ic.id
                    tmp2["coords"]= { "x":ic.x, "y":ic.y, "w":ic.w, "h":ic.h}
                    tmp2["conf"] = ic.conf
                    tmp2["attributes"] = ic.classifier
                    i_conf = i_conf + ic.conf
                    i_count = i_count +1
                    tmp1["infer_coords"].append(tmp2)

                tmp['coordinates'].append(tmp1)
               
            m_count = m_count -1 
            if m_count==0:
                response_data.append(tmp)
                continue
            else:
                tmp["model_conf"] = m_conf/m_count
                response_data.append(tmp)
        
        
        return Response(response_data)

###########
class get_p_n_r_cams(APIView):
    def post(self,request,p_id,s_id,d_id,c_id):
        input_iou = request.data["iou"]
        print(input_iou)
        i_list = images.objects.filter(cam_id=c_id).filter(site_id=s_id).filter(human_or_not=True)
        classes = get_sites_classes(sites.objects.filter(id= s_id))
        attributes = get_project_attributes(p_id)

        pred_per_attr = []
        gt_per_attr = []
        pred_per_class = []
        gt_per_class   =[]

        for c in classes:
            tmp={}
            tmp[c]={}
            pred_per_class.append(tmp)

        for c in classes:
            tmp={}
            tmp[c]={}
            gt_per_class.append(tmp)

        for k,v in attributes.items():
            tmp={}
            tmp[k]={}
            pred_per_attr.append(tmp)

        for k,v in attributes.items():
            tmp={}
            tmp[k]={}
            gt_per_attr.append(tmp)
        response_data=[]
        count = 0
        img_count = 0
        for i in i_list:
            print(img_count)
            count = count +1
            for k,v in attributes.items():
                human_coords = bbox.objects.filter(classifier=v).filter(image_id=i.id).filter(box_type="human")
                if human_coords.count() !=0 :
                    model_coords = bbox.objects.filter(classifier=v).filter(image_id=i.id).filter(box_type="model")
                    for p in pred_per_attr:
                        for k_1,v_1 in p.items():
                            if k_1 == k:
                                p[k][str(i.id)] ={}
                                p[k][str(i.id)]["boxes"]=[]
                                p[k][str(i.id)]["scores"]=[]
                                for m in model_coords:
                                    bbox_infor= [m.x,m.y,m.x+m.w,m.y+m.h]
                                    p[k][str(i.id)]["boxes"].append(bbox_infor)
                                    p[k][str(i.id)]["scores"].append(m.conf)

                    for g in gt_per_attr:
                        for k_1,v_1 in g.items():
                            if k_1 == k:
                                g[k][str(i.id)] =[]
                                for h in human_coords:
                                    bbox_infor= [h.x,h.y,h.x+h.w,h.y+h.h]
                                    g[k][str(i.id)].append(bbox_infor)


            for c in classes:
                human_coords = bbox.objects.filter(box_class=c).filter(image_id=i.id).filter(box_type="human")
                if human_coords.count() !=0:
                    model_coords = bbox.objects.filter(box_class=c).filter(image_id=i.id).filter(box_type="model")
                    for p in pred_per_class:
                        for k,v in p.items():
                            if k==c:
                                p[k][str(i.id)]={}
                                p[k][str(i.id)]["scores"]=[]
                                p[k][str(i.id)]["boxes"]=[]
                                for m in model_coords:
                                    bbox_infor = [m.x,m.y,m.x+m.w,m.y+m.h]
                                    p[k][str(i.id)]["boxes"].append(bbox_infor)
                                    p[k][str(i.id)]["scores"].append(m.conf)

                    for g in gt_per_class:
                        for n,o in g.items():
                            if n==c:
                                g[n][str(i.id)]=[]
                                for h in human_coords:
                                    bbox_infor = [h.x,h.y,h.x+h.w,h.y+h.h]
                                    g[n][str(i.id)].append(bbox_infor)
 
           
        response={} 
        response["classes"]=[]
        response["attributes"] =[]
        iou_range=[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1] 
        for p in pred_per_class:
            for g in gt_per_class:
                for k,v in p.items():
                     for x,z in g.items():
                         if k==x:
                             tmp = {}
                             tmp["class"] = k
                             tmp["precs"]=[]
                             tmp["recalls"]=[]
                             for x in iou_range:
                                 a = get_avg_precision_at_iou(z,v,iou_thr=x)
                                 if len(a["precisions"]) == 0:
                                     tmp["false_pos"]=0
                                     tmp["true_pos"]=0
                                     tmp["false_negs"]=0
                                 else:
                                     tmp["precs"].append(a["precisions"][0])
                                     tmp["recalls"].append(a["recalls"][0])
                                     if x == float(input_iou):
                                         tmp["true_pos"]     = a["total_tp"]
                                         tmp["false_pos"]     = a["total_fp"]
                                         tmp["false_negs"]     = a["total_fn"]
                                         print(a)
                             response["classes"].append(tmp)


        for p in pred_per_attr:
            for g in gt_per_attr:
                for k,v in p.items():
                     for x,z in g.items():
                         if k==x:
                             tmp = {}
                             tmp["attribute"] = str(attributes[k])
                             tmp["precs"]=[]
                             tmp["recalls"]=[]
                             for x in iou_range:
                                 a = get_avg_precision_at_iou(z,v,iou_thr=x)
                                 if len(a["precisions"]) == 0:
                                     tmp["false_pos"]=0
                                     tmp["false_negs"]=0
                                     tmp["true_pos"]=0
                                 else:
                                     tmp["precs"].append(a["precisions"][0])
                                     tmp["recalls"].append(a["recalls"][0])
                                     if x == float(input_iou):
                                         tmp["true_pos"]     = a["total_tp"]
                                         tmp["false_pos"]     = a["total_fp"]
                                         tmp["false_negs"]     = a["total_fn"]
                                         print(a)
                             response["attributes"].append(tmp)

        return Response(response)




########


class get_p_n_r(APIView):
    def post(self,request,p_id,s_id,d_id):
        input_iou = request.data["iou"]
        ground_model = request.data['gt']
        pred_model  = request.data['pt']
        conf = request.data['conf']

        i_list = images.objects.filter(date_id=d_id).filter(site_id=s_id).filter(human_or_not=True)
        classes = get_sites_classes(sites.objects.filter(id= s_id))

        classifier_query = classifier.objects.filter(site_id=15)
        classifier_query = classifier.objects.filter(site_id=15).distinct('classifier')
        attributes = {}
        for x in classifier_query:
            attributes[x.classifier]= []
            for y in  classifier.objects.filter(site_id=15):
                attributes[x.classifier].append(y.attribute)
 
      #  attributes = new_get_project_attributes(p_id)
        response_data=[]
        count = 0
        img_count  = 0


        '''
        create a list of objects after  matching ious 
        '''
        img_objects = []
        obj_list = []
        dangle_list = []
        for i in i_list:
            if ground_model == 'human' or ground_model =='model':
                human_coords = bbox.objects.filter(image_id=i.id).filter(box_type=ground_model)
            else:
                human_coords = infer_bbox.objects.filter(image_id=i.id).filter(box_type=ground_model).filter(conf__gte=conf)

            if human_coords.count() !=0 :
                if pred_model == 'human' or pred_model =='model':
                    model_coords = bbox.objects.filter(image_id=i.id).filter(box_type=pred_model)
                else:
                    model_coords = infer_bbox.objects.filter(image_id=i.id).filter(box_type=pred_model).filter(conf__gte=conf)
 
                m_list = []
                tmp_dangling = {}
                tmp_dangling['id'] = i.id
                tmp_dangling['m_coords'] = []
                tmp_dangling['h_coords'] = []
                
  
                for x in model_coords:
                    tmp = {}
                    tmp['attr'] = x.classifier
                    tmp['boxes'] = [x.x,x.y,x.w+x.x,x.h+x.y]
                    tmp['class'] = x.box_class
                    m_list.append(tmp)

                h_list = []
                for x in human_coords:
                    tmp = {}
                    tmp['attr'] = x.classifier
                    tmp['boxes'] = [x.x,x.y,x.w+x.x,x.h+x.y]
                    tmp['class'] = x.box_class
                    h_list.append(tmp)
                    
                
   
                '''
                compute matching boxes for classifier
                '''
                new_m_list = []
                new_h_list = []
                for x in m_list: 
                    for y in h_list:
                        '''
			calculate iou and append to list if iou > user iou
                        '''
                        iou =  calc_iou_individual(x['boxes'], y['boxes'])
                        if iou > float(input_iou):
                            tmp = {}
                           # tmp['pred_bbox'] =  x['boxes']     
                           # tmp['human_bbox'] = y['boxes']   
                            tmp['pred_attr'] = x['attr']
                            tmp['gt_attr'] = y['attr']
                            tmp['gt_class'] = y['class']
                            tmp['pred_class'] = x['class']
                            img_objects.append(tmp)
                      
                            new_m_list.append(x)
                            new_h_list.append(y)

                '''
                get dangling coordinates
                '''
                for x in m_list:
                    if x in new_m_list:
                        continue
                    else:
                        tmp_dangling['m_coords'].append(x) 


                for y in h_list:
                    if y in new_h_list:
                        continue
                    else:
                        tmp_dangling['h_coords'].append(y)
 
                dangle_list.append(tmp_dangling)

        '''
        compute detector pnr
        '''
        response = {}
        response['detector'] = []
        response['classifier'] = []

        if pred_model == 'model':     
            for c in classes:
                tp = 0
                fp = 0
                fn = 0
                tmp = {}
                tmp['class'] = c
                for i in img_objects:
                    if i['gt_class'] == c:
                        if i['pred_class'] == c:
                            tp = tp + 1
                        else:
                            fp = fp + 1
                    else:
                        continue

                for dangle in dangle_list:
                    for m in dangle['m_coords']:
                        if m['class'] == c:
                            fp = fp + 1

                    for h in dangle['h_coords']:
                        if h['class'] == c:
                            fn = fn + 1


                tmp['tp']  = tp
                tmp['fp']  = fp
                tmp['fn']  = fn
                response['detector'].append(tmp)

            for x in attributes.keys():
                attr_key = x
                attr_labels = attributes[x]

                '''
                create matrix for the specific classifier
                '''
                tmp = {}
                tmp['attributes'] = attr_labels
                matrix = []
                for i in range(len(attr_labels)):
                    row = []
                    for j in range(len(attr_labels)):
                        row.append(0)
                    matrix.append(row)

                for i in img_objects:
                    if attr_key in i['pred_attr'].keys():
                        if attr_key in i['gt_attr'].keys():

                            '''
                            get indexes of object  attributes wrt to attr_labels and increment matrix for that specific value
                            '''

                            p_idx = attr_labels.index(i['pred_attr'][attr_key])
                            g_idx = attr_labels.index(i['gt_attr'][attr_key])
                            matrix[p_idx][g_idx] = matrix[p_idx][g_idx] + 1

                tmp['confusion_matrix'] = matrix
                response['classifier'].append(tmp)
                #df = DataFrame(matrix)
                #df.columns = attr_labels
                #df.index   = attr_labels
                #print(df)



        else:
            model_character = infer_models.objects.get(model=pred_model).model_type
            if model_character == 'detector':
                for c in classes:
                    tp = 0
                    fp = 0
                    fn = 0
                    tmp = {}
                    tmp['class'] = c
                    for i in img_objects:
                        if i['gt_class'] == c:
                            if i['pred_class'] == c:
                                tp = tp + 1
                            else:
                                fp = fp + 1 
                        else:
                            continue
        
                    for dangle in dangle_list:
                        for m in dangle['m_coords']:
                            if m['class'] == c:
                                fp = fp + 1
        
                        for h in dangle['h_coords']:
                            if h['class'] == c:
                                fn = fn + 1    
                        
           
                    tmp['tp']  = tp        
                    tmp['fp']  = fp        
                    tmp['fn']  = fn
                    response['detector'].append(tmp)        
 
            if model_character == 'classifier':
                for x in attributes.keys():
                    attr_key = x
                    attr_labels = attributes[x]
                     
                    '''
                    create matrix for the specific classifier
                    ''' 
                    tmp = {}
                    tmp['attributes'] = attr_labels
                    matrix = []
                    for i in range(len(attr_labels)):
                        row = []
                        for j in range(len(attr_labels)):
                            row.append(0)
                        matrix.append(row)
                
                    for i in img_objects:
                        if attr_key in i['pred_attr'].keys():
                            if attr_key in i['gt_attr'].keys():

                                '''
                                get indexes of object  attributes wrt to attr_labels and increment matrix for that specific value
                                '''
                                
                                p_idx = attr_labels.index(i['pred_attr'][attr_key])
                                g_idx = attr_labels.index(i['gt_attr'][attr_key])
                                matrix[p_idx][g_idx] = matrix[p_idx][g_idx] + 1

                    tmp['confusion_matrix'] = matrix
                    response['classifier'].append(tmp)
                 #   df = DataFrame(matrix)
                 #   df.columns = attr_labels
                 #   df.index   = attr_labels
                 #   print(df) 
           
        print(response)
        return Response(response)



class list_class(APIView):

    def get(self,request):
        classes_data= classes.objects.all()
        class_serializer=classes_serializer(classes_data,many=True)
        return Response(class_serializer.data)


    def post(self,request):
        class_list = request.data['id']
        folder = request.data['folder']
        tmp ={}
        tmp["filter"] = class_list
        tmp["order"] = "class_id"
        create_dir = 'mkdir {}{}'.format(queried_classes,folder)
        os.system(create_dir)
        file_desc = "{}{}/file_description.json".format(queried_classes,folder)
        with open(file_desc, 'w') as f:
            json.dump(tmp, f)

        for c_id in class_list:
            image_list = images.objects.filter(classes_id=c_id)
            for x in image_list:
                img_url = x.image_url
                json_url = x.json_url
                download_image="aws s3 cp s3://{} {}{}/images/".format(img_url,queried_classes,folder)
                download_json = "aws s3 cp s3://{} {}{}/annotations/".format(json_url,queried_classes,folder)
                os.system(download_image)
                os.system(download_json)
 

        return Response(request.data)


class list_single_image(APIView):
     
    def get(self,request,p_id,s_id,d_name,i_id):
        image_s3= images.objects.get(id=i_id).image_url
        json_s3 = images.objects.get(id= i_id).json_url
        download_image="aws s3 cp s3://"+image_s3+ ' '+'static/image.jpg' 
        download_json="aws s3 cp s3://"+json_s3+' '+'static/json.json'
        os.system(download_image)
        os.system(download_json)

  
        return Response("http://35.154.101.205:8000/static/image.jpg")


class draw_images_projects(APIView):

    def get(self,request,p_id,s_id,d_name,i_id): 
        im = Image.open("/home/clyde/Downloads/automatic_annotator_tool/django_app/static/image.jpg")
        image = ImageDraw.Draw(im)
        with open("/home/clyde/Downloads/automatic_annotator_tool/django_app/static/json.json", encoding='utf-8') as data_file:
            data = json.loads(data_file.read())
     
        elements= 0
        counter = []
        for x in data:
            if x == 'event_json':
                elements = elements + 1
                counter.append(elements)

        for iterations in counter:
            xmin=data['event_json']['event']['coordinates']['xmin']
            ymin=data['event_json']['event']['coordinates']['ymin']
            width=data['event_json']['event']['coordinates']['width']
            height=data['event_json']['event']['coordinates']['height']
            a=(xmin,ymin)
            b=(xmin+width,ymin)
            c=(xmin,ymin+height)
            d=(xmin+width,ymin+height)
            line_color = (0, 0, 255)
            image.line([a,b], fill=line_color, width=2)
            image.line([c,d], fill=line_color, width=2)
            image.line([a,c], fill=line_color, width=2)
            image.line([b,d], fill=line_color, width=2)
            im.save('/home/clyde/Downloads/automatic_annotator_tool/django_app/static/image_bounding.jpg')



        return Response("http://35.154.101.205:8000/static/image_bounding.jpg")


class list_classes(APIView):

    def get(self,request):
        classes_data= classes.objects.all()
        class_serializer= classes_serializer(classes_data,many=True)
        return Response(class_serializer.data)


class list_class_images(APIView):

    def get(self,request,class_id):
        prefix = "https://s3.ap-south-1.amazonaws.com/"
        select_target_query="select * from uvdata_box where box_class_id = %s"
        try:
            cur.execute((select_target_query),(class_id,))
        except:
            print("could not fetch object class from db")
        image_class_records = cur.fetchall()
        image_ids=[]

        for x in image_class_records:
            image_ids.append(x[2])
        id_array=list(image_ids)
        images_summary=[]
        for x in id_array:
            
            i_query=images.objects.get(id=x)
            class_name= str(classes.objects.get(id=class_id))
            i_id=i_query.id,
            images_summary.append({
		"id":i_query.id,
		"image_url":prefix+i_query.image_url,
		"human_or_not":i_query.human_or_not,
		"filename":i_query.filename,
		"site_name":str(sites.objects.get(id=i_query.site_id).site_name),
		"project_name":str(projects.objects.get(id =sites.objects.get(id =i_query.site_id).project_id)),

                "human_coords":{
			"x":int(bbox.objects.filter(box_class = class_name).filter(image_id = i_id).get(box_type="human").x),
	 		"y":int(bbox.objects.filter(box_class = class_name).filter(image_id = i_id).get(box_type="human").y), 
			"w":int(bbox.objects.filter(box_class = class_name).filter(image_id = i_id).get(box_type="human").w),
			"h":int(bbox.objects.filter(box_class = class_name).filter(image_id = i_id).get(box_type="human").h),
			"class":class_name
                        },
                "model_coords":{
			"x":str(bbox.objects.filter(box_class = class_name).filter(image_id = i_id).get(box_type="model").x),
			"y":str(bbox.objects.filter(box_class = class_name).filter(image_id = i_id).get(box_type="model").y),
			"w":str(bbox.objects.filter(box_class = class_name).filter(image_id = i_id).get(box_type="model").w),
			"h":str(bbox.objects.filter(box_class = class_name).filter(image_id = i_id).get(box_type="model").h),
			"class":class_name
			},
                "infer_coords":{
			"x":str(bbox.objects.filter(box_class = class_name).filter(image_id = i_id).get(box_type="infer").x),
			"y":str(bbox.objects.filter(box_class = class_name).filter(image_id = i_id).get(box_type="infer").y),
		        "w":str(bbox.objects.filter(box_class = class_name).filter(image_id = i_id).get(box_type="infer").w),
			"h":str(bbox.objects.filter(box_class = class_name).filter(image_id = i_id).get(box_type="infer").h),
			"class":class_name
			},

                "conf":str(images.objects.get(id=i_query.id).conf) 
		})

        return Response(images_summary)


def get_target_classes(s_id):
    try:
        con = psycopg2.connect(host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",database="uvdb4uv1",user="postgres",password="uvdbuvdb")
        cur = con.cursor()
        query = "select classes_id from uvdata_sites_site_classes where sites_id = {}".format(s_id)
        cur.execute(query)
        model_records = cur.fetchall()
        for x in model_records:
            if str(classes.objects.get(id=x[0]).class_name) == "nodetections":
                return(x[0]) 
    except (Exception, psycopg2.Error) as error:
        print("could not insert in uvdata_bbox",error)
    finally:
        if (con):
            cur.close()
            con.close()


class compute_ssim(APIView):
    
    def post(self,request):
        s_id = request.data['s_id']
        target_class = get_target_classes(s_id)
        d_id = request.data['d_id']
        c_id = request.data["c_id"]
        print(s_id,d_id,c_id)
        d_name = dates.objects.get(id=d_id).date
        c_name = cam_id.objects.get(id=c_id).cam_id
        cam_images=[]
        pair_images=[]
        base_dir = '/data/downloads'
        s_query = sites.objects.get(id=s_id)
        s_name  = s_query.site_name
        p_id    = s_query.project_id
        p_name = projects.objects.get(id=p_id).project_name
        response = []
        image_list  = images.objects.filter(cam_id = c_id).filter(alerts__contains={'tamper':True}) 
        for i in image_list:
            cam_images.append("{}/{}/{}/{}/{}/{}".format(base_dir,p_name,s_name,d_name,c_name,i.filename))

        img_pairs= []
        for i in cam_images:
            suffix = i[-5:]
            if suffix == "0.jpg":
                tmp2=[]   
                tmp2.append(i[:-5]+'1.jpg')
                tmp2.append(i)
            if suffix == "1.jpg":
                tmp2=[]
                tmp2.append(i[:-5]+'0.jpg')
                tmp2.append(i)
            img_pairs.append(tmp2)
        img_pairs.sort()
        pair_images= list(img_pairs for img_pairs,_ in itertools.groupby(img_pairs))
        ssim_values=[]
        if len(pair_images) !=0: 
            for x in pair_images:
                image1 = imageio.imread(x[0])
                image2 = imageio.imread(x[1])
                max_val = 100 
                value = SSIM(image1, image2, max_val)
                print(value)
                ssim_values.append(value)
        if len(ssim_values)==0:
            return Response(0)
        else:
            avg_ssim = sum(ssim_values)/len(ssim_values)
            return Response(avg_ssim)




class compute_ssim_date(APIView):

    def post(self,request):
        d_id = request.data['d_id']
        d_query = dates.objects.get(id=d_id)
        p_id  = d_query.project_id
        s_id  = d_query.site_id
        p_name = projects.objects.get(id=p_id).project_name
        s_name = sites.objects.get(id=s_id).site_name
        d_name = d_query.date
        base_dir = './ssim_store/' 
        cams = cam_id.objects.filter(date_id=d_id)
        response = []
        for cam in cams:
            cam_images = []
            image_list  = images.objects.filter(cam_id = cam.id).filter(alerts__contains={'tamper':True})
            for i in image_list:
                cam_images.append(i.image_url)
               # cam_images.append("{}/{}/{}/{}/{}/{}".format(base_dir,p_name,s_name,d_name,cam.cam_id,i.filename))
            img_pairs= []
            for i in cam_images:
                suffix = i[-5:]
                if suffix == "0.jpg":
                    tmp2=[]
                    tmp2.append(i[:-5]+'1.jpg')
                    tmp2.append(i)
                if suffix == "1.jpg":
                    tmp2=[]
                    tmp2.append(i[:-5]+'0.jpg')
                    tmp2.append(i)
                img_pairs.append(tmp2)
            img_pairs.sort()
            pair_images= list(img_pairs for img_pairs,_ in itertools.groupby(img_pairs))
            ssim_values=[]
            if len(pair_images) !=0:
                for x in pair_images:
                    file_1 = 'wget -O ./ssim_store/file_1.jpg {}'.format(x[0])
                    file_2 = 'wget -O ./ssim_store/file_2.jpg {}'.format(x[1])
                    os.system(file_1)    
                    os.system(file_2)    
                   
                    image1 = imageio.imread('./ssim_store/file_1.jpg')
                    image2 = imageio.imread('./ssim_store/file_2.jpg')
                    max_val = 100
                    value = SSIM(image1, image2, max_val)
                    print(value)
                    ssim_values.append(value)

            if len(ssim_values)==0:
                continue
            else:
                avg_ssim = sum(ssim_values)/len(ssim_values)
                tmp= {}
                tmp['avg_ssim'] = avg_ssim
                tmp['cam_name'] = cam.cam_id
                response.append(tmp)

        with open('/data/ssim.json','w') as data:
            json.dump(response,data)

        return Response(response)


class login_page(APIView):
    def post(self,request):
        user = request.data['user']
        password = request.data['password']
        print(user,password) 
        try:
            verify = login.objects.get(user=user,password=password)
            print(verify.user_type)
            return Response({"user_type":verify.user_type})

        except ObjectDoesNotExist:
            return Response({"user_type":None})




class resources(APIView):
    def get(self,request):
        resources = cvat.objects.all()
        response = []
        for r in resources:
            tmp={}
            tmp['port'] = r.port
            tmp['job']  = r.job
            if type(r.annotators) == list:
                seperator = ', '
                tmp['annotators'] = seperator.join(r.annotators)
            else:
                tmp['annotators'] = None
            tmp['filters'] = ''
            applied_filters = []
            if type(r.filters) == dict:
                for k,v in r.filters.items():
                    print(k,v)
                    if v == True:
                        applied_filters.append(k)

            seperator=', '
            tmp['filters']  = seperator.join(applied_filters)          
            tmp['semi_annotations'] = r.predictions
            tmp['task']  = r.task	        
            tmp['images'] = bbox.objects.filter(port=r.port).filter(box_type='model').filter(task_id__isnull=False).distinct('image_id').count()
            tmp['completed'] = bbox.objects.filter(port=r.port).filter(box_type='human').distinct('image_id').count()
            response.append(tmp)
        return Response(response) 

  
'''
Download Images + JSons
'''

class download_testing(APIView):
    def post(self,request):
        from shutil import copyfile
        d_id = request.data['d_id']
        response = []
        s_id = dates.objects.get(id=d_id).site_id
        p_id = dates.objects.get(id=d_id).project_id
        s_name = sites.objects.get(id=s_id).site_name
        bucket =sites.objects.get(id=s_id).bucket 
        p_name = projects.objects.get(id=p_id).project_name
        d_name  = dates.objects.get(id=d_id).date
        ## 1,2,4,6,7,8
        i_ids   = images.objects.filter(date_id=d_id).filter(human_or_not=True)
        
        target_dir ='/data/{}_{}'.format(p_name.replace(' ','_'),d_name)
        if path.exists(target_dir) == True:
            shutil.rmtree(target_dir)
        else:
            os.mkdir(target_dir)

        if bucket == 'uploads.live.uncannysurveillance.com':
            base_dir = '/data/downloads/'
        if bucket == 'uploads.live.videoanalytics':
            base_dir = '/data/downloads/videoanalytics/'
        if bucket == 'localdatauv':
            base_dir = '/data/downloads/localData/'

        count = 0
        for i in i_ids:
            print(count)
            count = count + 1
            
            filename = i.id
            c_name = cam_id.objects.get(id=i.cam_id).cam_id
            tmp = {}
            tmp['filename'] = i.filename
            tmp['camera'] = c_name
            tmp['bbox']  = []
            boxes = bbox.objects.filter(box_type='human').filter(image_id=i.id) 
            if boxes:
                for b in boxes:
                    tmp2={}
                    tmp2['coordinates']={}
                    tmp2['coordinates']['x']= b.x 
                    tmp2['coordinates']['y']= b.y 
                    tmp2['coordinates']['w']= b.w 
                    tmp2['coordinates']['h']= b.h
                    tmp2['name']            = b.box_class
                    tmp2['attribute']       = b.classifier
                    tmp['bbox'].append(tmp2)
            print(tmp)         


            if path.exists(target_dir) == True:
                dest = '{}/{}'.format(target_dir,i.filename)
       
           
              #  download image and reotate
                os.system('wget -O {} {}'.format(dest,i.image_url))
                angle = (-1)*cam_id.objects.get(id = i.cam_id).angle
                print(angle,i.filename)
                image = cv2.imread(dest)
                rotated = imutils.rotate_bound(image, angle)
                os.remove(dest)
                cv2.imwrite(dest, rotated) 

               # download json 
                with open('{}/{}'.format(target_dir,i.filename.replace('.jpg','.txt')), 'w') as outfile:
                    json.dump(tmp, outfile)

       
            else:
                os.mkdir(target_dir) 
 
        return Response(int(i_ids.count()))




'''
Resources Page
'''
class updateannotators(APIView):
    def post(self,request):
        print(request.data)
        target = cvat.objects.get(port=request.data['port'])
        
        list_anns = []
        for x in request.data['anns'].split(','):
            list_anns.append(x)

        target.annotators = list_anns
        target.save()
        print(list_anns)

        return Response('recvd')


def Diff(li1, li2): 
    return (list(set(li1) - set(li2))) 



'''
Local Upload Functionality

'''
class localUpload(APIView):
    def get(self,request):
        base_dir = '/data/downloads'
        psql_dirs =[]
        psql = projects_s3.objects.filter(bucket='uploads.live.uncannysurveillance.com')
        for x in psql:
            psql_dirs.append(x.project)
        dirs = os.listdir(base_dir)
        response = []
        count = 1
        for x in dirs:
            if x != 'videoanalytics':
                tmp ={}
                tmp['text'] = x
                tmp['value'] = x
                tmp['sites'] =  []
                for y in os.listdir(base_dir+'/'+x):
                    tmp2 = {}
                    tmp2['text'] = y
                    tmp2['value'] = y
                    tmp['sites'].append(tmp2)

                response.append(tmp)
                count = count + 1

        return Response(response)






class postLocalUpload(APIView):
    def post(self,request):
        print(request.data)
        return Response('posted successfully')


class get_models_pnr(APIView):
    def get(self,request,d_id):
        print(d_id) 
        response = []
        models = dates.objects.get(id=d_id).models
        if models:
            for x in models:
                tmp ={}
                tmp['text'] = infer_models.objects.get(id=x).model
                tmp['value'] = infer_models.objects.get(id=x).model
                response.append(tmp)       
       
        tmp = {}
        tmp['text'] = 'model'
        tmp['value'] = 'model'
        response.append(tmp) 
        
        tmp = {}
        tmp['text'] = 'human'
        tmp['value'] = 'human'
        response.append(tmp) 
        
        return Response(response)


'''
get inference models
'''

class get_detector_infer_models_project(APIView):
    def get(self,request,p_id):
        response     = []
        all_models   = ['model','human']
        inferModels = dates.objects.filter(project_id=p_id).filter(models__isnull=False)       
        new_models = [] 
        for i in inferModels:
            for i_ in i.models:
                if i_ not in new_models:
                    new_models.append(i_)
        
        for m in new_models:
            i_name = infer_models.objects.get(id=m)
            if i_name.model_type == 'detector':
                all_models.append(i_name.model)


        for x in all_models:
            tmp={}
            tmp['text'] = x
            tmp['value']  = x
            response.append(tmp)      

        return Response(response)


class get_classifier_infer_models_project(APIView):
    def get(self,request,p_id):
        response     = []
        all_models   = ['model','human']
        inferModels = dates.objects.filter(project_id=p_id).filter(models__isnull=False)
        new_models = []
        for i in inferModels:
            for i_ in i.models:
                if i_ not in new_models:
                    new_models.append(i_)
        for m in new_models:
            i_name = infer_models.objects.get(id=m)
            if i_name.model_type == 'classifier':
                all_models.append(i_name.model)
        for x in all_models:
            tmp={}
            tmp['text'] = x
            tmp['value']  = x
            response.append(tmp)
        return Response(response)




class get_detector_infer_models_site(APIView):
    def get(self,request,s_id):
        response     = []
        all_models   = ['model','human']
        inferModels = dates.objects.filter(site_id=s_id).filter(models__isnull=False)
        new_models = []
        for i in inferModels:
            for i_ in i.models:
                if i_ not in new_models:
                    new_models.append(i_)
        for m in new_models:
            i_name = infer_models.objects.get(id=m)
            if i_name.model_type == 'detector':
                all_models.append(i_name.model)
        for x in all_models:
            tmp={}
            tmp['text'] = x
            tmp['value']  = x
            response.append(tmp)
        return Response(response)

class get_classifier_infer_models_site(APIView):
    def get(self,request,s_id):
        response     = []
        all_models   = ['model','human']
        inferModels = dates.objects.filter(site_id=s_id).filter(models__isnull=False)
        new_models = []
        for i in inferModels:
            for i_ in i.models:
                if i_ not in new_models:
                    new_models.append(i_)
        for m in new_models:
            i_name = infer_models.objects.get(id=m)
            if i_name.model_type == 'classifier':
                all_models.append(i_name.model)
        for x in all_models:
            tmp={}
            tmp['text'] = x
            tmp['value']  = x
            response.append(tmp)
        return Response(response)


class get_detector_infer_models_site(APIView):
    def get(self,request,s_id):
        response     = []
        all_models   = ['model','human']
        inferModels = dates.objects.filter(site_id=s_id).filter(models__isnull=False)
        new_models = []
        for i in inferModels:
            for i_ in i.models:
                if i_ not in new_models:
                    new_models.append(i_)
        for m in new_models:
            i_name = infer_models.objects.get(id=m)
            if i_name.model_type == 'detector':
                all_models.append(i_name.model)
        for x in all_models:
            tmp={}
            tmp['text'] = x
            tmp['value']  = x
            response.append(tmp)
        return Response(response)


class get_classifier_infer_models_site(APIView):
    def get(self,request,s_id):
        response     = []
        all_models   = ['model','human']
        inferModels = dates.objects.filter(site_id=s_id).filter(models__isnull=False)
        new_models = []
        for i in inferModels:
            for i_ in i.models:
                if i_ not in new_models:
                    new_models.append(i_)
        for m in new_models:
            i_name = infer_models.objects.get(id=m)
            if i_name.model_type == 'classifier':
                all_models.append(i_name.model)
        for x in all_models:
            tmp={}
            tmp['text'] = x
            tmp['value']  = x
            response.append(tmp)
        return Response(response)

class get_detector_infer_models_date(APIView):
    def get(self,request,d_id):
        response     = []
        all_models   = ['model','human']
        inferModels = dates.objects.filter(id=d_id).filter(models__isnull=False)
        new_models = []
        for i in inferModels:
            for i_ in i.models:
                if i_ not in new_models:
                    new_models.append(i_)
        for m in new_models:
            i_name = infer_models.objects.get(id=m)
            if i_name.model_type == 'detector':
                all_models.append(i_name.model)
        for x in all_models:
            tmp={}
            tmp['text'] = x
            tmp['value']  = x
            response.append(tmp)
        return Response(response)


class get_classifier_infer_models_date(APIView):
    def get(self,request,d_id):
        response     = []
        all_models   = ['model','human']
        inferModels = dates.objects.filter(id=d_id).filter(models__isnull=False)
        new_models = []
        for i in inferModels:
            for i_ in i.models:
                if i_ not in new_models:
                    new_models.append(i_)
        for m in new_models:
            i_name = infer_models.objects.get(id=m)
            if i_name.model_type == 'classifier':
                all_models.append(i_name.model)
        for x in all_models:
            tmp={}
            tmp['text'] = x
            tmp['value']  = x
            response.append(tmp)
        return Response(response)


class get_detector_infer_models_camid(APIView):
    def get(self,request,c_id):
        response     = []
        all_models   = ['model','human']
        inferModels = dates.objects.filter(id=cam_id.objects.get(id=c_id).date_id).filter(models__isnull=False)
        new_models = []
        for i in inferModels:
            for i_ in i.models:
                if i_ not in new_models:
                    new_models.append(i_)
        for m in new_models:
            i_name = infer_models.objects.get(id=m)
            if i_name.model_type == 'detector':
                all_models.append(i_name.model)
        for x in all_models:
            tmp={}
            tmp['text'] = x
            tmp['value']  = x
            response.append(tmp)

        return Response(response)


class get_classifier_infer_models_camid(APIView):
    def get(self,request,c_id):
        response     = []
        all_models   = ['model','human']
        inferModels = dates.objects.filter(id=cam_id.objects.get(id=c_id).date_id).filter(models__isnull=False)
        new_models = []
        for i in inferModels:
            for i_ in i.models:
                if i_ not in new_models:
                    new_models.append(i_)
        for m in new_models:
            i_name = infer_models.objects.get(id=m)
            if i_name.model_type == 'classifier':
                all_models.append(i_name.model)
        for x in all_models:
            tmp={}
            tmp['text'] = x
            tmp['value']  = x
            response.append(tmp)
        return Response(response)


'''
info section
'''

class pinfo_class(APIView):
    def post(self,request):
        p_id = request.data['p_id']
        boxes = request.data['box_or_imgs']
        pref_model = request.data['model']  
        conf = request.data['conf']      

        projects_data= projects.objects.get(id=p_id)
        tmp = {}
        tmp["image_count"] =0
        for y in  sites.objects.filter(project_id=p_id):
            if pref_model == 'model' or pref_model =='human':
                tmp["image_count"] = tmp['image_count']+int(bbox.objects.filter(site_id= y.id).filter(box_type=pref_model).distinct('filename').count())
            else:
                tmp["image_count"] = tmp['image_count'] + int(infer_bbox.objects.filter(site_id= y.id).filter(box_type=pref_model).distinct('filename').count())


        s_list  = sites.objects.filter(project_id=p_id)
        classes = get_sites_classes(s_list)
        tmp["id"] = p_id
        tmp['class_info']=[]
        for x in s_list:
            print(x.id,classes)

        for c in classes:
            tmp1={}
            tmp1['class'] = c
            tmp1['count'] = 0
            for s in s_list:
                if boxes == 'box':
                    if pref_model in ['model','human']:
                        query = bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(site_id=s.id).filter(conf__gte = str(int(conf)/100))
                    else:
                        query = infer_bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(site_id=s.id).filter(conf__gte = str(int(conf)/100))


                if boxes == 'images':
                    if pref_model in ['model','human']:
                        query = bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(site_id=s.id).values('filename').distinct()
                    else:
                       query = infer_bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(site_id=s.id).values('filename').distinct()


                tmp1['count'] = int(query.count()) + tmp1['count']

            tmp["class_info"].append(tmp1)

        return Response(tmp)     







class pinfo_classifier(APIView):
    def post(self,request):
        p_id = request.data['p_id']
        boxes = request.data['box_or_imgs']
        pref_model = request.data['model']
        conf = request.data['conf']
        projects_data= projects.objects.get(id=p_id)
        tmp = {}
        tmp["image_count"] =0
        for y in  sites.objects.filter(project_id=p_id):
            if pref_model == 'model' or pref_model =='human':
                tmp["image_count"] = tmp['image_count']+int(bbox.objects.filter(site_id= y.id).filter(box_type=pref_model).distinct('filename').count())
            else:
                tmp["image_count"] = tmp['image_count'] + int(infer_bbox.objects.filter(site_id= y.id).filter(box_type=pref_model).distinct('filename').count())

        s_list  = sites.objects.filter(project_id=p_id)
        classifiers = get_sites_classifiers(s_list)
        print(classifiers)
        tmp["id"] = p_id
        tmp['classifier_info'] = []
        for x in classifiers:
            tmp3 =  {}
            tmp3['classifier'] = x['classifier']
            tmp3['attribute_count'] = []
            for y in x['attributes']:
                tmp2 = {}
                tmp2['attribute']  = y
                tmp2['count'] = 0
                for s_id in x['s_ids']:
                    if boxes == 'box':
                        if pref_model in ['model','human']:
                            query = bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(site_id=s_id).filter(conf__gte = str(int(conf)/100))
                        else:
                            query = infer_bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(site_id=s_id).filter(conf__gte = str(int(conf)/100))


                    if boxes == 'images':
                        if pref_model in ['model','human']:
                            query = bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(site_id=s_id).values('filename').distinct()
                        else:
                            query = infer_bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(site_id=s_id).values('filename').distinct()

                    tmp2['count']    = query.count() + tmp2['count']

                tmp3['attribute_count'].append(tmp2)
            tmp['classifier_info'].append(tmp3)

        return Response(tmp)



class sinfo_class(APIView):
    def post(self,request):
        s_id = request.data['s_id']
        boxes = request.data['box_or_imgs']
        pref_model = request.data['model']
        conf = request.data['conf']
        tmp = {}
        tmp["image_count"] =bbox.objects.filter(site_id= s_id).filter(box_type=pref_model).distinct('filename').count()

        if pref_model == 'model' or pref_model =='human':
            tmp["image_count"] =bbox.objects.filter(site_id= s_id).filter(box_type=pref_model).distinct('filename').count()
        else:
            tmp["image_count"] =infer_bbox.objects.filter(site_id= s_id).filter(box_type=pref_model).distinct('filename').count()

        s_list  = [s_id]
        classes = get_site_classes(s_list)
        tmp["id"] = s_id
        tmp['class_info']=[]
        for c in classes:
            tmp1={}
            tmp1['class'] = c
            tmp1['count'] = 0
            for s in s_list:
                if boxes == 'box':
                    if pref_model in ['model','human']:
                        query = bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(site_id=s_id).filter(conf__gte = str(int(conf)/100))
                    else:
                        query = infer_bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(site_id=s_id).filter(conf__gte = str(int(conf)/100))
                if boxes == 'images':
                    if pref_model in ['model','human']:
                        query = bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(site_id=s_id).values('filename').distinct()
                    else:
                       query = infer_bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(site_id=s_id).values('filename').distinct()
                tmp1['count'] = int(query.count()) + tmp1['count']
            tmp["class_info"].append(tmp1)
        return Response(tmp)




class sinfo_classifier(APIView):
    def post(self,request):
        s_id = request.data['s_id']
        boxes = request.data['box_or_imgs']
        pref_model = request.data['model']
        conf = request.data['conf']
        tmp = {}

        if pref_model == 'model' or pref_model =='human':
            tmp["image_count"] =bbox.objects.filter(site_id= s_id).filter(box_type=pref_model).distinct('filename').count()
        else:
            tmp["image_count"] =infer_bbox.objects.filter(site_id= s_id).filter(box_type=pref_model).distinct('filename').count()


        s_list  = sites.objects.filter(id=s_id)
        classifiers = get_sites_classifiers(s_list)
        print(classifiers)
        tmp["id"] = s_id
        tmp['classifier_info'] = []
        for x in classifiers:
            tmp3 =  {}
            tmp3['classifier'] = x['classifier']
            tmp3['attribute_count'] = []
            for y in x['attributes']:
                tmp2 = {}
                tmp2['attribute']  = y
                tmp2['count'] = 0
                for s_id in x['s_ids']:
                    if boxes == 'box':
                        if pref_model in ['model','human']:
                            query = bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(site_id=s_id).filter(conf__gte = str(int(conf)/100))
                        else:
                            query = infer_bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(site_id=s_id).filter(conf__gte = str(int(conf)/100))
                
            
                    if boxes == 'images':
                        if pref_model in ['model','human']:
                            query = bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(site_id=s_id).values('filename').distinct()
                            print(query)
                        else:
                            query = infer_bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(site_id=s_id).values('filename').distinct()
                    tmp2['count']    = query.count() + tmp2['count']
                tmp3['attribute_count'].append(tmp2)
            tmp['classifier_info'].append(tmp3)
        return Response(tmp)


class site_filters(APIView):
    def get(self,request,s_id):
        queryset =  images.objects.filter(site_id=s_id)
        filters = get_filters(s_id)
        response =[]
       
        if filters !=0:
            for f in filters:
                tmp2={}
                tmp2['count'] = queryset.filter(alerts__contains={f:True}).count()
                tmp2['filter']   = f
                response.append(tmp2)
        return Response(response)


class dinfo_class(APIView):
    def post(self,request):
        d_id = request.data['d_id']
        boxes = request.data['box_or_imgs']
        pref_model = request.data['model']
        conf = request.data['conf']
       
        tmp = {}
        #tmp["image_count"] =images.objects.filter(date_id=d_id).count()

        if pref_model == 'model' or pref_model =='human':
            tmp["image_count"] =bbox.objects.filter(date_id= d_id).filter(box_type=pref_model).distinct('filename').count()
        else:
            tmp["image_count"] =infer_bbox.objects.filter(date_id= d_id).filter(box_type=pref_model).distinct('filename').count()

        s_list  = []
        for s in dates.objects.filter(id=d_id):
            s_list.append(s.site_id)
        classes = get_site_classes(s_list)
        tmp["id"] = d_id
        tmp['class_info']=[]
        for c in classes:
            tmp1={}
            tmp1['class'] = c
            if boxes == 'box':
                if pref_model in ['model','human']:
                    query = bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(date_id=d_id).filter(conf__gte = str(int(conf)/100))
                else:
                    query = infer_bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(date_id=d_id).filter(conf__gte = str(int(conf)/100))
                tmp1['count'] = int(query.count())
            if boxes == 'images':
                if pref_model in ['model','human']:
                    query = bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(date_id=d_id).values('filename').distinct()
                else:
                   query = infer_bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(date_id=d_id).values('filename').distinct()
                tmp1['count'] = int(query.count())
            tmp["class_info"].append(tmp1)
        return Response(tmp)


class dinfo_classifier(APIView):
    def post(self,request):
        d_id = request.data['d_id']
        boxes = request.data['box_or_imgs']
        pref_model = request.data['model']
        conf = request.data['conf']
        tmp = {}
        #tmp["image_count"] =int(images.objects.filter(date_id=d_id).count())
        
        if pref_model == 'model' or pref_model =='human':
            tmp["image_count"] =bbox.objects.filter(date_id= d_id).filter(box_type=pref_model).distinct('filename').count()
        else:
            tmp["image_count"] =infer_bbox.objects.filter(date_id= d_id).filter(box_type=pref_model).distinct('filename').count()

        s_list  = [sites.objects.get(id=dates.objects.get(id=d_id).site_id)]
        classifiers = get_sites_classifiers(s_list)
        tmp["id"] = d_id
        tmp['classifier_info'] = []
        for x in classifiers:
            tmp3 =  {}
            tmp3['classifier'] = x['classifier']
            tmp3['attribute_count'] = []
            for y in x['attributes']:
                tmp2 = {}
                tmp2['attribute']  = y
                if boxes == 'box':
                    if pref_model in ['model','human']:
                        query = bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(date_id=d_id).filter(conf__gte = str(int(conf)/100))
                    else:
                        query = infer_bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(date_id=d_id).filter(conf__gte = str(int(conf)/100))
                if boxes == 'images':
                    if pref_model in ['model','human']:
                        query = bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(date_id=d_id).values('filename').distinct()
                    else:
                        query = infer_bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(date_id=d_id).values('filename').distinct()
                tmp2['count']    = query.count() 
                tmp3['attribute_count'].append(tmp2)
            tmp['classifier_info'].append(tmp3)
        return Response(tmp)


class date_filters(APIView):
    def get(self,request,d_id):
        queryset =  images.objects.filter(date_id=d_id)
        s_id = dates.objects.get(id = d_id).site_id
        s_list = [s_id]
        filters = get_filters(s_id)
        response =[]
        classes = get_sites_classes(s_list)
        if filters !=0:
            for f in filters:
                tmp2={}
                tmp2['count'] = queryset.filter(alerts__contains={f:True}).count()
                tmp2['filter']   = f
                response.append(tmp2)
        return Response(response)





class cinfo_class(APIView):
    def post(self,request):
        c_id = request.data['c_id']
        boxes = request.data['box_or_imgs']
        pref_model = request.data['model']
        conf = request.data['conf']
        tmp = {}
        #tmp["image_count"] =images.objects.filter(cam_id=c_id).count()

        if pref_model == 'model' or pref_model =='human':
            tmp["image_count"] =bbox.objects.filter(cam_id= c_id).filter(box_type=pref_model).distinct(filename).count()
        else:
            tmp["image_count"] =infer_bbox.objects.filter(cam_id= c_id).filter(box_type=pref_model).distinct(filename).count()
       
        s_list  = [dates.objects.get(id= cam_id.objects.get(id=c_id).date_id).site_id]
        classes = get_site_classes(s_list)
        tmp["id"] = c_id
        tmp['class_info']=[]
        for c in classes:
            tmp1={}
            tmp1['class'] = c
            if boxes == 'box':
                if pref_model in ['model','human']:
                    query = bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(cam_id=c_id).filter(conf__gte = str(int(conf)/100))
                else:
                    query = infer_bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(cam_id=c_id).filter(conf__gte = str(int(conf)/100))
            if boxes == 'images':
                if pref_model in ['model','human']:
                    query = bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(cam_id=c_id).values('filename').distinct()
                else:
                   query = infer_bbox.objects.filter(box_class=c).filter(box_type=pref_model).filter(cam_id=c_id).values('filename').distinct()
            tmp1['count'] = int(query.count())
            tmp["class_info"].append(tmp1)
        return Response(tmp)


class cinfo_classifier(APIView):
    def post(self,request):
        c_id = request.data['c_id']
        boxes = request.data['box_or_imgs']
        pref_model = request.data['model']
        conf = request.data['conf']
        tmp = {}
        #tmp["image_count"] =int(images.objects.filter(cam_id=c_id).count())
    
        if pref_model == 'model' or pref_model =='human':
            tmp["image_count"] =bbox.objects.filter(cam_id= c_id).filter(box_type=pref_model).distinct(filename).count()
        else:
            tmp["image_count"] =infer_bbox.objects.filter(cam_id= c_id).filter(box_type=pref_model).distinct(filename).count()

        s_list  = [sites.objects.get(id = dates.objects.get(id= cam_id.objects.get(id=c_id).date_id).site_id)]
        classifiers = get_sites_classifiers(s_list)
        tmp["id"] = c_id
        tmp['classifier_info'] = []
        for x in classifiers:
            tmp3 =  {}
            tmp3['classifier'] = x['classifier']
            tmp3['attribute_count'] = []
            for y in x['attributes']:
                tmp2 = {}
                tmp2['attribute']  = y
                tmp2['count'] = 0
                if boxes == 'box':
                    if pref_model in ['model','human']:
                        query = bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(cam_id=c_id).filter(conf__gte = str(int(conf)/100))
                    else:
                        query = infer_bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(cam_id=d_id).filter(conf__gte = str(int(conf)/100))
                if boxes == 'images':
                    if pref_model in ['model','human']:
                        query = bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(cam_id=c_id).values('filename').distinct()
                    else:
                        query = infer_bbox.objects.filter(classifier__contains={x['classifier']:y}).filter(box_type=pref_model).filter(cam_id=c_id).values('filename').distinct()
                tmp2['count']    = query.count() + tmp2['count']
                tmp3['attribute_count'].append(tmp2)
            tmp['classifier_info'].append(tmp3)
        return Response(tmp)
                                                              

class cam_filters(APIView):
    def get(self,request,c_id):
        queryset =  images.objects.filter(cam_id=c_id)
        s_id = dates.objects.get(id=cam_id.objects.get(id=c_id).date_id).site_id
        s_list = [dates.objects.get(id=cam_id.objects.get(id=c_id).date_id).site_id]
        filters = get_filters(s_id)
        response =[]
        if filters != 0:
            for f in filters:
                tmp2={}
                tmp2['count'] = queryset.filter(alerts__contains={f:True}).count()
                tmp2['filter']   = f
                response.append(tmp2)
        print(response)
        return Response(response)







class upload_instance(APIView):

    def get(self,request):
        response = []
        instances = remote_instances.objects.all()
        for _instance in instances:
            tmp = {}
            tmp['public_ip'] = _instance.public_ip
            tmp['free_space'] = _instance.free_space
            tmp['gpu_availability'] = _instance.gpu_availability
            tmp['gpu_memory'] = _instance.gpu_memory
            tmp['user']  = _instance.user
            t = time.localtime()
            current_time = str(time.strftime("%H:%M:%S", t)).split(':')
            current_hr = int(current_time[0])
            current_min = int(current_time[1])

            registered_time = str(_instance.ping).split(':')
            registered_hr = int(registered_time[0])
            registered_min = int(registered_time[1])

            if current_hr == registered_hr:
                delta = current_min -registered_min
                if delta < 1:
                    tmp['active'] = 'active'
                else:
                    tmp['active'] = 'inactive'
       
            else:
                tmp['active'] = 'inactive'
             
            response.append(tmp)

        return Response(response)



    def post(self,request):
        req_1 = request.data
        storage = req_1['storage']
        user    = req_1['user']
        gpu_availability = req_1['gpu_available'] 
        pub_ip = req_1['public_ip']
        parent_dir = req_1['parent_dir']
        
        t = time.localtime()
        current_time = time.strftime("%H:%M:%S", t)
        check_db = remote_instances.objects.filter(public_ip = str(pub_ip)).count()
        if check_db ==0:
            upload_instance = remote_instances.objects.create(public_ip = str(pub_ip),user=str(user), gpu_availability = gpu_availability, free_space=storage, ping = current_time, parent_dir = parent_dir  )
            upload_instance.save() 

        else:
            query =  remote_instances.objects.get(public_ip = str(pub_ip))
            query.ping = current_time 
            query.save()

        return Response('got the vm')
