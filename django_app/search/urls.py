from django.conf.urls import url
from django.urls import path
from . import views
urlpatterns = [

    path('project/',views.list_projects.as_view()),
    path('p_class_info/',views.pinfo_class.as_view()),
    path('p_classifier_info/',views.pinfo_classifier.as_view()),
    path('getDetectorInfermodels/<int:p_id>/',views.get_detector_infer_models_project.as_view()),
    path('getClassifierInfermodels/<int:p_id>/',views.get_classifier_infer_models_project.as_view()),
#    path('project_info/<int:p_id>/',views.project_info.as_view()),

    path('project/<int:p_id>/',views.list_sites.as_view()),
    path('s_class_info/',views.sinfo_class.as_view()),
    path('s_classifier_info/',views.sinfo_classifier.as_view()),
    path('getDetectorInfermodelsSites/<int:s_id>/',views.get_detector_infer_models_site.as_view()),
    path('getClassifierInfermodelsSites/<int:s_id>/',views.get_classifier_infer_models_site.as_view()),
    path('siteFilters/<int:s_id>/',views.site_filters.as_view()),
    #path('site_info/<int:s_id>',views.site_info.as_view()),

    path('project/<int:p_id>/<int:s_id>/',views.list_dates.as_view()),
    path('d_class_info/',views.dinfo_class.as_view()),
    path('d_classifier_info/',views.dinfo_classifier.as_view()),
    path('getDetectorInfermodelsDates/<int:d_id>/',views.get_detector_infer_models_date.as_view()),
    path('getClassifierInfermodelsDates/<int:d_id>/',views.get_classifier_infer_models_date.as_view()),
    path('DateFilters/<int:d_id>/',views.date_filters.as_view()),
    path('date_meta/<int:d_id>/',views.date_meta.as_view()),
   
    path('project/<int:p_id>/<int:s_id>/<int:d_id>/',views.list_cams.as_view()),
    path('c_class_info/',views.cinfo_class.as_view()),
    path('c_classifier_info/',views.cinfo_classifier.as_view()),
    path('getDetectorInfermodelsCams/<int:c_id>/',views.get_detector_infer_models_camid.as_view()),
    path('getClassifierInfermodelsCams/<int:c_id>/',views.get_classifier_infer_models_camid.as_view()),
    path('CamsFilters/<int:c_id>/',views.cam_filters.as_view()),
#    path('cam_meta/<int:c_id>/',views.cam_meta.as_view()),
   
    path('project/<int:p_id>/<int:s_id>/<int:d_id>/<int:c_id>/', views.list_images.as_view()),
    path('p_n_r/<int:p_id>/<int:s_id>/<int:d_id>/', views.get_p_n_r.as_view()),
    path('p_n_r/<int:p_id>/<int:s_id>/<int:d_id>/<int:c_id>/', views.get_p_n_r_cams.as_view()),
    path('get_models_pnr/<int:d_id>/', views.get_models_pnr.as_view()),
    path('ssim/', views.compute_ssim.as_view()),
    path('login/', views.login_page.as_view()),
    path('date_ssim/', views.compute_ssim_date.as_view()),
          
    path('class/',views.list_class.as_view()),
    path('class/<int:class_id>/',views.list_class_images.as_view()), 


    path('download_annotations/',views.download_testing.as_view()),
    path('resources/',views.resources.as_view()),
    path('updateAnnotators/',views.updateannotators.as_view()),
    path('upload_instance/',views.upload_instance.as_view()),
#    path('post_upload/',views.postLocalUpload.as_view()),

]
