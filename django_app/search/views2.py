import sys
user = "clyde"
import os.path
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
import subprocess
import numpy as np
from django.shortcuts import render
from django.http import HttpResponse
import django
django.setup()
sys.path.append('/home/'+user+'/Downloads/automatic_annotator_tool/django_app/')
from .forms import project_search, site_search, class_search, update_db,bounding_box
from .serializers import projects_serializer,sites_serializer,images_serializer,classes_serializer
from uvdata.models import projects, sites, images, box, classes,cam_id
from django.views.generic import ListView, CreateView, UpdateView 
from django.forms import modelformset_factory
import os
import boto3
import uuid
from django.views.decorators.csrf import csrf_exempt
import json
import psycopg2
import sys
import shutil
import psycopg2
import json
import os
import os.path
from os import path
import zipfile
import glob
import botocore
from PIL import Image, ImageDraw
import datetime
########API view

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
####credentials 

client = boto3.client('s3', aws_access_key_id='AKIARX6S2JWR245EG3UD', aws_secret_access_key='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',)
s3 = boto3.resource('s3')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "uvlearn.settings")
bucket = s3.Bucket('uploads.live.uncannysurveillance.com')

#######setting up cursor
try:
    con = psycopg2.connect(

            host="uvdb4uv.c9hdyw02xzd1.ap-south-1.rds.amazonaws.com",
            database="uvdb4uv1",
            user="postgres",
            password="uvdbuvdb"

)
    con.autocommit = True
except:

    print ("Cannot connect to db")



cur = con.cursor()

#### upload and download directives

download_dir = "/home/clyde/Downloads/automatic_annotator_tool/data/queried_data/"
upload_dir = "/home/clyde/Downloads/automatic_annotator_tool/data/upload_data/"
tmp_dir="/home/clyde/Downloads/automatic_annotator_tool/data/"
display_dir="/home/clyde/Downloads/automatic_annotator_tool/data/display_images/"

###############################################################API VIews

def index(request):
    return render(request, 'search/index.html')

class s3_objects(APIView):
    
    def get(self,request):
        bucket_objects = [
			{'id':1,'project_name':'Aabmatica'},{'id':2,'project_name':'Assetz'},
			{'id':3,'project_name':'CP%2520Plus'},{'id':4,'project_name':'Flashparking'},
			{'id':5,'project_name':'GMI'},{'id':6,'project_name':'JSW'},
			{'id':7,'project_name':'McCain'},{'id':8,'project_name':'SelfParkFLL'},
			{'id':9,'project_name':'Subex'},{'id':10,'project_name':'TestOrg'},
			{'id':11,'project_name':'gmi'},{'id':12,'project_name':'ciss_demo'},
			{'id':13,'project_name':'sobha%2520city%2520serenity'},{'id':14,'project_name':'VaaaN%2520Infra'},{'id':15,'project_name':'uncanny'}
			]
        return Response(bucket_objects)

class s3_d1(APIView):

    def get(self,request,obj_1):
        counter = 1
        
        return Response(d1)

class list_projects(APIView):
    
    def get(self,request):
        projects_data= projects.objects.all()
        project_serializer=projects_serializer(projects_data,many=True)
        return Response(project_serializer.data)	

class list_sites(APIView):
    
    def get(self,request,p_id):
        sites_data= sites.objects.filter(project_id=p_id)
        site_serializer= sites_serializer(sites_data,many=True)
        return Response(site_serializer.data)

class list_images(APIView):

    def get(self,request,p_id,s_id):
        images_data= images.objects.filter(site_id=s_id)
        image_serializer= images_serializer(images_data,many=True)
        return Response(image_serializer.data)


class list_single_image(APIView):
     
    def get(self,request,p_id,s_id,i_id):
        images_data = images.objects.filter(id=i_id)
        image_serializer= images_serializer(images_data,many=True)
  
        return Response(image_serializer.data)


def draw_images(request,p_id,s_id,i_id):
    bb_form= bounding_box()
    image_s3= images.objects.get(id=i_id).image_url
    json_s3 = images.objects.get(id= i_id).json_url
    download_image="aws s3 cp s3://"+image_s3+ ' '+'../data/display_images/image.jpg'
    download_json="aws s3 cp s3://"+json_s3+' '+'../data/display_images/json.json'
    os.system(download_image)
    os.system(download_json)
       
    im = Image.open("/home/clyde/Downloads/automatic_annotator_tool/data/display_images/image.jpg")
    im.save('/home/clyde/Downloads/automatic_annotator_tool/django_app/static/tmp.jpg')

    if request.method == "POST":
        bb_form=bounding_box(request.POST)
        if bb_form.is_valid():
           info=bb_form.cleaned_data.get('bounding_box')
           if info =='2':
               im = Image.open("/home/clyde/Downloads/automatic_annotator_tool/data/display_images/image.jpg")
               image = ImageDraw.Draw(im)

               with open(display_dir+"json.json", encoding='utf-8') as data_file:
                   data = json.loads(data_file.read())

               elements= 0
               counter = []
               for x in data:
                   if x == 'event_json':
                       elements = elements + 1
                       counter.append(elements)

               for iterations in counter:
                   xmin=data['event_json']['event']['coordinates']['xmin']
                   ymin=data['event_json']['event']['coordinates']['ymin']
                   width=data['event_json']['event']['coordinates']['width']
                   height=data['event_json']['event']['coordinates']['height']
                   a=(xmin,ymin)
                   b=(xmin+width,ymin)
                   c=(xmin,ymin+height)
                   d=(xmin+width,ymin+height)
                   line_color = (0, 0, 255)
                   image.line([a,b], fill=line_color, width=2)
                   image.line([c,d], fill=line_color, width=2)
                   image.line([a,c], fill=line_color, width=2)
                   image.line([b,d], fill=line_color, width=2)
                   im.save('/home/clyde/Downloads/automatic_annotator_tool/django_app/static/tmp.jpg')

           if info == '1':
               im = Image.open("/home/clyde/Downloads/automatic_annotator_tool/data/display_images/image.jpg")
               im.save('/home/clyde/Downloads/automatic_annotator_tool/django_app/static/tmp.jpg')




    return render(request,'search/display_image.html',{'bb_form':bb_form})


class list_classes(APIView):

    def get(self,request):
        classes_data= classes.objects.all()
        class_serializer= classes_serializer(classes_data,many=True)
        return Response(class_serializer.data)


class list_class_sites(APIView):

    def get(self,request,class_id):
        select_target_query="select * from uvdata_sites_site_classes where classes_id = %s"
        try:
            cur.execute((select_target_query),(class_id,))
        except:
            print("could not fetch object class from db")
        site_class_records = cur.fetchall()
        site_ids=[]
        for x in site_class_records:
            site_ids.append(x[1])
        id_array=list(site_ids)
        sites_summary=[]
        for x in id_array:
            a=sites.objects.get(id=x)
            sites_summary.append({"id":a.id,"site_name":a.site_name})

        return Response(sites_summary)


class list_class_images(APIView):
 
    def get(self,request,class_id,s_id):
        images_data=images.objects.filter(site_id=s_id)
        image_serializer= images_serializer(images_data,many=True)
        return Response(image_serializer.data)

class list_class_single_image(APIView):

    def get(self,request,class_id,s_id,i_id):
        images_data = images.objects.filter(id=i_id)
        image_serializer= images_serializer(images_data,many=True)

        return Response(image_serializer.data)


class list_projects_filter(APIView):

    def get(self,request):
        projects_data= projects.objects.all()
        project_serializer=projects_serializer(projects_data,many=True)
        return Response(project_serializer.data)

class filter_sites(APIView):

    def get(self,request,p_id,s_id):
        project = projects.objects.all()
        return Response(project_id)



        

    def post(self,request,p_id,s_id):
        project_id_form = project_search()
        project_id=None
        if request.method == "POST":
            project_id_form=project_search(request.POST)
            if project_id_form.is_valid():
                project_name=project_id_form.cleaned_data.get('project_name')
                project_queryset= projects.objects.filter(project_name=project_name)
                project_id = []
                for x in project_queryset:                
                    project_id.append({"id":x.id,"project_name":x.project_name})

        return Response(project_id)

#    site_id_form = site_search(project_id=project_id) 
#    if request.method == "POST":
#        site_id_form = site_search(request.POST,project_id=project_id)
 #       site_ids=request.POST.getlist('site_name')
  #      name=request.POST.getlist('name')
  #      site_names=[]
  #      for x in site_ids:
            
   #         site_names.append(sites.objects.get(id=x).site_name)
        
   #     job_details = open("../job_details.txt","a")
  # #     print(name)
     #   for job_name in name:
      #      job_details.write(job_name)
       #     job_details.write(";")
        #    mkdir="mkdir ../data/queried_data/" + job_name
         #   os.system(mkdir)	
          #  for site_id in  site_ids:
            #    image_list=images.objects.filter(site_id=site_id)
           #     for x in image_list:
             #       download_destination_image=download_dir+job_name+"/" + x.filename
              #      download_destination_json=download_dir+job_name+"/" + x.filename.strip(".jpg")+".json"
               #     string= x.image_url.split('/',3)[3]
                #    string=string.replace('%20','\\ ')
                 #   download_image="aws s3 cp s3://" + string + ' ' + download_destination_image
                 #   download_json="aws s3 cp s3://" + string +' '+ download_destination_json
                 #   os.system(download_image)
                 #   os.system(download_json)
      #      job_details.write(str(site_names))
       #     job_details.write(";")
       #     job= download_dir + job_name + '/'
       #     command = "du -h {}".format(job)
       #     job_space = subprocess.check_output(command,shell=True).decode('ascii').rstrip("\t"+download_dir+job_name+"\n")
       #     job_details.write(str(job_space))
       #     job_details.write("\n")
       # job_details.close()


##########################################################################
def summary(request):
    
    projects_summary=projects.objects.all()
    sites_summary=sites.objects.all()
    classes_summary=classes.objects.all()

    return render(request,'search/search_summary.html',{'projects_summary':projects_summary,'sites_summary':sites_summary,'classes_summary':classes_summary,})
 

def projects_sites_tree(request):

    project_name = request.GET.get("name")
    print(project_name)
    project_id = projects.objects.get(project_name=project_name).id
    sites_summary= sites.objects.filter(project_id=project_id)
    return render(request,'search/sites_tree.html',{"project_name":project_name,"sites_summary":sites_summary,})


def projects_images_tree(request):

    site_name = request.GET.get("name")
    print(site_name)
    site_id = sites.objects.get(site_name=site_name).id
    images_summary= images.objects.filter(site_id=site_id)
    return render(request,'search/images_tree.html',{"site_name":site_name,"images_summary":images_summary,})

def projects_images_display(request):
    filename= request.GET.get("name")
    bb_form= bounding_box()
    if request.method == "POST":
        bb_form=bounding_box(request.POST)
        if bb_form.is_valid():
           info=bb_form.cleaned_data.get('bounding_box')
           if info =='2':
               im = Image.open("/home/clyde/Downloads/automatic_annotator_tool/data/display_images/image.jpg")
               image = ImageDraw.Draw(im)

               with open(display_dir+"anpr.json", encoding='utf-8') as data_file:
                   data = json.loads(data_file.read())

               elements= 0
               counter = []
               for x in data:
                   if x == 'event_json':
                       elements = elements + 1
                       counter.append(elements)

               for iterations in counter:
                   xmin=data['event_json']['event']['coordinates']['xmin']
                   ymin=data['event_json']['event']['coordinates']['ymin']
                   width=data['event_json']['event']['coordinates']['width']
                   height=data['event_json']['event']['coordinates']['height']
                   a=(xmin,ymin)
                   b=(xmin+width,ymin)
                   c=(xmin,ymin+height)
                   d=(xmin+width,ymin+height)
                   line_color = (0, 0, 255)
                   image.line([a,b], fill=line_color, width=2)
                   image.line([c,d], fill=line_color, width=2)
                   image.line([a,c], fill=line_color, width=2)
                   image.line([b,d], fill=line_color, width=2)
                   im.save('/home/clyde/Downloads/automatic_annotator_tool/django_app/static/tmp.jpg')
           
           if info == '1':
               im = Image.open("/home/clyde/Downloads/automatic_annotator_tool/data/display_images/image.jpg")
               im.save('/home/clyde/Downloads/automatic_annotator_tool/django_app/static/tmp.jpg')

    return render(request,'search/display_image.html',{'bb_form':bb_form,})




def classes_sites_tree(request):

    class_name = request.GET.get("name")
    print(class_name)
    class_id = classes.objects.get(class_name=class_name).id
    print(class_id)
    select_target_query="select * from uvdata_sites_site_classes where classes_id = %s"
    try:
        cur.execute((select_target_query),(class_id,))
    except:
        print("could not fetch object class from db")
    site_class_records = cur.fetchall()
    site_ids=[]
    for x in site_class_records:
        site_ids.append(x[1]) 
    id_array=list(site_ids)
    sites_summary=[]
    for x in id_array:
        temp=[]
        a=sites.objects.get(id=x)
        print(a)
        temp.append(a.site_name)
        temp.append(a.site_images)
        temp.append(a.annotated_images)
        sites_summary.append(temp)        

    return render(request,'search/classes_sites_tree.html',{"class_name":class_name,"sites_summary":sites_summary,})



def classes_images_tree(request):

    site_name = request.GET.get("name")
    site_id = sites.objects.get(site_name=site_name).id
    images_summary= images.objects.filter(site_id=site_id)

    return render(request,'search/classes_images_tree.html',{"site_name":site_name,"images_summary":images_summary,})





def search_sites(request):

    project_id_form = project_search()

    project_id=None
    if request.method == "POST":
        project_id_form=project_search(request.POST)
        if project_id_form.is_valid():
            project_name=project_id_form.cleaned_data.get('project_name')
            project_queryset= projects.objects.filter(project_name=project_name)
            for x in project_queryset:                
                project_id = x.id

    site_id_form = site_search(project_id=project_id) 
    if request.method == "POST":
        site_id_form = site_search(request.POST,project_id=project_id)
        site_ids=request.POST.getlist('site_name')
        name=request.POST.getlist('name')
        site_names=[]
        for x in site_ids:
            
            site_names.append(sites.objects.get(id=x).site_name)
        
        job_details = open("../job_details.txt","a")
  #      print(name)
        for job_name in name:
            job_details.write(job_name)
            job_details.write(";")
            mkdir="mkdir ../data/queried_data/" + job_name
            os.system(mkdir)	
            for site_id in  site_ids:
                image_list=images.objects.filter(site_id=site_id)
                for x in image_list:
                    bucket="uploads.live.uncannysurveillance.com/"
                    download_destination_image=download_dir+job_name+"/" + x.filename
                    download_destination_json=download_dir+job_name+"/" + x.filename.strip(".jpg")+".json"
                    image_string= x.image_url.split('/',1)[1].replace('%20','\\ ')
                    json_string=x.json_url.split('/',1)[1].replace('%20','\\ ')
                    download_image="aws s3 cp s3://" +bucket+ image_string + ' ' + download_destination_image
                    download_json="aws s3 cp s3://" +bucket+ json_string +' '+ download_destination_json
                    os.system(download_image)
                    os.system(download_json)
            job_details.write(str(site_names))
            job_details.write(";")
            job= download_dir + job_name + '/'
            command = "du -h {}".format(job)
            job_space = subprocess.check_output(command,shell=True).decode('ascii').rstrip("\t"+download_dir+job_name+"\n")
            job_details.write(str(job_space))
            job_details.write("\n")
        job_details.close()

    return render(request,'search/search_sites.html',{'project_id_form':project_id_form,'site_id_form':site_id_form,})

def search_class(request):

    class_id_form = class_search() 
    if request.method == "POST":
        class_id_form = class_search(request.POST)
        class_ids=request.POST.getlist('class_name')
        name= request.POST.getlist('name')
        class_names=[]
        for x in class_ids:
            class_names.append(classes.objects.get(id=x))
        
        job_details = open("../job_details.txt","a")
        for job_name in name:
            mkdir="mkdir ../data/queried_data/" + job_name
            os.system(mkdir)
            job_details.write(job_name)
            job_details.write(";")
            for class_id in  class_ids:
                select_target_query="select * from uvdata_box  where box_class_id = %s"
                try:
                    cur.execute((select_target_query),(class_id,))
                except:
                    print("could not select from box table")
            
                records = cur.fetchall()
                class_image_list=[]
                for record in records:
                    if not record[2] in class_image_list:
                        class_image_list.append(record[2])

                for x in class_image_list:
                    bucket="uploads.live.uncannysurveillance.com/"
                    x_image_url=images.objects.get(id=x).image_url
                    x_json_url=images.objects.get(id=x).json_url
                    x_image_name=images.objects.get(id=x).filename
                    download_destination_image=download_dir+job_name+"/"+x_image_name
                    download_destination_json=download_dir+job_name+"/"+x_image_name.strip(".jpg")+".json"
                    image_string= x_image_url.split('/',1)[1].replace('%20','\\ ')
                    json_string=x_json_url.split('/',1)[1].replace('%20','\\ ')
                    download_image="aws s3 cp s3://" +bucket+ image_string + ' ' + download_destination_image
                    download_json="aws s3 cp s3://" +bucket+ json_string +' '+ download_destination_json
                    os.system(download_image)
                    os.system(download_json)

 
            job_details.write(str(class_names))
            job_details.write(";")
            job= download_dir + job_name + '/'
            command = "du -h {}".format(job)
            job_space = subprocess.check_output(command,shell=True).decode('ascii').rstrip("\t"+download_dir+job_name+"\n")
            job_details.write(str(job_space))
            job_details.write("\n")
        job_details.close()

    return render(request,'search/search_class.html',{'class_id_form':class_id_form,})

def upload_db(request):

    choice_array=os.listdir(upload_dir)
    choices123= [(x,y) for x,y in enumerate(choice_array)]
    update_db_form = update_db(choices123=choices123)
    if request.method == "POST":
        update_db_form = update_db(request.POST,choices123=choices123)
        confirmation=request.POST.getlist('send')
        f= open("../data/zip.txt",'a')
        print(confirmation)
        for x in confirmation:
            f.write(choices123[int(x)][1].rstrip(".zip"))
            print(choices123[int(x)][1].rstrip(".zip"))
        f.close()
 
        if not confirmation:
            print("tick and submit to start uploading")
        
        if confirmation:   
            os.system('python3 scripts/send_s3_db.py')

        for x in confirmation:
            os.remove(upload_dir+choices123[int(x)][1])
    
    


    return render(request,'search/search_upload.html',{'update_db_form':update_db_form,})


@csrf_exempt
def upload_rtsp(request):



    def project_insert(project_table_project_name,project_table_project_description):
        try:
            cur.execute("insert into uvdata_projects (project_name,project_description) values (%s,%s)",(project_table_project_name,project_table_project_description))
        except:
            print ("could not insert in project table")

    def cam_insert(cam_name,site_id):
        try:
            cur.execute("insert into uvdata_cam_id (cam_id,site_id) values (%s,%s)",(cam_name,site_id))
        except:
            print ("could not insert in project table")

    def sites_update(site_table_site_name):
    
        site_images= images.objects.filter(site_id=sites.objects.get(site_name=site_table_site_name).id).count() + 1
        annotated_images= images.objects.filter(site_id=sites.objects.get(site_name=site_table_site_name).id).filter(human_or_not= False).count() + 1
    
        site_id= sites.objects.get(site_name=site_table_site_name).id
        try:
        
            cur.execute("UPDATE uvdata_sites SET site_images =%s WHERE id=%s", (site_images, site_id))
            cur.execute("UPDATE uvdata_sites SET annotated_images =%s WHERE id=%s", (annotated_images, site_id))
        except:
            print ("could not insert in sites table")
 

    def sites_insert(site_table_site_name):
    
        site_images=1
        annotated_images=1
        try:
            cur.execute("insert into uvdata_sites (site_name,site_images,annotated_images,project_id) values (%s,%s,%s,%s)",(site_table_site_name,site_images,annotated_images,projects.objects.get(project_name=project_table_project_name).id))   
        except:
            print ("could not insert in sites table")


    def images_insert(image_table_filename):

        try:
            cur.execute("insert into uvdata_images (filename,date,human_or_not,image_url,json_url,site_id) values (%s,%s,%s,%s,%s,%s)",(image_table_filename,image_table_date,image_table_human_or_not,image_table_image_url,image_table_json_url,sites.objects.get(site_name=site_table_site_name).id))
        except:
            print ("could not insert in images table")


    def class_insert(class_table_class_name):
    
        try:
            cur.execute("insert into uvdata_classes (class_name) values (%s)",(class_table_class_name,))
        except:
            print ("could not insert in classes table")



    def check_site_classes(site_id):
        select_target_query="select * from uvdata_sites_site_classes where sites_id = %s"
        try:
            cur.execute((select_target_query),(site_id,))
        except:
            print("could not fetch object class from db")


    def insert_site_classes(class_table_class_name):
        try:
            cur.execute("insert into uvdata_sites_site_classes (sites_id,classes_id) values (%s,%s)",(sites.objects.get(site_name=site_table_site_name).id,target_class_name[class_table_class_name]))
        except:
            print ("could not insert in classes_sites table")


    def check_box_records(class_id,image_id):
        select_target_query="select * from uvdata_box where box_class_id = %s and box_image_id= %s"
        try:
            cur.execute((select_target_query),(class_id,image_id,))
        except:
            print("cannot fetch from uvdata_box")


    def insert_box_records(class_id,image_id):
        try:
            cur.execute("insert into uvdata_box (box_class_id,box_image_id) values (%s,%s)",(class_id,image_id))
        except:
            print("could not insert in uvdata_box")


    target_class_name= {}
    available_classes=classes.objects.all().values()
    for x in available_classes:
        target_class_name.update({x['class_name']:x['id']})

    if request.method=="POST":
        cur = con.cursor()
#        r = requests.get('http://13.232.246.51:8000/search/upload_rtsp/')
 #       print(r)

 #       json_recv = request.FILES["upload_json"].read()
  
        json_recv= request.body.decode('utf-8')
        data = json.loads(json_recv)
        
        bucket = 'uploads.live.uncannysurveillance.com/'
        project_table_project_name=data["dashboard_info"]['orgName']
        project_table_project_description="ANPR"
        site_table_site_name=data['dashboard_info']['placeName']
        timestamp=data['event_json']['info']['event_timestamp']
        image_table_date=datetime.datetime.fromtimestamp(int(timestamp)/1000).strftime('%Y-%m-%d')
        time=datetime.datetime.utcfromtimestamp(int(timestamp)/1000).strftime('%H_%M_%S%P').lstrip("0").replace(" 0", " ")
        image_table_filename= time +'_'+ timestamp+ '_' + '0.jpg'
        json_file= time +'_'+ timestamp+ '_' + '1.json'
        c_id= data['dashboard_info']['_id']
        c_name= data['dashboard_info']['name']
        cam_name= c_id + '-' + c_name


        image_table_human_or_not=False
        image_table_image_url= bucket+project_table_project_name+'/'+site_table_site_name+'/'+image_table_date+'/'+cam_name+'/'+image_table_filename
        image_table_json_url= bucket+project_table_project_name+'/'+site_table_site_name+'/'+image_table_date+'/'+cam_name+'/'+json_file
        
        detections = 0
        for x in data:
            if x=="event_json":
                detections=detections+1

        class_table_class_name=[]
        for count in range(detections):
            class_table_class_name.append(data["event_json"]['event']['vehicle_category']['type'])

####project_table
        project_name=projects.objects.filter(project_name=project_table_project_name)
        if project_name.count() == 0:
            project_insert(project_table_project_name,project_table_project_description)
            print('projects complete')


####sites_table
        site_name=sites.objects.filter(site_name=site_table_site_name)
        if not site_name:
            sites_insert(site_table_site_name)
            print('sites complete')
        else:
            sites_update(site_table_site_name)


#######cam_table

        cam_objects=cam_id.objects.filter(cam_id=cam_name)
        site_id= sites.objects.get(site_name=site_table_site_name).id
        if cam_objects.count() == 0:
            cam_insert(cam_name,site_id)
            print('cam insert complete')


#####images table
        image_check = images.objects.filter(filename=image_table_filename)
        if not image_check:
            images_insert(image_table_filename)
            print('images complete')

#####classes table
        for class_count in range(len(class_table_class_name)):
            class_name=classes.objects.filter(class_name=class_table_class_name[class_count])
            if not class_name:
                class_insert(class_table_class_name[class_count])
                target_class_name.update( {class_table_class_name[class_count] : classes.objects.get(class_name=class_table_class_name[class_count]).id} )
                print('classes complete')

#####site-classes table

        site_id= sites.objects.get(site_name=site_table_site_name).id
        check_site_classes(site_id)
        site_class_records = cur.fetchall()
        if not site_class_records:
            for class_count in class_table_class_name:
                insert_site_classes(class_count)

        if site_class_records:
            for x in site_class_records:
                pulled_site_id=x[1]
                for y in class_table_class_name:
                    pulled_class_name=y
                    pulled_class_id= classes.objects.get(class_name=pulled_class_name).id
                    select_target_query="select * from uvdata_sites_site_classes where sites_id = %s and classes_id =%s"
                    try:
                        cur.execute((select_target_query),(pulled_site_id,pulled_class_id,))
                    except:
                        print("could not fetch object class from db")  

                    records = cur.fetchall()             
                    if not records:
                        cur.execute("insert into uvdata_sites_site_classes (sites_id,classes_id) values (%s,%s)",(pulled_site_id,pulled_class_id))

                        print('site-classes complete')


#####box table  
               
        for class_count in range(len(class_table_class_name)):
            class_id=classes.objects.get(class_name=class_table_class_name[class_count]).id
            image_id=images.objects.get(filename=image_table_filename).id
            check_box_records(class_id,image_id)
            box_records = cur.fetchall()
            if not box_records:
                insert_box_records(class_id,image_id)

                print('box complete')

##### send data to s3
#########
    print(target_class_name)

    con.commit() 
    cur.close()
       
    return render(request,'search/upload_rtsp.html',{})

