import django_tables2 as tables
from uvdata.models import search_box

class SearchTable(tables.Table):
    class Meta:
        model = search_box
        template_name = 'django_tables2/bootstrap.html'