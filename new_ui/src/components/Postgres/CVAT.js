import React, { Component } from 'react';
import './Meta.css';
import axios from 'axios';
import {Button,Form,Checkbox} from 'semantic-ui-react';


export class CVAT extends Component {

    state={
        total_images:'',
        annotated_images:'',
        total_cams:'',
        isLoading:true,
        filters : '',
        cams:[]

    }

    getPosts() {
        axios
        .get("http://34.93.111.56:8000/annotator/"+this.props.d_id)
        .then(response => {
          this.setState({
            total_images: response.data['total_images'],
            annotated_images: response.data['human_annotated'],
            total_cams:response.data['total_cams'],
            filters: response.data['filters'],
            isLoading: false
          });
          console.log(this.state);
        })
        
        // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, isLoading: false }));
      }
    
    componentDidMount(){
    this.getPosts()
    this.setState({cams:this.props.cams})
  //  console.log(this.props.cams)
    }

    render() {
        return (
            <div className="cvat">
            {!this.state.isLoading?
            <DisplayForm cam_ids={this.props.cams} filters = {this.state.filters} total={this.state.total_images} annotated={this.state.annotated_images} p_id={this.props.p_id} s_id={this.props.s_id} d_id={this.props.d_id} cams={this.state.total_cams} />
            :null}
            </div>
        )
    }
}

export default CVAT




const Seen = [
    { key: 'u', text: 'New', value: 'unseen' },
    { key: 's', text: 'Old', value: 'seen' }
  ]


const Detection = [
    { key: 'm', text: 'model', value: 'model' },
    { key: 'h', text: 'human', value: 'human' },
  ]


  const Batching = [
    { key: 'm', text: 'images', value: 'images' },
    { key: 'h', text: 'cam', value: 'cam' },
  ]

const Cam_Ids =[]

export class DisplayForm extends Component {

    state={
        limit:"",
        fresh:"",
        detection:'',
        submit:false,
        filters:[],
        response_ready:false,
        response_state:'',
        cam_ids:[],
        showOptions:false,
        clickedOption:'',
        saveIds:[],
        showIds:false,
        imagebatching:false,
        cambatching:false,
        bb:'',
        batchnumber:''
        
    }



    handleLimitButton=(e)=>{
        this.setState({limit: e.target.value});
        console.log(this.state.limit)

    }

    handleBatching=(e)=>{
        this.setState({batchnumber:e.target.value})
        console.log(e.target.value)
    }

    handleSeenButton=(e,{value})=>{
        
        this.setState({fresh: value});
        console.log(this.state.fresh)

    }

    handleBatchButton=(e,{value})=>{
        
        {value=='images'? this.setState({bb: value, imagebatching:true,cambatching:false}) : this.setState({bb: value, cambatching:true,imagebatching:false})}
      //  this.setState({bb: value});

      //  console.log(e.target.value)
        console.log(this.state.bb,this.state.cambatching,this.state.imagebatching)

    }

    handleDetectionButton=(e,{value})=>{
        
        this.setState({detection: value});
        console.log(this.state.detection)

    }

    handleFilters=(e,{value})=>{
        {this.state.filters.indexOf(value)!== -1? 
            this.setState({
                filters: this.state.filters.filter((x,i) => i != this.state.filters.indexOf(value) )
              })
            
            :
            this.state.filters.push(value)}
        console.log(this.state.filters)
    }

    handleSubmitButton=(e)=>{
        
        this.setState({submit: true});
        const postObject={"type":"date-annotate","batching":this.state.bb,"batchnumber":this.state.batchnumber,"p_id":this.props.p_id,"s_id":this.props.s_id,"d_id":this.props.d_id,"split":this.state.limit,"func":this.state.fresh,"preds":this.state.detection,"filters":this.state.filters,"cams":this.state.saveIds};
        console.log(postObject)
        axios.post("http://34.93.111.56:8000/annotator/post_info/", postObject)
        .then(res => {
            console.log(res)
            this.setState({response_state:res.data,response_ready:true})
            
        }).catch(error => {
            console.log(error)
            
        });

    }

    handleCamButton=(e)=>{
        const string = e.target.value;
        let cams = this.props.cam_ids;
               
        this.Cam_Ids = []
        this.setState({showOptions:false})
        cams.filter(cam=>{
            cam['cam'].toLowerCase().includes(string.toLowerCase())?
                this.Cam_Ids.push({text:cam['cam'],value:cam['id']}) &&
                this.setState({showOptions:true})
                :
                null
        })
        console.log(this.Cam_Ids,this.state.showOptions)
    }

    handleOptionSubmit=(e,{value})=>{
        this.state.saveIds.push(value)
        this.setState({showIds:true})
        console.log(this.state.saveIds)
    }

    handleDeleteButton=(e,{value})=>{
    
        const array = this.state.saveIds
        const index = array.indexOf(value)
        if (index !== -1) {
            console.log(index)
            array.splice(index, 1);
            this.state.saveIds = array;
          }
        this.setState({showIds:false})
        this.setState({showIds:true})
        console.log(this.state.saveIds)
    }

    render() {

        return (
            <div>
            {this.state.response_ready==false ?
                <div >
                <p>Total Images: {this.props.total}   Annotated Images: {this.props.annotated}   Total Cameras: {this.props.cams}</p>
                <Form>
                    
                


                <Form.Select
                    fluid
                    label='Batching Type'
                    options={Batching}
                    value={this.state.bb}
                    onChange={this.handleBatchButton.bind(this)} 
                    />
                   {this.state.cambatching==true?

                <Form.Input  
                    type="number" 
                    placeholder="CamId Batches" 
                    label="Batches of CamIds" 
                    onChange={this.handleBatching}
                    />
                    :
                    null

                    }

                    {this.state.imagebatching==true?

                <Form.Input  

                    type="number" 
                    placeholder="Image Batches" 
                    label="Max Images Per Task" 
                    onChange={this.handleBatching}
                    />
                    :
                    null

                    }

                {this.state.cambatching==true?
                <Form.Input  
                    min="0"
                    max="100"
                    type="number" 
                    placeholder="% of Images from every cam_id" 
                    label="Limit" 
                    onChange={this.handleLimitButton}
                    />
                :
                null}

                <Form.Select
                    fluid
                    // style={{left:"0",height:"0"}}
                    label='Seen/Unseen'
                    options={Seen}
                    placeholder='Seen/Unseen'
                    value={this.state.fresh}
                    onChange={this.handleSeenButton.bind(this)} 
                    />
             
                    <Form.Select
                    fluid
                    // style={{left:"0",height:"0"}}
                    label='Detections'
                    options={Detection}
                    placeholder='Model/Human'
                    value={this.state.detection}
                    onChange={this.handleDetectionButton.bind(this)}                    
                    />
                    <h5>Alert Filters:</h5>
                    {this.props.filters.map(filter=>{
                        return(
                         <p><Checkbox 
                         label = {filter}
                         value = {filter}
                         onChange ={this.handleFilters}
                         /></p>
                        )
                    })}


                    {this.state.cambatching==true?
                    <div>
                    <h5>Select Specific Cams</h5>
                    <Form.Input  
                    placeholder="Search Cams within Date"  
                    onChange={this.handleCamButton}
                    />
                   {this.state.showOptions==true? 
                    <div>
                    <div>
                    <Form.Select
                        options={this.Cam_Ids}
                        value={this.state.clickedOption}
                        onChange={this.handleOptionSubmit.bind(this)} 
                    />
                    </div>
                    <div>
                        
                    {this.state.showIds==true?this.state.saveIds.map(single=>{return(<Button value={single} onClick={this.handleDeleteButton.bind(this)}>{single}</Button>)}):null}
                    </div>
                    </div>  
                    :
                    null
                    }
                    </div>
                    :null}
                    <Form.Button className="cvatSubmit" onClick={this.handleSubmitButton}>Submit</Form.Button>
                </Form>
                </div>
            :
            this.state.response_state
            }    
            </div>
        )
    }
}


