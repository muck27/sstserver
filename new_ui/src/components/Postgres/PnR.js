import React, { Component } from 'react'
import './Meta.css'
import { Menu,Button,Form,Checkbox} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import './PnR.css';
import {Line} from 'react-chartjs-2';

export class PnR extends Component {
    constructor(props){
        super(props);
        this.state={
            iou:"",
            showPnR:false,
            detector:'',
            classifier:'',
            classes:[],
            isLoading:true,
            models: '',
            gt: '',
            pt: '',
            conf: '',

        }
    }

    handlePnRButton = (e) => {
        this.setState({iou: e.target.value});

    }
    
    handleConfButton = (e) => {
        this.setState({conf: e.target.value});

    }

    getPosts() {
        axios
        .get("http://34.93.111.56:8000/search/get_models_pnr/"+this.props.d_id+'/')
        .then(response => {
          this.setState({
            models: response.data,
       
          });
     //     console.log('hello from the other side')
     //     console.log(this.state.models);
        })

        .catch(error => this.setState({ error, isLoading: false }));
      }
    
    componentDidMount() {
        this.getPosts();
      }



    
    handleSubmit = (e) => {
        
        this.setState({showPnR:true});
        console.log(this.state.iou);
        const postObject={"iou":this.state.iou,"gt":this.state.gt,"pt":this.state.pt,"conf":this.state.conf};

        axios.post("http://34.93.111.56:8000/search/p_n_r/"+this.props.p_id+'/'+this.props.s_id + '/'+this.props.d_id + '/', postObject)
        .then(res => {
            this.setState({detector:res.data['detector'],classifier:res.data['classifier'],isLoading:false});

        }).catch(error => {
            console.log(error)
            
        });

    }
    
    handleGTModel = (e,{value}) => {
        this.setState({gt:value})
        
    }

    handlePredModel = (e,{value}) => {
        this.setState({pt:value})
        
    }

    render() {
        return (
            <div>
            <div className="pnr">
            <Form>
            <tr>
            <Form.Input  
            placeholder="Enter iou" 
            label="IOU" 
            onChange={this.handlePnRButton}
            />
            </tr>
            
            <tr>
            <Form.Select
            style={{width:"0"}}
            label='GroundTruth'
            options={this.state.models}
            placeholder='GroundTruth'
            value={this.state.gt}
            onChange={this.handleGTModel.bind(this)} 
            />    
            </tr>

            <tr>
            <Form.Select
            style={{width:"0"}}
            label='Prediction Model'
            options={this.state.models}
            placeholder='Prediction Model'
            value={this.state.pt}
            onChange={this.handlePredModel.bind(this)} 
            />    
            </tr>

            <tr>
            <Form.Input  
            placeholder="Enter confidence of bbox" 
            label="Confidence" 
            onChange={this.handleConfButton}
            />
            </tr>

            <tr>
            <Form.Button onClick={this.handleSubmit}>Submit</Form.Button>
            </tr>
            </Form>
            </div>
            <div>
            {this.state.showPnR==true ? this.state.isLoading==false ?

            <DisplayPnR d_id={this.props.d_id} detector={this.state.detector} classifier={this.state.classifier} />
            :
            null
            :
            null
            }
           </div>
           </div>
        )
    }
}

export default PnR




export class DisplayPnR extends Component {
    constructor(props){
        super(props);
        this.state={
            showTable:false,
            showClasses:false,
            showAttributes:false,
         

        }
    }
    


    render() {
    //    console.log(this.props.classifier)
    //    console.log(this.props.detector)
        return (
            
            <div className="pnr-checkbox">
                <table width="600"> 
                <tr>
                    <th style={{'background-color': 'green'}}>Detector</th>
                    <th style={{'background-color': 'green'}}>TP</th>
                    <th style={{'background-color': 'green'}}>FP</th>
                    <th style={{'background-color': 'green'}}>FN</th>
                </tr>

                {this.props.detector.map((single)=> { 

                    return(
                        <tr>
                        <td style={{'background-color': 'red'}} >{single.class}</td>
                        <td style={{'background-color': 'yellow'}}>{single.tp}</td>
                        <td style={{'background-color': 'yellow'}}>{single.fp}</td>
                        <td style={{'background-color': 'yellow'}}>{single.fn}</td>
                        </tr>
                    )
                    }
                    )}     

                </table>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                
                {this.props.classifier.map((single)=>{
                    console.log(single['attributes'])
                    console.log(single['confusion_matrix'])

                    return(
                        <table>
                      
                        <tr>
                        <th style={{'background-color': 'green'}} ></th>
                        {single['attributes'].map((headers)=>{
                        return(
                            <th style={{'background-color': 'green'}} >{headers}</th>
                            
                        )
                        })}

                        </tr>
                        

                        {single['confusion_matrix'].map((matrix,k)=>{
                            console.log(single['attributes'][k])  
                            return(
                                <tr>
                                <td style={{'background-color': 'red'}} >{single['attributes'][k]}</td>
                                {matrix.map((val)=>{
                                                     
                                    return(
                                        <td style={{'background-color': 'yellow'}}>{val}</td>
                                        )})}
                                </tr>
                            )                          
                       

                            })}
  
                      
                        </table>
                        

                        )   
                       
                    })}
                                         

              
            </div>
  
        )
    }
}



export class DrawClasses extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,
            precs:[],
            recalls:[],
            label:[]
         

        }
    }

    handleClasses=(e,{recalls,precs,label})=>{

        {this.state.show==false? this.setState({show:true,precs:precs,recalls:recalls,label:label}): this.setState({show:false,precs:[],recalls:[],label:''})}
        console.log(this.state.show,this.state.precs);
        

    }
    render() {
        return (
            <div>

                {this.props.classes.map((single)=>{
                    return(
                     <p>
                        <Checkbox 
                        onChange={this.handleClasses}
                        recalls= {single.recalls}
                        precs  = {single.precs}
                        label={single.class}
                        />
                     </p>
                    )
                    }
                    )}
                
                {this.state.show==true ? <DrawLine recalls={this.state.recalls} precisions={this.state.precs} label={this.state.label} /> : null }    
            </div>
        )
    }
}


export class DrawAttributes extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,
            precs:[],
            recalls:[],
            label:[]
         

        }
    }

    handleClasses=(e,{recalls,precs,label})=>{

        {this.state.show==false? this.setState({show:true,precs:precs,recalls:recalls,label:label}): this.setState({show:false,precs:[],recalls:[],label:''})}
        console.log(this.state.show,this.state.precs);
        

    }
    render() {
        return (
            <div>

                {this.props.attributes.map((single)=>{
                    return(
                    <p>
                        <Checkbox 
                        onChange={this.handleClasses}
                        recalls= {single.recalls}
                        precs  = {single.precs}
                        label={single.attribute}
                        />
                    </p>
                    )
                    }
                    )}
                
                {this.state.show==true ? <DrawLine recalls={this.state.recalls} precisions={this.state.precs} label={this.state.label} /> : null }    
            </div>
        )
    }
}



export class PnRTable extends Component {
    render() {
        return (
           
        <div>
        <table width="600"> 
        <tr>
            <th>Attribute</th>
            <th>TP</th>
            <th>FP</th>
            <th>FN</th>
        </tr>
       
        {this.props.attributes.map((single)=> { 

                return(
                    <tr>
                    <td>{single.attribute}</td>
                    <td>{single.true_pos}</td>
                    <td>{single.false_pos}</td>
                    <td>{single.false_negs}</td>
                    </tr>
                )
                }
                )}
        
        </table>
        <table width="600"> 
        <tr>
            <th>Classes</th>
            <th>TP</th>
            <th>FP</th>
            <th>FN</th>
        </tr>
       
        {this.props.classes.map((single)=> { 

                return(
                    <tr>
                    <td>{single.class}</td>
                    <td>{single.true_pos}</td>
                    <td>{single.false_pos}</td>
                    <td>{single.false_negs}</td>
                    </tr>
                )
                }
                )}

        </table>   
        </div>
      
        )
    }
}





export class DrawLine extends Component {

    data = {
        labels: this.props.recalls,
        datasets: [
            {   
            fill: false,
            lineTension: 0.5,
            backgroundColor: ["red", "green", "blue", "purple", "magenta","yellow","black","aqua","salmon", "darkgray"],
            borderWidth: 2,
            data: this.props.precisions
            }
        ]
        }
    
    check(){

        console.log(this.data)
    }
    render() {
        return (
            <div className="charts">
                <Line data={this.data}  options={{maintainAspectRatio: true,  title:{display:true,text:this.props.label,fontSize:30,legend:false},}}/>
              
            </div>
        )
    }
}

