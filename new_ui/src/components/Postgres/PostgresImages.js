import React, { Component } from 'react'
import axios from 'axios';
import './Postgres.css';
import { Menu,Button,Input} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import ShowDisplay from './ShowDisplay'

export class PostgresImages extends Component {
    constructor(props){
        super(props);
        this.state={
            iarray:[],
            isLoading: true,
            errors: null,
            activeElement: '',
            activeId:'',
            activeCoords:[],
            showDisplay:false,
            url:"",
            filename:''


        }
    }
    getPosts() {
        axios
        .get("http://34.93.111.56:8000/search/project/"+this.props.p_id+'/'+this.props.s_id+'/'+this.props.d_id+'/'+this.props.c_id)
        .then(response => {
          this.setState({
            iarray: response.data,
            isLoading: false
          });
          console.log(this.state.iarray);
        })
        
        // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, isLoading: false }));
      }
      
    handleClick=(e,{name,coordinates,img_url,file})=>{
        {this.state.showDisplay==true?this.setState({activeId:'',activeCoords:'',showDisplay:false}):this.setState({filename:file,url:img_url,activeId:name,activeCoords:coordinates,showDisplay:true})}
        
        console.log(this.state.activeId,this.state.activeCoords,this.state.url)

    }

    componentDidMount(){
        this.getPosts();
        
    }


    render() {
        return (
            <div className='nav-images  '>
            <h3>Images synced in Postgres</h3>
            <Menu vertical stackable pagination style={{overflow: 'auto', maxHeight: 800, width:340}} >
                {!this.state.isLoading ? (
                this.state.iarray.map((single,k) => {
                const { id, filename, time,image_url} = single;
                return (
                <td>
                  <td>
                  <Menu.Item 
                  as={Link} to="/postgres/images"
                  file={filename}
                  name={id}
                  img_url={image_url}
                  coordinates={single.coordinates}
                  active={this.state.activeElement ===id}
                  onClick={this.handleClick}
                  >
                  <td width="290px">{filename}</td>
                  </Menu.Item> 
                  </td>

                </td> 
                  
                );
              })
            )
          
          : (
            <p>Loading...</p>
          )}
        </Menu>
        {this.state.showDisplay==true? <ShowDisplay filename={this.state.filename} url={this.state.url} coords={this.state.activeCoords}/>: null}
        </div>
        )
    }
}

export default PostgresImages
