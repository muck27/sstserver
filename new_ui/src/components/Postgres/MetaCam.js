
import React, { Component } from 'react'
import {Icon,Accordion} from 'semantic-ui-react';
import "./Meta.css";
import Graph from './Graph'
import ClassInfoCams from './ClassInfoCams'
import ClassifierInfoCams from './ClassifierInfoCams'
import FilterInfoCams from './FilterInfoCams'


export class MetaCam extends Component {

    state = { activeIndex: 0 }
    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index
    
        this.setState({ activeIndex: newIndex })
      }

    render() {

        const { activeIndex } = this.state
        return(

            <div className="meta-cam-new">
            <Accordion fluid styled>
                <Accordion.Title
                active={activeIndex === 0}
                index={0}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Detector Distribution
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 0}>
                <ClassInfoCams id={this.props.id}/>
                </Accordion.Content>

                <Accordion.Title
                active={activeIndex === 1}
                index={1}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Classifier Distribution
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 1}>
                <ClassifierInfoCams  id={this.props.id} />
                </Accordion.Content>

                <Accordion.Title
                active={activeIndex === 2}
                index={2}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Filter Distribution
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 2}>
                <FilterInfoCams  id={this.props.id} />
                </Accordion.Content>
            </Accordion>
 
            </div>

        ) 
  
    }
}

export default MetaCam

