import React, { Component } from 'react';
import {Pie,Bar} from 'react-chartjs-2';

export class Graph extends Component {
//    constructor(props){
//    super(props);
    state={
        labels:[],
        datasets: [{
            backgroundColor: ['red','blue','green','yellow'],
            data: []
            }],

        chart2:{

            labels:[],
            datasets: [{
                backgroundColor: ['red','blue','green','yellow'],
                data: []
                }],
        }
        

    }


    updateLabel(className){
        this.state.labels.push(className);
   //     console.log(this.state.labels);
    }
    
    updateData(newdata){
        this.state.datasets[0].data.push(newdata);
    //    console.log(this.state.datasets[0].data);
    }

    updateLabelConf(className){
        this.state.chart2.labels.push(className);
        console.log(this.state.chart2.labels);
    }
    
    updateDataConf(newdata){
        this.state.chart2.datasets[0].data.push(newdata);
        console.log(this.state.chart2.datasets);
    }




    render() {
        console.log(this.props)
        return (
            <div>

                <div>
                {this.props.mod=="human"?
                <div>
                <div>
                {this.props.class_info.map((single) =>{
                    return(
                    <p>
                       <p> {this.updateLabel(single.class)}</p>
                       <p> {this.updateData(single.h_count)}</p>                
                    </p>);
                })}
                </div>
                <div>
                {this.props.conf.map((single) =>{
                    return(
                    <p>
                       <p> {this.updateLabelConf(single.class)}</p>
                       <p> {this.updateDataConf(single.conf.h_conf)}</p>                
                    </p>);
                })}
  
                </div>
                </div>
                :null
                }

                </div>
       
                <div>
                {this.props.mod=="model"?
                 <div>
                 <div>               
                {this.props.class_info.map((single) =>{
                    return(
                    <p>
                       <p> {this.updateLabel(single.class)}</p>
                       <p> {this.updateData(single.m_count)}</p>                
                    </p>);
   
                })}
                </div>
                <div>
                {this.props.conf.map((single) =>{
                    return(
                    <p>
                       <p> {this.updateLabelConf(single.class)}</p>
                      <p> {this.updateDataConf(single.conf.m_conf)}</p>    
                         
                    </p>);
   
                })}

                </div>
                </div>
                :null
                }
                </div>
            
          
                <div>
                {this.props.mod=="infer"?
                <div>
                <div>
                {this.props.class_info.map((single) =>{
                    return(
                    <p>
                       <p> {this.updateLabel(single.class)}</p>
                       <p> {this.updateData(single.i_count)}</p>                
                    </p>);
  
                })}
                </div>
                <div>
                {this.props.conf.map((single) =>{
                    return(
                    <p>
                       <p> {this.updateLabelConf(single.class)}</p>
                       <p> {this.updateDataConf(single.conf.i_conf)}</p>                
                    </p>);
  
                })}

                </div>
                </div>
                :
                null}
                </div>
      
                <p>Total Image Count : {this.props.image_count}</p>
                <h>Classes Distribution</h>
                <Pie data={{labels:this.state.labels,datasets:this.state.datasets}}/>
                <p></p>
                <p></p>
                <h>Average Confidence</h>
                <Bar data={this.state.chart2} />
            </div>
        )
    }
}

export default Graph
