
import React, { Component } from 'react'
import {Icon,Accordion} from 'semantic-ui-react';
import "./Meta.css";
import Graph from './Graph'
import ClassInfo from './ClassInfo'
import ClassifierInfo from './ClassifierInfo'


export class Meta extends Component {

    state = { activeIndex: 0 }
    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index
    
        this.setState({ activeIndex: newIndex })
      }

    render() {

        const { activeIndex } = this.state
        return(

            <div className="meta-div-project">
            <Accordion fluid styled>
                <Accordion.Title
                active={activeIndex === 0}
                index={0}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Detector Distribution
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 0}>
                <ClassInfo id={this.props.id}/>
                </Accordion.Content>

                <Accordion.Title
                active={activeIndex === 1}
                index={1}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Classifier Distribution
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 1}>
                <ClassifierInfo  id={this.props.id} />
                </Accordion.Content>
            </Accordion>
 
            </div>

        ) 
 
    }
}

export default Meta



