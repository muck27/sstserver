import React, { Component } from 'react'
import './Meta.css'
import { Menu,Button,Form,Checkbox,Image} from 'semantic-ui-react';
import Boundingbox from './Boundingbox';

export class ShowDisplay extends Component {

    constructor(props){
        super(props);
        this.state={
            iarray:[],
            isLoading: true,
            errors: null,
            activeElement: '',
            activeId:'',
            activeCoords:[],
            showDisplay:false,
            url:"",
            filename:'',
            showHuman:false,
            showModel:false,
            showInfer:false,
            showClass:false,
            whichDet:'',
            submit:false,
            boxes:'',
            options:{
                colors: {
                  normal: 'rgba(255,225,255,1)',
                  selected: 'rgba(0,225,204,1)',
                  unselected: 'rgba(100,100,100,1)'
                },
                style: {
                  maxWidth: '100%',
                  maxHeight: '90vh'
                },
                showLabels: false
              }

        }
    }

    componentDidMount(){
   // console.log(this.props.url,this.props.coords, this.props.filename)
    }

    handleHuman=()=>{
        {this.state.showHuman==true?this.setState({showHuman:false,showModel:false,showInfer:false,whichDet:''}):this.setState({showHuman:true,showInfer:false,showModel:false,whichDet:'human_coords'})}
     //   console.log('human'+this.state.showHuman+this.state.whichDet)
    }

    handleModel=()=>{
        {this.state.showModel==true?this.setState({showHuman:false,showModel:false,showInfer:false,whichDet:''}):this.setState({showHuman:false,showInfer:false,showModel:true,whichDet:'model_coords'})}
     //   console.log('model'+this.state.showModel+this.state.whichDet)
    }

    handleInfer=()=>{
        {this.state.showInfer==true?this.setState({showHuman:false,showModel:false,showInfer:false,whichDet:''}):this.setState({showHuman:true,showInfer:true,showModel:false,whichDet:'infer_coords'})}
     //   console.log('infer'+this.state.showInfer+this.state.whichDet)
    }




    handleSubmit=()=>{
        this.setState({submit:true})

        const boxes=[]
        {this.state.whichDet=='model_coords'?        
        this.props.coords.map(single=>{
            single['model_coords'].map(double=>{
                const param  = []
                param.push(double['coords']['x'])
                param.push(double['coords']['y'])
                param.push(double['coords']['w'])
                param.push(double['coords']['h'])
                boxes.push(param)
            
            })
        })
        :null}

        {this.state.whichDet=='human_coords'?        
        this.props.coords.map(single=>{
            single['human_coords'].map(double=>{
                const param  = []
                param.push(double['coords']['x'])
                param.push(double['coords']['y'])
                param.push(double['coords']['w'])
                param.push(double['coords']['h'])
                boxes.push(param)
            
            })
        })
        :null}

        {this.state.whichDet=='infer_coords'?        
        this.props.coords.map(single=>{
            single['infer_coords'].map(double=>{
                const param  = []
                param.push(double['coords']['x'])
                param.push(double['coords']['y'])
                param.push(double['coords']['w'])
                param.push(double['coords']['h'])
                boxes.push(param)
            
            })
        })
        :null}

  


        this.setState({boxes:boxes})
        console.log(this.state.boxes)

    }



    render() {
        return (
            <div>
            <div className="display">
            <p>
            <Checkbox 
            onChange={this.handleHuman}   
            label='human' 
            />
            </p>
            <p>
            <Checkbox 
            onChange={this.handleInfer}        
            label='infer'
            />
            </p>
            <p>
            <Checkbox 
            onChange={this.handleModel}        
            label='model' 
            />
            </p>
            </div>
            <div className="display-classes">
            <Button onClick={this.handleSubmit}> Submit </Button>
            </div>
            
            {this.state.submit==true?<div className="display-image"><Boundingbox image={this.props.url} boxes={this.state.boxes}/></div>:<div className="display-image"><Boundingbox image={this.props.url} /></div>}
         
            </div>
        )
    }
}

export default ShowDisplay