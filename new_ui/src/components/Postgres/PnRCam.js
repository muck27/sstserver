import React, { Component } from 'react'
import './Meta.css'
import { Menu,Button,Form,Checkbox} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import './PnR.css';
import {Line} from 'react-chartjs-2';

export class PnR extends Component {
    constructor(props){
        super(props);
        this.state={
            iou:"",
            showPnR:false,
            attributes:[],
            classes:[],
            isLoading:true,

        }
    }

    handlePnRButton = (e) => {
        this.setState({iou: e.target.value});

    }
    

    
    handleSubmit = (e) => {
        
        this.setState({showPnR:true});
        console.log(this.state.iou);
        const postObject={"iou":this.state.iou};
        axios.post("http://34.93.111.56:8000/search/p_n_r/"+this.props.p_id+'/'+this.props.s_id + '/'+this.props.d_id + '/'+this.props.c_id+'/', postObject)
        .then(res => {
            this.setState({attributes:res.data['attributes'],classes:res.data['classes'],isLoading:false});
            
        }).catch(error => {
            console.log(error)
            
        });
    }
    


    render() {
        return (
            <div>
            <div className="pnr-cam">
            <Form>
            <tr>
            <Form.Input  
            placeholder="Enter iou" 
            label="IOU" 
            onChange={this.handlePnRButton}
            />
            </tr>
            <tr>
            <Form.Button onClick={this.handleSubmit}>Submit</Form.Button>
            </tr>
            </Form>
            </div>
            <div>
            {this.state.showPnR==true ? this.state.isLoading==false ?

            <DisplayPnR d_id={this.props.d_id} attributes={this.state.attributes} classes={this.state.classes} />
            :
            null
            :
            null
            }
           </div>
           </div>
        )
    }
}

export default PnR




export class DisplayPnR extends Component {
    constructor(props){
        super(props);
        this.state={
            showTable:false,
            showClasses:false,
            showAttributes:false,
         

        }
    }
     
    handleTable=(e)=> {
        {this.state.showTable==true? this.setState({showTable:false}) : this.setState({showTable:true}) }
       
        console.log('table'+this.state.showTable)
    }

    handleClasses=(e)=>{
        {this.state.showClasses==true? this.setState({showClasses:false}) : this.setState({showClasses:true}) }
        
        console.log('classe'+this.state.showClasses)
    }
    handleAttributes=(e)=>{

        {this.state.showAttributes==true? this.setState({showAttributes:false}) : this.setState({showAttributes:true}) }
        console.log('attribute'+this.state.showAttributes)
    }



    render() {

        return (
        <div className="pnr-checkbox2" >  
        <p>
        <Checkbox 
        onChange={this.handleTable}
        
        label='Table' 
        />
        </p>
        <p>
        <Checkbox 
        onChange={this.handleClasses}
        
        label='Classes'
        />
        </p>
        <p>
        <Checkbox 
        onChange={this.handleAttributes}
        
        label='Attributes' 
        />
        </p>

        {this.state.showTable==true?

        <PnRTable attributes={this.props.attributes} classes={this.props.classes}/>
        
        :
        null
        }

        {this.state.showClasses==true?

        <DrawClasses classes={this.props.classes}/>

        :
        null
        }


        {this.state.showAttributes==true?

        <DrawAttributes attributes={this.props.attributes}/>

        :
        null
        }
        </div>    

  
        )
    }
}



export class DrawClasses extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,
            precs:[],
            recalls:[],
            label:[]
         

        }
    }

    handleClasses=(e,{recalls,precs,label})=>{

        {this.state.show==false? this.setState({show:true,precs:precs,recalls:recalls,label:label}): this.setState({show:false,precs:[],recalls:[],label:''})}
        console.log(this.state.show,this.state.precs);
        

    }
    render() {
        return (
            <div>

                {this.props.classes.map((single)=>{
                    return(
                     <p>
                        <Checkbox 
                        onChange={this.handleClasses}
                        recalls= {single.recalls}
                        precs  = {single.precs}
                        label={single.class}
                        />
                     </p>
                    )
                    }
                    )}
                
                {this.state.show==true ? <DrawLine recalls={this.state.recalls} precisions={this.state.precs} label={this.state.label} /> : null }    
            </div>
        )
    }
}


export class DrawAttributes extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,
            precs:[],
            recalls:[],
            label:[]
         

        }
    }

    handleClasses=(e,{recalls,precs,label})=>{

        {this.state.show==false? this.setState({show:true,precs:precs,recalls:recalls,label:label}): this.setState({show:false,precs:[],recalls:[],label:''})}
        console.log(this.state.show,this.state.precs);
        

    }
    render() {
        return (
            <div>

                {this.props.attributes.map((single)=>{
                    return(
                    <p>
                        <Checkbox 
                        onChange={this.handleClasses}
                        recalls= {single.recalls}
                        precs  = {single.precs}
                        label={single.attribute}
                        />
                    </p>
                    )
                    }
                    )}
                
                {this.state.show==true ? <DrawLine recalls={this.state.recalls} precisions={this.state.precs} label={this.state.label} /> : null }    
            </div>
        )
    }
}



export class PnRTable extends Component {
    render() {
        return (
           
        <div>
        <table width="600"> 
        <tr>
            <th>Attribute</th>
            <th>TP</th>
            <th>FP</th>
            <th>FN</th>
        </tr>
       
        {this.props.attributes.map((single)=> { 

                return(
                    <tr>
                    <td>{single.attribute}</td>
                    <td>{single.true_pos}</td>
                    <td>{single.false_pos}</td>
                    <td>{single.false_negs}</td>
                    </tr>
                )
                }
                )}
        
        </table>
        <table width="600"> 
        <tr>
            <th>Classes</th>
            <th>TP</th>
            <th>FP</th>
            <th>FN</th>
        </tr>
       
        {this.props.classes.map((single)=> { 

                return(
                    <tr>
                    <td>{single.class}</td>
                    <td>{single.true_pos}</td>
                    <td>{single.false_pos}</td>
                    <td>{single.false_negs}</td>
                    </tr>
                )
                }
                )}

        </table>   
        </div>
      
        )
    }
}





export class DrawLine extends Component {

    data = {
        labels: this.props.recalls,
        datasets: [
            {   
            fill: false,
            lineTension: 0.5,
            backgroundColor: ["red", "green", "blue", "purple", "magenta","yellow","black","aqua","salmon", "darkgray"],
            borderWidth: 2,
            data: this.props.precisions
            }
        ]
        }
    
    check(){

        console.log(this.data)
    }
    render() {
        return (
            <div className="charts">
                <Line data={this.data}  options={{maintainAspectRatio: true,  title:{display:true,text:this.props.label,fontSize:30,legend:false},}}/>
              
            </div>
        )
    }
}
