import  './Postgres.css'
import React, { Component} from 'react';
import { Menu,Button} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import Meta from './Meta'


export class Postgres extends Component {

  constructor(props){
    super(props);
    this.state = {
      parray: [],
      info_array:[],
      isLoading: true,
      errors: null,
      activeElement: '',
      activeId:'',
      activeImageCount:'',
      activeProject:'',
      activeClassInfo:[],
      activeAvgConf:[],
      showMeta:false
      }
  }

  getPosts() {
    axios
    .get("http://34.93.111.56:8000/search/project/")
    .then(response => {
      this.setState({
        parray: response.data,
        isLoading: false
      });
      console.log(this.state.parray);
    })
    
    // If we catch any errors connecting, let's update accordingly
    .catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.getPosts();
  }

  handleItemClick = (e, { name }) => {
              
              this.setState({ activeElement: name }); 
              this.props.onSubmit(name)
          }
       
       
  
  handleItemButton = (e,{id}) => {
    {this.state.showMeta == true? this.setState({showMeta:false}): this.setState({activeId:id,showMeta:true})}
 
  
  }


  render() {
    const {activeImageCount,activeAvgConf,activeClassInfo,activeProject,activeId} = this.state
    return (
      <div className="nav">
      <h3>Projects Synced in Postgres</h3>

      <Menu vertical stackable pagination style={{overflow: 'auto', maxHeight: 800 }} >
          {!this.state.isLoading ? (
            this.state.parray.map(single => {
              const { id, project_name } = single;
              return (
                <td>
                  <td>
                  <Menu.Item 
                  as={Link} to="/postgres/sites"
                  name={id}
                  active={this.state.activeElement ===id}
                  onClick={this.handleItemClick}
                  >
                  <td width="100px">{project_name}</td>
                  </Menu.Item> 
                  </td>
                  <td>
                  <Button
                  color="red" 
                  id = {id}
                  onClick={this.handleItemButton}
                  >Info
                  </Button>
                  </td>
                </td> 
                  
                );
              })
            )
          
          : (
            <p>Loading...</p>
          )}
        </Menu>
        <div>
        
         { this.state.showMeta==true ?
     
          <Meta id={activeId} />
          :
          null
        }
        
        </div>

    </div>
    )
  }
}

export default Postgres
