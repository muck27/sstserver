import React, { Component } from 'react'
import axios from 'axios';
import {Pie,Bar} from 'react-chartjs-2';

export class FilterInfoDates extends Component {


    state = {
        labels:[],
        datasets : [    {'backgroundColor': ['red','blue','green','yellow'],'data':[]}],
        data:[],
        isLoading:true,
    }

    componentDidMount(){
        axios
        .get("http://34.93.111.56:8000/search/DateFilters/"+this.props.id+'/')
        .then(resp => {
 
            this.setState({  
              data: resp.data,
              isLoading:false
            });
            this.state.data.map((single)=>{
                this.state.labels.push(single['filter'])
                this.state.datasets[0]['data'].push(single['count'])
            })  
      
          })

          .catch(error => this.setState({ error, isLoading: false }));

    }


    render() {

        
        return (
            <div>

                <Pie data={{labels:this.state.labels,datasets:this.state.datasets}}/>  
                <p>(Image Count)</p>  
            
            </div>
        )
    }
}

export default FilterInfoDates
