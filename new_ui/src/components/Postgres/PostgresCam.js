import React, { Component } from 'react'
import './Postgres.css'
import axios from 'axios';
import { Menu,Button,Input,Icon} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import MetaCam from './MetaCam';
import PnR from './PnRCam';
import SSIM from './SSIM';
import FilterCam from './FilterCam';

export class PostgresCam extends Component {
    constructor(props){
        super(props);
        this.state={
            carray:[],
            isLoading: true,
            errors: null,
            activeElement: '',
            activeId:'',
            activeImageCount:'',
            activeDate:'',
            activeClassInfo:[],
            activeAvgConf:[],
            showMeta:false,
            showPnR:false,
            showSSIM:false,
            gotSSIM:true,
            valueSSIM:'',
            activeFilters:[]
        }
    }
    


    getPosts() {
        axios
        .get("http://34.93.111.56:8000/search/project/"+this.props.p_id+'/'+this.props.s_id+'/'+this.props.d_id)
        .then(response => {
          this.setState({
            carray: response.data,
            isLoading: false
          });
          console.log(this.state.carray);
        })
        
        // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, isLoading: false }));
      }
      
      
    handleMetaButton = (e, { id}) => {
      console.log('meta button is pressed')
      {this.state.showMeta == true? this.setState({showMeta:false,showCVAT:false,showPnR:false,isSSIMLoading:true}): this.setState({activeId:id,showMeta:true,showCVAT:false,showPnR:false,isSSIMLoading:true})}


    }

    handlePnRButton = (e,{id}) => {
        {this.state.showPnR==true?
        this.setState({activeId:'',showMeta:false,showPnR:false,showSSIM:false,gotSSIM:false,valueSSIM:''})
        :
        this.setState({activeId:id,showPnR:true,showMeta:false,showSSIM:false,gotSSIM:false,valueSSIM:''})
        }
        
        console.log(this.state.showPnR)
      }

    handleSSIMButton = (e,{id}) => {
        {this.state.showSSIM==true?
        this.setState({activeId:'',showSSIM:false,gotSSIM:false,valueSSIM:''})
        :
        this.setState({activeId:id,showPnR:false,showMeta:false,showSSIM:true})
        }
        
        console.log(this.state.showSSIM,this.state.activeId)
      }

    updateSSIM(value){

        this.setState({showSSIM:false,gotSSIM:true,valueSSIM:value})
        console.log('after updating' + this.state.showSSIM, this.state.valueSSIM, this.state.gotSSIM)
    }


    handleCamClick=(e,{name})=>{
       
        this.props.onSubmit(name);

    }

    componentDidMount() {
        this.getPosts();
      }


    render() {
        return (
            <div>
            <div className="nav-cam">
                <h3>Cam-Ids Synced in Postgres</h3>
                <Menu vertical stackable pagination style={{overflow: 'auto', maxHeight: 800, width:540}} >
                {!this.state.isLoading ? (
                this.state.carray.map(single => {
                const { id, cam } = single;
                return (
                <td>
                  <td>
                  <Menu.Item 
                  as={Link} to="/postgres/images"
                  name={id}
                  active={this.state.activeElement ===id}
                  onClick={this.handleCamClick}
                  >
                  <td width="290px">{cam}</td>
                  </Menu.Item> 
                  </td>
                  <td>
                  <Button 
               //   as={Link} to='/postgres/camids'
                  color="red"
                  id = {id}
                  onClick={this.handleMetaButton}
                  >Info
                  </Button>
                  </td>
                  <td>
                  <Button 
                  color="black"
              //    as={Link} to='/postgres/camids'
                  id = {id}
                  onClick={this.handleSSIMButton}
                  disabled={this.state.activeId==id?false:true}
                  >{id ==this.state.activeId ? this.state.gotSSIM ==true ? this.state.valueSSIM: <p><Icon name="clock"/> </p>: <p>SSIM</p>}
                  </Button>
                  </td>
                  <td>
                  </td>
                  <td>
                  <Button 
                  color="white"
              //    as={Link} to='/postgres/camids'
                  id = {id}
                  onClick={this.handlePnRButton}
                  >PnR
                  </Button>
                  
                  </td>
                </td> 
                  
                );
              })
            )
          
          : (
            <p>Loading...</p>
          )}
        </Menu>
            </div>

            <div>
                    
            { this.state.showMeta==true ?
            <div>
           
           <MetaCam id={this.state.activeId} />
         
            </div>
            :
            null    
            }
            </div>

            { this.state.showPnR==true ?
    
            <PnR d_id={this.state.activeId} p_id={this.props.p_id} s_id={this.props.s_id} c_id={this.state.activeId}/>
            :
            null
            }

            { this.state.showSSIM==true ?
    
            <SSIM d_id={this.props.d_id} p_id={this.props.p_id} s_id={this.props.s_id} c_id={this.state.activeId} onSubmit={this.updateSSIM.bind(this)}/>
            :
            null
            }
        </div>
        )
    }
}

export default PostgresCam
