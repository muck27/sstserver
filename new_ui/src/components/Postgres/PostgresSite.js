import React, { Component } from 'react'
import { Menu,Button} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import MetaSite from './MetaSite';
import Filter from './FILTER';

export class PostgresSite extends Component {
    constructor(props){
        super(props);
        this.state={
            sarray:[],
            isLoading: true,
            errors: null,
            activeElement: '',
            activeId:'',
            activeImageCount:'',
            activeSite:'',
            activeClassInfo:[],
            activeAvgConf:[],
            showMeta:false,
            activeFilters:[]
        }
    }
    
    getPosts() {
        axios
        .get("http://34.93.111.56:8000/search/project/"+this.props.p_id)
        .then(response => {
          this.setState({
            sarray: response.data,
            isLoading: false
          });
          console.log(this.state.sarray);
        })
        
        // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, isLoading: false }));
      }
    
    componentDidMount() {
        this.getPosts();
      }
    
    handleItemClick = (e, { name }) => {
              
        this.setState({ activeElement: name }); 
     //   console.log('wegwgwgwegwegwegwegwegweg'+name);//  console.log("new synced moduls "+ synced);  
        this.props.onSubmit(name)
    }
 
 
    handleItemButton = (e,{id}) => {
  
      {this.state.showMeta == true? this.setState({showMeta:false}): this.setState({activeId:id,showMeta:true})}
 
      console.log('pressed info button')
    }


    render() {
        const {activeImageCount,activeAvgConf,activeClassInfo,activeSite,activeId} = this.state
        return (
            <div className="nav">
            <h3>Sites Synced in Postgres</h3>    
            <Menu vertical stackable pagination style={{overflow: 'auto', maxHeight: 800 }} >
            {!this.state.isLoading ? (
            this.state.sarray.map(single => {
              const { id, image_count, site_name, class_info, avg_conf,filters } = single;
              return (
                <td>
                  <td>
                  <Menu.Item 
                  as={Link} to="/postgres/dates"
                  name={id}
                  active={this.state.activeElement ===id}
                  onClick={this.handleItemClick}
                  >
                  <td width="100px">{site_name}</td>
                  </Menu.Item> 
                  </td>
                  <td>
                  <Button 
                  color="red"
                  id = {id}
                  onClick={this.handleItemButton}
                  >Info
                  </Button>
                  </td>
                </td> 
                  
                );
              })
            )
          
          : (
            <p>Loading...</p>
          )}
        </Menu>
        <div>
                    
        { this.state.showMeta==true ?
        <div>
        <MetaSite id={activeId} />
{/*
        <Filter alerts={this.state.activeFilters}/>

*/}
        </div>
        :
        null
        }
        </div>
        </div>
        )
    }
}

export default PostgresSite
