import React, { Component } from 'react'
import {Pie,Bar} from 'react-chartjs-2';
import './Meta.css'

export class FILTER extends Component {
    
    state={
        labels:[],
        datasets: [{
            backgroundColor: ['red','blue','green','yellow'],
            data: []
            }],
    }
    
    render() {
        {this.props.alerts.map(single=>{
            this.state.labels.push(single['filter'])
            this.state.datasets[0].data.push(single['count'])
        })}
        return (
            <div className='meta-filters'>
                <h className="headerForDiv">AlertWise Distribution of Data</h>
                <p></p>
                <p></p>
                <Pie data={{labels:this.state.labels,datasets:this.state.datasets}}/>
            </div>
        )
    }
}

export default FILTER
