import React, { Component } from 'react'
import axios from 'axios';
import {Form,Button} from 'semantic-ui-react';
import {Pie,Bar} from 'react-chartjs-2';

export default class ClassifierInfoSites extends Component {


    state = {
        isLoading:true,
        submitLoading:false,
        inferModels:[],
        classModel:'',
        box_or_images:[{'text':'box','value':'box'},{'text':'images','value':'images'}],
        box_img:'',
        class_conf:'',
        showSubmit:false,
        total_images:'',
        labels_:[],
        data:[],
        datasets:'',
        img_count:''
    }
    

    handleModels=(e,{value})=>{
        this.setState({classModel:value})
    }

    handleType=(e,{value})=>{
        this.setState({box_img:value,showSubmit:true})
    }

    handleConf=(e)=>{
        
        this.setState({class_conf:e.target.value})
    }

    handleSubmit=()=>{
        this.setState({'data':[],'datasets':'','labels_':[]})
        let postObject = {'s_id':this.props.id,'model':this.state.classModel, 'box_or_imgs':this.state.box_img, 'conf':this.state.class_conf}
        axios
        .post("http://34.93.111.56:8000/search/s_class_info/",postObject)
        .then(resp => {
            this.setState({
              resp_data: resp.data,
              submitLoading:true
            });
            
            {this.state.resp_data['class_info'].map((single)=> 
                    
            this.setState({labels_:[...this.state.labels_,single['class']],data:[...this.state.data,single['count']]})
           
   //           console.log(single)
          )}
          
          this.setState({img_count:this.state.resp_data['image_count']})
          this.setState({datasets:[{'backgroundColor': ['red','blue','green','yellow'],'data':this.state.data}]})
          console.log(this.state.labels_,this.state.datasets)
          })
          
          // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, submitLoading: false }));
        

    }


    componentDidMount(){
        axios
        .get("http://34.93.111.56:8000/search/getDetectorInfermodelsSites/"+this.props.id+'/')
        .then(resp => {
            this.setState({  
              inferModels: resp.data,
              isLoading:false
            });
            
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isLoading: false }));

    }


    render() {
        
        return (
           
            <div>
            
                <Form>
            
                    <Form.Select
                    style={{width:"0"}}
                    label='Choose Model'
                    options={this.state.inferModels}
                    placeholder='Model'
                    value={this.state.classModel}
                    onChange={this.handleModels.bind(this)} 
                    />

                    <Form.Select
                    style={{width:"0"}}
                    label='Box/Images'
                    options={this.state.box_or_images}
                    placeholder='Box/Images'
                    value={this.state.box_img}
                    onChange={this.handleType.bind(this)} 
                    />

                    {this.state.box_img== 'box'?

                    <Form.Input  
                    min="0"
                    max="100"
                    type="number" 
                    placeholder="conf in %" 
                    label="confidence" 
                    onChange={this.handleConf.bind(this)}
                    />      

                    :
                    null
                    }

                    {this.state.showSubmit==true?  <Button onClick={this.handleSubmit}>Submit</Button> :null}

                    {this.state.submitLoading==true ? <div> <Pie data={{labels:this.state.labels_,datasets:this.state.datasets}}/> <p>Image Count: {this.state.img_count}</p> </div> : null}            
        
                </Form> 
        
            </div>
        )
    }
}
