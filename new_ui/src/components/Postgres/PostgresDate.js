import React, { Component } from 'react'
import { Menu,Button,Input} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import MetaDate from './MetaDate';
import PnR from './PnR';
import CVAT from './CVAT';
import Filter from './FILTER';
import {Line} from 'react-chartjs-2';

export class PostgresDate extends Component {
    constructor(props){
        super(props);
        this.state={
            darray:[],
            isLoading: true,
            errors: null,
            activeElement: '',
            activeId:'',
            activeImageCount:'',
            activeDate:'',
            activeClassInfo:[],
            activeAvgConf:[],
            showMeta:false,
            showPnR:false,
            showCVAT:false,
            activeFilters:[],
            activeCams:[],
            ssimValue : '',
            isSSIMLoading:true,
            isDownloadLoading:true,
            totalDownloads:'',
            activeDownloadId:''
        }
    }
    

    getPosts() {
        axios
        .get("http://34.93.111.56:8000/search/project/"+this.props.p_id+'/'+this.props.s_id)
        .then(response => {
          this.setState({
            darray: response.data,
            isLoading: false
          });
       //   console.log('hello from the other side')
       //   console.log(this.state.darray);
        })
        
        // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, isLoading: false }));
      }
    
    componentDidMount() {
        this.getPosts();
      }
    
    handleItemClick = (e, { name }) => {
              
        this.setState({ activeElement: name });  
        this.props.onSubmit(name)
    }
 
 
    handleMetaButton = (e,{id}) => {
      {
          console.log('meta button is pressed')
          {this.state.showMeta == true? this.setState({showMeta:false,showCVAT:false,showPnR:false,isSSIMLoading:true}): this.setState({activeId:id,showMeta:true,showCVAT:false,showPnR:false,isSSIMLoading:true})}
 
      }  

    }

    handleCvatButton = (e,{id}) => {
      console.log('cvat button is pressed')
      this.setState({showCVAT:false,showMeta:false,showPnR:false,isSSIMLoading:true})
      axios
      .get("http://34.93.111.56:8000/search/date_meta/"+id)
      .then(response => {
        this.setState({
          activeId:response.data['id'],
          activeImageCount:response.data['image_count'],
          activeDate:response.data['date'],
          activeClassInfo:response.data['class_info'],
          activeAvgConf:response.data['avg_conf'],
          activeFilters:response.data['filters'],
          showCVAT:true
        });
      })
       
      .catch(error => this.setState({ error}));      

  }  

      

    handlePnRButton = (e,{id}) => {
        {this.state.showPnR==true?
        this.setState({activeId:'',showPnR:false})
        :
        this.setState({activeId:id,showPnR:true,showMeta:false,showCVAT:false,isSSIMLoading:true})
        }
      
        console.log(this.state.showPnR)
      }

    handleSSIMButton = (e,{id}) =>{
      console.log(id)
      this.setState({showCVAT:false,showPnR:false,showMeta:false,isSSIMLoading:true})
      const postSSIMObject = {"d_id":id}
      axios
      .post("http://34.93.111.56:8000/search/date_ssim/",postSSIMObject)
      .then(response => {
        this.setState({
          ssimValue: response.data,
          isSSIMLoading: false
        });
        console.log(this.state.ssimValue)
      })
      
      // If we catch any errors connecting, let's update accordingly
      .catch(error => this.setState({ error, isLoading: false }));

    }
    

    handleROIButton=(e,{id})=>{
        
      this.setState({submit: true});
      const postObject={"type":"date-roi","d_id":id,"p_id":this.props.p_id,"s_id":this.props.s_id,"split":0,"func":"unseen","preds":"model","filters":[],"cams":[],"batching":"images","batchnumber":1000};
      console.log(postObject)
      console.log(postObject)
      axios.post("http://34.93.111.56:8000/annotator/post_info/", postObject)
      .then(res => {
          console.log(res)
    //      this.setState({response_state:res.data,response_ready:true})
          
      }).catch(error => {
          console.log(error)
          
      }
      )}


    handleDownloadButton(e,{id}){
      
        console.log(id)
        const postObject = {"d_id":id}
        axios
        .post("http://34.93.111.56:8000/search/download_annotations/",postObject)
        .then(response => {
          this.setState({
            totalDownloads:response.data,
            activeDownloadId:id,
            isDownloadLoading: false
          
          });
          console.log(this.state.totalDownloads)
        })
        
        // If we catch any errors connecting, let's update accordingly
       // .catch(error => this.setState({ error, isLoading: false }));
  
    }


    render() {
        const {activeImageCount,activeAvgConf,activeClassInfo,activeDate,activeId} = this.state
        return (
            <div className="nav">
            <h3>Dates Synced in Postgres</h3>
            <Menu vertical stackable pagination style={{overflow: 'auto', maxHeight: 800, width:580}} >
            {!this.state.isLoading ? (
            this.state.darray.map(single => {
              const { id, date} = single;
              return (
                <td>
                  <td>
                  <Menu.Item 
                  as={Link} to="/postgres/camids"
                  name={id}
                  active={this.state.activeElement ===id}
                  onClick={this.handleItemClick}
                  >
                  <td width="100px">{date}</td>
                  </Menu.Item> 
                  </td>
                  <td>
                  <Button 
            //      as={Link} to='/postgres/dates'
                  color="red"
                  id = {id}
                  onClick={this.handleMetaButton}
                  >Info
                  </Button>
                  </td>
                  <td>
                  <Button 
                  color="black"
                  as={Link} to='/postgres/dates'
                  id = {id}
                  type='date-annotate'
                  onClick={this.handleCvatButton}
                  >Annotate
                  </Button>
                  </td>
                  <td>
                  <Button 
                  color="red"
            //      as={Link} to='/postgres/dates'
                  id = {id}
                  onClick={this.handlePnRButton}
                  >PnR
                  </Button>
                  </td>
                  <td>
                  <Button 
                  color="black"
            //      as={Link} to='/postgres/dates'
                  id = {id}
                  //disabled={this.state.activeId==id?false:true}
                  onClick={this.handleSSIMButton}
                  >SSIM
                  </Button>
                  </td>

                  <td>
                  <Button 
            //      as={Link} to='/postgres/dates'
                  color="red"
                  id = {id}
                  onClick={this.handleDownloadButton.bind(this)}
                  > {id !=this.state.activeDownloadId?<p1>DL</p1>:this.state.totalDownloads}
                  </Button>
                  </td>
                  <td>
                  <Button 
            //      as={Link} to='/postgres/dates'
                  color="black"
                  id = {id}
                  type='date-roi'
                  onClick={this.handleROIButton}
                  > <p>roi</p>
                  </Button>
                  </td>
                </td> 
                  
                );
              })
            )
          
          : (
            <p>Loading...</p>
          )}
        </Menu>
        <div>
                    
        { this.state.showMeta==true ?
        <div>
        <MetaDate id={activeId} />
        </div>
        :
        null
        }
        </div>
        
        <div className="pnr"n>
        { this.state.showPnR==true ?
    
        <PnR d_id={this.state.activeId} p_id={this.props.p_id} s_id={this.props.s_id} />
        :
        null
        }
        </div>

        <div>
        { this.state.showCVAT==true ?
    
        <CVAT d_id={this.state.activeId} p_id={this.props.p_id} s_id={this.props.s_id} cams={this.state.activeCams}/>
        :
        null
        }
        </div>
        <div>
        {
          this.state.isSSIMLoading==false ? 
        <DateSSIM values = {this.state.ssimValue}/>
          :
          null
        }
        </div>
        </div>
        )
    }
}

export default PostgresDate




export class DateSSIM extends Component {

  state = {
    labels: [],
    datasets: [
      {
        label: 'SSIM',
        fill: false,
        lineTension: 0.5,
        backgroundColor: 'rgba(75,192,192,1)',
        borderColor: 'rgba(0,0,0,1)',
        borderWidth: 2,
        data: []
      }
    ]
  }

  render() 
  {
  
    return (
      <div className='datessim'>
        {this.props.values.map(single=>{
          this.state.labels.push(single.cam_name)
          this.state.datasets[0].data.push(single.avg_ssim)  
        })
      }
      <Line
          data={this.state}
          options={{title:{display:true,text:'Average SSIM For CamIds',fontSize:20},legend:{display:true,position:'right'}}}
        />
      </div>
    )
  }
}


