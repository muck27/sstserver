import React, { Component } from 'react'
import axios from 'axios';
import {Form,Button} from 'semantic-ui-react';
import {Pie,Bar} from 'react-chartjs-2';


export default class ClassifierInfoDates extends Component {

    state = {
        isLoading:true,
        inferModels:[],
        classModel:'',
        box_or_images:[{'text':'box','value':'box'},{'text':'images','value':'images'}],
        box_img:'',
        class_conf:'',
        showSubmit:false,
        labels_:[],
        titles:[],
        datasets:'',
        final_data:[],
        new_data:[],
        img_count: ''  
    }
    

    handleModels=(e,{value})=>{
        this.setState({classModel:value})
    }

    handleType=(e,{value})=>{
        this.setState({box_img:value,showSubmit:true})
    }

    handleConf=(e)=>{
        
        this.setState({class_conf:e.target.value})
    }


    handleSubmit=()=>{
        this.setState({final_data:[],'data':[],'datasets':'','labels_':[]})
        let postObject = {'d_id':this.props.id,'model':this.state.classModel, 'box_or_imgs':this.state.box_img, 'conf':this.state.class_conf}
        axios
        .post("http://34.93.111.56:8000/search/d_classifier_info/",postObject)
        .then(resp => {
            this.setState({
              resp_data: resp.data,
            
            });
            
            this.setState({img_count:this.state.resp_data['image_count']})
            var dict = this.state.resp_data
            var dict2 = dict['classifier_info']
                   

            for (var key in dict2){
                var classifier = {'title':'', 'labels':[], 'datasets':[{'backgroundColor': ['red','blue','green','yellow'],'data':[]}]}   
                classifier.title = dict2[key]['classifier']
                var dict3 = dict2[key]['attribute_count']
                
                for (var key2 in dict3){
             
                    classifier.labels.push(dict3[key2]['attribute'])
                    classifier.datasets[0]['data'].push(dict3[key2]['count'])
                }
                
               this.setState({final_data:[...this.state.final_data,classifier]})
               
            }
            this.setState({submitLoading:true})
            console.log(this.state.final_data)


          })
          
          // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, submitLoading: false }));
        
        }

    componentDidMount(){
        axios
        .get("http://34.93.111.56:8000/search/getClassifierInfermodelsDates/"+this.props.id+'/')
        .then(resp => {
            this.setState({  
              inferModels: resp.data,
              isLoading:false
            });
            
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isLoading: false }));

    }


    render() {
        
        return (
           
            <div>
            
                <Form>
            
                    <Form.Select
                    style={{width:"0"}}
                    label='Choose Model'
                    options={this.state.inferModels}
                    placeholder='Model'
                    value={this.state.classModel}
                    onChange={this.handleModels.bind(this)} 
                    />

                    <Form.Select
                    style={{width:"0"}}
                    label='Box/Images'
                    options={this.state.box_or_images}
                    placeholder='Box/Images'
                    value={this.state.box_img}
                    onChange={this.handleType.bind(this)} 
                    />

                    {this.state.box_img== 'box'?

                    <Form.Input  
                    min="0"
                    max="100"
                    type="number" 
                    placeholder="conf in %" 
                    label="confidence" 
                    onChange={this.handleConf.bind(this)}
                    />      

                    :
                    null
                    }

                    {this.state.showSubmit==true?  <Button onClick={this.handleSubmit}>Submit</Button> :null}

                    {this.state.submitLoading==true? 
                    
                    <div>
                    {this.state.final_data.map((single,id)=>{
                        return(
                            <Pie data={{labels:this.state.final_data[id].labels,datasets:this.state.final_data[id].datasets}}/>    
                        )
                        
                        
                   
                    })}
                    <p>Image Count : {this.state.img_count}</p>
                    </div>
                :null}        
                </Form> 

            </div>
        )
    }
}
