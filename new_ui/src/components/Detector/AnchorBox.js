import React, { Component } from 'react'
import axios from 'axios';
import {Table,Form,Checkbox,Button} from 'semantic-ui-react';

export class AnchorBox extends Component {

    state={
        isLoading:true,
        dir_list:[],
        errror:'',
        folder:'',
        width:'',
        height:'',
        clusters:'',
        resp_data:'',
        submitLoading:true,
        model_list:[],
        model_folder:'',
        instance_list:[],
        final_instance:'',
        response_data:[]
    }

    componentDidMount(){

        axios
        .get("http://34.93.111.56:8000/train/anchor_box/")
        .then(resp => {
            this.setState({
              response_data: resp.data,
             
              isLoading:false
            });
            
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isLoading: false }));


         axios
        .get("http://34.93.111.56:8000/train/get_instances/")
        .then(resp => {
            this.setState({
              instance_list: resp.data,
              isLoading:false
            });
            console.log(this.state.dir_list);
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isLoading: false }));
         
    }


    handleTrainingFolder=(e,{value})=>{

        this.setState({folder:value})
     

    }

    handleInstance=(e,{value})=>{

        this.setState({final_instance:value})
        console.log(this.state.response_data)
        this.state.response_data.map(single=>{
             if (single.instance == value){this.setState({dir_list:single.datasets,model_list:single.models})}
        })
    }

    handleModelFolder=(e,{value})=>{

        this.setState({model_folder:value})

    }


    handleHeight=(e)=>{
        
        this.setState({height:e.target.value})
    }

    handleWidth=(e)=>{
        
        this.setState({width:e.target.value})
    }

    handleClusters=(e)=>{
        
        this.setState({clusters:e.target.value})
    }

    handleSubmit=()=>{
        let postObject= {"instance": this.state.final_instance,"folder":this.state.folder,"height":parseInt(this.state.height,10),"width":parseInt(this.state.width,10),"clusters":parseInt(this.state.clusters,10),"model":this.state.model_folder}
        console.log(postObject)
        axios
        .post("http://34.93.111.56:8000/train/anchor_box/",postObject)
        .then(resp => {
            this.setState({
              resp_data: resp.data,
              submitLoading:false
            });
            console.log(this.state.resp_data);
          })
          
          // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, submitLoading: false }));
    }

    render() {
        return (
            <div>
                {this.state.isLoading==false?
                <Form>

                    <Form.Select
                    style={{width:"0"}}
                    label='Choose Instance'
                    options={this.state.instance_list}
                    placeholder='Select Instance'
                    value={this.state.final_instance}
                    onChange={this.handleInstance.bind(this)} 
                    />


                    <Form.Select
                    style={{width:"0"}}
                    label='Image Folder'
                    options={this.state.dir_list}
                    placeholder='Choose Data'
                    value={this.state.folder}
                    onChange={this.handleTrainingFolder.bind(this)} 
                    />

                    <Form.Select
                    style={{width:"0"}}
                    label='Model Folder'
                    options={this.state.model_list}
                    placeholder='Choose Model'
                    value={this.state.model_folder}
                    onChange={this.handleModelFolder.bind(this)} 
                    />


                    <Form.Input  
                    min="0"
                    max="1000"
                    type="number" 
                    placeholder="Height" 
                    label="Height" 
                    onChange={this.handleHeight.bind(this)}
                    />


                    <Form.Input  
                    min="0"
                    max="1000"
                    type="number" 
                    placeholder="Width" 
                    label="Width" 
                    onChange={this.handleWidth.bind(this)}
                    /> 
                    

                    <Form.Input  
                    min="0"
                    max="1000"
                    type="number" 
                    placeholder="Clusters" 
                    label="Clusters" 
                    onChange={this.handleClusters.bind(this)}
                    />

                    <Button onClick={this.handleSubmit}>Submit</Button>
                </Form>
                :null}
                <p></p>
                {this.state.submitLoading==false?<h>Anchor Boxes are :{this.state.resp_data}</h>:null}
            </div>
        )
    }
}

export default AnchorBox


