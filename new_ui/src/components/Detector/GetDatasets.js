import React, { Component } from 'react'
import axios from 'axios';
import {Table,Form,Checkbox,Button} from 'semantic-ui-react';



export class GetDatasets extends Component {
    state={
        isLoading:true,
        process_list:[],
        error:''
    }

 

    componentDidMount(){

        axios
        .get("http://34.93.111.56:8000/train/remote_processes/")
        .then(resp => {
            this.setState({
              process_list: resp.data,
              isLoading:false
            });
            console.log(this.state.process_list);
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isLoading: false }));
    }


    render() {
        return (
                        <Table celled compact definition>
                        <Table.Header fullWidth>
                            <Table.Row>
                            <Table.HeaderCell>instance</Table.HeaderCell>
                            <Table.HeaderCell>Datasets</Table.HeaderCell>
                            <Table.HeaderCell>Models</Table.HeaderCell>
                            <Table.HeaderCell>mAP</Table.HeaderCell>
                            <Table.HeaderCell>Anchor Box </Table.HeaderCell>
                             <Table.HeaderCell>Infer Status </Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                        {this.state.process_list.map(single=>{
                    
                            return(
                                <Table.Row>
                                <Table.Cell>{single['instance']}</Table.Cell>    
                                <Table.Cell>{single['datasets'].map(double=>{return(<p>{double}</p>)})} </Table.Cell>
                                <Table.Cell>{single['models'].map(double=>{return(<p>{double}</p>)})}</Table.Cell>
                                <Table.Cell>{single['detectorMAP'].map(double=>{return(<div> <p>{double['dataset']}</p> <p>{double['model']}</p>   <p>Model MAP: {double['value']}</p>     </div>)})}</Table.Cell>
                                <Table.Cell>{single['anchorBox'].map(double=>{return(<div> <p>{double['dataset']}</p>  <p>{double['model']}</p>   <p>{double['value']}</p>  </div>     )})}</Table.Cell>
                                <Table.Cell>{single['Infer'].map(double=>{return(<div> <p>{double['dataset']}</p>  <p>{double['model']}</p>   <p>{double['value']}</p>  </div>     )})}</Table.Cell>
                         
                                </Table.Row>
						    )
                        })}
                        </Table.Body>

                    </Table>
        )
    }
}

export default GetDatasets


