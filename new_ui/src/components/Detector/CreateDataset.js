import React, { Component } from 'react'
import {Table,Form,Checkbox,Button} from 'semantic-ui-react';
import axios from 'axios';
import styles from './Detector.css'
const Detector = [
    { key: 'd', text: 'darknet', value: 'darknet' },

  ]

export class CreateDataset extends Component {
    constructor(props){
        super(props);
        this.state={
            isProjectLoading:true,
            isSiteLoading:true,
            isDateLoading:true,
            detectorType:'',
            p_name:'',
            s_name:'',
            d_name:'',
            p_list:[],
            s_list:[],
            d_list:[],
            instance_list:[],
            showSites:false,
            showDates:false,
            target_dates:[],
            showImages:false,
            split:'',
            confirm_dataset:false,
            response:'',
            isResponseLoading:true,
            final_instance:'',
            final_names:''
    
        }
    
      }
    
    
    handleDetectorType=(e,{value})=>{
        this.setState({detectorType:value})
        console.log(this.state.detectorType)
    }
    

    handleInstance=(e,{value})=>{
        this.setState({final_instance:value})
        console.log(this.state.final_instance)
    }

    handleProject=(e,{value})=>{
        this.setState({p_name:value})
        console.log(this.state.p_name)
        axios
        .get("http://34.93.111.56:8000/train/sites/"+value)
        .then(resp => {
            this.setState({
              s_list: resp.data,
              isSiteLoading:false,
              
            });
            this.setState({showSites:true})
            console.log(this.state.s_list);
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isSiteLoading: false }));
    }


    handleSite=(e,{value})=>{
        this.setState({s_name:value})
        
        axios
        .get("http://34.93.111.56:8000/train/dates/"+value)
        .then(resp => {
            this.setState({
              d_list: resp.data,
              isDateLoading:false,
              
            });
            this.setState({showDates:true})
            console.log(this.state.d_list);
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isDateLoading: false }));
    }

    handleDate=(e,{value})=>{
        {this.state.target_dates.indexOf(value)!== -1? 
            this.setState({
                target_dates: this.state.target_dates.filter((x,i) => i != this.state.target_dates.indexOf(value) )
              })
            
            :
            this.state.target_dates.push(value)}
        
        console.log(this.state.target_dates)
    }


    componentDidMount=()=>{
        
        axios
        .get("http://34.93.111.56:8000/train/projects/")
        .then(resp => {
            this.setState({
              p_list: resp.data,
              isProjectLoading:false
            });
            console.log(this.state.p_list);
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isProjectLoading: false }));



         axios
        .get("http://34.93.111.56:8000/train/get_instances/")
        .then(resp => {
            this.setState({
              instance_list: resp.data,
           //   isProjectLoading:false
            });
           // console.log(this.state.p_list);
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isProjectLoading: false }));

    }

    handleSplitButton=(e)=>{
        this.setState({split: e.target.value,confirm_dataset:true});
        console.log(this.state.split)

    }

    handleSubmitButton=()=>{
        
        this.setState({showImages:true})
        console.log(this.state.target_dates)
    }

    handleResetButton=()=>{
        
        this.setState({showImages:false,})
        console.log(this.state.target_dates)
    }

    handleConfirmButton=()=>{

        const data = new FormData() 
        data.append('d_id', this.state.target_dates)
        data.append('split',parseFloat(this.state.split, 10))
        data.append('instance',this.state.final_instance)
        data.append('class_list',this.state.final_names)
        data.append('model','detector')

      //  let pushObject={'d_id':this.state.target_dates,'split':parseFloat(this.state.split, 10),'instance': this.state.final_instance,'model':'detector','class_list':this.state.final_names}
      //  console.log(pushObject)
        axios
        .post("http://34.93.111.56:8000/train/create_dataset/",data)
        .then(resp => {
            this.setState({
              response: resp.data,
              isResponseLoading:false
            });
            console.log(this.state.p_list);
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isProjectLoading: false }));
    }

    handleNames=event=>{

        console.log(event.target.files[0])
        this.setState({final_names:event.target.files[0]})

    }

    render() {
        return (
            <div>{this.state.isResponseLoading==true?
                <Form>
                    <Form.Select
                    style={{width:"0"}}
                    label='Choose Active Instance'
                    options={this.state.instance_list}
                    placeholder='instance uid'
                    value={this.state.final_instance}
                    onChange={this.handleInstance.bind(this)} 
                    />


                    <Form.Select
                    style={{width:"0"}}
                    label='Detector Type'
                    options={Detector}
                    placeholder='Detector'
                    value={this.state.detectorType}
                    onChange={this.handleDetectorType.bind(this)} 
                    />
                   <Form.Select
                    style={{width:"0"}}
                    label='Project'
                    options={this.state.p_list}
                    placeholder='Project'
                    value={this.state.p_name}
                    onChange={this.handleProject.bind(this)} 
                    /> 
                  
                   {this.state.showSites==true?
                     
                   <Form.Select
                   style={{width:"0"}}
                   label='Site'
                   options={this.state.s_list}
                   placeholder='Site'
                   value={this.state.s_name}
                   onChange={this.handleSite.bind(this)} 
                   />
                    :null}

                   {this.state.showDates==true?
                   <div>
                       <h>Date</h>
                    </div>
                    :
                   null}
                   
                   <div className="compact-dates">
                   {this.state.showDates==true?
                   this.state.d_list.map(single=>{
                    return(        
                        <Checkbox
                        
                        label={single.text}
                        value={single.value} 
                        onChange={this.handleDate}                       
                        />                       
                    )                     
                   })
                   :null
                   }
                   </div>

                   {this.state.showDates==true?

                   <div>
                   <Button size="mini" onClick={this.handleSubmitButton}>Submit</Button>
                   <Button size="mini" onClick={this.handleResetButton}>Reset</Button>
                   </div>
                    :
                   null}
                
                   {this.state.showImages==true?

                   <div className={styles.imgCalc}>
                   <ImageCalc class_list={this.state.s_list} target_dates={this.state.target_dates} d_list={this.state.d_list}/>
                   </div>
                    :
                   null}

                   {this.state.showImages==true?

                   <div>
                    <p></p>                  
                    <Form.Input  
                    min="0"
                    max="1"
                    type="float" 
                    placeholder="split ratio" 
                    label="Split" 
                    onChange={this.handleSplitButton}
                    />
                   

                    <p>
                    Choose Det.Names
                    </p>
                    <p>
                    <input  type="file" name="data.names" onChange={this.handleNames.bind(this)}/>
                    </p>
                    </div>
                    :
                   null}
                   { this.state.confirm_dataset==true?<Button onClick={this.handleConfirmButton}>Confirm Dataset</Button>:null}
                </Form>
                :
                this.state.response}
            </div>
        )
    }
}
    

export default CreateDataset





export class ImageCalc extends Component {

    state={

        class_values:[],
        row_values:[],
        total_vals:[]
    }

    pushClasses(class_v){

        {this.state.class_values.indexOf(class_v)!== -1? null:this.state.class_values.push(class_v)}
       // console.log(class_v)
    }

    rowVals(value){
        this.state.row_values.push(value)
        console.log(this.state.row_values)
    }

    colVals(value){
        const arrSum = arr => arr.reduce((a,b) => a + b, 0)
       
        this.state.total_vals.push(arrSum(value))
        console.log(this.state.total_vals)
    }



    componentWillMount(){
        
        {this.props.class_list[0]['class_info'].map(single=>{
            const class_v = single.class_name
            this.pushClasses(class_v)
        })}


        {this.props.d_list.map(single=>{

            if (this.props.target_dates.indexOf(single['value']) !==-1){
            let arrValue=[]
            single['class_info'].map(double=>{
                this.state.class_values.map(triple=>{
                    {triple==double['class_name'] ? arrValue.push(double['total_images']):null}
                })
                
            })
            this.rowVals(arrValue)
        }
        })}

        {this.state.class_values.map(single=>{
            let ColValue=[]
            {this.props.d_list.map(double=>{
                if (this.props.target_dates.indexOf(double['value']) !==-1){
                    double['class_info'].map(triple=>{
                        if (triple['class_name']==single){
                            ColValue.push(triple['total_images'])
                        }
                    })
                }
            })}
            this.colVals(ColValue)
        })}
    }


    render() {
       

 
        return (
            <div >
                <Table celled>

                    <Table.Header>
                        <Table.Row> 
                        {this.state.class_values.map(single=>{

                        return(                        
                            <Table.HeaderCell>{single}</Table.HeaderCell>  
                        )})}
                        </Table.Row> 
                        
                    </Table.Header>

                    <Table.Body>
                            
                        {this.state.row_values.map(row=>{
                            return(
                            <Table.Row>
                            {row.map(cell=>{
                                return(<Table.Cell>{cell}</Table.Cell>)
                            })}
                            </Table.Row>)

                        })}
                        <Table.Row>
                        {this.state.total_vals.map(total=>{
                            return(
                                <Table.Cell positive>{total}</Table.Cell>
                            )
                        })}
                        </Table.Row>
                                                   
                    </Table.Body>

                </Table>
            </div>
        )
    }
}




