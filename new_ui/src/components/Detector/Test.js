import React, { Component } from 'react'
import axios from 'axios';
import {Table,Form,Menu,Button} from 'semantic-ui-react';
import './Detector.css'


export class Train extends Component {

    state={
        data:[],
        isLoading:true,
        folder:'',
        weights:'',
        configs:'',
        config:'',
        weightFolder:'',
        allWeights:[],
        weightFile:'',
        showImages:false,
        submitLoading:true,
        resp_data:[],
        models:[],
        model_folder:'',
        instance_list:'',
        final_instance:'',
        response_data:[],
        dir_list:[],
        model_list:[],
        folder:'',
        model_folder:''


    }


    componentDidMount(){
        axios
        .get("http://34.93.111.56:8000/train/anchor_box/")
        .then(resp => {
            this.setState({
              response_data: resp.data,
             
              isLoading:false
            });
            
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isLoading: false }));



        axios
        .get("http://34.93.111.56:8000/train/get_instances/")
        .then(resp => {
            this.setState({
              instance_list: resp.data,
           //   isProjectLoading:false
            });
           // console.log(this.state.p_list);
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isProjectLoading: false }));

    }

    handleTrainingFolder=(e,{value})=>{
        this.setState({folder:value}) 
    }



    handleInstances=(e,{value})=>{

        this.setState({final_instance:value})
        console.log(this.state.response_data)
        this.state.response_data.map(single=>{
             if (single.instance == value){this.setState({dir_list:single.datasets,model_list:single.models})}
        })
    }


 


    handleSubmit=()=>{
        this.setState({submitLoading:true})
        let postObject = {

            "data_folder": this.state.folder,
            "model_folder": this.state.model_folder,
            "instance":this.state.final_instance
        }
       console.log(postObject)
        axios
        .post("http://34.93.111.56:8000/infer/dates/",postObject)
        .then(resp => {
            this.setState({
              submitLoading:false,
              resp_data:resp.data
            });
            console.log(this.state.resp_data);
          })
          
          // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, submitLoading: false }));
    }

    handleSubmitImages=()=>{
        this.setState({showImages:true})
        console.log(this.state.resp_data)
    
    }


    handleModelFolder=(e,{value})=>{

        this.setState({model_folder:value})

    }
     
     
    handleTrainingFolder=(e,{value})=>{

        this.setState({folder:value})
     

    }


    render() {
        return (
            <div>

           
        
            <Form>

            <Form.Select
            style={{width:"0"}}
            label='Choose Instance :  '
            options={this.state.instance_list}
            placeholder= 'Instance UID'
            value={this.state.final_instance}
            onChange ={this.handleInstances.bind(this)} 
            />  



                    <Form.Select
                    style={{width:"0"}}
                    label='Image Folder'
                    options={this.state.dir_list}
                    placeholder='Choose Data'
                    value={this.state.folder}
                    onChange={this.handleTrainingFolder.bind(this)} 
                    />

                    <Form.Select
                    style={{width:"0"}}
                    label='Model Folder'
                    options={this.state.model_list}
                    placeholder='Choose Model'
                    value={this.state.model_folder}
                    onChange={this.handleModelFolder.bind(this)} 
                    />
            {/*
            <Form.Select
            style={{width:"0"}}
            label='Choose Weight Folder'
            options={this.state.weights}
            placeholder='Weight Folder'
            value={this.state.weightFolder}
            onChange={this.handleWeightFolder.bind(this)} 
            />  

            <Form.Select
            style={{width:"0"}}
            label='Choose Weight File'
            options={this.state.allWeights}
            placeholder='Weight File'
            value={this.state.weightFile}
            onChange={this.handleWeightFile.bind(this)} 
            /> 
            
            */}
            </Form>
            <Button onClick={this.handleSubmit}>Submit</Button>
            
           

            </div>

        )
    }
}

export default Train





export class DisplayImages extends Component {

    state={
        display:false,
        remote_url:''
    }
    handleDisplayImage=(e,{value})=>{
        this.setState({display:true,remote_url:value})
        console.log(this.state.remote_url)
    }

    render() {
        
        return (
            <div>
                <Menu vertical stackable pagination style={{width:320}} >
                <div className="test-display">
                    {this.props.data.map(single=>{
                    return(
                    <div>
                    <Menu.Item 
                    name={single.img}
                    value={single.remote_loc}
                    onClick={this.handleDisplayImage}
                    />
                  
                    </div>
                    )
                    })}
                </div>
                </Menu>
                
            </div>
        )
    }
}




export class DisplayBoundingBox extends Component {
    render() {
        return (
            <div className="display-img">
                <img src={this.props.data}  />
            </div>
        )
    }
}





