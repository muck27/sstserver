import React, { Component } from 'react'
import axios from 'axios';
import {Table,Form,Checkbox,Button} from 'semantic-ui-react';

export class CalculateMAP extends Component {
    state={
        data:[],
        isLoading:true,

        submitLoading:true,
        models:[],
        instance_list:[],
        final_instance:'',
        response_data:[],
        dir_list:[],
        model_list:[],
        final_dataset:'',
        final_model:''

    }

    componentDidMount(){


         axios
        .get("http://34.93.111.56:8000/train/anchor_box/")
        .then(resp => {
            this.setState({
              response_data: resp.data,
             
              isLoading:false
            });
            
          })
          
          // If we catch any errors connecting, let's update accordingly

         axios
        .get("http://34.93.111.56:8000/train/get_instances/")
        .then(resp => {
            this.setState({
              instance_list: resp.data,
              isLoading:false
            });
            console.log(this.state.dir_list);
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isLoading: false }));

    }


    handleModelFolder=(e,{value})=>{

        this.setState({final_model:value})

    }


    handleTrainingFolder=(e,{value})=>{
        this.setState({final_dataset:value})     
    }


    handleSubmit=()=>{
        let postObject = {
            "instance":this.state.final_instance,
           
            "dataset": this.state.final_dataset,
  
            "model": this.state.final_model
        }
        console.log(postObject)
        axios
        .post("http://34.93.111.56:8000/train/map_module/",postObject)
        .then(resp => {
            this.setState({
              precision: resp.data['precision'],
              recall: resp.data['recall'],
              f1_score: resp.data['f1_score'],
              submitLoading:false
            });
            console.log(this.state.resp_data);
          })
          
          // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, submitLoading: false }));
    }


     handleInstance=(e,{value})=>{

        this.setState({final_instance:value})
        console.log(this.state.response_data)
        this.state.response_data.map(single=>{
             if (single.instance == value){this.setState({dir_list:single.datasets,model_list:single.models})}
        })
    }



    render() {
        return (
            <div>
            {this.state.submitLoading==true?
            <Form>
            
             <Form.Select
                    style={{width:"0"}}
                    label='Choose Instance'
                    options={this.state.instance_list}
                    placeholder='Select Instance'
                    value={this.state.final_instance}
                    onChange={this.handleInstance.bind(this)} 
             />

                    <Form.Select
                    style={{width:"0"}}
                    label='Image Folder'
                    options={this.state.dir_list}
                    placeholder='Choose Data'
                    value={this.state.final_dataset}
                    onChange={this.handleTrainingFolder.bind(this)} 
                    />

                    <Form.Select
                    style={{width:"0"}}
                    label='Model Folder'
                    options={this.state.model_list}
                    placeholder='Choose Model'
                    value={this.state.final_model}
                    onChange={this.handleModelFolder.bind(this)} 
                    />

            

            <Button onClick={this.handleSubmit}>Submit</Button>            
            </Form>              
            :
            <div>
            <p>Process has been registered</p>

            </div>
            }

            </div>
        )
    }
}

export default CalculateMAP


