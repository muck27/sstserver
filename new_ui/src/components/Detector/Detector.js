import React, { Component } from 'react'
import './Detector.css'
import { Accordion,Icon } from 'semantic-ui-react'
import CreateDataset from './CreateDataset'
import GetDatasets from './GetDatasets'
import AnchorBox from './AnchorBox'
import CalculateMAP from './CalculateMap'
import Train from './Train'
import Test from './Test'
import ConnectVM from './ConnectVM'
import UploadDetectorModel from './UploadDetectorModel'


export class Detector extends Component {
    state = { activeIndex: 0 }

    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index
    
        this.setState({ activeIndex: newIndex })
      }


    render() {
        const { activeIndex } = this.state
        return (
            <div className='train-detector'>
            <Accordion fluid styled>
                <Accordion.Title
                active={activeIndex === 0}
                index={0}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Connect to VM
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 0}>
                <ConnectVM />
                </Accordion.Content>

                <Accordion.Title
                active={activeIndex === 1}
                index={1}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Download Detector Model
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 1}>
                <UploadDetectorModel />
                </Accordion.Content>

            
                <Accordion.Title
                active={activeIndex === 2}
                index={2}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Create Datasets
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 2}>
                <CreateDataset />
                </Accordion.Content> 

                <Accordion.Title
                active={activeIndex === 3}
                index={3}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Anchor Box Calculation
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 3}>
                <AnchorBox />
                </Accordion.Content>

                <Accordion.Title
                active={activeIndex === 4}
                index={4}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                mAP Calculation
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 4}>
                <CalculateMAP />
                </Accordion.Content>
                

                <Accordion.Title
                active={activeIndex === 5}
                index={5}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Start Training
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 5}>
                <Train />
                </Accordion.Content>

                <Accordion.Title
                active={activeIndex === 6}
                index={6}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Model Inference to DB
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 6}>
                <Test />
                </Accordion.Content>


                <Accordion.Title
                active={activeIndex === 7}
                index={7}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Process Stats
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 7}>
                <GetDatasets />
                </Accordion.Content>
                </Accordion> 

            </div>
        )
    }
}

export default Detector



