import React, { Component } from 'react'
import axios from 'axios';
import {Table,Form,Checkbox,Button} from 'semantic-ui-react';




var model_type = [
    {'text': 'detector','value':'detector'},
    {'text':'sst-detector','value':'sst-detector'}
]


export class UploadDetector extends Component {


    constructor(props) {
        super(props);
        this.state = {
                configs:[],
                weights:[],
                instances:[],
                model_name : '',
                weight_path: null,
                cfg_path : null,
                all_models: [],
                final_config:'',
                final_weight:'',
                final_names:null,
                final_data: null,
                final_instance:'',
                model_type:'',
                final_roi_weight: '',
                final_proposal_config:''

        }
   
    }


     componentDidMount(){
        axios
        .get("http://34.93.111.56:8000/train/upload_detector/")
        .then(resp => {
            
            this.setState({
              
              configs: resp.data['configs'],
              weights:resp.data['weights'],
              instances : resp.data['instances']
            });
            
          })
         
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isLoading: false }));

          
    }


 

    handleModelName = (e,value) => {
        this.setState({model_name:e.target.value})
       
    }


    handleSubmit = () => {

    const data = new FormData() 
    data.append('cfg', this.state.final_config)
    data.append('weight',this.state.final_weight)
    data.append('model_name',this.state.model_name)
    data.append('names_file',this.state.final_names)
    data.append('instance_id',this.state.final_instance)
    data.append('roi_weight',this.state.final_roi_weight)
    data.append('detector_type',this.state.model_type)
    data.append('proposal_cfg',this.state.final_proposal_config)
    axios.post("http://34.93.111.56:8000/train/upload_detector/", data, { 
      // receive two    parameter endpoint url ,form data
    })
    .then(res => { // then print response status
    console.log(res.statusText)
    })

    }

    handleCFG=(e,value)=>{

        this.setState({final_config:value['value']})

    }

    handleProposalCFG=(e,value)=>{

        this.setState({final_proposal_config:value['value']})

    }


    handleModelType=(e,value)=>{

        this.setState({model_type:value['value']})

    }


    handleWeight=(e,value)=>{

        this.setState({final_weight:value['value']})

    }

    handleROIWeight=(e,value)=>{

        this.setState({final_roi_weight:value['value']})

    }


    handleNames=event=>{

        this.setState({final_names:event.target.files[0]})
        console.log(event.target.files[0])

    }

    handleInstances=(e,value)=>{

        this.setState({final_instance:value['value']})

    }


    render() {
        return (
            <div>
               


             <Form.Select
            style={{width:"0"}}
            label='Choose Instance :  '
            options={this.state.instances}
            placeholder= 'Instance UID'
            value={this.state.final_instance}
            onChange ={this.handleInstances.bind(this)} 
            />  


            <Form.Select
            style={{width:"0"}}
            label='Model Type :  '
            options={model_type}
            placeholder= 'Model Type'
            value={this.state.model_type}
            onChange ={this.handleModelType.bind(this)} 
            />  


            <Form.Input  
            type="text"
            placeholder="give more specifity"
            label="Model Name :  "
            value= {this.state.model_name}
            onChange={this.handleModelName}
            />

            <Form.Select
            style={{width:"0"}}
            label='CFG file :  '
            options={this.state.configs}
            placeholder= 'cfg file'
            value={this.state.final_config}
            onChange ={this.handleCFG.bind(this)} 
            />  


            <Form.Select
            style={{width:"0"}}
            label='Weight file :  '
            options={this.state.weights}
            placeholder= 'weight file'
            value={this.state.final_weight}
            onChange ={this.handleWeight.bind(this)} 
            />  


            {this.state.model_type == 'sst-detector'?
            
            <div>
             <Form.Select
            style={{width:"0"}}
            label='ROI Weights :  '
            options={this.state.weights}
            placeholder= 'roi class weight file'
            value={this.state.final_roi_weight}
            onChange ={this.handleROIWeight.bind(this)} 
            /> 
            
            <Form.Select
            style={{width:"0"}}
            label='Proposal CFG file :  '
            options={this.state.configs}
            placeholder= 'proposal cfg file'
            value={this.state.final_proposal_config}
            onChange ={this.handleProposalCFG.bind(this)} 
            />  
            </div>
            :

            null
            }



               

            <p>
            Choose Det.Names
            </p>
            <p>
            <input type="file" name="det.names" onChange={this.handleNames}/>
            </p>

            


            <p>
             <Button onClick={this.handleSubmit}>Submit</Button>
            </p>

            </div>

            

        )
    }
}

export default UploadDetector


