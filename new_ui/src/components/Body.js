import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'
import NavBar from './NavBar'
import Home from './Home/Home'
import Watchlist from './Watchlist/Watchlist'
import Site from './Watchlist/Site'
import Date from './Watchlist/Date';

import Postgres from './Postgres/Postgres'
import PostgresSite from './Postgres/PostgresSite'
import PostgresDate from './Postgres/PostgresDate'
import PostgresCam from './Postgres/PostgresCam'
import PostgresImages from './Postgres/PostgresImages'
import { Form} from 'semantic-ui-react';
import axios from 'axios';  
import Active from './Active/Active'
import Resources from './Resources/Resources'
import Upload from './Upload/Upload'
import Detector from './Detector/Detector'
import Classifier from './Classifier/Classifier'

export class  Body extends Component {

    getProjectId(p_id){
    
      this.setState({project:p_id})
    }

    
    getSiteId(s_id){
      this.setState({site:s_id})

    }

    getDateId(d_id){
      this.setState({date:d_id})

    }
    getCamId(c_id){
      this.setState({cam:c_id})
      console.log(this.state.project,this.state.site,this.state.date,this.state.cam)

    }

    getProject(p_name){
      this.setState({p_name:p_name})
    }

    getSite(s_name){
      this.setState({s_name:s_name})

    }

    getDate(d_name){
      this.setState({d_name:d_name})

    }
    getCam(c_name){
      this.setState({c_name:c_name})

    }

    getBucket(bucket){
  
      this.setState({bucket:bucket})
     
    }

    getConfirmation(confirmation,role){
  
      this.setState({login:confirmation,role:role})
     
    }
  

    constructor(props){
      super(props);
      this.state={project:'',site:'',date:'',cam:'',p_name:'',s_name:'',d_name:'',c_name:'',bucket:'',login:false,role:''};
      
      
      console.log('here is body conosle' + this.state.site);
    }
  
    
    render() {
        return (
            <div>
            {this.state.login==true?
            <Router>

            <div>
              
              <NavBar />
            
              <Route exact path="/home" component={Home} />
               
              { this.state.role=='superuser'? 
              <div>
              <Route exact path="/watchlist" component={() => <Watchlist onSubmit={this.getProjectId.bind(this)} onSubmitTwo={this.getProject.bind(this)} onSubmitThree={this.getBucket.bind(this)}/>} />
              <Route exact path="/watchlist/site" component={()=> <Site p_id={this.state.project} onSubmit={this.getSiteId.bind(this)} onSubmitTwo={this.getProject.bind(this)}/>} />
              <Route exact path="/watchlist/site/date" component={()=> <Date s_id={this.state.site} p_id={this.state.project} />} />
              </div>
              :null}
          
             { (this.state.role=='superuser'|| this.state.role=='engineer') ?     
             <div>     
              <Route exact path="/postgres" component={()=> <Postgres onSubmit={this.getProjectId.bind(this)}/>} />
              <Route exact path="/postgres/sites" component={()=> <PostgresSite p_id={this.state.project} onSubmit={this.getSiteId.bind(this)} />} />
              <Route exact path="/postgres/dates" component={()=> <PostgresDate s_id={this.state.site} p_id={this.state.project} onSubmit={this.getDateId.bind(this)} />} />
              <Route exact path="/postgres/camids" component={()=> <PostgresCam  s_id={this.state.site} p_id={this.state.project} d_id={this.state.date} onSubmit={this.getCamId.bind(this)}/>} />
              <Route exact path="/postgres/images" component={()=> <PostgresImages s_id={this.state.site} p_id={this.state.project} d_id={this.state.date} c_id={this.state.cam} />} />
              
              <Route exact path="/active" component={Active} />
              <Route exact path="/train/detector" component={Detector} />
              <Route exact path="/train/classifier" component={Classifier} />
              <Route exact path="/upload/files" component={Upload} />
              </div>
              :null}
              
              { (this.state.role=='chief-annotator' || this.state.role=='engineer' || this.state.role=='superuser')? 
              <Route exact path="/resources" component={Resources} />
              :
              null}
            </div>
          </Router>
          :
          <LoginPage onSubmit={this.getConfirmation.bind(this)} />
          }
          </div>
        
        )
    }
}

export default Body;


export class LoginPage extends React.Component {

  constructor(props){
    super(props);
    this.state={
        password:"",
        user:"",
        user_type:'',
        confirmation:false,
        confirmation_users:['superuser','engineer','chief-annotator']

    }
}

  handleUser(e){
    console.log(e.target.value)
    this.setState({user:e.target.value});
  }

  handlePassword(e){
    console.log(e.target.value)
    this.setState({password:e.target.value});
  }

  handleSubmit(e){
  
  let postObject = {"user":this.state.user,"password":this.state.password};
  axios.post("http://34.93.111.56:8000/search/login/",postObject)
  .then(res => {
      this.setState({user_type:res.data['user_type']});
      if(this.state.confirmation_users.indexOf(this.state.user_type) !== -1){
        this.setState({confirmation:true})
      } 
      else{
        this.setState({confirmation:false})
      }
      this.props.onSubmit(this.state.confirmation,this.state.user_type);
      console.log(this.state.user_type,this.state.confirmation)
      
  }).catch(error => {
      console.log(error)
      
  });

  console.log(postObject)
  }


  render() {
    return (
      <div>
      <div>
      <img src={require('./uncanny.jpg') } height="900" width="1920"  />
      </div>
      <div className="login"> 
        <Form >
          <Form.Input  
            placeholder="User" 
            label="User" 
            onChange={this.handleUser.bind(this)}
          />
          <Form.Input  
            placeholder="Password" 
            label="Password" 
            onChange={this.handlePassword.bind(this)}
          />
          <Form.Button onClick={this.handleSubmit.bind(this)}>Submit</Form.Button>
        </Form>
      </div>
      </div>
    )
  }
}


