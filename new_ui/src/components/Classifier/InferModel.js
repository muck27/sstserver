import React, { Component } from 'react'
import axios from 'axios';
import {Table,Form,Checkbox,Button} from 'semantic-ui-react';

const final_attributes = [];

export class DownloadClassifierModel extends Component {


    constructor(props) {
        super(props);
        this.state = {
                models :[],
                sites :[],
                classifier : [],
                final_model : '',
                final_site : '',
                final_site_id : '',
                final_classifier:'',
                isLoading : true,
                showAttributes:false,
                attributes:[],
                final_list:[],
                showOptions:false,
                bgColor2: 'red',
                instances:[],
                final_instance:''

                       
        
    }

    }

     componentDidMount(){
        axios
        .get("http://34.93.111.56:8000/train/upload_classifier/")
        .then(resp => {


            this.setState({
              
              models : resp.data.models,
              sites : resp.data.classifier,
              instances:resp.data.instances,
              isLoading:false
             
            });

            
          })
         
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isLoading: false }));

 }


 



    handleSubmit = () => {

    const data = new FormData() 
    data.append('model', this.state.final_model)
    data.append('site', this.state.final_site_id)
    data.append('classifier', this.state.final_classifier)
    data.append('attributes',final_attributes)
    data.append('instance',this.state.final_instance)
    axios.post("http://34.93.111.56:8000/train/upload_classifier/", data, { 
      //receive two    parameter endpoint url ,form data
    })
    .then(res => { // then print response status
    console.log('submit')
    })



    }

    handleModel=(e,value)=>{

        this.setState({final_model:value['value']})

    }



    handleClassifier=(e,value)=>{
        this.setState({showAttributes:false})
        this.setState({final_classifier:value['value']})
        this.arrangeAttrs(value['value'])
        this.setState({showAttributes:true})
        this.checkattrs()
    }

    arrangeAttrs=(classifier)=>{
    
                 
                 this.state.sites.map(single=>{
                 if (single.value == this.state.final_site){

            
                   single['classifier_info'].map(double=>{

                        if (double['value'] == classifier){
                                this.setState({attributes:double['attributes']})
						}
                        
                   })

                   }
                })
    
    }


    checkattrs(){
            console.log(this.state.attributes)
            
    
    }


     handleSite=(e,value)=>{

        this.setState({final_site:value['value']})
        this.state.sites.map((single )=>{
               if(single.text == value['value'])
               {
               this.setState({classifier : single.classifier_info,final_site_id : single.id})
              console.log(single)
               }
           
             
        })
        

    }

    handleAttribute=(e,value)=>{

             
            if(e.target.style['backgroundColor'] == "red"){
                
                e.target.style['backgroundColor'] ="blue";
                final_attributes.push(value['children']);
                console.log(final_attributes)
                return
                
            }
            if(e.target.style['backgroundColor'] == "blue"){

                e.target.style['backgroundColor'] ="red";
                const index = final_attributes.indexOf(value['children'])
                if (index !== -1) {final_attributes.splice(index, 1);}
                console.log(final_attributes)
                return

            }
           
	}


     handleRemoveAttribute(e,value){
           
           
            console.log(value)
	}


    handleInstance=(e,value)=>{

        this.setState({final_instance:value['value']})
        
    }

    render() {
        return (
            <div>
               

            <Form.Select
            style={{width:"0"}}
            label='Instance :  '
            options={this.state.instances}
            placeholder= 'select active instances'
            value={this.state.final_instance}
            onChange ={this.handleInstance.bind(this)} 
            />  

            <Form.Select
            style={{width:"0"}}
            label='Model :  '
            options={this.state.models}
            placeholder= 'select model from s3'
            value={this.state.final_model}
            onChange ={this.handleModel.bind(this)} 
            />  

            <Form.Select
            style={{width:"0"}}
            label='Site :  '
            options={this.state.sites}
            placeholder= 'select site for model'
            value={this.state.final_site}
            onChange ={this.handleSite.bind(this)} 
            />  

            <Form.Select
            style={{width:"0"}}
            label='Classifier :  '
            options={this.state.classifier}
            placeholder= 'select classifier for model'
            value={this.state.final_classifier}
            onChange ={this.handleClassifier.bind(this)} 
            />  

        
            <p>{this.state.showAttributes==true?

            
            this.state.attributes.map(double=>{
            
                  return(
                <Button primary style={{backgroundColor: "red"}}  onClick= {this.handleAttribute}>{double['text']}</Button>
                   )
            })

            :null}</p>

            <p>
             <Button onClick={this.handleSubmit}>Submit</Button>
            </p>

            </div>

            

        )
    }
}

export default DownloadClassifierModel



