import React, { Component } from 'react'
import axios from 'axios';
import {Table,Form,Checkbox,Button} from 'semantic-ui-react';



export class ConnectVM extends Component {



    constructor(props) {
        super(props);
        this.state = {
                instances :[],
                isLoading: true,
                false_states :['hello','bye']
    }
    }


    
     componentDidMount(){
        axios
        .get("http://34.93.111.56:8000/search/upload_instance/")
        .then(resp => {

           
            this.setState({
              
              instances : resp['data'],
              isLoading:false
             
            });

            
          })
         
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isLoading: false }));
       
 }


 

    render() {

        
        return (
         

                    <Table celled compact definition>
                        <Table.Header fullWidth>
                            <Table.Row>
                            <Table.HeaderCell>public_ip</Table.HeaderCell>
                            <Table.HeaderCell>free space</Table.HeaderCell>
                            <Table.HeaderCell>gpu availability</Table.HeaderCell>
                            <Table.HeaderCell>gpu memory</Table.HeaderCell>
                            <Table.HeaderCell>user</Table.HeaderCell>
                            <Table.HeaderCell>activity</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                        {this.state.instances.map(single=>{
                    
                            return(
                                <Table.Row>
                                <Table.Cell>{single['public_ip']}</Table.Cell>    
                                <Table.Cell>{single['free_space']} GB</Table.Cell>
                                <Table.Cell>{single['gpu_availability']?'Yes':'No'}</Table.Cell>
                                <Table.Cell>{single['gpu_memory']}</Table.Cell>
                                <Table.Cell>{single['user']}</Table.Cell>
                                <Table.Cell>{single['active']}</Table.Cell>
                                </Table.Row>
						    )
                        })}
                        </Table.Body>

                    </Table>
         

        )
    }
}

export default ConnectVM


