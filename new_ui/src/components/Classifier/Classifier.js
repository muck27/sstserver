import React, { Component } from 'react'
import './Classifier.css'
import { Accordion,Icon } from 'semantic-ui-react'
import CreateDataset from './CreateDataset'
import GetDatasets from './GetDatasets'
import DownloadClassifierModel from './DownloadClassifierModel'
import InferModel from './InferModel'
import ConnectVM from './ConnectVM'



export class Classifier extends Component {
    state = { activeIndex: 0 }

    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index
    
        this.setState({ activeIndex: newIndex })
      }


    render() {
        const { activeIndex } = this.state
        return (
            <div className='train-detector'>
            <Accordion fluid styled>
                <Accordion.Title
                active={activeIndex === 0}
                index={0}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Connect to VM
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 0}>
                <ConnectVM />
                </Accordion.Content>



                <Accordion.Title
                active={activeIndex === 1}
                index={1}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Download Classifier Model
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 1}>
                <DownloadClassifierModel />
                </Accordion.Content>
            
                <Accordion.Title
                active={activeIndex === 2}
                index={2}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Create Datasets
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 2}>
                <CreateDataset />
                </Accordion.Content> 


                <Accordion.Title
                active={activeIndex === 3}
                index={3}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Datasets Created
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 3}>
                <GetDatasets />
                </Accordion.Content>


                <Accordion.Title
                active={activeIndex === 4}
                index={4}
                onClick={this.handleClick}
                >
                <Icon name='dropdown' />
                Infer Model
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 4}>
                <InferModel />
                </Accordion.Content>
                </Accordion> 

            </div>
        )
    }
}

export default Classifier

