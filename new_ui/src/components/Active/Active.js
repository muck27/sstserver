import './Active.css'
import { Input, Label, Menu } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import React, { Component } from 'react'
import CVAT from './CVAT'
import Detector from './Detector'

export class Active extends Component {

  constructor(props){
    super(props);
    this.state={
        activeArray:[],
        isLoading: true,
        errors: null,
        showCVAT:false,
        showClassifier:false,
        showDetector:false,
    }
}

  handleCVATClick=(e)=>{

    this.setState({showCVAT:true,showClassifier:false,showDetector:false})
  }

  handleClassifierClick=(e)=>{

    this.setState({showCVAT:false,showClassifier:true,showDetector:false})
  }

  handleDetectorClick=(e)=>{

    this.setState({showCVAT:false,showClassifier:false,showDetector:true})
  }

  check=()=>{
    console.log(this.state.showCVAT,this.state.showClassifier,this.state.showDetector)
  }
  render(){
  return (
    <div className='active-containers'>
      <Menu >
        <Menu.Item header>Active Docker Containers</Menu.Item>
        <Menu.Item
          name='CVAT'
          onClick={this.handleCVATClick}
        />
        <Menu.Item
          name='Classifier'
          onClick={this.handleClassifierClick}
        />
        <Menu.Item
          name='Detector'
          onClick={this.handleDetectorClick}
        />
      </Menu>
      {this.state.showCVAT==true?<CVAT  />: null}
      {this.state.showDetector==true?<Detector />:null}
    </div>
  )
}
}
export default Active


