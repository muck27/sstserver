import React, { Component } from 'react'
import axios from 'axios';
import { Input, Label, Menu,Button } from 'semantic-ui-react'

export class CVAT extends Component {

    constructor(props){
        super(props);
        this.state={
            activeArray:[],
            isLoading: true,
            isRespLoading: true,
            errors: null,
            activeElement: '',
            response:''

        }
    }

    getPosts() {
        axios
        .get("http://34.93.111.56:8000/annotator/active/")
        .then(response => {
          this.setState({
            activeArray: response.data,
            isLoading: false
          });
         
          console.log(this.state.activeArray);
        })
        
        // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, isLoading: false }));
      }

    
    handleButton=(e,{port})=>{
    const postObject={"port":port}
    if (window.confirm('Do you wish to stop port?'))
    axios.post("http://34.93.111.56:8000/annotator/active/",postObject)
    .then(resp => {
        this.setState({
          response: resp.data,
          isRespLoading:false
        });
        console.log(this.state.response);
      })
      
      // If we catch any errors connecting, let's update accordingly
      .catch(error => this.setState({ error, isLoading: false }));
    }

    componentDidMount(){this.getPosts();}

    render() {
        return (
            <div className="cvat-container">
                {this.state.isRespLoading==true?
                this.state.activeArray.map(single=>{return(
                <Menu vertical  >
                    <Menu.Item>
                     <Menu.Header>Project</Menu.Header>

                    <Menu.Menu>
                    <Menu.Item
                    name={single.project}
                    />
                    </Menu.Menu>
                    </Menu.Item>

                    <Menu.Item>
                    <Menu.Header>Site</Menu.Header>

                    <Menu.Menu>
                    <Menu.Item
                    name={single.site}
                    />

                    </Menu.Menu>
                    </Menu.Item>

                    <Menu.Item>   
                    <Menu.Header>Date</Menu.Header>

                    <Menu.Menu>
                    <Menu.Item
                    name={single.date}
                    />
                    </Menu.Menu>
                    </Menu.Item>

                    <Menu.Item>
                    <Menu.Header>Port</Menu.Header>

                    <Menu.Menu>
                    <Menu.Item
                    >
                    {single.port}
                    </Menu.Item>

    
                    </Menu.Menu>
                    </Menu.Item>
                    <Button
                    port={single.port}
                    onClick={this.handleButton}
                    >Close Port</Button>
                    </Menu>
                )})
                :
                (this.state.response)
                
                }
            </div>
        )
    }
}

export default CVAT
