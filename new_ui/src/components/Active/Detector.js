import React, { Component } from 'react'
import axios from 'axios';
import { Input, Label, Menu,Button } from 'semantic-ui-react'
import {Line} from 'react-chartjs-2';


export class Detector extends Component {


    constructor(props){
        super(props);
        this.state={
            activeArray:[],
            isLoading: true,
            isRespLoading: true,
            errors: null,
            activeElement: '',
            response:'',

        }
    }

    componentDidMount(){
        this.interval = setInterval(() => 
        axios
        .get("http://34.93.111.56:8000/train/get_results/")
        .then(response => {
          this.setState({
            activeArray: response.data,
            isLoading: false
          });
          console.log(this.state.activeArray);
        })
        
        // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, isLoading: false })), 5000);

      }
cd 
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    


    render() {
        return (
            <div className="detector-container">
                {this.state.isRespLoading==true?
                this.state.activeArray.map(single=>{return(
                    <Menu vertical style={{width:400}} >

                    <Menu.Item>
                     <Menu.Header>Training Folder</Menu.Header>
                    <Menu.Menu>
                    <Menu.Item
                    name={single.folder}
                    />
                    </Menu.Menu>
                    </Menu.Item>

                    <Menu.Item>
                    <Menu.Header>Iterations</Menu.Header>
                    <Menu.Menu>
                    <Menu.Item
                    name={single.iters}
                    />
                    </Menu.Menu>
                    </Menu.Item>

                    <Menu.Item>   
                    <Menu.Header>Average Loss</Menu.Header>
                    <Menu.Menu>
                    <Menu.Item 
                    name={single.avg_loss}
                    />
                    </Menu.Menu>
                    </Menu.Item>

                    <Menu.Item>
                    <Menu.Header>Current Learning Rate</Menu.Header>
                    <Menu.Menu>
                    <Menu.Item
                    >
                    {single.current_lr}
                    </Menu.Item>
                    </Menu.Menu>
                    </Menu.Item>

                    <Menu.Item>
                    <Menu.Header>Model Config Iterations</Menu.Header>
                    <Menu.Menu>
                    <Menu.Item
                    >
                    {single.cfg_iter}
                    </Menu.Item>
                    </Menu.Menu>
                    </Menu.Item>


                    <Menu.Item>
                    <Menu.Header>Port</Menu.Header>
                    <Menu.Menu>
                    <Menu.Item
                    >
                    {single.port}
                    </Menu.Item>
                    </Menu.Menu>
                    </Menu.Item>


                    <Menu.Item>
                    <Menu.Header>Loss Curve</Menu.Header>
                    <Menu.Menu>
                    <Menu.Item
                    >
                    <LossCurve data = {single.graph_values} />
                    </Menu.Item>
                    </Menu.Menu>
                    </Menu.Item>

                    <Button
                    port={single.port}
                    onClick={this.handleButton}
                    >Close Port</Button>
                    </Menu>
                )})
                :
                (this.state.response)
                
                }
            </div>
        )
    }
}

export default Detector




export class LossCurve extends Component {
    state={
        labels:[],
        datasets: [{
            
            data: []
            }]}
    
    componentDidMount(){
        
        this.props.data.map(single=>{
            this.state.labels.push(single['iter']);
            this.state.datasets[0].data.push(single['loss']);
        })
        console.log(this.props.data)

    }

    render() {
        return (
            <div>
                <Line data={this.state}  options={{maintainAspectRatio: true}}/>
            </div>
        )
    }
}

