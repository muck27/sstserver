import React, { Component } from 'react'
import axios from 'axios';
import { Table, Menu,Confirm} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import './Watchlist.css';

export class Date extends Component {

    state = {
        darray: [],
        isLoading: true,
        errors: null,
        activeElement: '',
        open:false
    }


    openConfirm  = () => this.setState({ open: true })
    closeConfirm = () => this.setState({ open: false })


    getPosts() {
        axios
        .get("http://34.93.111.56:8000/download_s3/sync/"+this.props.p_id+'/'+this.props.s_id)
        .then(response => {
          this.setState({
            darray: response.data,
            isLoading: false
          });
          console.log(this.state.darray);
        })
        
        // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, isLoading: false }));
      }
    

      componentDidMount() {
        this.getPosts();
      }
    
      handleItemClick = (e, { name }) => {
                  
                this.setState({ activeElement: name }); 
                console.log(this.state.activeElement);
                const postObject = {
                    d_id: name,
                };
                  
                if (window.confirm('Are you sure you wish to download date?')) 
                    axios.post("http://34.93.111.56:8000/download_s3/sync/"+this.props.p_id+'/'+this.props.s_id, postObject)
                    .then((res) => {
                        console.log(res.data)

                    }).catch((error) => {
                        console.log(error)
                    });
      
                  
                  
                }


    render() {
       console.log(this.props.bucket)
        const { activeElement, isLoading, darray } = this.state;
        return (
            <div className="nav-dates">
            <Menu vertical stackable style={{overflow: 'auto', maxHeight: 800 }} >
            {!isLoading ? (
            darray.map(single => {
              const { d_id, dates,objects } = single;
              
              return (
                    <div>
                    <Menu.Item 
                        as={Link} to="/watchlist/site/date"
                        name={d_id}
                        active={activeElement === dates}
                        onClick={this.handleItemClick}
                    >
                    <p>
                      {dates}
                    </p>
                    <p>
                      Objects: {objects}
                    </p>

                    </Menu.Item>

                    </div>
                );
              })
            )
          
          : (
            <p>Loading...</p>
          )}
        </Menu>
        
        
        </div>
        )
    }
}

export default Date
