import React, { Component } from 'react'
import axios from 'axios';
import { Icon, Menu} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import './Watchlist.css';


export class Site extends Component {

    state = {
        sites: [],
        isLoading: true,
        errors: null,
        activeElement: '',
        
      }
      
      getPosts() {
        axios
        .get("http://34.93.111.56:8000/download_s3/sync/"+this.props.p_id)
        .then(response => {
          this.setState({
            sites: response.data,
            isLoading: false
          });
          console.log(this.state.sites);
        })
        
        // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, isLoading: false }));
      }
    
      componentDidMount() {
        this.getPosts();
      }
    
      handleItemClick = (e, { name }) => {
                  
                  this.setState({ activeElement: name }); 
                  console.log(this.state.activeElement);
                  this.props.onSubmit(name)  
                  
                }


    render() {
        const { activeElement, isLoading, sites } = this.state;
        return (
            <div className="nav-sites">
            <Menu vertical stackable style={{overflow: 'auto', maxHeight: 800 }} >
            {!isLoading ? (
            sites.map(single => {
              const { synced,s_id,sites } = single;
              
              return (
                  <Menu.Item 
                  as={Link} to="/watchlist/site/date"
                  name={s_id}
                  active={activeElement === s_id}
                  onClick={this.handleItemClick}
                  >
                  <div>
                      {synced ?
                      <IconicName sname={sites}/>                                            
                      :
                      (<p>{sites}</p>)
                      }
                  </div>
                  </Menu.Item> 
                );
              })
            )
          
          : (
            <p>Loading...</p>
          )}
        </Menu>
        
        
        </div>
        )
    }
}
export default Site;

class IconicName extends Component {
  render() {
    return (
      <div>
        {this.props.sname}
        <Icon name='check' color="green" />
    
      </div>
    )
  }
}

