import React, { Component} from 'react';
import { Icon, Menu} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import Site from './Site';
import './Watchlist.css';

export class Watchlist extends Component {
  constructor(props){
    super(props);
    this.state = {
      parray: [],
      isLoading: true,
      errors: null,
      activeElement: '',
      getBucket:true,
      bucketarray:''
      }
  }
  getPosts() {
    axios
    .get("http://34.93.111.56:8000/download_s3/sync/")
    .then(response => {
      this.setState({
        parray: response.data,
        isLoading: false
      });
      console.log(this.state.projects);
    })
    
    // If we catch any errors connecting, let's update accordingly
    .catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.getPosts();
  }

  handleItemClick = (e, { name }) => {
              
              this.setState({ activeElement: name }); 
              console.log(this.state.activeElement);//  console.log("new synced moduls "+ synced);  
              this.props.onSubmit(name);
              
              
            }


  handleBucketClick =(e,{name})=>{
              console.log(name)
            
              {this.state.parray.map(single=>{
                single['bucket'] == name ? this.setState({bucketarray:single['info']}) :null
              })}
              console.log(this.state.bucketarray)
              this.setState({getBucket:false})
            //  this.props.onSubmitThree(name)
              console.log('helooooooooooo')
            }



  render() {
    
    const { activeElement, isLoading, parray } = this.state;
    console.log(this.state.bucketarray )
    return (

      <div className="nav-projects">
      {this.state.getBucket==false?
      <Menu vertical stackable pagination style={{overflow: 'auto', maxHeight: 800 }} >
          {!isLoading ? (
            this.state.bucketarray.map(single => {
              const { p_id, projects, synced } = single;
              return (
                  <Menu.Item 
                  as={Link} to="/watchlist/site"
                  name={p_id}
                  active={activeElement === p_id}
                  onClick={this.handleItemClick}
                  >
                  <div>
                      {synced ?
                      <IconicName pname={projects}/>                                            
                      :
                      (<p>{projects}</p>)
                      }
                  </div>
                  
                  </Menu.Item> 
                );
              })
            )
          
          : (
            <p>Loading...</p>
          )}
        </Menu>
       :
      <Menu vertical stackable pagination style={{overflow: 'auto', maxHeight: 800 }}>
        <Menu.Item
        name = 'uploads.live.uncannysurveillance.com'
        onClick={this.handleBucketClick.bind(this)}
        >
        Uncanny Surveillance
        </Menu.Item>

        <Menu.Item
        name = "uploads.live.videoanalytics"
        onClick={this.handleBucketClick.bind(this)}
        >
        Video Analytics
        </Menu.Item>
      </Menu>
        }
        </div>


    
    );
  
}
}

export default Watchlist;






class IconicName extends Component {
  render() {
    return (
      <div>
        {this.props.pname}
        <Icon name='check' color="red" />
    
      </div>
    )
  }
}

