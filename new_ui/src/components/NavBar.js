import React from 'react'
import { Menu } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { Icon } from 'semantic-ui-react'

import './Login.css'

class NavBar extends React.Component {
  
  constructor(props){
    super(props);
    this.state= {activeItem:'',login:false,user_type:''} 
  }



  handleItemClick = (e, { name }) => {this.setState({ activeItem: name }); console.log(this.state.activeItem)}
  

  render() {
    const { activeItem } = this.state.activeItem

    return (
      
      <div >
    
      <Menu  vertical stackable style={{overflow: 'auto', maxHeight: 800, width:200}} >
        <Menu.Item 
          as={Link} to='/home'
          name='home'
          active={activeItem === 'home'}
          onClick={this.handleItemClick}
        >
        <Icon name='home' />
        Home
        </Menu.Item>

        <Menu.Item 
          as={Link} to='/watchlist'
          name='watchlist'
          active={activeItem === 'watchlist'}
          onClick={this.handleItemClick}
        >
        <Icon name='cloud download' />
        
          Watchlist
        </Menu.Item>

        <Menu.Item  
          as={Link} to='/postgres'
          name='postgres'
          active={activeItem === 'postgres'}
          onClick={this.handleItemClick}
        >
        <Icon name='database' />
          Postgres
        </Menu.Item>

        <Menu.Item 
          as={Link} to='/active'
          name='active'
          active={activeItem === 'active'}
          onClick={this.handleItemClick}
        >
        <Icon name='clock outline' />
          Active Process
        
        </Menu.Item>

        <Menu.Item 
          as={Link} to='/train/detector'
          name='train-detector'
          active={activeItem === 'train'}
          onClick={this.handleItemClick}
        >
        <Icon name='paper plane' />
          Train Detector
        
        </Menu.Item>


        
        <Menu.Item 
          as={Link} to='/train/classifier'
          name='train-classifier'
          active={activeItem === 'train'}
          onClick={this.handleItemClick}
        >
        <Icon name='paper plane' />
          Train Classifier
        
        </Menu.Item>


        <Menu.Item 
          as={Link} to='/upload/files'
          name='upload'
          active={activeItem === 'upload'}
          onClick={this.handleItemClick}
        >
        <Icon name='paper plane' />
          Local Upload
        
      </Menu.Item>


        <Menu.Item 
          as={Link} to='/resources'
          name='resources'
          active={activeItem === 'resource'}
          onClick={this.handleItemClick}
        >
        <Icon name='paper plane' />
          Resources
        
      </Menu.Item>
      </Menu>
      
      
      
      
      </div>

    )
  }
}

export default NavBar



