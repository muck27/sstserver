import React, { Component } from 'react'
import axios from 'axios';
import { Table , Label,Button,Form} from 'semantic-ui-react'
//import './Resources.css';

export class Resources extends Component {
    constructor(props){
        super(props);
        this.state={
            resource_array:[],
            isLoading: true,
            error: null,
            editOn:false,
            activeId:'',
            listAnnotators:''

        }
    }
    

    componentDidMount(){
        axios
        .get("http://34.93.111.56:8000/search/resources/")
        .then(response => {
          this.setState({
            resource_array: response.data,
            isLoading: false
          });
          console.log(this.state.resource_array);
        })
        
        // If we catch any errors connecting, let's update accordingly
        .catch(error => this.setState({ error, isLoading: true }));
      }
        
    

    handleEditClick(e,{id}){
        {this.state.editOn==true?this.setState({editOn:false,activeId:''}):this.setState({editOn:true,activeId:id})}
        console.log(id)
    }

    handleSubmit(e,{id}){
        console.log('post request from here',id)
        console.log(this.state.listAnnotators)
        let postObject = {'anns':this.state.listAnnotators,'port':id}
        axios.post("http://34.93.111.56:8000/search/updateAnnotators/",postObject)
        .then(response => {
     //     this.setState({
     //       resource_array: response.data,
     //       isLoading: false
     //     });
     //     console.log(this.state.resource_array);
          console.log(response)
    })
        
        // If we catch any errors connecting, let's update accordingly
//        .catch(error => this.setState({ error, isLoading: true }));
        this.setState({editOn:false,activeId:''})

    }


    handleFormButton(e){
      console.log(e.target.value)
      this.setState({listAnnotators:e.target.value})
    }


    render() {
        return (
            <div>
{this.state.isLoading==false?
<Table celled size='small' striped >
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>port</Table.HeaderCell>
        <Table.HeaderCell>job</Table.HeaderCell>
        <Table.HeaderCell>task</Table.HeaderCell>
        <Table.HeaderCell>filter</Table.HeaderCell>
        <Table.HeaderCell>semi-annotation</Table.HeaderCell>
        <Table.HeaderCell>annotators</Table.HeaderCell>
        <Table.HeaderCell>images</Table.HeaderCell>
        <Table.HeaderCell>completed</Table.HeaderCell>
        <Table.HeaderCell>edit</Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    
   
    {this.state.resource_array.map(single=>{
 
    return(
        <Table.Body>
      <Table.Row>
        <Table.Cell>{single['port']}</Table.Cell>
        <Table.Cell>{single['job']}</Table.Cell>
        <Table.Cell>{single['task']}</Table.Cell>
        <Table.Cell>{single['filters']}</Table.Cell>
        <Table.Cell>{single['semi_annotations']}</Table.Cell>
        <Table.Cell>{single['annotators']}</Table.Cell>
        <Table.Cell>{single['images']}</Table.Cell>
        <Table.Cell>{single['completed']}</Table.Cell>
        {this.state.activeId !=single['port']?
        <Table.Cell>
        <Button 
        primary
        id = {single['port']}
        onClick = {this.handleEditClick.bind(this)}   
        >
          edit
        </Button>
        </Table.Cell>
        :
        <Table.Cell>
        <Form>
          <Form.Field>
            <label>list annotators and separate by ','</label>
            <input onChange={this.handleFormButton.bind(this)} placeholder='first name' />
          </Form.Field>
          <Button
          secondary
          id = {single['port']}
          onClick = {this.handleSubmit.bind(this)}
          >Submit</Button>
        </Form>
        </Table.Cell>        
        }
      </Table.Row>
      </Table.Body>
    )})}

  </Table>
:
null}




            </div>
        )
    }
}

export default Resources

