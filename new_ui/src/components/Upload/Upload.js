import React, { Component } from 'react';
import axios from 'axios';
import {Table,Form,Checkbox,Button} from 'semantic-ui-react';
import './Upload.css';




export class Upload extends Component {

    state={
        data:[],
        isLoading:true,
        dir:'',
        project:'',
        site: '',
        site_dirs:'',
        typeofUpload: [

            {'text':'video','value':'video'},
            {'text':'images','value':'images'},
            {'text':'images+jsons','value':'images+jsons'}
        ]

    }

    componentDidMount(){
        axios
        .get("http://34.93.111.56:8000/localData/local_upload/")
        .then(resp => {
            this.setState({
              data: resp.data,
              isLoading:false
            });
            console.log(this.state.data);
          })
          
          // If we catch any errors connecting, let's update accordingly
          .catch(error => this.setState({ error, isLoading: false }));

    }

    handleProject(e,{value}){
        this.setState({project:value})
        const sites = this.state.data.find((element) => {
            return element.text === value;
          })
        console.log(this.state.project)
        this.setState({site_dirs:sites['sites']})
    }


    handleSite(e,{value}){
        this.setState({site:value})
        console.log(this.state.project)
        console.log(this.state.site)
        
    }


    handleType(e,{value}){
        this.setState({type:value})
        console.log(this.state.site)
        console.log(this.state.project)
    }


    handleSubmitButton(e){

        console.log(this.state.site)        
        const postObject = {'project':this.state.project,'site':this.state.site,'type':this.state.type}
        console.log(postObject)
        axios.post("http://34.93.111.56:8000/localData/post_upload/", postObject)
        .then(res => {
            console.log(res)
            this.setState({response_state:res.data,response_ready:true})
            
        }).catch(error => {
            console.log(error)
            
        });


    }

    render() {
        return (
            <div className='upload'>
            <Form>


                <Form.Select
                style={{width:"0"}}
                label='Project'
                options={this.state.data}
                placeholder='Sync Project'
                value={this.state.project}
                onChange={this.handleProject.bind(this)} 
                 />   

    
                <Form.Select
                style={{width:"0"}}
                label='Site'
                options={this.state.site_dirs}
                placeholder='Sync Site'
                value={this.state.site}
                onChange={this.handleSite.bind(this)} 
               /> 

                <Form.Select
                style={{width:"0"}}
                label='Choose Upload Type'
                options= {this.state.typeofUpload}
                placeholder='Sync Type'
                value={this.state.type}
                onChange={this.handleType.bind(this)} 
                 />   


            <Form.Button onClick={this.handleSubmitButton.bind(this)}>Submit</Form.Button>
            


            </Form>
            </div>
        )
    }
}

export default Upload

