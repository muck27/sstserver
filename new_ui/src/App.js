import React, { Component } from 'react';
import HeaderNav from './components/HeaderNav'
import Body from './components/Body'
import './App.css'
class App extends Component {
  
  render() {
    return (
      <div>
        <div className="App-header">
          <HeaderNav />
        </div>
        <div> 
            <Body />
        </div>
        </div>
    );
  }
}

export default App;

