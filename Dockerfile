FROM python:3
ENV PYTHONBUFFERED 1
Run mkdir /automatic_annotator_tool
Run mkdir /etc/uwsgi
Run mkdir /etc/uwsgi/sites
COPY ./uwsgi_params /etc/uwsgi/sites/

WORKDIR /automatic_annotator_tool
COPY . /automatic_annotator_tool

RUN curl -sL https://deb.nodesource.com/setup_8.x |  bash -
RUN apt-get update && apt-get install -y \
    python-pip \
    vim \
    nginx \ 
    libgeos-dev \
    nodejs \
    npm \
    docker.io

########build docker images 
RUN curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
RUN chmod +x /usr/local/bin/docker-compose

#######set nginx conf params
WORKDIR /automatic_annotator_tool
COPY ./uvlearn_open.conf /etc/nginx/sites-enabled/
RUN service nginx start

#######install django reqs
RUN pip3 install -r ./django_app/requirements.txt

#######create s3 data reqs
RUN mkdir /data/
RUN mkdir /data/downloads
Volume /data

#######react components
#WORKDIR /automatic_annotator_tool/react_app
#RUN npm cache clean
#RUN npm install
#RUN  npm install pm2 -g 

WORKDIR /automatic_annotator_tool

######S3 creds
ARG AWS_KEY='AKIARX6S2JWR245EG3UD'
ARG AWS_SECRET_KEY='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'

RUN aws configure set aws_access_key_id $AWS_KEY \
&& aws configure set aws_secret_access_key $AWS_SECRET_KEY 
